/*
 * CFFMetric - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.SeekableInputOffsetFilter;

public class CFFMetric extends Metric {
	public String registry = "";

	public String ordering = "";

	public int supplement = 0;

	public int CIDCount = 8720;

	public int CIDFontType = 0;

	public String fdFontName = "";

	public int paintType = 0;

	public long charstringsOffset = 0L;

	public int charstringType = 2;

	public long charstringsLength = 0L;

	public boolean isPredefinedEncoding = true;

	public long encodingOffset = 0L;

	public boolean isPredefinedCharset = true;

	public long charsetOffset = 0L;

	public final int charsetFormat;

	public long charsetLength = 0L;

	public int fontNumber = 0;

	public long privateDictOffset = 0L;

	public long privateDictLength = 0L;

	public long fdArrayOffset = 0L;

	public long fdSelectOffset = 0L;

	private Hashtable glyphNames;

	private Hashtable glyphCIDs;

	private final Array names;

	private final Array topDicts;

	private final Array strings;

	private final boolean nameExists;

	static final int MAX_OPERANDS = 100;

	public static String[] DEFAULT_STRINGS = { ".notdef", "space", "exclam",
			"quotedbl", "numbersign", "dollar", "percent", "ampersand",
			"quoteright", "parenleft", "parenright", "asterisk", "plus",
			"comma", "hyphen", "period", "slash", "zero", "one", "two",
			"three", "four", "five", "six", "seven", "eight", "nine", "colon",
			"semicolon", "less", "equal", "greater", "question", "at", "A",
			"B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
			"O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"bracketleft", "backslash", "bracketright", "asciicircum",
			"underscore", "quoteleft", "a", "b", "c", "d", "e", "f", "g", "h",
			"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
			"v", "w", "x", "y", "z", "braceleft", "bar", "braceright",
			"asciitilde", "exclamdown", "cent", "sterling", "fraction", "yen",
			"florin", "section", "currency", "quotesingle", "quotedblleft",
			"guillemotleft", "guilsinglleft", "guilsinglright", "fi", "fl",
			"endash", "dagger", "daggerdbl", "periodcentered", "paragraph",
			"bullet", "quotesinglbase", "quotedblbase", "quotedblright",
			"guillemotright", "ellipsis", "perthousand", "questiondown",
			"grave", "acute", "circumflex", "tilde", "macron", "breve",
			"dotaccent", "dieresis", "ring", "cedilla", "hungarumlaut",
			"ogonek", "caron", "emdash", "AE", "ordfeminine", "Lslash",
			"Oslash", "OE", "ordmasculine", "ae", "dotlessi", "lslash",
			"oslash", "oe", "germandbls", "onesuperior", "logicalnot", "mu",
			"trademark", "Eth", "onehalf", "plusminus", "Thorn", "onequarter",
			"divide", "brokenbar", "degree", "thorn", "threequarters",
			"twosuperior", "registered", "minus", "eth", "multiply",
			"threesuperior", "copyright", "Aacute", "Acircumflex", "Adieresis",
			"Agrave", "Aring", "Atilde", "Ccedilla", "Eacute", "Ecircumflex",
			"Edieresis", "Egrave", "Iacute", "Icircumflex", "Idieresis",
			"Igrave", "Ntilde", "Oacute", "Ocircumflex", "Odieresis", "Ograve",
			"Otilde", "Scaron", "Uacute", "Ucircumflex", "Udieresis", "Ugrave",
			"Yacute", "Ydieresis", "Zcaron", "aacute", "acircumflex",
			"adieresis", "agrave", "aring", "atilde", "ccedilla", "eacute",
			"ecircumflex", "edieresis", "egrave", "iacute", "icircumflex",
			"idieresis", "igrave", "ntilde", "oacute", "ocircumflex",
			"odieresis", "ograve", "otilde", "scaron", "uacute", "ucircumflex",
			"udieresis", "ugrave", "yacute", "ydieresis", "zcaron",
			"exclamsmall", "Hungarumlautsmall", "dollaroldstyle",
			"dollarsuperior", "ampersandsmall", "Acutesmall",
			"parenleftsuperior", "parenrightsuperior", "twodotenleader",
			"onedotenleader", "zerooldstyle", "oneoldstyle", "twooldstyle",
			"threeoldstyle", "fouroldstyle", "fiveoldstyle", "sixoldstyle",
			"sevenoldstyle", "eightoldstyle", "nineoldstyle", "commasuperior",
			"threequartersemdash", "periodsuperior", "questionsmall",
			"asuperior", "bsuperior", "centsuperior", "dsuperior", "esuperior",
			"isuperior", "lsuperior", "msuperior", "nsuperior", "osuperior",
			"rsuperior", "ssuperior", "tsuperior", "ff", "ffi", "ffl",
			"parenleftinferior", "parenrightinferior", "Circumflexsmall",
			"hyphensuperior", "Gravesmall", "Asmall", "Bsmall", "Csmall",
			"Dsmall", "Esmall", "Fsmall", "Gsmall", "Hsmall", "Ismall",
			"Jsmall", "Ksmall", "Lsmall", "Msmall", "Nsmall", "Osmall",
			"Psmall", "Qsmall", "Rsmall", "Ssmall", "Tsmall", "Usmall",
			"Vsmall", "Wsmall", "Xsmall", "Ysmall", "Zsmall", "colonmonetary",
			"onefitted", "rupiah", "Tildesmall", "exclamdownsmall",
			"centoldstyle", "Lslashsmall", "Scaronsmall", "Zcaronsmall",
			"Dieresissmall", "Brevesmall", "Caronsmall", "Dotaccentsmall",
			"Macronsmall", "figuredash", "hypheninferior", "Ogoneksmall",
			"Ringsmall", "Cedillasmall", "questiondownsmall", "oneeighth",
			"threeeighths", "fiveeighths", "seveneighths", "onethird",
			"twothirds", "zerosuperior", "foursuperior", "fivesuperior",
			"sixsuperior", "sevensuperior", "eightsuperior", "ninesuperior",
			"zeroinferior", "oneinferior", "twoinferior", "threeinferior",
			"fourinferior", "fiveinferior", "sixinferior", "seveninferior",
			"eightinferior", "nineinferior", "centinferior", "dollarinferior",
			"periodinferior", "commainferior", "Agravesmall", "Aacutesmall",
			"Acircumflexsmall", "Atildesmall", "Adieresissmall", "Aringsmall",
			"AEsmall", "Ccedillasmall", "Egravesmall", "Eacutesmall",
			"Ecircumflexsmall", "Edieresissmall", "Igravesmall", "Iacutesmall",
			"Icircumflexsmall", "Idieresissmall", "Ethsmall", "Ntildesmall",
			"Ogravesmall", "Oacutesmall", "Ocircumflexsmall", "Otildesmall",
			"Odieresissmall", "OEsmall", "Oslashsmall", "Ugravesmall",
			"Uacutesmall", "Ucircumflexsmall", "Udieresissmall", "Yacutesmall",
			"Thornsmall", "Ydieresissmall", "001.000", "001.001", "001.002",
			"001.003", "Black", "Bold", "Book", "Light", "Medium", "Regular",
			"Roman", "Semibold" };

	static final short hVersion = 0;

	static final short hNotice = 1;

	static final short hFullName = 2;

	static final short hFamilyName = 3;

	static final short hWeight = 4;

	static final short hFontBBox = 5;

	static final short pStdHW = 10;

	static final short pStdVW = 11;

	static final short hCharset = 15;

	static final short hEncoding = 16;

	static final short hCharStrings = 17;

	static final short hPrivate = 18;

	static final short hSubrs = 19;

	static final short hIsFixedPitch = -1;

	static final short hItalicAngle = -2;

	static final short hUnderlinePosition = -3;

	static final short hUnderlineThickness = -4;

	static final short hPaintType = -5;

	static final short hCharstringType = -6;

	static final short hROS = -30;

	static final short hCIDFontVersion = -31;

	static final short hCIDFontRevision = -32;

	static final short hCIDFontType = -33;

	static final short hCIDCount = -34;

	static final short hFDArray = -36;

	static final short hFDSelect = -37;

	static final short hFontName = -38;

	public void dump(PrintStream printstream) {
		super.dump(printstream);
		printstream.println("\n\u00d1FF-specific font data:");
	}

	public CFFMetric(SeekableInput seekableinput, String string)
			throws IOException, FontFileFormatError {
		isCFF = true;
		nameExists = string != null;
		if (nameExists)
			fontName = string;
		seekableinput.skipBytes(2);
		int i = seekableinput.readUnsignedByte();
		int i_0_ = seekableinput.readUnsignedByte();
		seekableinput.seek((long) i);
		names = readINDEXData(seekableinput);
		topDicts = readINDEXData(seekableinput);
		strings = readINDEXData(seekableinput);
		if (names.length() == 0)
			throw new FontFileFormatError(
					"Invalid CFF font data: font name table is empty");
		if (names.length() > 1)
			throw new FontFileFormatError(
					"Unsupported CFF font: the font contains more than one subfont");
		if (nameExists) {
			boolean bool = false;
			for (fontNumber = 0; fontNumber < names.length(); fontNumber++) {
				if (string.equals(new String((byte[]) names.get(fontNumber)))) {
					bool = true;
					break;
				}
			}
			if (!bool)
				throw new FontFileFormatError(
						"CFF font data doesn't contain supplied font name");
		} else
			fontNumber = 0;
		DataInputStream datainputstream = (new DataInputStream(
				new ByteArrayInputStream((byte[]) topDicts.get(fontNumber))));
		isCIDFont = false;
		isFixedPitch = false;
		italicAngle = 0.0F;
		underlinePosition = -100.0F;
		underlineThickness = 50.0F;
		fontBBox = new float[] { 0.0F, 0.0F, 0.0F, 0.0F };
		while (datainputstream.available() > 0) {
			float[] fs = new float[100];
			for (int i_1_ = 0; i_1_ < fs.length; i_1_++)
				fs[i_1_] = -1.0F;
			float[] fs_2_ = new float[2];
			int i_3_ = 0;
			while (readOperand(datainputstream, fs_2_) > 0 && fs_2_[1] == 0.0F) {
				fs[i_3_] = fs_2_[0];
				i_3_++;
			}
			if (fs_2_[1] != -1.0F) {
				int i_4_ = (int) fs_2_[0];
				switch (i_4_) {
				case 2:
					fullName = getString(fs[0], strings);
					break;
				case 3:
					family = getString(fs[0], strings);
					break;
				case 0:
					version = getString(fs[0], strings);
					break;
				case 1:
					notice = getString(fs[0], strings);
					break;
				case 4:
					weight = getString(fs[0], strings);
					break;
				case 5:
					fontBBox[0] = fs[0];
					fontBBox[1] = fs[1];
					fontBBox[2] = fs[2];
					fontBBox[3] = fs[3];
					break;
				case -30:
					registry = getString(fs[0], strings);
					ordering = getString(fs[1], strings);
					supplement = (int) fs[2];
					isCIDFont = true;
					isPredefinedEncoding = false;
					isPredefinedCharset = false;
					break;
				case -33:
					CIDFontType = (int) fs[0];
					if (CIDFontType != 0)
						throw new FontFileFormatError(
								"Unsupported CFF CID font type: " + CIDFontType);
				/* fall through */
				case 16:
					encodingOffset = (long) (int) fs[0];
					if (encodingOffset > 1L)
						isPredefinedEncoding = false;
					break;
				case -6:
					charstringType = (int) fs[0];
					if (charstringType < 1 && charstringType > 2)
						throw new FontFileFormatError(
								"Unsupported type of CFF Charstring encoding: "
										+ charstringType);
					break;
				case 15:
					charsetOffset = (long) (int) fs[0];
					if (charsetOffset > 2L)
						isPredefinedCharset = false;
					break;
				case 18:
					privateDictLength = (long) (int) fs[0];
					privateDictOffset = (long) (int) fs[1];
					break;
				case 17:
					charstringsOffset = (long) (int) fs[0];
				/* fall through */
				case -2:
					italicAngle = fs[0];
					break;
				case -3:
					underlinePosition = fs[0];
					break;
				case -4:
					underlineThickness = fs[0];
					break;
				case -1:
					isFixedPitch = fs[0] != 0.0F;
					break;
				case -5:
					paintType = (int) fs[0];
					break;
				case -34:
					CIDCount = (int) fs[0];
					break;
				case -38:
					fdFontName = getString(fs[0], strings);
					break;
				case -36:
					fdArrayOffset = (long) (int) fs[0];
				/* fall through */
				case -37:
					fdSelectOffset = (long) (int) fs[0];
					break;
				}
			}
		}
		seekableinput.seek(charstringsOffset);
		charstringsLength = getINDEXLength(seekableinput);
		glyphNames = new Hashtable();
		glyphCIDs = new Hashtable();
		seekableinput.seek(charstringsOffset);
		numGlyphs = readINDEXElementsCount(seekableinput);
		if (isPredefinedCharset) {
			charsetFormat = 0;
			throw new FontFileFormatError(
					"Support for OpenType/CFF fonts with predefined charset not implemented yet");
		}
		seekableinput.seek(charsetOffset);
		charsetFormat = seekableinput.readUnsignedByte();
		charsetLength++;
		glyphNames.put(new Integer(0), ".notdef");
		glyphCIDs.put(new Integer(0), new Integer(0));
		int i_5_ = 1;
		switch (charsetFormat) {
		case 0:
			for (int i_6_ = 0; i_6_ < numGlyphs - 1; i_6_++) {
				int i_7_ = seekableinput.readUnsignedShort();
				addGlyphSelector(i_5_ + i_6_, i_7_);
			}
			i_5_ += numGlyphs - 1;
			charsetLength += (long) ((numGlyphs - 1) * 2);
			break;
		case 1:
		case 2: {
			int i_8_;
			for (/**/; i_5_ < numGlyphs; i_5_ += i_8_ + 1) {
				int i_9_ = seekableinput.readUnsignedShort();
				charsetLength += 2L;
				i_8_ = (charsetFormat == 1 ? seekableinput.readUnsignedByte()
						: seekableinput.readUnsignedShort());
				CFFMetric cffmetric_10_ = this;
				cffmetric_10_.charsetLength = cffmetric_10_.charsetLength
						+ (charsetFormat == 1 ? 1L : 2L);
				for (int i_11_ = 0; i_11_ <= i_8_; i_11_++)
					addGlyphSelector(i_5_ + i_11_, i_9_ + i_11_);
			}
			break;
		}
		default:
			throw new FontFileFormatError(
					"Invalid CFF font data: Charset encoding type is incorrect");
		}
		if (isCIDFont) {
			if (charsetOffset == 0L)
				throw new FontFileFormatError(
						"Invalid CFF font data: Charsets data are missing");
			if (fdArrayOffset == 0L)
				throw new FontFileFormatError(
						"Invalid CFF font data: FD Array operator is missing");
		}
	}

	long readBytes(SeekableInput seekableinput, int i) throws IOException {
		long l = 0L;
		for (int i_12_ = 0; i_12_ < i; i_12_++) {
			l = 256L * l;
			l += (long) seekableinput.readUnsignedByte();
		}
		return l;
	}

	int readOperand(DataInputStream datainputstream, float[] fs)
			throws IOException {
		int i = 0;
		int i_13_ = datainputstream.readUnsignedByte();
		i++;
		if (i_13_ >= 32 && i_13_ <= 246) {
			fs[0] = (float) (i_13_ - 139);
			fs[1] = 0.0F;
		} else if (i_13_ >= 247 && i_13_ <= 250) {
			fs[0] = (float) ((i_13_ - 247) * 256
					+ datainputstream.readUnsignedByte() + 108);
			fs[1] = 0.0F;
			i++;
		} else if (i_13_ >= 251 && i_13_ <= 254) {
			fs[0] = (float) (-(i_13_ - 251) * 256
					- datainputstream.readUnsignedByte() - 108);
			fs[1] = 0.0F;
			i++;
		} else if (i_13_ == 28) {
			fs[0] = (float) (datainputstream.readUnsignedByte() * 256 + datainputstream
					.readUnsignedByte());
			fs[1] = 0.0F;
			i += 2;
		} else if (i_13_ == 29) {
			fs[0] = (float) (datainputstream.readUnsignedByte() * 256 * 256
					* 256 + datainputstream.readUnsignedByte() * 256 * 256
					+ datainputstream.readUnsignedByte() * 256 + datainputstream
					.readUnsignedByte());
			fs[1] = 0.0F;
			i += 4;
		} else if (i_13_ == 30) {
			String string = "";
			for (;;) {
				int i_14_ = datainputstream.readUnsignedByte();
				byte i_15_ = (byte) (i_14_ >> 4);
				byte i_16_ = (byte) (i_14_ & 0xf);
				i++;
				if (i_15_ == 15)
					break;
				string += parseRealValue(i_15_);
				if (i_16_ == 15)
					break;
				string += parseRealValue(i_16_);
			}
			fs[0] = new Float(string).floatValue();
			fs[1] = 0.0F;
		} else if (i_13_ >= 0 && i_13_ <= 21) {
			if (i_13_ == 12) {
				fs[0] = (float) -datainputstream.readUnsignedByte();
				fs[1] = 1.0F;
				i++;
			} else {
				fs[0] = (float) i_13_;
				fs[1] = 1.0F;
			}
		} else {
			fs[0] = 0.0F;
			fs[1] = -1.0F;
		}
		return i;
	}

	String parseRealValue(byte i) throws IOException {
		if (i < 10)
			return "" + i;
		switch (i) {
		case 10:
			return ".";
		case 11:
			return "E";
		case 12:
			return "E-";
		case 14:
			return "-";
		default:
			throw new IOException("Invalid CFF font data");
		}
	}

	public int readINDEXElementsCount(SeekableInput seekableinput)
			throws IOException, FontFileFormatError {
		return seekableinput.readUnsignedShort();
	}

	public int readINDEXOffsetElementSize(SeekableInput seekableinput)
			throws IOException, FontFileFormatError {
		int i = seekableinput.readUnsignedByte();
		if (i < 1 && i > 4)
			throw new FontFileFormatError(
					"Invalid CFF font data: offset element size is " + i
							+ "; it is out of range 1-4");
		return i;
	}

	public long readINDEXOffset(SeekableInput seekableinput, int i)
			throws IOException, FontFileFormatError {
		return readBytes(seekableinput, i);
	}

	public Array readINDEXData(SeekableInput seekableinput) throws IOException,
			FontFileFormatError {
		Array array = new Array();
		int i = readINDEXElementsCount(seekableinput);
		if (i == 0)
			return array;
		int i_17_ = readINDEXOffsetElementSize(seekableinput);
		long[] ls = new long[i + 1];
		long[] ls_18_ = new long[i];
		ls[0] = readINDEXOffset(seekableinput, i_17_);
		for (int i_19_ = 0; i_19_ < i; i_19_++) {
			ls[i_19_ + 1] = readINDEXOffset(seekableinput, i_17_);
			ls_18_[i_19_] = ls[i_19_ + 1] - ls[i_19_];
		}
		for (int i_20_ = 0; i_20_ < i; i_20_++) {
			byte[] is = new byte[(int) ls_18_[i_20_]];
			seekableinput.readFully(is);
			array.put(i_20_, is);
		}
		return array;
	}

	public long getINDEXLength(SeekableInput seekableinput) throws IOException,
			FontFileFormatError {
		int i = readINDEXElementsCount(seekableinput);
		if (i == 0)
			return 2L;
		int i_21_ = readINDEXOffsetElementSize(seekableinput);
		seekableinput.skipBytes(i * i_21_);
		long l = readINDEXOffset(seekableinput, i_21_);
		return (long) (3 + (i + 1) * i_21_) + (l - 1L);
	}

	String getString(float f, Array array) throws FontFileFormatError {
		if (f < (float) DEFAULT_STRINGS.length)
			return DEFAULT_STRINGS[(int) f];
		int i = (int) f - DEFAULT_STRINGS.length;
		if (i >= array.length())
			throw new FontFileFormatError(
					"Error while parsing CFF metric: STRING INDEX out of boundary");
		return new String((byte[]) array.get(i));
	}

	private void addGlyphSelector(int i, int i_22_) throws FontFileFormatError {
		glyphCIDs.put(new Integer(i), new Integer(i_22_));
		if (!isCIDFont)
			glyphNames.put(new Integer(i), getString((float) i_22_, strings));
	}

	public int getGlyphCID(int i) throws FontFileFormatError {
		if (i >= numGlyphs)
			throw new FontFileFormatError(
					"Error in OpenType/CFF font: Glyph ID " + i
							+ "exceeds max number of glyphs (" + numGlyphs
							+ ")");
		return ((Integer) glyphCIDs.get(new Integer(i))).intValue();
	}

	public String getGlyphName(int i) throws FontFileFormatError {
		if (!isCIDFont)
			return (String) glyphNames.get(new Integer(i));
		throw new FontFileFormatError(
				"OpenType/CFF (CID) font has no glyph names");
	}

	public byte[] int2short(int i) {
		byte[] is = new byte[2];
		is[0] = (byte) (i >> 8 & 0xff);
		is[1] = (byte) (i & 0xff);
		return is;
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length == 1)
			new CFFMetric(new SeekableFileInputStream(strings[0]), null)
					.dump(System.out);
		else if (strings.length == 2) {
			if ("dump".equals(strings[0])) {
				FileOutputStream fileoutputstream = null;
				SeekableFileInputStream seekablefileinputstream = null;
				try {
					fileoutputstream = new FileOutputStream(new File(strings[1]
							+ ".dump"));
					TTMetric ttmetric = new TTMetric(
							new SeekableFileInputStream(strings[1]), 1);
					seekablefileinputstream = new SeekableFileInputStream(
							strings[1]);
					seekablefileinputstream.seek(ttmetric.cff_offset);
					long l = ttmetric.cff_length;
					byte[] is = new byte[16384];
					while (l > 0L) {
						int i = l < (long) is.length ? (int) l : is.length;
						l -= (long) i;
						seekablefileinputstream.read(is, 0, i);
						fileoutputstream.write(is, 0, i);
					}
				} catch (IOException ioexception) {
					ioexception.printStackTrace();
				} finally {
					seekablefileinputstream.close();
					fileoutputstream.close();
				}
			}
			if ("subset".equals(strings[0])) {
				FileOutputStream fileoutputstream = null;
				SeekableFileInputStream seekablefileinputstream = null;
				try {
					fileoutputstream = new FileOutputStream(new File(strings[1]
							+ ".subset"));
					TTMetric ttmetric = new TTMetric(
							new SeekableFileInputStream(strings[1]), 1);
					CFFMetric cffmetric = new CFFMetric(
							(new SeekableInputOffsetFilter(
									new SeekableFileInputStream(strings[1]),
									ttmetric.cff_offset)), null);
					seekablefileinputstream = new SeekableFileInputStream(
							strings[1]);
					seekablefileinputstream.seek(ttmetric.cff_offset);
					byte[] is = new byte[(int) cffmetric.charstringsOffset];
					seekablefileinputstream.readFully(is);
					fileoutputstream.write(is);
					int i = cffmetric
							.readINDEXElementsCount(seekablefileinputstream);
					int i_23_ = (cffmetric
							.readINDEXOffsetElementSize(seekablefileinputstream));
					fileoutputstream.write(cffmetric.int2short(i));
					fileoutputstream.write(i_23_);
					is = new byte[(i + 1) * i_23_];
					seekablefileinputstream.readFully(is);
					fileoutputstream.write(is);
					is = new byte[(int) (cffmetric.charstringsLength
							- (long) ((i + 1) * i_23_) - 1L - 2L)];
					seekablefileinputstream.readFully(is);
					for (int i_24_ = 0; i_24_ < is.length; i_24_++)
						is[i_24_] = (byte) 0;
					fileoutputstream.write(is);
					is = new byte[(int) (ttmetric.cff_length - (cffmetric.charstringsOffset + cffmetric.charstringsLength))];
					seekablefileinputstream.readFully(is);
					fileoutputstream.write(is);
				} catch (IOException ioexception) {
					ioexception.printStackTrace();
				} catch (Exception exception) {
					exception.printStackTrace();
				} finally {
					seekablefileinputstream.close();
					fileoutputstream.close();
				}
			}
		} else
			System.err
					.println("Usage: java com.renderx.fonts.CFFMetric <(embed | subset)?> <OpenType/CFF font filename>");
	}
}