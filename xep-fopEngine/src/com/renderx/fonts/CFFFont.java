/*
 * CFFFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.renderx.util.Array;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.SeekableInputOffsetFilter;

import junit.framework.Assert;
import junit.framework.TestCase;

public class CFFFont {
	public UsedGlyphs used = new UsedGlyphs();

	public final int format;

	private final CFFMetric cffm;

	private final SeekableInput in;

	private long deltaCharstringsLength = 0L;

	private long deltaCharsetLength = 0L;

	private byte[] glyphoffsets = null;

	private byte[] glyphdata = null;

	private byte[] charsetdata = null;

	private int numSubsettedGlyphs = 0;

	private static final int TYPE1 = 1;

	private static final int TYPE2 = 2;

	public static class Test extends TestCase {
		public void testCFFMetricCreation() throws IOException,
				FontFileFormatError {
			try {
				resourceToFile("com/renderx/fonts/TEST_DATA/TimesRoman.test",
						"temp.tst");
				CFFMetric cffmetric = new CFFMetric(
						(new SeekableInputOffsetFilter(
								new SeekableFileInputStream("temp.tst"), 0L)),
						null);
				Assert.assertTrue(true);
			} catch (FontFileFormatError fontfileformaterror) {
				Assert
						.assertTrue("Support for OpenType/CFF fonts with predefined charset not implemented yet"
								.equals(fontfileformaterror.getMessage()));
			}
		}

		private void resourceToFile(String string, String string_0_)
				throws IOException {
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string);
			FileOutputStream fileoutputstream = new FileOutputStream(string_0_);
			byte[] is = new byte[1024];
			int i;
			while ((i = inputstream.read(is, 0, 1024)) >= 0)
				fileoutputstream.write(is, 0, i);
			fileoutputstream.close();
		}
	}

	public CFFFont(SeekableInput seekableinput, CFFMetric cffmetric)
			throws IOException, FontFileFormatError {
		in = seekableinput;
		cffm = cffmetric;
		used.clearGlyphs();
		format = cffmetric.charstringType;
		addGlyph(0);
	}

	public void addGlyph(int i) throws FontFileFormatError {
		if (!used.checkGlyph((char) i)) {
			used.markGlyph((char) i);
			if (format == 1)
				throw new FontFileFormatError(
						"Subsetting for Type 1 OpenType/CFF fonts not implemented yet");
		}
	}

	public byte[] createSubset() throws IOException, FontFileFormatError {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		in.seek(0L);
		byte[] is = new byte[(int) cffm.charstringsOffset];
		in.readFully(is);
		bytearrayoutputstream.write(is);
		int i = cffm.readINDEXElementsCount(in);
		int i_1_ = cffm.readINDEXOffsetElementSize(in);
		bytearrayoutputstream.write(int2short(i));
		bytearrayoutputstream.write(i_1_);
		writeIn2Out(in, bytearrayoutputstream, (long) ((i + 1) * i_1_));
		in.seek(cffm.charstringsOffset);
		Array array = new Array(i);
		array = cffm.readINDEXData(in);
		for (int i_2_ = 0; i_2_ < i; i_2_++) {
			int i_3_ = cffm.isCIDFont ? cffm.getGlyphCID(i_2_) : i_2_;
			byte[] is_4_ = (byte[]) array.get(i_2_);
			if (!used.checkGlyph((char) i_3_)) {
				for (int i_5_ = 0; i_5_ < is_4_.length; i_5_++)
					is_4_[i_5_] = (byte) 0;
			}
			bytearrayoutputstream.write(is_4_);
		}
		is = new byte[16384];
		int i_6_ = -1;
		while ((i_6_ = in.read(is)) != -1)
			bytearrayoutputstream.write(is, 0, i_6_);
		return bytearrayoutputstream.toByteArray();
	}

	byte[] int2short(int i) {
		byte[] is = new byte[2];
		is[0] = (byte) (i >> 8 & 0xff);
		is[1] = (byte) (i & 0xff);
		return is;
	}

	void writeIn2Out(SeekableInput seekableinput, OutputStream outputstream,
			long l) throws IOException, FontFileFormatError {
		if (l < 0L)
			throw new FontFileFormatError(
					"Trying to write data of negative length; CFF subsetting is broken");
		long l_7_ = l;
		byte[] is = new byte[16384];
		int i;
		for (/**/; l_7_ > 0L; l_7_ -= (long) i) {
			i = l_7_ > (long) is.length ? is.length : (int) l_7_;
			seekableinput.read(is, 0, i);
			outputstream.write(is, 0, i);
		}
	}
}