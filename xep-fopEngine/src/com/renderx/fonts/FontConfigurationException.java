/*
 * FontConfigurationException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class FontConfigurationException extends Exception {
	public FontConfigurationException() {
		/* empty */
	}

	public FontConfigurationException(String string) {
		super(string);
	}
}