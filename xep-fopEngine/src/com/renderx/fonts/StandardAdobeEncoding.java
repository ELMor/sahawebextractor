/*
 * StandardAdobeEncoding - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import com.renderx.util.Hashtable;

public class StandardAdobeEncoding {
	protected static final Hashtable encoding = new Hashtable();

	public static String getGlyphName(int i) {
		return getGlyphName("" + i);
	}

	public static String getGlyphName(String string) {
		return (String) encoding.get(string);
	}

	static {
		encoding.put("32", "space");
		encoding.put("33", "exclam");
		encoding.put("34", "quotedbl");
		encoding.put("35", "numbersign");
		encoding.put("36", "dollar");
		encoding.put("37", "percent");
		encoding.put("38", "ampersand");
		encoding.put("39", "quoteright");
		encoding.put("40", "parenleft");
		encoding.put("41", "parenright");
		encoding.put("42", "asterisk");
		encoding.put("43", "plus");
		encoding.put("44", "comma");
		encoding.put("45", "hyphen");
		encoding.put("46", "period");
		encoding.put("47", "slash");
		encoding.put("48", "zero");
		encoding.put("49", "one");
		encoding.put("50", "two");
		encoding.put("51", "three");
		encoding.put("52", "four");
		encoding.put("53", "five");
		encoding.put("54", "six");
		encoding.put("55", "seven");
		encoding.put("56", "eight");
		encoding.put("57", "nine");
		encoding.put("58", "colon");
		encoding.put("59", "semicolon");
		encoding.put("60", "less");
		encoding.put("61", "equal");
		encoding.put("62", "greater");
		encoding.put("63", "question");
		encoding.put("64", "at");
		encoding.put("65", "A");
		encoding.put("66", "B");
		encoding.put("67", "C");
		encoding.put("68", "D");
		encoding.put("69", "E");
		encoding.put("70", "F");
		encoding.put("71", "G");
		encoding.put("72", "H");
		encoding.put("73", "I");
		encoding.put("74", "J");
		encoding.put("75", "K");
		encoding.put("76", "L");
		encoding.put("77", "M");
		encoding.put("78", "N");
		encoding.put("79", "O");
		encoding.put("80", "P");
		encoding.put("81", "Q");
		encoding.put("82", "R");
		encoding.put("83", "S");
		encoding.put("84", "T");
		encoding.put("85", "U");
		encoding.put("86", "V");
		encoding.put("87", "W");
		encoding.put("88", "X");
		encoding.put("89", "Y");
		encoding.put("90", "Z");
		encoding.put("91", "bracketleft");
		encoding.put("92", "backslash");
		encoding.put("93", "bracketright");
		encoding.put("94", "asciicircum");
		encoding.put("95", "underscore");
		encoding.put("96", "quoteleft");
		encoding.put("97", "a");
		encoding.put("98", "b");
		encoding.put("99", "c");
		encoding.put("100", "d");
		encoding.put("101", "e");
		encoding.put("102", "f");
		encoding.put("103", "g");
		encoding.put("104", "h");
		encoding.put("105", "i");
		encoding.put("106", "j");
		encoding.put("107", "k");
		encoding.put("108", "l");
		encoding.put("109", "m");
		encoding.put("110", "n");
		encoding.put("111", "o");
		encoding.put("112", "p");
		encoding.put("113", "q");
		encoding.put("114", "r");
		encoding.put("115", "s");
		encoding.put("116", "t");
		encoding.put("117", "u");
		encoding.put("118", "v");
		encoding.put("119", "w");
		encoding.put("120", "x");
		encoding.put("121", "y");
		encoding.put("122", "z");
		encoding.put("123", "braceleft");
		encoding.put("124", "bar");
		encoding.put("125", "braceright");
		encoding.put("126", "asciitilde");
		encoding.put("161", "exclamdown");
		encoding.put("162", "cent");
		encoding.put("163", "sterling");
		encoding.put("164", "fraction");
		encoding.put("165", "yen");
		encoding.put("166", "florin");
		encoding.put("167", "section");
		encoding.put("168", "currency");
		encoding.put("169", "quotesingle");
		encoding.put("170", "quotedblleft");
		encoding.put("171", "guillemotleft");
		encoding.put("172", "guilsinglleft");
		encoding.put("173", "guilsinglright");
		encoding.put("174", "fi");
		encoding.put("175", "fl");
		encoding.put("177", "endash");
		encoding.put("178", "dagger");
		encoding.put("179", "daggerdbl");
		encoding.put("180", "periodcentered");
		encoding.put("182", "paragraph");
		encoding.put("183", "bullet");
		encoding.put("184", "quotesinglbase");
		encoding.put("185", "quotedblbase");
		encoding.put("186", "quotedblright");
		encoding.put("187", "guillemotright");
		encoding.put("188", "ellipsis");
		encoding.put("189", "perthousand");
		encoding.put("191", "questiondown");
		encoding.put("193", "grave");
		encoding.put("194", "acute");
		encoding.put("195", "circumflex");
		encoding.put("196", "tilde");
		encoding.put("197", "macron");
		encoding.put("198", "breve");
		encoding.put("199", "dotaccent");
		encoding.put("200", "dieresis");
		encoding.put("202", "ring");
		encoding.put("203", "cedilla");
		encoding.put("205", "hungarumlaut");
		encoding.put("206", "ogonek");
		encoding.put("207", "caron");
		encoding.put("208", "emdash");
		encoding.put("225", "AE");
		encoding.put("227", "ordfeminine");
		encoding.put("232", "Lslash");
		encoding.put("233", "Oslash");
		encoding.put("234", "OE");
		encoding.put("235", "ordmasculine");
		encoding.put("241", "ae");
		encoding.put("245", "dotlessi");
		encoding.put("248", "lslash");
		encoding.put("249", "oslash");
		encoding.put("250", "oe");
		encoding.put("251", "germandbls");
	}
}