/*
 * PFxFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import com.renderx.util.Hashtable;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;

public final class PFxFont {
	private static final byte PFB_MARKER = -128;

	private static final int reexec_default = 55665;

	private boolean separated = false;

	private boolean subsetinprogress = false;

	private static int charsPerRow = 64;

	protected static final char[] hd = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public String head;

	public byte[] body;

	private byte[] bodye;

	public String bodytail;

	public byte[] subsetbody;

	public String tail = (new String(
			"0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\n0000000000000000000000000000000000000000000000000000000000000000\ncleartomark"));

	int privateDictLength;

	Vector subsetchars;

	Hashtable chars;

	public PFxFont(SeekableInput seekableinput) throws IOException {
		separated = false;
		head = "";
		boolean bool = seekableinput.readByte() == -128;
		seekableinput.seek(0L);
		if (bool)
			PFBdecode(seekableinput);
		else
			PFAdecode(seekableinput);
	}

	public void PFAdecode(SeekableInput seekableinput) throws IOException {
		String string;
		while ((string = seekableinput.readLine()) != null
				&& !string.startsWith("currentfile eexec")) {
			StringBuffer stringbuffer = new StringBuffer();
			PFxFont pfxfont_0_ = this;
			pfxfont_0_.head = stringbuffer.append(pfxfont_0_.head).append(
					string).append("\n").toString();
		}
		StringBuffer stringbuffer = new StringBuffer();
		PFxFont pfxfont_1_ = this;
		pfxfont_1_.head = stringbuffer.append(pfxfont_1_.head).append(string)
				.append("\n").toString();
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		boolean bool = false;
		int i = -1;
		while ((string = seekableinput.readLine()) != null
				&& !string.startsWith("0000000000000000000")) {
			string = string.toLowerCase();
			int i_2_ = string.length();
			for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
				int i_4_;
				if ((i_4_ = hexval(string.charAt(i_3_))) != -1) {
					if (i == -1)
						i = i_4_;
					else {
						bytearrayoutputstream.write((i << 4) + i_4_);
						i = -1;
					}
				}
			}
		}
		body = bytearrayoutputstream.toByteArray();
		while ((string = seekableinput.readLine()) != null
				&& string.startsWith("0000000000000000000")) {
			/* empty */
		}
		bytearrayoutputstream = new ByteArrayOutputStream();
		byte[] is = new byte[1024];
		int i_5_;
		while ((i_5_ = seekableinput.read(is)) != -1)
			bytearrayoutputstream.write(is, 0, i_5_);
	}

	public void PFBdecode(SeekableInput seekableinput) throws IOException {
		Object object = null;
		Object object_6_ = null;
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		int i = 0;
		boolean bool = false;
		byte i_7_;
		while ((i_7_ = seekableinput.readByte()) == -128 && i < 2) {
			byte i_8_ = seekableinput.readByte();
			int i_9_ = 0;
			int i_10_ = 1;
			if (i_8_ == 3)
				break;
			for (int i_11_ = 0; i_11_ < 4; i_11_++) {
				int i_12_;
				i_9_ = i_9_
						+ ((i_12_ = seekableinput.readByte()) >= 0 ? i_12_
								: 256 + i_12_) * i_10_;
				i_10_ *= 256;
			}
			byte[] is = new byte[i_9_];
			ByteArrayOutputStream bytearrayoutputstream_13_ = new ByteArrayOutputStream();
			ByteArrayOutputStream bytearrayoutputstream_14_ = new ByteArrayOutputStream();
			int i_15_;
			for (boolean bool_16_ = false; (i_9_ != 0
					&& (i_15_ = seekableinput.read(is, 0, i_9_)) <= i_9_ && i_15_ != -1); i_9_ -= i_15_) {
				if (i_8_ == 1)
					bytearrayoutputstream_14_.write(is);
				else {
					bytearrayoutputstream_13_.write(is);
					for (int i_17_ = 0; i_17_ < is.length; i_17_++) {
						if ((i_17_ & 0x1f) == 31)
							bytearrayoutputstream_14_.write(10);
						bytearrayoutputstream_14_
								.write(hd[(is[i_17_] & 0xf0) >> 4]);
						bytearrayoutputstream_14_.write(hd[is[i_17_] & 0xf]);
					}
				}
			}
			if (i == 0) {
				String string = new String(bytearrayoutputstream_14_
						.toByteArray(), 0, bytearrayoutputstream_14_.size());
				int i_18_;
				if ((i_18_ = string.lastIndexOf("currentfile eexec")) != -1) {
					StringBuffer stringbuffer = new StringBuffer();
					PFxFont pfxfont_19_ = this;
					pfxfont_19_.head = stringbuffer.append(pfxfont_19_.head)
							.append(string.substring(0, i_18_ + 17)).append(
									"\n").toString();
					i++;
				} else {
					StringBuffer stringbuffer = new StringBuffer();
					PFxFont pfxfont_20_ = this;
					pfxfont_20_.head = stringbuffer.append(pfxfont_20_.head)
							.append(string).toString();
				}
			} else if (i == 1) {
				String string = new String(bytearrayoutputstream_14_
						.toByteArray(), 0, bytearrayoutputstream_14_.size());
				int i_21_;
				if ((i_21_ = string.indexOf("0000000000000000000")) == -1)
					bytearrayoutputstream.write(bytearrayoutputstream_13_
							.toByteArray());
				else
					bytearrayoutputstream.write(bytearrayoutputstream_14_
							.toByteArray(), 0, i_21_);
			}
		}
		body = bytearrayoutputstream.toByteArray();
	}

	void separateBody() throws IOException {
		if (!separated) {
			bodye = Type1Encryption.decrypt(55665, body);
			int i = 4;
			boolean bool = false;
			String string = "";
			boolean bool_22_ = true;
			while (i < bodye.length && bool_22_) {
				int i_23_;
				for (i_23_ = i; i_23_ < bodye.length && bodye[i_23_] != 10; i_23_++) {
					/* empty */
				}
				string = new String(bodye, i, i_23_ - i);
				if (string.indexOf("/CharStrings") != -1)
					bool_22_ = false;
				else
					i = i_23_ + 1;
			}
			if (!bool_22_) {
				i += (string.indexOf("/CharStrings") + new String(
						"/CharStrings").length());
				privateDictLength = i;
			}
			i = skipSpace(i);
			int i_24_ = new Integer(new String(bodye, i, skipDigits(i) - i))
					.intValue();
			i = skipLine(i);
			chars = new Hashtable();
			boolean bool_25_ = false;
			while (i < bodye.length && !bool_25_) {
				i = skipSpace(i);
				if (bodye[i] == 47) {
					int i_26_ = ++i;
					i = skipWord(i);
					String string_27_ = new String(bodye, i_26_, i - i_26_);
					i_26_ = skipSpace(i);
					i = skipDigits(i_26_);
					int i_28_ = new Integer(new String(bodye, i_26_, i - i_26_))
							.intValue();
					i = skipSpace(i);
					i_26_ = i;
					i = skipWord(i);
					String string_29_ = new String(bodye, i_26_, i - i_26_);
					i++;
					byte[] is = new byte[i_28_];
					System.arraycopy(bodye, i, is, 0, i_28_);
					i += i_28_;
					i = skipSpace(i);
					i_26_ = i;
					i = skipLine(i);
					String string_30_ = new String(bodye, i_26_, i - i_26_);
					CharString charstring = new CharString(is, string_29_,
							string_30_);
					chars.put(string_27_, charstring);
				} else if (bodye[i] == 37)
					i = skipLine(i);
				else {
					int i_31_ = i;
					i = skipWord(i);
					String string_32_ = new String(bodye, i_31_, i - i_31_);
					if (string_32_.startsWith("end")) {
						bool_25_ = true;
						bodytail = new String(bodye, i_31_, bodye.length
								- i_31_);
					}
				}
			}
			separated = true;
		}
	}

	public void newSubset() throws IOException {
		subsetchars = new Vector();
		subsetinprogress = true;
		separateBody();
	}

	public void addGlyph(String string, Encoding encoding) throws IOException {
		if (chars.get(string) == null)
			throw new IOException("Wrong glyph " + string);
		if (!subsetchars.contains(string)) {
			subsetchars.addElement(string);
			CharString charstring = (CharString) chars.get(string);
			int[] is = charstring.composeGlyphs();
			if (is != null) {
				addGlyph(StandardAdobeEncoding.getGlyphName(is[0]), encoding);
				addGlyph(StandardAdobeEncoding.getGlyphName(is[1]), encoding);
			}
		}
	}

	public void writePFASubset(String string) throws IOException {
		FileOutputStream fileoutputstream = new FileOutputStream(string);
		fileoutputstream.write(writePFxSubset(false));
		fileoutputstream.close();
	}

	public void writePFBSubset(String string) throws IOException {
		FileOutputStream fileoutputstream = new FileOutputStream(string);
		fileoutputstream.write(writePFxSubset(true));
		fileoutputstream.close();
	}

	public byte[] writePFASubset() throws IOException {
		return writePFxSubset(false);
	}

	public byte[] writePFBSubset() throws IOException {
		return writePFxSubset(true);
	}

	private void writeChar(String string, OutputStream outputstream)
			throws IOException {
		CharString charstring = (CharString) chars.get(string);
		outputstream.write(new String("/" + string + " "
				+ charstring.encodedChar.length + " "
				+ charstring.firstDelimiter + " ").getBytes());
		outputstream.write(charstring.encodedChar);
		outputstream.write(new String(" " + charstring.secondDelimiter + "")
				.getBytes());
	}

	public void endSubset() throws IOException {
		if (subsetinprogress) {
			ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
			bytearrayoutputstream.write(bodye, 0, privateDictLength);
			bytearrayoutputstream.write(new String(" "
					+ (subsetchars.size() + 1) + " dict dup begin\n")
					.getBytes());
			Enumeration enumeration = subsetchars.elements();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				writeChar(string, bytearrayoutputstream);
			}
			writeChar(".notdef", bytearrayoutputstream);
			bytearrayoutputstream.write(bodytail.getBytes());
			subsetbody = bytearrayoutputstream.toByteArray();
			Type1Encryption.encrypt(55665, subsetbody);
			subsetinprogress = false;
		}
	}

	public byte[] writePFxSubset(boolean bool) throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		if (bool) {
			bytearrayoutputstream.write(-128);
			bytearrayoutputstream.write(1);
			bytearrayoutputstream.write(int2long(head.length()));
		}
		bytearrayoutputstream.write(head.getBytes());
		endSubset();
		if (bool) {
			bytearrayoutputstream.write(-128);
			bytearrayoutputstream.write(2);
			bytearrayoutputstream.write(int2long(subsetbody.length));
			bytearrayoutputstream.write(subsetbody);
		} else
			encode(new ByteArrayInputStream(subsetbody), bytearrayoutputstream);
		if (bool) {
			bytearrayoutputstream.write(-128);
			bytearrayoutputstream.write(1);
			bytearrayoutputstream.write(int2long(tail.length()));
		}
		bytearrayoutputstream.write(tail.getBytes());
		if (bool) {
			bytearrayoutputstream.write(-128);
			bytearrayoutputstream.write(3);
		}
		return bytearrayoutputstream.toByteArray();
	}

	int skipSpace(int i) throws IOException {
		while (i < bodye.length) {
			switch (bodye[i]) {
			case 9:
			case 10:
			case 13:
			case 32:
				i++;
				break;
			case 37:
				i = skipLine(i);
			/* fall through */
			default:
				return i;
			}
		}
		if (i > bodye.length)
			throw new IOException("Broken font file body");
		return i;
	}

	int skipWord(int i) {
		while (i < body.length) {
			switch (bodye[i]) {
			case 9:
			case 10:
			case 13:
			case 32:
				return i;
			default:
				i++;
			}
		}
		return i;
	}

	int skipLine(int i) throws IOException {
		while (i < body.length) {
			switch (bodye[i]) {
			case 10:
				i++;
				return skipSpace(i);
			case 13:
				i++;
				return skipSpace(i);
			default:
				i++;
			}
		}
		return i;
	}

	int skipDigits(int i) {
		while (i < body.length) {
			switch (bodye[i]) {
			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
				i++;
				break;
			default:
				return i;
			}
		}
		return i;
	}

	private static int hexval(int i) {
		switch (i) {
		case 48:
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
			return i - 48;
		case 97:
		case 98:
		case 99:
		case 100:
		case 101:
		case 102:
			return i - 97 + 10;
		default:
			return -1;
		}
	}

	public static void encode(InputStream inputstream, OutputStream outputstream)
			throws IOException {
		byte[] is = new byte[charsPerRow + 1];
		byte[] is_33_ = new byte[1024];
		int i = 0;
		int i_34_;
		while ((i_34_ = inputstream.read(is_33_)) > 0) {
			for (int i_35_ = 0; i_35_ < i_34_; i_35_++) {
				is[i++] = (byte) hd[(is_33_[i_35_] & 0xf0) >> 4];
				is[i++] = (byte) hd[is_33_[i_35_] & 0xf];
				if (i >= charsPerRow) {
					outputstream.write(is, 0, i);
					outputstream.write(10);
					i = 0;
				}
			}
			if (i > 0) {
				outputstream.write(is, 0, i);
				outputstream.write(10);
			}
		}
	}

	byte[] int2long(int i) {
		byte[] is = new byte[4];
		is[3] = (byte) (i >> 24 & 0xff);
		is[2] = (byte) (i >> 16 & 0xff);
		is[1] = (byte) (i >> 8 & 0xff);
		is[0] = (byte) (i & 0xff);
		return is;
	}

	public static void main(String[] strings) throws Exception {
		PFxFont pfxfont = new PFxFont(new SeekableFileInputStream(strings[0]));
		pfxfont.newSubset();
		pfxfont.writePFBSubset(strings[1]);
	}
}