/*
 * FontCatalog - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.sax.ConfigHandler;
import com.renderx.sax.ConfigHandlerAdapter;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.UnicodeData;
import com.renderx.util.User;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class FontCatalog {
	private static final String DEFAULT_FAMILY = "Helvetica";

	private static final String FONTLIST = "resource:com/renderx/fonts/fonts.xml";

	private static final int WEIGHT_100 = 0;

	private static final int WEIGHT_200 = 1;

	private static final int WEIGHT_300 = 2;

	private static final int WEIGHT_400 = 3;

	private static final int WEIGHT_500 = 4;

	private static final int WEIGHT_600 = 5;

	private static final int WEIGHT_700 = 6;

	private static final int WEIGHT_800 = 7;

	private static final int WEIGHT_900 = 8;

	private static final int WEIGHT_TOTAL = 9;

	private static final int WEIGHT_NORMAL = 3;

	private static final int WEIGHT_BOLD = 6;

	private static final int WEIGHT_DEFAULT = 3;

	private static final int STYLE_NORMAL = 0;

	private static final int STYLE_ITALIC = 1;

	private static final int STYLE_OBLIQUE = 2;

	private static final int STYLE_BACKSLANT = 3;

	private static final int STYLE_TOTAL = 4;

	private static final int STYLE_SLANTED = 2;

	private static final int STYLE_DEFAULT = 0;

	private static final Hashtable styleIndexes = new Hashtable();

	private static final int VARIANT_NORMAL = 0;

	private static final int VARIANT_SMALLCAPS = 1;

	private static final int VARIANT_TOTAL = 2;

	private static final int VARIANT_DEFAULT = 0;

	private static final Hashtable variantIndexes;

	static final double DEFAULT_EXTRA_SLANT_ANGLE = 12.0;

	static final double DEFAULT_SMALLCAP_SIZE_RATIO = 0.8;

	private final Hashtable catalog;

	private final Hashtable aliases;

	private final Hashtable languageAliases;

	private final ErrorHandler errHandler;

	private final URLCache cache;

	private FontFamilyDescriptor defaultFamily;

	private String configFileName;

	private class Config implements ConfigHandler {
		private static final String NSURI = "http://www.renderx.com/XEP/config";

		private String defaultFamilyName = "Helvetica";

		private int skip = 0;

		private final List tmpAliases = new List();

		private Config() {
			/* empty */
		}

		public void startDocument(Hashtable hashtable) {
			hashtable.put("embed", "auto");
			hashtable.put("subset", "true");
			hashtable.put("ligatures", "");
			configFileName = (String) hashtable.get("document");
		}

		public void endDocument(Hashtable hashtable) throws SAXException {
			defaultFamily = ((FontFamilyDescriptor) catalog
					.get(makeFamilyName(defaultFamilyName)));
			if (defaultFamily == null)
				throw new SAXException("Default font family '"
						+ defaultFamilyName
						+ "' is not defined in the configuration file");
			if (defaultFamily.fonts[3][0][0].getMetric() == null)
				throw new SAXException("Default font family '"
						+ defaultFamilyName
						+ "' is not accessible; cannot proceed");
			processAliases();
		}

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes, Hashtable hashtable) {
			if (skip > 0)
				skip++;
			else if ("".equals(string)
					|| "http://www.renderx.com/XEP/config".equals(string)) {
				String string_2_ = attributes.getValue("embed");
				if (string_2_ != null)
					hashtable.put("embed", string_2_);
				String string_3_ = attributes.getValue("subset");
				if (string_3_ != null)
					hashtable.put("subset", string_3_);
				String string_4_ = attributes.getValue("ligatures");
				if (string_4_ != null) {
					StringBuffer stringbuffer = new StringBuffer();
					for (int i = 0; i < string_4_.length(); i++) {
						char c = string_4_.charAt(i);
						if (c != ' ' && c != ',' && string_4_.indexOf(c) == i) {
							char[] cs = (char[]) UnicodeData.LIGATURES.get(c);
							if (cs != null && cs.length > 1)
								stringbuffer.append(c);
							else
								printWarning("Character U+"
										+ Integer.toHexString(c)
										+ " is not recognized as a ligature; character ignored");
						}
					}
					hashtable.put("ligatures", stringbuffer.toString());
				}
				if (string_0_.equals("fonts")) {
					String string_5_ = attributes.getValue("default-family");
					if (string_5_ != null)
						defaultFamilyName = string_5_;
				} else if (string_0_.equals("font-family")) {
					if (hashtable.get("family") != null) {
						printError("Nested 'font-family' elements are not permitted: inner element ignored");
						skip = 1;
					} else {
						String string_6_ = getRequiredValue(string_0_,
								attributes, "name");
						if (string_6_ != null)
							hashtable.put("family", new FontFamilyDescriptor(
									string_6_));
					}
				} else if (string_0_.equals("font")) {
					Hashtable hashtable_7_ = new Hashtable();
					hashtable_7_.put("embed", hashtable.get("embed"));
					hashtable_7_.put("subset", hashtable.get("subset"));
					hashtable_7_.put("ligatures", hashtable.get("ligatures"));
					copyProperty(hashtable_7_, attributes, "weight");
					copyProperty(hashtable_7_, attributes, "style");
					copyProperty(hashtable_7_, attributes, "variant");
					hashtable.put("font-properties", hashtable_7_);
				} else if (string_0_.equals("font-data")) {
					Hashtable hashtable_8_ = (Hashtable) hashtable
							.get("font-properties");
					if (hashtable_8_ == null)
						printError("'font-data' element outside of 'font'; element ignored");
					else {
						URLSpec urlspec = (URLSpec) hashtable.get("base");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"afm");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"pfa");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"pfb");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"ttf");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"otf");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"ttc");
						copyURLProperty(hashtable_8_, attributes, urlspec,
								"glyph-list");
						copyProperty(hashtable_8_, attributes, "subfont");
					}
				} else if (string_0_.equals("transform")) {
					Hashtable hashtable_9_ = (Hashtable) hashtable
							.get("font-properties");
					if (hashtable_9_ == null)
						printError("'transform' element outside of 'font'; element ignored");
					else {
						copyProperty(hashtable_9_, attributes, "slant-angle");
						copyProperty(hashtable_9_, attributes, "smallcap-size");
					}
				} else if (string_0_.equals("language")) {
					String string_10_ = getRequiredValue(string_0_, attributes,
							"codes");
					if (string_10_ != null) {
						if ("".equals(string_10_.replace(',', ' ').trim())) {
							printError("'codes' attribute on 'language' element contains no meaningful tokens: '"
									+ string_10_ + "'");
							skip = 1;
						} else
							hashtable.put("language-codes", string_10_);
					}
				} else if (string_0_.equals("font-alias")) {
					String string_11_ = getRequiredValue(string_0_, attributes,
							"name");
					String string_12_ = getRequiredValue(string_0_, attributes,
							"value");
					if (string_11_ != null && string_12_ != null) {
						tmpAliases.append(hashtable.get("language-codes"));
						tmpAliases.append(string_11_.trim());
						tmpAliases.append(string_12_.trim());
					}
				}
			}
		}

		public void endElement(String string, String string_13_,
				String string_14_, Hashtable hashtable) {
			if (skip > 0)
				skip--;
			else if ("".equals(string)
					|| "http://www.renderx.com/XEP/config".equals(string)) {
				if (string_13_.equals("font-family")) {
					FontFamilyDescriptor fontfamilydescriptor = (FontFamilyDescriptor) hashtable
							.get("family");
					try {
						fontfamilydescriptor.fillAllSlots();
					} catch (FontConfigurationException fontconfigurationexception) {
						printException(("Cannot configure font family '"
								+ fontfamilydescriptor.familyName + "'"),
								fontconfigurationexception);
					}
					String string_15_ = makeFamilyName(fontfamilydescriptor.familyName);
					if (catalog.get(string_15_) != null)
						printError("Font family '"
								+ fontfamilydescriptor.familyName
								+ "' defined twice; first definition preserved");
					else
						catalog.put(string_15_, fontfamilydescriptor);
				} else if (string_13_.equals("font")) {
					FontFamilyDescriptor fontfamilydescriptor = (FontFamilyDescriptor) hashtable
							.get("family");
					if (fontfamilydescriptor == null)
						printError("'font' element outside of 'font-family' ignored");
					else {
						try {
							fontfamilydescriptor
									.addFontRecord((Hashtable) hashtable
											.get("font-properties"));
						} catch (FontConfigurationException fontconfigurationexception) {
							printException(
									("Cannot add font descriptor to font family '"
											+ fontfamilydescriptor.familyName + "'; element skipped"),
									fontconfigurationexception);
						}
					}
				}
			}
		}

		public void characters(char[] cs, int i, int i_16_, Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		public void processingInstruction(String string, String string_17_,
				Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		private final String getRequiredValue(String string,
				Attributes attributes, String string_18_) {
			String string_19_ = attributes.getValue(string_18_);
			if (string_19_ == null) {
				printError("Mandatory attribute '" + string_18_
						+ "' is missing on element '" + string
						+ "'; element skipped");
				skip = 1;
			}
			return string_19_;
		}

		private final void copyURLProperty(Hashtable hashtable,
				Attributes attributes, URLSpec urlspec, String string) {
			String string_20_ = attributes.getValue(string);
			if (string_20_ != null) {
				try {
					hashtable.put(string, new URLSpec(urlspec, string_20_));
				} catch (MalformedURLException malformedurlexception) {
					printException(
							("Error processing attribute '" + string + "' on element 'font-data'; attribute ignored"),
							malformedurlexception);
				}
			}
		}

		private final void copyProperty(Hashtable hashtable,
				Attributes attributes, String string) {
			String string_21_ = attributes.getValue(string);
			if (string_21_ != null)
				hashtable.put(string, string_21_);
		}

		private void processAliases() {
			Enumeration enumeration = tmpAliases.elements();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				String string_22_ = (String) enumeration.nextElement();
				String string_23_ = (String) enumeration.nextElement();
				String string_24_ = makeFamilyName(string_22_);
				if (aliases.get(string_24_) != null) {
					printError("Alias '" + string_22_
							+ "' redefined; the first definition is kept");
					break;
				}
				List list = new List();
				StringTokenizer stringtokenizer = new StringTokenizer(
						string_23_, ",");
				while (stringtokenizer.hasMoreTokens()) {
					String string_25_ = stringtokenizer.nextToken().trim();
					if (string_25_ != null && string_25_.length() != 0) {
						if (catalog.get(makeFamilyName(string_25_)) == null)
							printWarning("Alias '"
									+ string_22_
									+ "' contains reference to an undefined font family '"
									+ string_25_ + "'");
						else
							list.append(makeFamilyName(string_25_));
					}
				}
				if (list.isEmpty())
					printError("No meaningful family names in the value of alias: '"
							+ string_22_ + "'='" + string_23_ + "'");
				else if (string == null)
					aliases.put(string_24_, list);
				else {
					StringTokenizer stringtokenizer_26_ = new StringTokenizer(
							string, " \t\r\n,");
					while (stringtokenizer_26_.hasMoreTokens()) {
						String string_27_ = stringtokenizer_26_.nextToken()
								.toLowerCase();
						Hashtable hashtable = (Hashtable) languageAliases
								.get(string_27_);
						if (hashtable == null) {
							hashtable = new Hashtable();
							languageAliases.put(string_27_, hashtable);
						}
						hashtable.put(string_24_, list);
					}
				}
			}
		}
	}

	private class FontFamilyDescriptor {
		final String familyName;

		final FontRecord[][][] fonts = new FontRecord[9][4][2];

		FontFamilyDescriptor(String string) {
			familyName = string.trim();
			for (int i = 0; i < 9; i++) {
				for (int i_28_ = 0; i_28_ < 4; i_28_++) {
					for (int i_29_ = 0; i_29_ < 2; i_29_++)
						fonts[i][i_28_][i_29_] = null;
				}
			}
		}

		void addFontRecord(Hashtable hashtable)
				throws FontConfigurationException {
			int i = 3;
			String string = (String) hashtable.get("weight");
			if (string != null) {
				string = string.trim();
				if ("normal".equalsIgnoreCase(string))
					i = 3;
				else if ("bold".equalsIgnoreCase(string))
					i = 6;
				else if ("100".equalsIgnoreCase(string))
					i = 0;
				else if ("200".equalsIgnoreCase(string))
					i = 1;
				else if ("300".equalsIgnoreCase(string))
					i = 2;
				else if ("400".equalsIgnoreCase(string))
					i = 3;
				else if ("500".equalsIgnoreCase(string))
					i = 4;
				else if ("600".equalsIgnoreCase(string))
					i = 5;
				else if ("700".equalsIgnoreCase(string))
					i = 6;
				else if ("800".equalsIgnoreCase(string))
					i = 7;
				else if ("900".equalsIgnoreCase(string))
					i = 8;
				else
					printError("Unrecognized weight value '" + string
							+ "' in font family " + familyName);
			}
			int i_30_ = 0;
			String string_31_ = (String) hashtable.get("style");
			if (string_31_ != null) {
				Integer integer = (Integer) FontCatalog.styleIndexes
						.get(string_31_.trim().toLowerCase());
				if (integer == null)
					printError("Unrecognized style value '" + string_31_
							+ "' in font family " + familyName);
				else
					i_30_ = integer.intValue();
			}
			int i_32_ = 0;
			String string_33_ = (String) hashtable.get("variant");
			if (string_33_ != null) {
				Integer integer = (Integer) FontCatalog.variantIndexes
						.get(string_33_.trim().toLowerCase());
				if (integer == null)
					printError("Unrecognized variant value '" + string_33_
							+ "' in font family " + familyName);
				else
					i_32_ = integer.intValue();
			}
			fonts[i][i_30_][i_32_] = new FontRecord(familyName, hashtable,
					cache, errHandler);
		}

		void fillAllSlots() throws FontConfigurationException {
			for (int i = 0; i < 4; i++) {
				for (int i_34_ = 0; i_34_ < 2; i_34_++) {
					int i_35_;
					for (i_35_ = 0; i_35_ < 9 && fonts[i_35_][i][i_34_] == null; i_35_++) {
						/* empty */
					}
					if (i_35_ != 9) {
						for (int i_36_ = 0; i_36_ < i_35_; i_36_++)
							fonts[i_36_][i][i_34_] = fonts[i_35_][i][i_34_];
						while (i_35_ < 9) {
							int i_37_ = i_35_;
							for (i_35_ = i_37_ + 1; i_35_ < 9
									&& fonts[i_35_][i][i_34_] == null; i_35_++) {
								/* empty */
							}
							if (i_35_ == 9) {
								for (int i_38_ = i_37_ + 1; i_38_ < 9; i_38_++)
									fonts[i_38_][i][i_34_] = fonts[i_37_][i][i_34_];
							} else {
								for (int i_39_ = i_37_ + 1; i_39_ < i_35_; i_39_++)
									fonts[i_39_][i][i_34_] = fonts[(i_39_
											- i_37_ <= i_35_ - i_39_ ? i_37_
											: i_35_)][i][i_34_];
							}
						}
					}
				}
			}
			for (int i = 0; i < 9; i++) {
				for (int i_40_ = 0; i_40_ < 2; i_40_++) {
					if (fonts[i][2][i_40_] == null
							&& fonts[i][1][i_40_] != null)
						fonts[i][2][i_40_] = fonts[i][1][i_40_];
					else if (fonts[i][2][i_40_] != null
							&& fonts[i][1][i_40_] == null)
						fonts[i][1][i_40_] = fonts[i][2][i_40_];
					if (fonts[i][0][i_40_] == null) {
						int i_41_;
						for (i_41_ = 0; i_41_ < 4
								&& fonts[i][i_41_][i_40_] == null; i_41_++) {
							/* empty */
						}
						if (i_41_ == 4)
							continue;
						fonts[i][0][i_40_] = fonts[i][i_41_][i_40_];
					}
					if (fonts[i][2][i_40_] == null) {
						fonts[i][2][i_40_] = fonts[i][0][i_40_].cloneRecord();
						fonts[i][2][i_40_].extraSlantAngle = 12.0;
						fonts[i][1][i_40_] = fonts[i][2][i_40_];
					}
					if (fonts[i][3][i_40_] == null) {
						fonts[i][3][i_40_] = fonts[i][0][i_40_].cloneRecord();
						fonts[i][3][i_40_].extraSlantAngle = -12.0;
					}
				}
			}
			for (int i = 0; i < 9; i++) {
				for (int i_42_ = 0; i_42_ < 4; i_42_++) {
					if (fonts[i][i_42_][0] == null) {
						if (fonts[i][i_42_][1] != null)
							fonts[i][i_42_][0] = fonts[i][i_42_][1];
						else
							throw new FontConfigurationException(
									"No meaningful font records in the descriptor");
					} else if (fonts[i][i_42_][1] == null) {
						fonts[i][i_42_][1] = fonts[i][i_42_][0].cloneRecord();
						fonts[i][i_42_][1].makeSmallCaps = true;
						fonts[i][i_42_][1].smallCapSizeRatio = 0.8;
					}
				}
			}
		}
	}

	private static final int getWeightIndex(int i) {
		if (i <= 100)
			return 0;
		if (i <= 200)
			return 1;
		if (i <= 300)
			return 2;
		if (i <= 400)
			return 3;
		if (i <= 500)
			return 4;
		if (i <= 600)
			return 5;
		if (i <= 700)
			return 6;
		if (i <= 800)
			return 7;
		return 8;
	}

	private static final int getStyleIndex(String string) {
		if (string == null)
			return 0;
		Integer integer = (Integer) styleIndexes.get(string.trim()
				.toLowerCase());
		return integer == null ? 0 : integer.intValue();
	}

	private static final int getVariantIndex(String string) {
		if (string == null)
			return 0;
		Integer integer = (Integer) variantIndexes.get(string.trim()
				.toLowerCase());
		return integer == null ? 0 : integer.intValue();
	}

	public FontCatalog() throws IOException, FontConfigurationException {
		this(new DefaultErrorHandler());
	}

	public FontCatalog(ErrorHandler errorhandler) throws IOException,
			FontConfigurationException {
		this(new URLCache(errorhandler), errorhandler);
	}

	public FontCatalog(URLCache urlcache, ErrorHandler errorhandler)
			throws IOException, FontConfigurationException {
		this(User.getProperty("com.renderx.xep.CONFIG"), urlcache, errorhandler);
	}

	public FontCatalog(String string, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException,
			FontConfigurationException {
		this(new URLSpec(
				string == null ? "resource:com/renderx/fonts/fonts.xml"
						: string), urlcache, errorhandler);
	}

	public FontCatalog(URLSpec urlspec, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException,
			FontConfigurationException {
		catalog = new Hashtable();
		aliases = new Hashtable();
		languageAliases = new Hashtable();
		defaultFamily = null;
		configFileName = null;
		cache = urlcache;
		errHandler = errorhandler;
		GlyphList.init();
		try {
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			xmlreader.setContentHandler(new ConfigHandlerAdapter(new Config(),
					urlspec));
			xmlreader.parse(new InputSource(urlspec));
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			throw new FontConfigurationException("Cannot parse " + urlspec
					+ ": " + exception.toString());
		}
	}

	public FontCatalog(org.xml.sax.InputSource inputsource,
			XMLReader xmlreader, URLCache urlcache, ErrorHandler errorhandler)
			throws IOException, FontConfigurationException {
		catalog = new Hashtable();
		aliases = new Hashtable();
		languageAliases = new Hashtable();
		defaultFamily = null;
		configFileName = null;
		cache = urlcache;
		errHandler = errorhandler;
		GlyphList.init();
		try {
			xmlreader.setContentHandler(new ConfigHandlerAdapter(new Config(),
					new URLSpec(inputsource.getSystemId())));
			xmlreader.parse(inputsource);
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			throw new FontConfigurationException("Cannot parse "
					+ inputsource.getSystemId() + ": " + exception.toString());
		}
	}

	private static final String makeFamilyName(String string) {
		if (string == null)
			return null;
		StringBuffer stringbuffer = new StringBuffer(string.length());
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (!Character.isSpaceChar(c) && c != '\'' && c != '\"')
				stringbuffer.append(c);
		}
		return stringbuffer.toString().toLowerCase();
	}

	public FontRecord getFontRecord(String string, int i, String string_43_,
			String string_44_) {
		FontFamilyDescriptor fontfamilydescriptor = (FontFamilyDescriptor) catalog
				.get(makeFamilyName(string));
		if (fontfamilydescriptor == null)
			fontfamilydescriptor = defaultFamily;
		int i_45_ = getWeightIndex(i);
		int i_46_ = getStyleIndex(string_43_);
		int i_47_ = getVariantIndex(string_44_);
		return fontfamilydescriptor.fonts[i_45_][i_46_][i_47_];
	}

	public List findAll(String string, int i, String string_48_,
			String string_49_, String string_50_, ErrorHandler errorhandler) {
		int i_51_ = getWeightIndex(i);
		int i_52_ = getStyleIndex(string_48_);
		int i_53_ = getVariantIndex(string_49_);
		List list = new List();
		StringTokenizer stringtokenizer = new StringTokenizer(string.trim(),
				",");
		if (!stringtokenizer.hasMoreTokens()) {
			list.append(defaultFamily.fonts[i_51_][i_52_][i_53_]);
			return list;
		}
		Hashtable hashtable = null;
		if (string_50_ != null) {
			string_50_ = string_50_.toLowerCase().trim();
			hashtable = (Hashtable) languageAliases.get(string_50_);
			if (hashtable == null) {
				int i_54_ = string_50_.indexOf('-');
				if (i_54_ != -1) {
					string_50_ = string_50_.substring(0, i_54_).trim();
					hashtable = (Hashtable) languageAliases.get(string_50_);
				}
			}
		}
		while (stringtokenizer.hasMoreTokens()) {
			String string_55_ = makeFamilyName(stringtokenizer.nextToken());
			List list_56_ = null;
			if (hashtable != null)
				list_56_ = (List) hashtable.get(string_55_);
			if (list_56_ == null)
				list_56_ = (List) aliases.get(string_55_);
			if (list_56_ == null) {
				FontFamilyDescriptor fontfamilydescriptor = (FontFamilyDescriptor) catalog
						.get(string_55_);
				if (fontfamilydescriptor != null) {
					FontRecord fontrecord = fontfamilydescriptor.fonts[i_51_][i_52_][i_53_];
					if (fontrecord.getMetric() != null)
						list.append(fontrecord);
				}
			} else {
				Enumeration enumeration = list_56_.elements();
				while (enumeration.hasMoreElements()) {
					FontFamilyDescriptor fontfamilydescriptor = ((FontFamilyDescriptor) catalog
							.get((String) enumeration.nextElement()));
					if (fontfamilydescriptor != null) {
						FontRecord fontrecord = fontfamilydescriptor.fonts[i_51_][i_52_][i_53_];
						if (fontrecord.getMetric() != null)
							list.append(fontrecord);
					}
				}
			}
		}
		if (list.isEmpty()) {
			if (errorhandler != null)
				errorhandler
						.warning("could not find any font family matching \""
								+ string + "\"; replaced by "
								+ defaultFamily.familyName);
			list.append(defaultFamily.fonts[i_51_][i_52_][i_53_]);
		}
		return list;
	}

	public void printWarning(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.warning(configFileName + ": " + string);
			}
		}
	}

	public void printError(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.error(configFileName + ": " + string);
			}
		}
	}

	public void printException(String string, Exception exception) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.exception(configFileName + ": " + string, exception);
			}
		}
	}

	static {
		styleIndexes.put("normal", new Integer(0));
		styleIndexes.put("italic", new Integer(1));
		styleIndexes.put("oblique", new Integer(2));
		styleIndexes.put("backslant", new Integer(3));
		variantIndexes = new Hashtable();
		variantIndexes.put("normal", new Integer(0));
		variantIndexes.put("small-caps", new Integer(1));
	}
}