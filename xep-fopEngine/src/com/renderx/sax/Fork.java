/*
 * Fork - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class Fork implements ContentHandler {
	protected ContentHandler h1 = null;

	protected ContentHandler h2 = null;

	public Fork(ContentHandler contenthandler, ContentHandler contenthandler_0_) {
		h1 = contenthandler;
		h2 = contenthandler_0_;
	}

	public void startDocument() throws SAXException {
		if (h1 != null)
			h1.startDocument();
		if (h2 != null)
			h2.startDocument();
	}

	public void endDocument() throws SAXException {
		if (h1 != null)
			h1.endDocument();
		if (h2 != null)
			h2.endDocument();
	}

	public void startElement(String string, String string_1_, String string_2_,
			Attributes attributes) throws SAXException {
		if (h1 != null)
			h1.startElement(string, string_1_, string_2_, attributes);
		if (h2 != null)
			h2.startElement(string, string_1_, string_2_, attributes);
	}

	public void endElement(String string, String string_3_, String string_4_)
			throws SAXException {
		if (h1 != null)
			h1.endElement(string, string_3_, string_4_);
		if (h2 != null)
			h2.endElement(string, string_3_, string_4_);
	}

	public void processingInstruction(String string, String string_5_)
			throws SAXException {
		if (h1 != null)
			h1.processingInstruction(string, string_5_);
		if (h2 != null)
			h2.processingInstruction(string, string_5_);
	}

	public void characters(char[] cs, int i, int i_6_) throws SAXException {
		if (h1 != null)
			h1.characters(cs, i, i_6_);
		if (h2 != null)
			h2.characters(cs, i, i_6_);
	}

	public void ignorableWhitespace(char[] cs, int i, int i_7_)
			throws SAXException {
		if (h1 != null)
			h1.ignorableWhitespace(cs, i, i_7_);
		if (h2 != null)
			h2.ignorableWhitespace(cs, i, i_7_);
	}

	public void startPrefixMapping(String string, String string_8_)
			throws SAXException {
		if (h1 != null)
			h1.startPrefixMapping(string, string_8_);
		if (h2 != null)
			h2.startPrefixMapping(string, string_8_);
	}

	public void endPrefixMapping(String string) throws SAXException {
		if (h1 != null)
			h1.endPrefixMapping(string);
		if (h2 != null)
			h2.endPrefixMapping(string);
	}

	public void skippedEntity(String string) throws SAXException {
		if (h1 != null)
			h1.skippedEntity(string);
		if (h2 != null)
			h2.skippedEntity(string);
	}

	public void setDocumentLocator(Locator locator) {
		if (h1 != null)
			h1.setDocumentLocator(locator);
		if (h2 != null)
			h2.setDocumentLocator(locator);
	}
}