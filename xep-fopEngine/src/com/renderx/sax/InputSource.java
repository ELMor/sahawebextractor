/*
 * InputSource - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import com.renderx.util.URLSpec;
import com.renderx.util.URLUtil;

public class InputSource extends org.xml.sax.InputSource {
	public InputSource() {
		/* empty */
	}

	public InputSource(String string) throws IOException {
		this(new URLSpec(string));
	}

	public InputSource(URLSpec urlspec) throws IOException {
		this();
		String string = urlspec.toString();
		if ("file".equalsIgnoreCase(urlspec.getProtocol()))
			string = URLUtil.urlDecode(string);
		this.setSystemId(urlspec.toString());
		if (urlspec.scheme != 3)
			this.setByteStream(urlspec.openStream());
	}

	public InputSource(InputStream inputstream) {
		super(inputstream);
	}

	public InputSource(Reader reader) {
		super(reader);
	}
}