/*
 * Serializer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import com.renderx.util.Hashtable;
import com.renderx.util.Stack;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class Serializer implements ContentHandler {
	protected final Writer out;

	private String topURI = null;

	private Hashtable uritab;

	private Hashtable prefixtab;

	private boolean hasUTF8;

	private boolean opentag;

	private boolean closeOnExit;

	private Stack elementNames;

	private int prefIndex;

	private static final String[] prefixCharacters = { "a", "b", "c", "d", "e",
			"f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
			"s", "t", "u", "v", "w", "x", "y", "z" };

	public static final class Test extends TestCase {
		private XMLReader parser;

		public void setUp() {
			try {
				parser = XMLReaderFactory.createXMLReader();
			} catch (SAXException saxexception) {
				Assert.assertTrue(false);
			}
		}

		private boolean boomerang(String string) {
			try {
				StringWriter stringwriter = new StringWriter();
				parser.setContentHandler(new Serializer(stringwriter));
				parser.parse(new org.xml.sax.InputSource(new StringReader(
						string)));
				return (stringwriter.toString()
						.equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
								+ string));
			} catch (IOException ioexception) {
				return false;
			} catch (SAXException saxexception) {
				return false;
			}
		}

		public void testSingleton() {
			Assert.assertTrue(boomerang("<a/>"));
		}

		public void testSimple() {
			Assert.assertTrue(boomerang("<a><b/></a>"));
		}

		public void testSimpleWithAttrs() {
			Assert.assertTrue(boomerang("<a x=\"y\"><b c=\"d\"/></a>"));
		}

		public void testNamespace() {
			Assert.assertTrue(boomerang("<x:a xmlns:x=\"y\"><x:b/></x:a>"));
		}
	}

	public Serializer(String string) throws IOException {
		this(new BufferedOutputStream(new FileOutputStream(string)));
		closeOnExit = true;
	}

	public Serializer(OutputStream outputstream) throws IOException {
		uritab = new Hashtable();
		prefixtab = new Hashtable();
		hasUTF8 = true;
		opentag = true;
		closeOnExit = false;
		elementNames = new Stack();
		prefIndex = 0;
		OutputStreamWriter outputstreamwriter;
		try {
			outputstreamwriter = new OutputStreamWriter(outputstream, "UTF8");
			hasUTF8 = true;
		} catch (UnsupportedEncodingException unsupportedencodingexception) {
			outputstreamwriter = new OutputStreamWriter(outputstream);
			hasUTF8 = false;
		}
		out = outputstreamwriter;
	}

	public Serializer(Writer writer) {
		uritab = new Hashtable();
		prefixtab = new Hashtable();
		hasUTF8 = true;
		opentag = true;
		closeOnExit = false;
		elementNames = new Stack();
		prefIndex = 0;
		out = writer;
	}

	protected void closeTag() throws IOException {
		if (opentag) {
			out.write(">");
			opentag = false;
		}
	}

	public void startDocument() throws SAXException {
		opentag = false;
		elementNames.clear();
		topURI = null;
		try {
			out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void endDocument() throws SAXException {
		try {
			closeTag();
			out.flush();
			if (closeOnExit)
				out.close();
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void processingInstruction(String string, String string_0_)
			throws SAXException {
		try {
			closeTag();
			out.write("<?");
			writeMarkupElement(string);
			if (string_0_ != null && string_0_.length() > 0) {
				out.write(' ');
				writeMarkupElement(string_0_);
			}
			out.write("?>");
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void characters(char[] cs, int i, int i_1_) throws SAXException {
		try {
			closeTag();
			writePCDATA(cs, i, i_1_);
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void ignorableWhitespace(char[] cs, int i, int i_2_)
			throws SAXException {
		characters(cs, i, i_2_);
	}

	public void skippedEntity(String string) {
		/* empty */
	}

	public void startPrefixMapping(String string, String string_3_) {
		/* empty */
	}

	public void endPrefixMapping(String string) {
		/* empty */
	}

	public void setDocumentLocator(Locator locator) {
		/* empty */
	}

	public void startElement(String string, String string_4_, String string_5_,
			Attributes attributes) throws SAXException {
		String string_6_ = null;
		if (string_4_ != null) {
			if (string == null || string.length() == 0)
				string_5_ = string_4_;
			else {
				string_6_ = (String) uritab.get(string);
				if (string_6_ == null) {
					if (string.equals("http://www.w3.org/XML/1998/namespace"))
						string_6_ = "xml";
					else
						string_6_ = assignPrefix(string, string_5_);
				}
				string_5_ = string_6_ + ":" + string_4_;
			}
		}
		elementNames.push(string_5_);
		try {
			closeTag();
			out.write("<");
			writeMarkupElement(string_5_);
			if (string_4_ != null && string != null && string.length() != 0
					&& !string.equals("http://www.w3.org/XML/1998/namespace")
					&& !string.equals(topURI)) {
				out.write(" xmlns:");
				writeMarkupElement(string_6_);
				out.write("=\"");
				writeMarkupElement(string);
				out.write("\"");
				if (topURI == null)
					topURI = string;
			}
			if (attributes != null) {
				Hashtable hashtable = new Hashtable();
				int i = attributes.getLength();
				for (int i_7_ = 0; i_7_ < i; i_7_++) {
					String string_8_ = attributes.getLocalName(i_7_);
					String string_9_ = attributes.getURI(i_7_);
					String string_10_ = attributes.getQName(i_7_);
					if (string_8_ != null) {
						if (string_9_ == null || string_9_.length() == 0)
							string_10_ = string_8_;
						else {
							string_6_ = (String) uritab.get(string_9_);
							if (string_6_ == null) {
								if (string_9_
										.equals("http://www.w3.org/XML/1998/namespace"))
									string_6_ = "xml";
								else
									string_6_ = assignPrefix(string_9_,
											string_10_);
							}
							string_10_ = string_6_ + ":" + string_8_;
							if (!hashtable.containsKey(string_9_)
									&& !string_9_.equals(string)
									&& !string_9_.equals(topURI)
									&& !(string_9_
											.equals("http://www.w3.org/XML/1998/namespace"))) {
								out.write(" xmlns:");
								writeMarkupElement(string_6_);
								out.write("=\"");
								writeMarkupElement(string_9_);
								out.write("\"");
								hashtable.put(string_9_, string_6_);
							}
						}
					}
					out.write(" ");
					writeMarkupElement(string_10_);
					out.write("=\"");
					writeMarkupElement(attributes.getValue(i_7_));
					out.write("\"");
				}
			}
			opentag = true;
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void endElement(String string, String string_11_, String string_12_)
			throws SAXException {
		String string_13_ = (String) elementNames.pop();
		if (string_11_ != null) {
			if (string == null || string.length() == 0)
				string_12_ = string_11_;
			else {
				String string_14_ = (String) uritab.get(string);
				if (string_14_ == null) {
					if ("http://www.w3.org/XML/1998/namespace".equals(string))
						string_14_ = "xml";
					else
						throw new SAXException("Namespace '" + string
								+ "' first mentioned on the closing tag '"
								+ string_11_ + "'");
				}
				string_12_ = string_14_ + ":" + string_11_;
			}
		}
		if (!string_12_.equals(string_13_))
			throw new SAXException("Open tag '" + string_13_
					+ "' does not match closing tag '" + string_12_ + "'");
		try {
			if (opentag) {
				out.write("/>");
				opentag = false;
			} else {
				out.write("</");
				writeMarkupElement(string_13_);
				out.write(">");
			}
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	protected void writeNormalized(char[] cs, int i, int i_15_, boolean bool)
			throws IOException {
		for (int i_16_ = 0; i_16_ < i_15_; i_16_++) {
			char c = cs[i + i_16_];
			switch (c) {
			case '<':
				out.write("&lt;");
				break;
			case '>':
				out.write("&gt;");
				break;
			case '&':
				out.write("&amp;");
				break;
			case '\"':
				if (bool) {
					out.write("&quot;");
					break;
				}
			/* fall through */
			case '\'':
				if (bool) {
					out.write("&apos;");
					break;
				}
			/* fall through */
			case '\n':
				if (bool) {
					out.write("&#xA;");
					break;
				}
			/* fall through */
			case '\t':
				if (bool) {
					out.write("&#x9;");
					break;
				}
			/* fall through */
			default:
				if (!hasUTF8 && (c > '\u007f' || c < 0))
					out.write("&#" + (int) c + ";");
				else
					out.write(c);
			}
		}
	}

	protected void writeMarkupElement(String string) throws IOException {
		writeNormalized(string.toCharArray(), 0, string.length(), true);
	}

	protected void writePCDATA(char[] cs, int i, int i_17_) throws IOException {
		writeNormalized(cs, i, i_17_, false);
	}

	protected String assignPrefix(String string, String string_18_)
			throws SAXException {
		String string_19_ = null;
		if (string_18_ != null && string_18_.indexOf(':') != -1) {
			string_19_ = string_18_.substring(0, string_18_.indexOf(':'));
			if (prefixtab.containsKey(string_19_))
				string_19_ = null;
		}
		if (string_19_ == null) {
			int i = prefixCharacters.length;
			while (prefIndex < 2147483647) {
				int i_20_ = ++prefIndex;
				string_19_ = "";
				while (i_20_ != 0) {
					int i_21_ = i_20_ % i;
					i_20_ = (i_20_ - i_21_) / i;
					string_19_ += prefixCharacters[i_21_];
				}
				if (!string_19_.equals("xml")
						&& !prefixtab.containsKey(string_19_))
					break;
			}
			if (prefIndex == 2147483647)
				throw new SAXException(
						"Cannot assign more than 2147483645 different prefixes");
		}
		uritab.put(string, string_19_);
		prefixtab.put(string_19_, string);
		return string_19_;
	}
}