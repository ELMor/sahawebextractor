/*
 * JAXPSourceConverter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import com.renderx.util.URLSpec;
import com.renderx.util.URLUtil;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class JAXPSourceConverter {
	public InputStream stream = null;

	public org.xml.sax.InputSource source = null;

	public XMLReader parser = null;

	public JAXPSourceConverter(Source source) throws SAXException, IOException {
		String string = source.getSystemId();
		if (string == null)
			string = URLUtil.toURL(new File("untitled")).toExternalForm();
		if (source instanceof SAXSource) {
			SAXSource saxsource = (SAXSource) source;
			this.source = saxsource.getInputSource();
			parser = saxsource.getXMLReader();
		} else if (source instanceof DOMSource) {
			DOMSource domsource = (DOMSource) source;
			this.source = new NullSource(string);
			parser = new DOMWalker(domsource.getNode());
		} else if (source instanceof StreamSource) {
			StreamSource streamsource = (StreamSource) source;
			this.source = new InputSource(string);
			if (streamsource.getInputStream() != null)
				this.source.setByteStream(streamsource.getInputStream());
			else if (streamsource.getReader() != null)
				this.source.setCharacterStream(streamsource.getReader());
			else if (streamsource.getSystemId() != null) {
				URLSpec urlspec = new URLSpec(streamsource.getSystemId());
				stream = urlspec.openStream();
				this.source.setByteStream(stream);
			} else
				throw new IOException("No meaningful data in a StreamSource");
		}
		if (parser == null)
			parser = XMLReaderFactory.createXMLReader();
	}
}