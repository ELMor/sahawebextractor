/*
 * ConfigHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import com.renderx.util.Hashtable;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public interface ConfigHandler {
	public void startDocument(Hashtable hashtable) throws SAXException;

	public void endDocument(Hashtable hashtable) throws SAXException;

	public void startElement(String string, String string_0_, String string_1_,
			Attributes attributes, Hashtable hashtable) throws SAXException;

	public void endElement(String string, String string_2_, String string_3_,
			Hashtable hashtable) throws SAXException;

	public void characters(char[] cs, int i, int i_4_, Hashtable hashtable)
			throws SAXException;

	public void processingInstruction(String string, String string_5_,
			Hashtable hashtable) throws SAXException;
}