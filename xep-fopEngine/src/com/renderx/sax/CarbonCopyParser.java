/*
 * CarbonCopyParser - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

public class CarbonCopyParser extends XMLFilterImpl {
	private SAXStorage carbonCopy = new SAXStorage();

	private ContentHandler handler = null;

	public CarbonCopyParser(XMLReader xmlreader) {
		super(xmlreader);
		try {
			xmlreader
					.setFeature("http://xml.org/sax/features/namespaces", true);
			xmlreader.setFeature(
					"http://xml.org/sax/features/namespace-prefixes", false);
		} catch (SAXException saxexception) {
			/* empty */
		}
	}

	public void setFeature(String string, boolean bool)
			throws SAXNotSupportedException, SAXNotRecognizedException {
		if (string.equals("http://xml.org/sax/features/namespaces") && !bool)
			throw new SAXNotSupportedException(this.getClass().getName()
					+ " does not support setting feature " + string + " to "
					+ bool);
		super.setFeature(string, bool);
	}

	public ContentHandler getContentHandler() {
		return handler;
	}

	public void setContentHandler(ContentHandler contenthandler) {
		handler = contenthandler;
		super.setContentHandler(new Fork(contenthandler, carbonCopy));
	}

	public SAXStorage getCC() {
		return carbonCopy;
	}
}