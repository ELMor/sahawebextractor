/*
 * XMLReaderAdapter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

import com.renderx.util.Hashtable;
import com.renderx.util.User;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.DocumentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.Parser;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributeListImpl;

public class XMLReaderAdapter implements Parser, ContentHandler {
	boolean namespacesUndeclared = true;

	private static final String DEFAULT_RESOLVER_CLASS_NAME = "none";

	private static final Hashtable namespaceTable = new Hashtable();

	static final String ALIEN_NAMESPACE_URI = "[alien namespace]";

	static final String ALIEN_NAMESPACE_PREFIX = "alien";

	private XMLReader parent = null;

	private DocumentHandler handler = null;

	public XMLReaderAdapter(XMLReader xmlreader) throws SAXException {
		parent = xmlreader;
		if (xmlreader != null) {
			xmlreader.setContentHandler(this);
			try {
				xmlreader.setFeature("http://xml.org/sax/features/namespaces",
						true);
				xmlreader
						.setFeature(
								"http://xml.org/sax/features/namespace-prefixes",
								false);
			} catch (SAXException saxexception) {
				/* empty */
			}
			String string = User.getProperty("com.renderx.sax.entityresolver");
			if (string == null)
				string = "none";
			if (!"none".equals(string)) {
				try {
					xmlreader.setEntityResolver((EntityResolver) Class.forName(
							string).newInstance());
				} catch (Exception exception) {
					throw new SAXException("Cannot create entity resolver "
							+ string + ": " + exception.toString());
				}
			}
		}
	}

	public void setDocumentHandler(DocumentHandler documenthandler) {
		handler = documenthandler;
	}

	public void setLocale(Locale locale) throws SAXException {
		/* empty */
	}

	public void parse(org.xml.sax.InputSource inputsource) throws SAXException,
			IOException {
		parent.parse(inputsource);
	}

	public void parse(String string) throws SAXException, IOException {
		parent.parse(string);
	}

	public void setDTDHandler(DTDHandler dtdhandler) {
		parent.setDTDHandler(dtdhandler);
	}

	public void setEntityResolver(EntityResolver entityresolver) {
		parent.setEntityResolver(entityresolver);
	}

	public void setErrorHandler(ErrorHandler errorhandler) {
		parent.setErrorHandler(errorhandler);
	}

	public void startElement(String string, String string_0_, String string_1_,
			Attributes attributes) throws SAXException {
		AttributeListImpl attributelistimpl = new AttributeListImpl();
		for (int i = 0; i < attributes.getLength(); i++) {
			String string_2_ = attributes.getQName(i);
			if (string_2_ == null
					|| (!string_2_.startsWith("xmlns:") && !string_2_
							.equals("xmlns"))) {
				String string_3_ = buildQualifiedName(attributes.getURI(i),
						attributes.getLocalName(i));
				attributelistimpl.addAttribute(string_3_, "CDATA", attributes
						.getValue(i));
			}
		}
		if (namespacesUndeclared) {
			namespacesUndeclared = false;
			Enumeration enumeration = namespaceTable.keys();
			while (enumeration.hasMoreElements()) {
				String string_4_ = (String) enumeration.nextElement();
				String string_5_ = (String) namespaceTable.get(string_4_);
				attributelistimpl.addAttribute("xmlns:" + string_5_, "CDATA",
						string_4_);
			}
			attributelistimpl.addAttribute("xmlns:alien", "CDATA",
					"[alien namespace]");
		}
		handler.startElement(buildQualifiedName(string, string_0_),
				attributelistimpl);
	}

	public void endElement(String string, String string_6_, String string_7_)
			throws SAXException {
		handler.endElement(buildQualifiedName(string, string_6_));
	}

	private String buildQualifiedName(String string, String string_8_) {
		if (string == null || string.length() == 0)
			return string_8_;
		if (string.equals("http://www.w3.org/XML/1998/namespace"))
			return "xml:" + string_8_;
		String string_9_ = (String) namespaceTable.get(string);
		if (string_9_ == null)
			string_9_ = "alien";
		return string_9_ + ":" + string_8_;
	}

	public void startPrefixMapping(String string, String string_10_) {
		/* empty */
	}

	public void endPrefixMapping(String string) {
		/* empty */
	}

	public void skippedEntity(String string) {
		/* empty */
	}

	public void startDocument() throws SAXException {
		namespacesUndeclared = true;
		handler.startDocument();
	}

	public void endDocument() throws SAXException {
		handler.endDocument();
	}

	public void characters(char[] cs, int i, int i_11_) throws SAXException {
		handler.characters(cs, i, i_11_);
	}

	public void ignorableWhitespace(char[] cs, int i, int i_12_)
			throws SAXException {
		handler.ignorableWhitespace(cs, i, i_12_);
	}

	public void processingInstruction(String string, String string_13_)
			throws SAXException {
		handler.processingInstruction(string, string_13_);
	}

	public void setDocumentLocator(Locator locator) {
		handler.setDocumentLocator(locator);
	}

	static {
		namespaceTable.put("http://www.w3.org/1999/XSL/Transform", "xsl");
		namespaceTable.put("http://www.w3.org/1999/XSL/Format", "fo");
		namespaceTable.put("http://www.w3.org/2000/svg", "svg");
		namespaceTable.put("http://www.w3.org/1999/xlink", "xlink");
		namespaceTable.put("http://www.renderx.com/XSL/Extensions", "rx");
	}
}