/*
 * SAXStorage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class SAXStorage extends XMLPlayer implements ContentHandler {
	private List queue = new List();

	private StringStorage cacheURI = new StringStorage();

	private StringStorage cacheLocalName = new StringStorage();

	private StringStorage cacheQName = new StringStorage();

	private StringStorage cachePrefix = new StringStorage();

	private StringStorage cacheAttrLocalName = new StringStorage();

	private StringStorage cacheAttrQName = new StringStorage();

	private StringStorage cacheAttrType = new StringStorage();

	private StringStorage cacheAttrValue = new StringStorage();

	private StringStorage cachePITarget = new StringStorage();

	private StringStorage cachePIData = new StringStorage();

	private static class StringStorage extends Hashtable {
		private StringStorage() {
			/* empty */
		}

		public String store(String string) {
			if (string == null)
				return string;
			String string_0_ = (String) this.get(string);
			if (string_0_ != null)
				return string_0_;
			this.put(string, string);
			return string;
		}
	}

	class EndPrefixMappingRecord implements SAXRecord {
		private String prefix;

		EndPrefixMappingRecord(String string) {
			prefix = cachePrefix.store(string);
		}

		public void play() throws SAXException {
			handler.endPrefixMapping(prefix);
		}
	}

	class StartPrefixMappingRecord implements SAXRecord {
		private String uri;

		private String prefix;

		StartPrefixMappingRecord(String string, String string_1_) {
			prefix = cachePrefix.store(string);
			uri = cacheURI.store(string_1_);
		}

		public void play() throws SAXException {
			handler.startPrefixMapping(prefix, uri);
		}
	}

	class IgnorableWhitespaceRecord implements SAXRecord {
		private char[] whitespace;

		IgnorableWhitespaceRecord(char[] cs, int i, int i_2_) {
			whitespace = new char[i_2_];
			for (int i_3_ = 0; i_3_ < i_2_; i_3_++)
				whitespace[i_3_] = cs[i + i_3_];
		}

		public void play() throws SAXException {
			handler.ignorableWhitespace(whitespace, 0, whitespace.length);
		}
	}

	class CharactersRecord implements SAXRecord {
		private char[] text;

		CharactersRecord(char[] cs, int i, int i_4_) {
			text = new char[i_4_];
			for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
				text[i_5_] = cs[i + i_5_];
		}

		public void play() throws SAXException {
			handler.characters(text, 0, text.length);
		}
	}

	class ProcessingInstructionRecord implements SAXRecord {
		private String target;

		private String data;

		ProcessingInstructionRecord(String string, String string_6_) {
			target = cachePITarget.store(string);
			data = cachePIData.store(string_6_);
		}

		public void play() throws SAXException {
			handler.processingInstruction(target, data);
		}
	}

	class EndElementRecord implements SAXRecord {
		private String uri;

		private String localName;

		private String qName;

		EndElementRecord(String string, String string_7_, String string_8_) {
			uri = cacheURI.store(string);
			localName = cacheLocalName.store(string_7_);
			qName = cacheQName.store(string_8_);
		}

		public void play() throws SAXException {
			handler.endElement(uri, localName, qName);
		}
	}

	class StartElementRecord implements SAXRecord {
		private String uri;

		private String localName;

		private String qName;

		private AttributesImpl atts;

		StartElementRecord(String string, String string_9_, String string_10_,
				Attributes attributes) {
			uri = cacheURI.store(string);
			localName = cacheLocalName.store(string_9_);
			qName = cacheQName.store(string_10_);
			if (attributes == null)
				atts = null;
			else {
				atts = new AttributesImpl();
				for (int i = 0; i < attributes.getLength(); i++) {
					String string_11_ = attributes.getQName(i);
					if (string_11_ == null || !string_11_.startsWith("xmlns:"))
						atts.addAttribute(cacheURI.store(attributes.getURI(i)),
								cacheAttrLocalName.store(attributes
										.getLocalName(i)), cacheAttrQName
										.store(string_11_), cacheAttrType
										.store(attributes.getType(i)),
								cacheAttrValue.store(attributes.getValue(i)));
				}
			}
		}

		public void play() throws SAXException {
			handler.startElement(uri, localName, qName, atts);
		}
	}

	class EndDocumentRecord implements SAXRecord {
		public void play() throws SAXException {
			handler.endDocument();
		}
	}

	class StartDocumentRecord implements SAXRecord {
		public void play() throws SAXException {
			handler.startDocument();
		}
	}

	static interface SAXRecord {
		public void play() throws SAXException;
	}

	public void startDocument() throws SAXException {
		queue.append(new StartDocumentRecord());
	}

	public void endDocument() throws SAXException {
		queue.append(new EndDocumentRecord());
	}

	public void startElement(String string, String string_12_,
			String string_13_, Attributes attributes) throws SAXException {
		queue.append(new StartElementRecord(string, string_12_, string_13_,
				attributes));
	}

	public void endElement(String string, String string_14_, String string_15_)
			throws SAXException {
		queue.append(new EndElementRecord(string, string_14_, string_15_));
	}

	public void processingInstruction(String string, String string_16_)
			throws SAXException {
		queue.append(new ProcessingInstructionRecord(string, string_16_));
	}

	public void characters(char[] cs, int i, int i_17_) throws SAXException {
		queue.append(new CharactersRecord(cs, i, i_17_));
	}

	public void ignorableWhitespace(char[] cs, int i, int i_18_)
			throws SAXException {
		queue.append(new IgnorableWhitespaceRecord(cs, i, i_18_));
	}

	public void startPrefixMapping(String string, String string_19_)
			throws SAXException {
		queue.append(new StartPrefixMappingRecord(string, string_19_));
	}

	public void endPrefixMapping(String string) throws SAXException {
		queue.append(new EndPrefixMappingRecord(string));
	}

	public void setDocumentLocator(Locator locator) {
		/* empty */
	}

	public void skippedEntity(String string) {
		/* empty */
	}

	public void play() throws SAXException {
		Enumeration enumeration = queue.elements();
		while (enumeration.hasMoreElements())
			((SAXRecord) enumeration.nextElement()).play();
	}

	public void clear() {
		queue.clear();
		cacheURI.clear();
		cacheLocalName.clear();
		cacheQName.clear();
		cacheAttrLocalName.clear();
		cacheAttrQName.clear();
		cacheAttrType.clear();
		cacheAttrValue.clear();
		cachePITarget.clear();
		cachePIData.clear();
	}

	public void finalize() throws Throwable {
		super.finalize();
		clear();
	}
}