/*
 * XMLReaderFactory - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.parsers.SAXParserFactory;

import com.renderx.util.URLSpec;
import com.renderx.util.User;

import org.xml.sax.EntityResolver;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.ParserAdapter;

public class XMLReaderFactory {
	private static final String DEFAULT_RESOLVER_CLASS_NAME = "none";

	static class DefaultEntityResolver implements EntityResolver {
		private EntityResolver parent = null;

		public DefaultEntityResolver() {
			this(null);
		}

		public DefaultEntityResolver(EntityResolver entityresolver) {
			parent = entityresolver;
		}

		public org.xml.sax.InputSource resolveEntity(String string,
				String string_0_) throws SAXException, IOException {
			if (parent != null) {
				try {
					org.xml.sax.InputSource inputsource = parent.resolveEntity(
							string, string_0_);
					if (inputsource != null)
						return inputsource;
				} catch (MalformedURLException malformedurlexception) {
					/* empty */
				}
			}
			Object object = null;
			URLSpec urlspec;
			try {
				urlspec = new URLSpec(string_0_);
			} catch (MalformedURLException malformedurlexception) {
				return null;
			}
			if (urlspec.scheme == 3)
				return null;
			return new InputSource(urlspec);
		}
	}

	public static final XMLReader createXMLReader() throws SAXException {
		Object object = null;
		XMLReader xmlreader;
		try {
			try {
				xmlreader = SAXParserFactory.newInstance().newSAXParser()
						.getXMLReader();
			} catch (Exception exception) {
				throw new SAXException("cannot create SAX2 parser: "
						+ exception);
			}
		} catch (Error error) {
			try {
				xmlreader = org.xml.sax.helpers.XMLReaderFactory
						.createXMLReader();
			} catch (Error error_1_) {
				xmlreader = new ParserAdapter();
			}
		}
		xmlreader.setFeature("http://xml.org/sax/features/namespaces", true);
		xmlreader.setFeature("http://xml.org/sax/features/namespace-prefixes",
				false);
		xmlreader.setEntityResolver(getDefaultResolver());
		return xmlreader;
	}

	public static EntityResolver getDefaultResolver() throws SAXException {
		String string = User.getProperty("com.renderx.sax.entityresolver");
		if (string == null)
			string = "none";
		EntityResolver entityresolver = null;
		if (!"none".equals(string)) {
			try {
				entityresolver = (EntityResolver) Class.forName(string)
						.newInstance();
			} catch (Exception exception) {
				throw new SAXException("Cannot create entity resolver "
						+ string + ": " + exception.toString());
			}
		}
		return new DefaultEntityResolver(entityresolver);
	}
}