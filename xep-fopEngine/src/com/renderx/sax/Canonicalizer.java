/*
 * Canonicalizer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.FileOutputStream;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class Canonicalizer extends DefaultHandler {
	private int level = 0;

	private int sigLevel = 0;

	private StringBuffer sigBuffer = null;

	private StringBuffer bodyBuffer = null;

	public String signature = null;

	public byte[] body = null;

	public void startDocument() {
		sigBuffer = new StringBuffer();
		bodyBuffer = new StringBuffer();
		signature = null;
		level = 0;
		sigLevel = 0;
	}

	public void endDocument() {
		body = new byte[bodyBuffer.length()];
		for (int i = 0; i < body.length; i++)
			body[i] = (byte) bodyBuffer.charAt(i);
	}

	public void startElement(String string, String string_0_, String string_1_,
			Attributes attributes) throws SAXException {
		level++;
		if (sigLevel > 0)
			sigLevel++;
		else if ("signature".equals(string_0_))
			sigLevel = 1;
		else {
			write("<");
			if (string != null && string.length() != 0)
				writeEscaped(string + ":");
			writeEscaped(string_0_);
			int i = attributes.getLength();
			int[] is = new int[i];
			for (int i_2_ = 0; i_2_ < i; i_2_++) {
				String string_3_ = attributes.getLocalName(i_2_);
				int i_4_;
				for (i_4_ = 0; i_4_ < i_2_
						&& attributes.getLocalName(is[i_4_]).compareTo(
								string_3_) < 0; i_4_++) {
					/* empty */
				}
				for (int i_5_ = i_2_; i_5_ > i_4_; i_5_--)
					is[i_5_] = is[i_5_ - 1];
				is[i_4_] = i_2_;
			}
			for (int i_6_ = 0; i_6_ < i; i_6_++) {
				int i_7_ = is[i_6_];
				String string_8_ = attributes.getURI(i_7_);
				write(" ");
				if (string_8_ != null && string_8_.length() > 0
						&& !string_8_.equals(string))
					writeEscaped(string_8_ + ":");
				writeEscaped(attributes.getLocalName(i_7_));
				write("=\"");
				writeEscaped(attributes.getValue(i_7_));
				write("\"");
			}
			write(">");
		}
	}

	public void endElement(String string, String string_9_, String string_10_)
			throws SAXException {
		level--;
		if (sigLevel > 0) {
			sigLevel--;
			if (sigLevel == 0)
				signature = sigBuffer.toString();
		} else
			write("</>");
	}

	public void processingInstruction(String string, String string_11_)
			throws SAXException {
		write("<?");
		writeEscaped(string);
		write(" ");
		writeEscaped(string_11_);
		write("?>");
	}

	public void characters(char[] cs, int i, int i_12_) throws SAXException {
		if (level != 0) {
			if (sigLevel > 0)
				sigBuffer.append(cs, i, i_12_);
			else
				writeEscaped(cs, i, i_12_);
		}
	}

	public void ignorableWhitespace(char[] cs, int i, int i_13_)
			throws SAXException {
		characters(cs, i, i_13_);
	}

	private void writeEscaped(String string) {
		writeEscaped(string.toCharArray(), 0, string.length());
	}

	private void writeEscaped(char[] cs, int i, int i_14_) {
		for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
			char c = cs[i + i_15_];
			switch (c) {
			case '<':
				write("&lt;");
				break;
			case '>':
				write("&gt;");
				break;
			case '&':
				write("&amp;");
				break;
			case '\"':
				write("&quot;");
				break;
			case '\'':
				write("&apos;");
				break;
			default:
				if (c > '\u007f' || c < ' ')
					write("&#" + (int) c + ";");
				else
					write(c);
			}
		}
	}

	private void write(String string) {
		bodyBuffer.append(string);
	}

	private void write(char c) {
		bodyBuffer.append(c);
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length != 2) {
			System.err
					.println("Usage: java com.renderx.crypto.SignedXMLBuffer <XML file> <output file>");
			System.exit(1);
		}
		XMLReader xmlreader = XMLReaderFactory.createXMLReader();
		Canonicalizer canonicalizer = new Canonicalizer();
		xmlreader.setContentHandler(canonicalizer);
		xmlreader.parse(new org.xml.sax.InputSource(strings[0]));
		FileOutputStream fileoutputstream = new FileOutputStream(strings[1]);
		fileoutputstream.write(canonicalizer.body);
		fileoutputstream.close();
		System.out.println("Signature: " + canonicalizer.signature);
	}
}