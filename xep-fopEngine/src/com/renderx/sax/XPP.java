/*
 * XPP - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.IOException;

import com.jclark.xsl.sax.XSLProcessorImpl;

import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XPP {
	protected XSLProcessorImpl xt = new XSLProcessorImpl();

	public XPP(XMLReader xmlreader, org.xml.sax.InputSource inputsource)
			throws IOException, SAXException {
		setXMLReader(xmlreader);
		xt.loadStylesheet(inputsource);
	}

	public void setErrorHandler(ErrorHandler errorhandler) {
		xt.setErrorHandler(errorhandler);
	}

	public void parse(org.xml.sax.InputSource inputsource,
			ContentHandler contenthandler) throws IOException, SAXException {
		xt.setDocumentHandler(new HandlerAdapter(contenthandler));
		xt.parse(inputsource);
	}

	public void setXMLReader(XMLReader xmlreader) throws SAXException {
		xt.setParser(new XMLReaderAdapter(xmlreader));
	}
}