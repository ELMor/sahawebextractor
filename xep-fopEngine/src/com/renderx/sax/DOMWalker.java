/*
 * DOMWalker - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class DOMWalker extends XMLPlayer {
	private Node docroot = null;

	public DOMWalker(Node node) {
		docroot = node;
	}

	public DOMWalker(Document document) {
		docroot = document.getDocumentElement();
	}

	public void play() throws SAXException {
		handler.startDocument();
		if (docroot != null) {
			Document document = docroot.getOwnerDocument();
			if (document == null)
				throw new SAXException(
						"DOM element does not have an owner document: cannot determine DOM Level");
			DOMImplementation domimplementation = document.getImplementation();
			if (domimplementation.hasFeature("XML", "2.0"))
				traverseNS(docroot);
			else
				throw new SAXException("DOM Level 1 traversal unsupported");
		}
		handler.endDocument();
	}

	private void traverseNS(Node node) throws SAXException {
		if (node != null) {
			switch (node.getNodeType()) {
			case 1: {
				Object object = null;
				Object object_0_ = null;
				Object object_1_ = null;
				NamedNodeMap namednodemap = node.getAttributes();
				AttributesImpl attributesimpl = new AttributesImpl();
				if (namednodemap != null) {
					for (int i = 0; i < namednodemap.getLength(); i++) {
						Attr attr = (Attr) namednodemap.item(i);
						String string = attr.getName();
						if (!string.startsWith("xmlns:")
								&& !string.equals("xmlns")) {
							String string_2_ = attr.getLocalName();
							if (string_2_ == null)
								string_2_ = string;
							String string_3_ = attr.getNamespaceURI();
							if (string_3_ == null)
								string_3_ = "";
							attributesimpl.addAttribute(string_3_, string_2_,
									string, "CDATA", attr.getValue());
						}
					}
				}
				Element element = (Element) node;
				String string = element.getLocalName();
				String string_4_ = element.getTagName();
				if (string == null)
					string = string_4_;
				String string_5_ = element.getNamespaceURI();
				if (string_5_ == null)
					string_5_ = "";
				handler.startElement(string_5_, string, string_4_,
						attributesimpl);
				NodeList nodelist = node.getChildNodes();
				if (nodelist != null) {
					for (int i = 0; i < nodelist.getLength(); i++)
						traverseNS(nodelist.item(i));
				}
				handler.endElement(string_5_, string, string_4_);
				break;
			}
			case 3:
			case 4: {
				String string = node.getNodeValue();
				if (string != null)
					handler
							.characters(string.toCharArray(), 0, string
									.length());
				break;
			}
			case 7: {
				ProcessingInstruction processinginstruction = (ProcessingInstruction) node;
				handler.processingInstruction(
						processinginstruction.getTarget(),
						processinginstruction.getData());
				break;
			}
			}
		}
	}
}