/*
 * TeXHyphenatorFactory - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.hyphen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.StringTokenizer;

import com.renderx.sax.ConfigHandler;
import com.renderx.sax.ConfigHandlerAdapter;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class TeXHyphenatorFactory implements HyphenatorFactory {
	private String configFileName = null;

	private final Hashtable langtab;

	private final ErrorHandler errHandler;

	private final Hyphenator fallbackHyphenator;

	private class Config implements ConfigHandler {
		private static final String NSURI = "http://www.renderx.com/XEP/config";

		private int skip = 0;

		private Config() {
			/* empty */
		}

		public void startDocument(Hashtable hashtable) {
			configFileName = (String) hashtable.get("document");
		}

		public void endDocument(Hashtable hashtable) throws SAXException {
			/* empty */
		}

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes, Hashtable hashtable) {
			if (skip > 0)
				skip++;
			else if ("".equals(string)
					|| "http://www.renderx.com/XEP/config".equals(string)) {
				if (string_0_.equals("language")) {
					String string_2_ = getRequiredValue(string_0_, attributes,
							"codes");
					if (string_2_ != null) {
						hashtable.put("language-codes", string_2_);
						String string_3_ = attributes.getValue("name");
						if (string_3_ != null)
							hashtable.put("language", string_3_);
					}
				} else if (string_0_.equals("hyphenation")) {
					String string_4_ = (String) hashtable.get("language-codes");
					if (string_4_ == null) {
						printError("'hyphenation' element is not a child of 'language' element; element ignored");
						skip = 1;
					} else {
						String string_5_ = (String) hashtable.get("language");
						String string_6_ = getRequiredValue(string_0_,
								attributes, "pattern");
						if (string_6_ != null) {
							Object object = null;
							URLSpec urlspec;
							try {
								urlspec = new URLSpec(((URLSpec) hashtable
										.get("base")), string_6_);
							} catch (MalformedURLException malformedurlexception) {
								printException(
										("Invalid URL to hyphenation pattern for language " + string_5_),
										malformedurlexception);
								skip = 1;
								return;
							}
							String string_7_ = attributes.getValue("encoding");
							StringTokenizer stringtokenizer = new StringTokenizer(
									string_4_, " \t\r\n,");
							if (!stringtokenizer.hasMoreTokens()) {
								printError("Empty list of codes for language "
										+ string_5_);
								skip = 1;
							} else {
								HyphenatorRecord hyphenatorrecord = new HyphenatorRecord(
										string_5_, urlspec, string_7_);
								while (stringtokenizer.hasMoreTokens())
									langtab.put(stringtokenizer.nextToken()
											.toLowerCase(), hyphenatorrecord);
							}
						}
					}
				}
			}
		}

		public void endElement(String string, String string_8_,
				String string_9_, Hashtable hashtable) {
			if (skip > 0)
				skip--;
			else if (!"".equals(string)
					&& !"http://www.renderx.com/XEP/config".equals(string)) {
				/* empty */
			}
		}

		public void characters(char[] cs, int i, int i_10_, Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		public void processingInstruction(String string, String string_11_,
				Hashtable hashtable) {
			if (skip > 0) {
				/* empty */
			}
		}

		private final String getRequiredValue(String string,
				Attributes attributes, String string_12_) {
			String string_13_ = attributes.getValue(string_12_);
			if (string_13_ == null) {
				printError("Mandatory attribute '" + string_12_
						+ "' is missing on element '" + string
						+ "'; element skipped");
				skip = 1;
			}
			return string_13_;
		}
	}

	class HyphenatorRecord {
		final String language;

		final URLSpec table;

		final String encoding;

		Hyphenator hyphenator = null;

		HyphenatorRecord(String string, URLSpec urlspec, String string_14_) {
			language = string;
			table = urlspec;
			encoding = string_14_;
		}

		synchronized Hyphenator getHyphenator() {
			if (hyphenator != null)
				return hyphenator;
			try {
				InputStream inputstream = table.openStream();
				try {
					BufferedReader bufferedreader = (new BufferedReader(
							new InputStreamReader(
									inputstream,
									(encoding == null ? "ISO-8859-1" : encoding))));
					hyphenator = new TeXHyphenator(bufferedreader, errHandler);
				} catch (UnsupportedEncodingException unsupportedencodingexception) {
					printError("Unsupported encoding: " + encoding);
					hyphenator = fallbackHyphenator;
				} finally {
					inputstream.close();
				}
			} catch (Exception exception) {
				printException(
						("Cannot initialize hyphenator for language " + language),
						exception);
				hyphenator = fallbackHyphenator;
			}
			return hyphenator;
		}
	}

	public TeXHyphenatorFactory(URLSpec urlspec, ErrorHandler errorhandler)
			throws IOException, SAXException {
		langtab = new Hashtable();
		fallbackHyphenator = new DumbHyphenator();
		errHandler = errorhandler;
		XMLReader xmlreader = XMLReaderFactory.createXMLReader();
		xmlreader.setContentHandler(new ConfigHandlerAdapter(new Config(),
				urlspec));
		xmlreader.parse(new InputSource(urlspec));
	}

	public TeXHyphenatorFactory(org.xml.sax.InputSource inputsource,
			XMLReader xmlreader, ErrorHandler errorhandler) throws IOException,
			SAXException {
		langtab = new Hashtable();
		fallbackHyphenator = new DumbHyphenator();
		errHandler = errorhandler;
		xmlreader.setContentHandler(new ConfigHandlerAdapter(new Config(),
				new URLSpec(inputsource.getSystemId())));
		xmlreader.parse(inputsource);
	}

	public Hyphenator getHyphenator(String string) {
		if (string == null)
			return fallbackHyphenator;
		string = string.toLowerCase().trim();
		HyphenatorRecord hyphenatorrecord = (HyphenatorRecord) langtab
				.get(string);
		if (hyphenatorrecord == null) {
			int i = string.indexOf('-');
			if (i != -1) {
				string = string.substring(0, i).trim();
				hyphenatorrecord = (HyphenatorRecord) langtab.get(string);
			}
		}
		return (hyphenatorrecord == null ? fallbackHyphenator
				: hyphenatorrecord.getHyphenator());
	}

	public void printWarning(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.warning(configFileName + ": " + string);
			}
		}
	}

	public void printError(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.error(configFileName + ": " + string);
			}
		}
	}

	public void printException(String string, Exception exception) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.exception(configFileName + ": " + string, exception);
			}
		}
	}
}