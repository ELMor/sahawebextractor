/*
 * DumbHyphenator - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.hyphen;

public class DumbHyphenator implements Hyphenator {
	public String hyphenate(String string, int i, int i_0_) {
		if (i < 1)
			i = 1;
		if (i_0_ < 1)
			i_0_ = 1;
		if (string.length() < i_0_ + i)
			return string;
		if (string.indexOf('-', i) == -1 && string.indexOf('\u2010', i) == -1)
			return string;
		StringBuffer stringbuffer = new StringBuffer(string.length());
		int i_1_ = string.length() - i_0_;
		for (int i_2_ = 0; i_2_ < string.length(); i_2_++) {
			char c = string.charAt(i_2_);
			stringbuffer.append(c);
			if (i_2_ > i && i_2_ < i_1_ && (c == '-' || c == '\u2010'))
				stringbuffer.append('\u200b');
		}
		return stringbuffer.toString();
	}
}