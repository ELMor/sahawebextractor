/*
 * HyphenatorFactory - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.hyphen;

public interface HyphenatorFactory {
	public Hyphenator getHyphenator(String string);
}