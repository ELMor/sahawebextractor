/*
 * EPSImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.OutputStream;
import java.util.StringTokenizer;

import com.renderx.util.SeekableInput;

public class EPSImage extends VectorImage {
	private boolean hasBinaryHeader = false;

	public long psOffset = 0L;

	public long psLength = 0L;

	public long wmfOffset = 0L;

	public long wmfLength = 0L;

	public long tiffOffset = 0L;

	public long tiffLength = 0L;

	public double[] boundingBox = null;

	public double[] hiResBoundingBox = null;

	public static class EPSIPreview extends BitmapImage {
		public long rasterOffset = 0L;

		public int numLines = 0;

		public EPSIPreview() {
			canCopyData = false;
			canExpandData = true;
			colorSpace = (short) 5;
		}

		public void parse() {
			/* empty */
		}

		public void copyData(OutputStream outputstream)
				throws ImageFormatException {
			throw new ImageFormatException("EPSI data cannot be copied");
		}

		public void expandData(OutputStream outputstream) throws IOException,
				ImageFormatException {
			SeekableInput seekableinput = this.openSeekableImageStream();
			try {
				seekableinput.seek(rasterOffset);
				int i = (pxWidth * bitsPerComponent + 7) / 8;
				byte[] is = new byte[pxHeight * i];
				int i_0_ = 0;
				while_2_: for (int i_1_ = 0; i_1_ < numLines; i_1_++) {
					String string = seekableinput.readLine();
					if (string.length() == 0)
						throw new ImageFormatException("EPS image "
								+ this.toDisplayString()
								+ ": empty line in EPSI preview section");
					if (string.charAt(0) != '%')
						throw new ImageFormatException(
								"EPS image "
										+ this.toDisplayString()
										+ ": line in EPSI preview section does not start with '%'");
					for (int i_2_ = 1; i_2_ < string.length() - 1; i_2_ += 2) {
						int i_3_ = "0123456789ABCDEFabcdef".indexOf(string
								.charAt(i_2_));
						int i_4_ = "0123456789ABCDEFabcdef".indexOf(string
								.charAt(i_2_ + 1));
						if (i_3_ > 15)
							i_3_ -= 6;
						if (i_4_ > 15)
							i_4_ -= 6;
						if (i_3_ == -1 || i_4_ == -1)
							break;
						is[i_0_++] = (byte) ((i_3_ << 4) + i_4_);
						if (i_0_ == is.length)
							break while_2_;
					}
				}
				int i_5_ = pxHeight;
				while (i_5_-- > 0)
					outputstream.write(is, i_5_ * i, i);
			} finally {
				seekableinput.close();
			}
		}
	}

	public EPSImage() {
		mimetype = "application/postscript";
	}

	public void parse() throws IOException, ImageFormatException {
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			hasBinaryHeader = (seekableinput.readUnsignedByte() == 197
					&& seekableinput.readUnsignedByte() == 208
					&& seekableinput.readUnsignedByte() == 211 && seekableinput
					.readUnsignedByte() == 198);
			if (hasBinaryHeader) {
				psOffset = readUnsignedInt(seekableinput);
				psLength = readUnsignedInt(seekableinput);
				wmfOffset = readUnsignedInt(seekableinput);
				wmfLength = readUnsignedInt(seekableinput);
				tiffOffset = readUnsignedInt(seekableinput);
				tiffLength = readUnsignedInt(seekableinput);
				if (tiffOffset > 0L)
					hasBitmapPreview = true;
			} else {
				psOffset = 0L;
				psLength = seekableinput.length();
			}
			seekableinput.seek(psOffset);
			String string = seekableinput.readLine();
			if (!string.startsWith("%!PS-Adobe"))
				throw new ImageFormatException("invalid EPSF file "
						+ this.toDisplayString()
						+ ": PostScript header comment not found");
			while ((string = seekableinput.readLine()) != null) {
				StringTokenizer stringtokenizer = new StringTokenizer(string);
				if (stringtokenizer.hasMoreTokens()) {
					if (!string.startsWith("%"))
						break;
					String string_6_ = stringtokenizer.nextToken();
					if (string_6_.equals("%%EndComments")
							|| string_6_.equals("%"))
						break;
					if (string_6_.equals("%%BoundingBox:")) {
						boundingBox = new double[4];
						boundingBox[0] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						boundingBox[1] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						boundingBox[2] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						boundingBox[3] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
					} else if (string_6_.equals("%%HiResBoundingBox:")) {
						hiResBoundingBox = new double[4];
						hiResBoundingBox[0] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						hiResBoundingBox[1] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						hiResBoundingBox[2] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
						hiResBoundingBox[3] = Double.valueOf(
								stringtokenizer.nextToken()).doubleValue();
					}
				}
			}
			if (hiResBoundingBox != null) {
				width = hiResBoundingBox[2] - hiResBoundingBox[0];
				height = hiResBoundingBox[3] - hiResBoundingBox[1];
			} else if (boundingBox != null) {
				width = boundingBox[2] - boundingBox[0];
				height = boundingBox[3] - boundingBox[1];
			} else
				throw new ImageFormatException("invalid EPSF file "
						+ this.toDisplayString()
						+ ": '%%BoundingBox:' DSC comment not found");
			if (!hasBitmapPreview) {
				seekableinput.seek(psOffset);
				while ((string = seekableinput.readLine()) != null) {
					if (string.startsWith("%%BeginPreview:")) {
						hasBitmapPreview = true;
						break;
					}
				}
			}
		} finally {
			seekableinput.close();
		}
	}

	private long readUnsignedInt(SeekableInput seekableinput)
			throws IOException {
		byte[] is = new byte[4];
		seekableinput.readFully(is);
		long l = 0L;
		for (int i = 3; i >= 0; i--) {
			l <<= 8;
			l += (long) (0xff & is[i]);
		}
		return l;
	}

	public BitmapImage getBitmapPreview() throws ImageFormatException,
			IOException {
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			if (tiffOffset > 0L) {
				TIFFImage tiffimage = new TIFFImage(factory.makeImage(source),
						tiffOffset);
				tiffimage.parse();
				tiffimage.horzResolution *= tiffimage.width / width;
				tiffimage.vertResolution *= tiffimage.height / height;
				tiffimage.width = width;
				tiffimage.height = height;
				return tiffimage;
			}
			seekableinput.seek(psOffset);
			Object object = null;
			String string;
			while ((string = seekableinput.readLine()) != null) {
				if (string.startsWith("%%BeginPreview:")) {
					StringTokenizer stringtokenizer = new StringTokenizer(
							string);
					stringtokenizer.nextToken();
					EPSIPreview epsipreview = new EPSIPreview();
					epsipreview.pxWidth = new Integer(stringtokenizer
							.nextToken()).intValue();
					epsipreview.pxHeight = new Integer(stringtokenizer
							.nextToken()).intValue();
					epsipreview.bitsPerComponent = new Integer(stringtokenizer
							.nextToken()).intValue();
					epsipreview.numLines = new Integer(stringtokenizer
							.nextToken()).intValue();
					epsipreview.rasterOffset = seekableinput.getFilePointer();
					epsipreview.source = source;
					epsipreview.width = width;
					epsipreview.height = height;
					epsipreview.horzResolution = ((double) (epsipreview.pxWidth * 72) / epsipreview.width);
					epsipreview.vertResolution = ((double) (epsipreview.pxHeight * 72) / epsipreview.height);
					return epsipreview;
				}
			}
		} catch (UnregisteredMIMETypeException unregisteredmimetypeexception) {
			/* empty */
		} finally {
			seekableinput.close();
		}
		return null;
	}
}