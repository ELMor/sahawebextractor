/*
 * XMLVectorImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import com.renderx.graphics.vector.ImageTree;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.URLSpec;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public abstract class XMLVectorImage extends VectorImage {
	public abstract void parse(XMLReader xmlreader,
			org.xml.sax.InputSource inputsource) throws IOException,
			ImageFormatException, SAXException;

	public final void parse() throws IOException, ImageFormatException {
		try {
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			InputStream inputstream = this.openImageStream();
			try {
				InputSource inputsource = new InputSource(inputstream);
				inputsource.setSystemId(getSourceId());
				parse(xmlreader, inputsource);
			} finally {
				inputstream.close();
			}
		} catch (SAXException saxexception) {
			throw new ImageFormatException(saxexception.toString(), mimetype);
		}
	}

	public ImageTree getImageTree(XMLReader xmlreader,
			org.xml.sax.InputSource inputsource) throws IOException,
			ImageFormatException, SAXException {
		throw new ImageFormatException(
				("Vector rendering of " + mimetype + " not supported"),
				mimetype);
	}

	public final ImageTree getImageTree() throws IOException,
			ImageFormatException {
		try {
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			Object object = null;
			InputStream inputstream = this.openImageStream();
			ImageTree imagetree;
			try {
				InputSource inputsource = new InputSource(inputstream);
				inputsource.setSystemId(getSourceId());
				imagetree = getImageTree(xmlreader, inputsource);
			} finally {
				inputstream.close();
			}
			return imagetree;
		} catch (SAXException saxexception) {
			saxexception.printStackTrace();
			throw new ImageFormatException(saxexception.toString(), mimetype);
		}
	}

	private final String getSourceId() throws MalformedURLException {
		switch (source.scheme) {
		case 2: {
			String string = source.resource;
			int i = string.lastIndexOf('/');
			if (i != -1)
				string = string.substring(i + 1);
			return new URLSpec(string).toString();
		}
		case 1: {
			URLSpec urlspec = source.getBase();
			if (urlspec != null)
				return urlspec.toString() + ".data";
			return source.toString();
		}
		case 3:
			return source.toString();
		default:
			return null;
		}
	}
}