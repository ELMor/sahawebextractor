/*
 * InvalidImageClassException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.graphics;

public class InvalidImageClassException extends Exception
{
    public Class imgclass = null;
    
    public InvalidImageClassException() {
	/* empty */
    }
    
    public InvalidImageClassException(String string) {
	super(string);
    }
    
    public InvalidImageClassException(Class var_class) {
	this(Image.class + " is not a superclass of " + var_class, var_class);
    }
    
    public InvalidImageClassException(String string, Class var_class) {
	super(string);
	imgclass = var_class;
    }
}
