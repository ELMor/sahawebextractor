/*
 * GIFImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

import com.renderx.util.LZWDecoderStream;
import com.renderx.util.SeekableInput;

public class GIFImage extends BitmapImage {
	private long dataOffset = -1L;

	private int minimumCodeSize = -1;

	private boolean interlaced = false;

	public GIFImage() {
		mimetype = "image/gif";
	}

	public void parse() throws IOException, ImageFormatException {
		colorSpace = (short) 6;
		compressionMethod = (short) 7;
		canCopyData = false;
		canExpandData = true;
		usesBigEndian = false;
		bitsPerComponent = 8;
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(0L);
			byte[] is = new byte[6];
			seekableinput.readFully(is);
			if (is[0] != 71 || is[1] != 73 || is[2] != 70)
				throw new ImageFormatException(
						"Invalid GIF signature in file '"
								+ this.toDisplayString() + "'");
			if (is[3] != 56 || is[4] != 55 && is[4] != 57 || is[5] != 97)
				throw new ImageFormatException("Unknown GIF version in file '"
						+ this.toDisplayString() + "'");
			colorTable = null;
			seekableinput.skipBytes(4);
			int i = seekableinput.readUnsignedByte();
			seekableinput.skipBytes(1);
			int i_0_ = seekableinput.readUnsignedByte();
			if (i_0_ != 0) {
				double d = Math.sqrt(((double) i_0_ + 15.0) / 64.0);
				horzResolution /= d;
				vertResolution *= d;
			}
			if ((i & 0x80) != 0) {
				int i_1_ = 2 << (i & 0x7);
				colorTable = new byte[3 * i_1_];
				seekableinput.readFully(colorTable);
			}
			while_3_: do {
				int i_2_;
				for (;;) {
					i_2_ = seekableinput.readUnsignedByte();
					if (i_2_ == 59)
						throw new ImageFormatException(
								"No raster data found in GIF image '"
										+ this.toDisplayString() + "'");
					if (i_2_ == 44) {
						seekableinput.skipBytes(4);
						pxWidth = readUnsignedShortLE(seekableinput);
						pxHeight = readUnsignedShortLE(seekableinput);
						this.setDimensions();
						i = seekableinput.readUnsignedByte();
						if ((i & 0x40) != 0)
							interlaced = true;
						if ((i & 0x80) != 0) {
							int i_3_ = 2 << (i & 0x7);
							colorTable = new byte[3 * i_3_];
							seekableinput.readFully(colorTable);
						}
						if (colorTable == null)
							throw new ImageFormatException(
									"Invalid GIF image '"
											+ this.toDisplayString()
											+ "': color table not found");
						minimumCodeSize = seekableinput.readUnsignedByte();
						if (minimumCodeSize < 2 || minimumCodeSize > 12)
							throw new ImageFormatException(
									"GIF image '"
											+ this.toDisplayString()
											+ "' has incorrect value for initial LZW code: "
											+ minimumCodeSize);
						dataOffset = seekableinput.getFilePointer();
						break while_3_;
					}
					if (i_2_ != 33)
						break;
					int i_4_ = seekableinput.readUnsignedByte();
					if (i_4_ == 249) {
						seekableinput.read();
						i = seekableinput.readUnsignedByte();
						seekableinput.skipBytes(2);
						if ((i & 0x1) != 0) {
							transparentColor = new byte[1];
							seekableinput.readFully(transparentColor);
						} else
							seekableinput.skipBytes(1);
						seekableinput.skipBytes(1);
					} else if (i_4_ == 1) {
						seekableinput.skipBytes(13);
						skipBlocks(seekableinput);
					} else if (i_4_ == 254)
						skipBlocks(seekableinput);
					else if (i_4_ == 255) {
						seekableinput.skipBytes(12);
						skipBlocks(seekableinput);
					} else
						skipBlocks(seekableinput);
				}
				throw new ImageFormatException("Unexpected block type: " + i_2_);
			} while (false);
		} finally {
			seekableinput.close();
		}
	}

	public void copyData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		throw new ImageFormatException(
				"Cannot copy LZW compressed data in GIF image. LZW stream should be uncompressed.");
	}

	public void expandData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		Object object = null;
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(dataOffset);
			LZWDecoderStream lzwdecoderstream;
			if (interlaced)
				lzwdecoderstream = new LZWDecoderStream(bytearrayoutputstream,
						minimumCodeSize, true, false, -1);
			else
				lzwdecoderstream = new LZWDecoderStream(outputstream,
						minimumCodeSize, true, false, -1);
			byte[] is = new byte[260];
			boolean bool = false;
			for (;;) {
				int i = seekableinput.readUnsignedByte();
				if (i == 0)
					break;
				seekableinput.readFully(is, 0, i);
				try {
					lzwdecoderstream.write(is, 0, i);
				} catch (IOException ioexception) {
					throw new IOException("GIF image '"
							+ this.toDisplayString() + "': "
							+ ioexception.getMessage());
				}
			}
			lzwdecoderstream.flush();
			if (interlaced) {
				byte[] is_5_ = bytearrayoutputstream.toByteArray();
				int[] is_6_ = new int[pxHeight];
				if (is_5_.length != pxHeight * pxWidth)
					throw new ImageFormatException("Number of pixels is: "
							+ is_5_.length + " while image dimensions are: "
							+ pxHeight + "x" + pxWidth);
				int[] is_7_ = new int[4];
				is_7_[0] = (int) Math.ceil((double) pxHeight / 8.0);
				is_7_[1] = (int) Math.ceil(((double) pxHeight - 4.0) / 8.0);
				is_7_[2] = (int) Math.ceil(((double) pxHeight - 2.0) / 4.0);
				is_7_[3] = (int) Math.ceil(((double) pxHeight - 1.0) / 2.0);
				int i = 0;
				for (int i_8_ = 0; i_8_ < 4; i_8_++) {
					int i_9_;
					for (i_9_ = 0; i_9_ < is_7_[i_8_]; i_9_++) {
						switch (i_8_) {
						case 3:
							is_6_[1 + i_9_ * 2] = i + i_9_;
							break;
						case 2:
							is_6_[2 + i_9_ * 4] = i + i_9_;
							break;
						case 1:
							is_6_[4 + i_9_ * 8] = i + i_9_;
							break;
						case 0:
							is_6_[0 + i_9_ * 8] = i + i_9_;
							break;
						}
					}
					i += i_9_;
				}
				for (int i_10_ = 0; i_10_ < pxHeight; i_10_++)
					outputstream.write(is_5_, is_6_[i_10_] * pxWidth, pxWidth);
				outputstream.flush();
			}
		} finally {
			seekableinput.close();
		}
	}

	private final int readUnsignedShortLE(SeekableInput seekableinput)
			throws IOException {
		int i = seekableinput.read();
		int i_11_ = seekableinput.read();
		if ((i | i_11_) < 0)
			throw new EOFException();
		return (i_11_ << 8) + (i << 0);
	}

	private int skipBlocks(SeekableInput seekableinput) throws IOException {
		int i = 0;
		for (;;) {
			int i_12_ = seekableinput.readUnsignedByte();
			if (i_12_ == 0)
				break;
			i += i_12_;
			seekableinput.skipBytes(i_12_);
		}
		return i;
	}
}