/*
 * JPEGImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.renderx.util.SeekableInput;

public class JPEGImage extends BitmapImage {
	public JPEGImage() {
		mimetype = "image/jpeg";
	}

	public void parse() throws IOException, ImageFormatException {
		compressionMethod = (short) 3;
		bitsPerComponent = 8;
		usesBigEndian = false;
		canCopyData = true;
		canExpandData = false;
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			boolean bool = false;
			seekableinput.seek(2L);
			int i = seekableinput.readUnsignedByte();
			int i_0_ = 0;
			while (i != 218) {
				for (/**/; i != 255; i = seekableinput.readUnsignedByte()) {
					/* empty */
				}
				for (/**/; i == 255; i = seekableinput.readUnsignedByte()) {
					/* empty */
				}
				int i_1_ = seekableinput.readUnsignedShort() - 2;
				if (i >= 192 && i <= 195) {
					seekableinput.skipBytes(1);
					int i_2_ = seekableinput.readUnsignedShort();
					int i_3_ = seekableinput.readUnsignedShort();
					int i_4_ = seekableinput.readUnsignedByte();
					if (pxWidth < i_3_)
						pxWidth = i_3_;
					if (pxHeight < i_2_)
						pxHeight = i_2_;
					if (i_0_ < i_4_)
						i_0_ = i_4_;
					seekableinput.skipBytes(i_1_ - 6);
				} else if (i == 224) {
					seekableinput.skipBytes(7);
					int i_5_ = seekableinput.readUnsignedByte();
					double d = (double) seekableinput.readUnsignedShort();
					double d_6_ = (double) seekableinput.readUnsignedShort();
					switch (i_5_) {
					case 1:
						horzResolution = d;
						vertResolution = d_6_;
						break;
					case 2:
						horzResolution = 2.54 * d;
						vertResolution = 2.54 * d_6_;
						break;
					case 0: {
						double d_7_ = Math.sqrt(d_6_ / d);
						horzResolution /= d_7_;
						vertResolution *= d_7_;
						break;
					}
					default:
						throw new ImageFormatException(("Invalid unit type ("
								+ i_5_ + ") in JPEG image: " + this
								.toDisplayString()), mimetype);
					}
					seekableinput.skipBytes(i_1_ - 12);
				} else if (i == 238) {
					if (i_1_ >= 5) {
						i_1_ -= 5;
						byte[] is = new byte[5];
						seekableinput.readFully(is);
						bool = (is[0] == 65 && is[1] == 100 && is[2] == 111
								&& is[3] == 98 && is[4] == 101);
					}
					if (i_1_ > 0)
						seekableinput.skipBytes(i_1_);
				} else
					seekableinput.skipBytes(i_1_);
			}
			switch (i_0_) {
			case 1:
				colorSpace = (short) 4;
				break;
			case 3:
				colorSpace = (short) 1;
				break;
			case 4:
				colorSpace = bool ? (short) 3 : (short) 2;
				break;
			default:
				throw new ImageFormatException(
						("Invalid number of color components (" + i_0_
								+ ") in JPEG image " + this.toDisplayString()),
						mimetype);
			}
		} catch (EOFException eofexception) {
			throw new ImageFormatException(
					("Premature end of file or corrupt data in JPEG image " + this
							.toDisplayString()), mimetype);
		} finally {
			seekableinput.close();
		}
		this.setDimensions();
	}

	public void copyData(OutputStream outputstream) throws IOException {
		InputStream inputstream = this.openImageStream();
		try {
			byte[] is = new byte[8192];
			int i;
			while ((i = inputstream.read(is)) != -1)
				outputstream.write(is, 0, i);
		} finally {
			inputstream.close();
		}
	}

	public void expandData(OutputStream outputstream)
			throws ImageFormatException {
		throw new ImageFormatException("JPEG image '" + this.toDisplayString()
				+ "' cannot be expanded: not yet implemented");
	}
}