/*
 * UnregisteredMIMETypeException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.graphics;

public class UnregisteredMIMETypeException extends Exception {
	public String mimetype = "";

	public UnregisteredMIMETypeException(String string) {
		this(string, "MIME type '" + string + "' is not registered");
	}

	public UnregisteredMIMETypeException(String string, String string_0_) {
		super(string_0_);
		mimetype = string;
	}
}