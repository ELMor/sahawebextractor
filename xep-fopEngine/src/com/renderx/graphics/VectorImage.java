/*
 * VectorImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;

import com.renderx.graphics.vector.ImageTree;

public abstract class VectorImage extends Image {
	public boolean supportsVectorRendering = false;

	public boolean hasBitmapPreview = false;

	public boolean supportsRasterizing = false;

	public abstract void parse() throws IOException, ImageFormatException;

	public BitmapImage getBitmapPreview() throws IOException,
			ImageFormatException {
		throw new ImageFormatException(
				("Bitmap preview not available for " + mimetype), mimetype);
	}

	public BitmapImage rasterize(double d) throws IOException,
			ImageFormatException {
		throw new ImageFormatException(
				("Rasterization of " + mimetype + " not supported"), mimetype);
	}

	public ImageTree getImageTree() throws IOException, ImageFormatException {
		throw new ImageFormatException(
				("Vector rendering of " + mimetype + " not supported"),
				mimetype);
	}
}