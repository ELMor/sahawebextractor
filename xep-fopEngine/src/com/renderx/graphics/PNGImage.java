/*
 * PNGImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import com.renderx.util.SeekableInput;
import com.renderx.util.URLSpec;

public class PNGImage extends BitmapImage {
	private long dataOffset = -1L;

	private boolean interlaced = false;

	private int bitDepth = 0;

	private int bitsPerPixel = 0;

	private int colorType = 0;

	private static final double SRGB_GAMMA = 2.2;

	private static final double GAMMA_TOLERANCE = 0.05;

	private double gammaExponent = 1.0;

	boolean needsGammaCorrection = false;

	private static final int PNG_COLOR_GRAY = 0;

	private static final int PNG_COLOR_RGB = 2;

	private static final int PNG_COLOR_PALETTE = 3;

	private static final int PNG_COLOR_GRAY_ALPHA = 4;

	private static final int PNG_COLOR_RGB_ALPHA = 6;

	private static final String[] colorTypeNames = { "Grayscale", "Error",
			"Truecolor", "Index", "Grayscale with alpha", "Error",
			"Truecolor with alpha" };

	private static final int PNG_FILTER_NONE = 0;

	private static final int PNG_FILTER_SUB = 1;

	private static final int PNG_FILTER_UP = 2;

	private static final int PNG_FILTER_AVERAGE = 3;

	private static final int PNG_FILTER_PAETH = 4;

	private static final String[] filterTypeNames = { "None", "Sub", "Up",
			"Average", "Paeth" };

	class PNGInterlacedDecoder extends PNGDecoder {
		int pass = 0;

		int lineno = 0;

		byte[][] accumulator = null;

		int pixelsPerByte = 1;

		int mask = 0;

		int[] pixelMasks = { 0, 128, 192, 224, 240, 248, 252, 254, 255 };

		int[] size_x = new int[7];

		int[] size_y = new int[7];

		int[] offset_x = { 0, 4, 0, 2, 0, 1 };

		int[] offset_y = { 0, 0, 2, 0, 1, 0 };

		int[] step_x = { 8, 8, 4, 4, 2, 2 };

		int[] step_y = { 4, 4, 4, 2, 2, 1 };

		public PNGInterlacedDecoder(OutputStream outputstream) {
			super(outputstream);
			pixelsPerByte = 8 / bitsPerComponent;
			mask = pixelMasks[bitsPerComponent];
			size_x[0] = (pxWidth + 7) / 8;
			size_y[0] = (pxHeight + 7) / 8;
			size_x[1] = (pxWidth + 3) / 8;
			size_y[1] = (pxHeight + 7) / 8;
			size_x[2] = (pxWidth + 3) / 4;
			size_y[2] = (pxHeight + 3) / 8;
			size_x[3] = (pxWidth + 1) / 4;
			size_y[3] = (pxHeight + 3) / 4;
			size_x[4] = (pxWidth + 1) / 2;
			size_y[4] = (pxHeight + 1) / 4;
			size_x[5] = pxWidth / 2;
			size_y[5] = (pxHeight + 1) / 2;
			size_x[6] = pxWidth;
			size_y[6] = pxHeight / 2;
			accumulator = (new byte[(pxHeight + 1) / 2][(pxWidth * bitsPerPixel + 7) / 8]);
			for (int i = 0; i < accumulator.length; i++) {
				for (int i_0_ = 0; i_0_ < accumulator[i].length; i_0_++)
					accumulator[i][i_0_] = (byte) 0;
			}
			pass = 0;
			lineno = 0;
			initLines(size_x[pass]);
		}

		protected void terminateScanline() throws IOException,
				ImageFormatException {
			if (pass == 6) {
				writeLine(readyLine);
				if (++lineno < accumulator.length)
					writeLine(accumulator[lineno]);
			} else {
				int i = offset_x[pass] * elementSize;
				int i_1_ = step_x[pass] * elementSize;
				byte[] is = accumulator[offset_y[pass] + step_y[pass] * lineno];
				if (elementSize == 3) {
					for (int i_2_ = 0; i_2_ < readyLine.length; i_2_ += 3) {
						is[i] = readyLine[i_2_];
						is[i + 1] = readyLine[i_2_ + 1];
						is[i + 2] = readyLine[i_2_ + 2];
						i += i_1_;
					}
				} else if (bitsPerComponent == 8) {
					for (int i_3_ = 0; i_3_ < readyLine.length; i_3_++) {
						is[i] = readyLine[i_3_];
						i += i_1_;
					}
				} else {
					for (int i_4_ = 0; i_4_ < readyLine.length; i_4_++) {
						for (int i_5_ = 0; i_5_ < 8 && i < pxWidth; i_5_ += bitsPerComponent) {
							int i_6_ = readyLine[i_4_] << i_5_ & mask;
							is[i / pixelsPerByte] |= (i_6_ >> i % pixelsPerByte
									* bitsPerComponent);
							i += i_1_;
						}
					}
				}
				if (++lineno == size_y[pass])
					nextPass();
			}
		}

		protected void nextPass() throws IOException, ImageFormatException {
			while (++pass < 6 && (size_x[pass] == 0 || size_y[pass] == 0)) {
				/* empty */
			}
			if (pass == 6) {
				writeLine(accumulator[0]);
				if (size_x[pass] == 0 || size_y[pass] == 0)
					return;
			}
			lineno = 0;
			initLines(size_x[pass]);
		}
	}

	class PNGSequentialDecoder extends PNGDecoder {
		public PNGSequentialDecoder(OutputStream outputstream) {
			super(outputstream);
			initLines(pxWidth);
		}

		protected void terminateScanline() throws IOException {
			writeLine(readyLine);
		}
	}

	abstract class PNGDecoder {
		OutputStream out = null;

		byte[] currentLine = null;

		byte[] readyLine = null;

		int elementSize = 1;

		int cursor = 0;

		int byteCount = 0;

		boolean[] byteSieve = new boolean[8];

		byte[] gammaLUT = null;

		PNGPredictor pred = null;

		private PNGPredictor predictor_NONE = new NonePredictor();

		private PNGPredictor predictor_SUB = new SubPredictor();

		private PNGPredictor predictor_UP = new UpPredictor();

		private PNGPredictor predictor_AVERAGE = new AveragePredictor();

		private PNGPredictor predictor_PAETH = new PaethPredictor();

		private byte[] gammaTable = null;

		class PaethPredictor extends PNGPredictor {
			PaethPredictor() {
				super();
			}

			int pred(int i, int i_7_, int i_8_) {
				int i_9_ = i + i_7_ - i_8_;
				int i_10_ = Math.abs(i_9_ - i);
				int i_11_ = Math.abs(i_9_ - i_7_);
				int i_12_ = Math.abs(i_9_ - i_8_);
				if (i_10_ <= i_11_ && i_10_ <= i_12_)
					return i;
				if (i_11_ <= i_12_)
					return i_7_;
				return i_8_;
			}

			byte decode(int i) {
				int i_13_;
				if (cursor < elementSize)
					i_13_ = pred(0, readyLine[cursor] & 0xff, 0);
				else
					i_13_ = pred(currentLine[cursor - elementSize] & 0xff,
							readyLine[cursor] & 0xff, readyLine[cursor
									- elementSize] & 0xff);
				return (byte) ((i & 0xff) + i_13_);
			}
		}

		class AveragePredictor extends PNGPredictor {
			AveragePredictor() {
				super();
			}

			byte decode(int i) {
				int i_14_ = readyLine[cursor] & 0xff;
				if (cursor >= elementSize)
					i_14_ += currentLine[cursor - elementSize] & 0xff;
				return (byte) ((i & 0xff) + (i_14_ >> 1));
			}
		}

		class UpPredictor extends PNGPredictor {
			UpPredictor() {
				super();
			}

			byte decode(int i) {
				return (byte) ((i & 0xff) + (readyLine[cursor] & 0xff));
			}
		}

		class SubPredictor extends PNGPredictor {
			SubPredictor() {
				super();
			}

			byte decode(int i) {
				if (cursor < elementSize)
					return (byte) i;
				return (byte) ((i & 0xff) + (currentLine[cursor - elementSize] & 0xff));
			}
		}

		class NonePredictor extends PNGPredictor {
			NonePredictor() {
				super();
			}

			byte decode(int i) {
				return (byte) i;
			}
		}

		abstract class PNGPredictor {
			abstract byte decode(int i);
		}

		public PNGDecoder(OutputStream outputstream) {
			out = outputstream;
			elementSize = colorSpace == 1 ? 3 : 1;
			if (bitDepth == 16) {
				byteSieve[1] = byteSieve[3] = byteSieve[5] = byteSieve[7] = true;
				if (colorType == 4)
					byteSieve[2] = byteSieve[6] = true;
				else if (colorType == 6)
					byteSieve[6] = true;
			} else if (colorType == 4)
				byteSieve[1] = byteSieve[3] = byteSieve[5] = byteSieve[7] = true;
			else if (colorType == 6)
				byteSieve[3] = byteSieve[7] = true;
			if (bitsPerComponent == 8 && needsGammaCorrection) {
				gammaLUT = new byte[256];
				for (int i = 0; i < 256; i++)
					gammaLUT[i] = computeGammaCorrection(i);
			}
		}

		public void write(byte[] is) throws IOException, ImageFormatException {
			write(is, 0, is.length);
		}

		public void write(byte[] is, int i, int i_15_) throws IOException,
				ImageFormatException {
			for (/**/; i_15_ > 0; i_15_--) {
				write(is[i]);
				i++;
			}
		}

		public void write(int i) throws IOException, ImageFormatException {
			if (!byteSieve[byteCount++ & 0x7]) {
				if (pred == null) {
					setPredictor(i & 0xff);
					cursor = byteCount = 0;
				} else {
					currentLine[cursor] = pred.decode(i);
					if (++cursor == currentLine.length) {
						byte[] is = readyLine;
						readyLine = currentLine;
						currentLine = is;
						pred = null;
						terminateScanline();
					}
				}
			}
		}

		void initLines(int i) {
			int i_16_ = (i * bitsPerPixel + 7) / 8;
			currentLine = new byte[i_16_];
			readyLine = new byte[i_16_];
			for (int i_17_ = 0; i_17_ < i_16_; i_17_++)
				currentLine[i_17_] = readyLine[i_17_] = (byte) 0;
			pred = null;
			cursor = byteCount = 0;
		}

		private void setPredictor(int i) throws ImageFormatException {
			switch (i) {
			case 0:
				pred = predictor_NONE;
				break;
			case 1:
				pred = predictor_SUB;
				break;
			case 2:
				pred = predictor_UP;
				break;
			case 3:
				pred = predictor_AVERAGE;
				break;
			case 4:
				pred = predictor_PAETH;
				break;
			default:
				pred = null;
				throw new ImageFormatException(
						"Incorrect value for filter type (" + i
								+ ") in PNG image "
								+ PNGImage.this.toDisplayString());
			}
		}

		protected void writeLine(byte[] is) throws IOException {
			if (gammaLUT == null)
				out.write(is);
			else {
				for (int i = 0; i < is.length; i++)
					out.write(gammaLUT[is[i] & 0xff]);
			}
		}

		protected abstract void terminateScanline() throws IOException,
				ImageFormatException;
	}

	public PNGImage() {
		mimetype = "image/png";
	}

	byte computeGammaCorrection(int i) {
		double d = (double) (i & 0xff) / 255.0;
		d = Math.pow(d, gammaExponent);
		return (byte) (int) Math.round(d * 255.0);
	}

	public void parse() throws IOException, ImageFormatException {
		canExpandData = true;
		canCopyData = false;
		compressionMethod = (short) 2;
		predictor = (short) 2;
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			byte[] is = new byte[8];
			seekableinput.readFully(is);
			if (is[0] != -119 || is[1] != 80 || is[2] != 78 || is[3] != 71
					|| is[4] != 13 || is[5] != 10 || is[6] != 26 || is[7] != 10)
				throw new ImageFormatException(
						"Corrupt signature in PNG image "
								+ this.toDisplayString());
			boolean bool = false;
			byte[] is_18_ = new byte[4];
			for (;;) {
				dataOffset = seekableinput.getFilePointer();
				int i = seekableinput.readInt();
				seekableinput.readFully(is_18_);
				if (is_18_[0] == 73 && is_18_[1] == 72 && is_18_[2] == 68
						&& is_18_[3] == 82)
					parse_IHDR_chunk(seekableinput, (long) i);
				else if (is_18_[0] == 80 && is_18_[1] == 76 && is_18_[2] == 84
						&& is_18_[3] == 69)
					parse_PLTE_chunk(seekableinput, i);
				else if (is_18_[0] == 116 && is_18_[1] == 82 && is_18_[2] == 78
						&& is_18_[3] == 83)
					parse_tRNS_chunk(seekableinput, i);
				else if (is_18_[0] == 112 && is_18_[1] == 72 && is_18_[2] == 89
						&& is_18_[3] == 115)
					parse_pHYs_chunk(seekableinput, i);
				else if (is_18_[0] == 103 && is_18_[1] == 65 && is_18_[2] == 77
						&& is_18_[3] == 65)
					parse_gAMA_chunk(seekableinput, i);
				else {
					if (is_18_[0] == 73 && is_18_[1] == 68 && is_18_[2] == 65
							&& is_18_[3] == 84)
						break;
					if (is_18_[0] == 73 && is_18_[1] == 69 && is_18_[2] == 78
							&& is_18_[3] == 68)
						throw new ImageFormatException(
								"Premature IEND chunk in PNG image "
										+ this.toDisplayString()
										+ ": no data chunks found");
					if ((is_18_[0] & 0x20) == 0)
						throw new ImageFormatException(
								"Unknown critical chunk type ["
										+ Integer.toHexString(is_18_[0]) + "h "
										+ Integer.toHexString(is_18_[1]) + "h "
										+ Integer.toHexString(is_18_[2]) + "h "
										+ Integer.toHexString(is_18_[3]) + "h]"
										+ " in PNG image "
										+ this.toDisplayString());
					seekableinput.skipBytes(i + 4);
				}
			}
			this.setDimensions();
			canCopyData = (bitDepth <= 8
					&& (colorType == 0 || colorType == 2 || colorType == 3)
					&& !interlaced && (!needsGammaCorrection || colorType == 3));
		} finally {
			seekableinput.close();
		}
	}

	private void parse_IHDR_chunk(SeekableInput seekableinput, long l)
			throws IOException, ImageFormatException {
		pxWidth = seekableinput.readInt();
		pxHeight = seekableinput.readInt();
		bitDepth = seekableinput.readUnsignedByte();
		colorType = seekableinput.readUnsignedByte();
		switch (colorType) {
		case 0:
			if (bitDepth == 1 || bitDepth == 2 || bitDepth == 4
					|| bitDepth == 8 || bitDepth == 16) {
				bitsPerPixel = bitsPerComponent = bitDepth == 16 ? 8 : bitDepth;
				colorSpace = (short) 4;
			} else
				throw new ImageFormatException("Invalid bit depth " + bitDepth
						+ " for color type " + colorType + " ("
						+ colorTypeNames[colorType] + ") in PNG image "
						+ this.toDisplayString());
			break;
		case 4:
			if (bitDepth == 8 || bitDepth == 16) {
				bitsPerPixel = bitsPerComponent = 8;
				colorSpace = (short) 4;
			} else
				throw new ImageFormatException("Invalid bit depth " + bitDepth
						+ " for color type " + colorType + " ("
						+ colorTypeNames[colorType] + ") in PNG image "
						+ this.toDisplayString());
			break;
		case 2:
		case 6:
			if (bitDepth == 8 || bitDepth == 16) {
				bitsPerComponent = 8;
				bitsPerPixel = bitsPerComponent * 3;
				colorSpace = (short) 1;
			} else
				throw new ImageFormatException("Invalid bit depth " + bitDepth
						+ " for color type " + colorType + " ("
						+ colorTypeNames[colorType] + ") in PNG image "
						+ this.toDisplayString());
			break;
		case 3:
			if (bitDepth == 1 || bitDepth == 2 || bitDepth == 4
					|| bitDepth == 8) {
				bitsPerPixel = bitsPerComponent = bitDepth;
				colorSpace = (short) 6;
			} else
				throw new ImageFormatException("Invalid bit depth " + bitDepth
						+ " for color type " + colorType + " ("
						+ colorTypeNames[colorType] + ") in PNG image "
						+ this.toDisplayString());
			break;
		default:
			throw new ImageFormatException("Invalid color type " + colorType
					+ " in PNG image " + this.toDisplayString());
		}
		if (seekableinput.readByte() != 0)
			throw new ImageFormatException(
					"Unknown (non-standard) compression method in PNG image "
							+ this.toDisplayString());
		if (seekableinput.readByte() != 0)
			throw new ImageFormatException(
					"Unknown (non-standard) filtering method in PNG image "
							+ this.toDisplayString());
		int i = seekableinput.readUnsignedByte();
		if (i > 1)
			throw new ImageFormatException(
					"Unknown (non-standard) interlace method in PNG image "
							+ this.toDisplayString());
		interlaced = i == 1;
		seekableinput.skipBytes(4);
	}

	private void parse_PLTE_chunk(SeekableInput seekableinput, int i)
			throws IOException, ImageFormatException {
		if (colorType != 3)
			seekableinput.skipBytes(i);
		else {
			colorTable = new byte[i];
			seekableinput.readFully(colorTable);
			if (needsGammaCorrection) {
				for (int i_19_ = 0; i_19_ < i; i_19_++)
					colorTable[i_19_] = computeGammaCorrection(colorTable[i_19_]);
				needsGammaCorrection = false;
			}
		}
		seekableinput.skipBytes(4);
	}

	private void parse_gAMA_chunk(SeekableInput seekableinput, int i)
			throws IOException, ImageFormatException {
		double d = ((double) ((long) seekableinput.readInt() & 0xffffffffL) / 100000.0);
		if (d == 0.0)
			throw new ImageFormatException("Zero gamma in PNG image "
					+ this.toDisplayString());
		gammaExponent = 1.0 / (2.2 * d);
		needsGammaCorrection = Math.abs(gammaExponent - 1.0) > 0.05;
		if (colorSpace != 6 && bitsPerComponent < 8)
			needsGammaCorrection = false;
		seekableinput.skipBytes(4);
	}

	private void parse_tRNS_chunk(SeekableInput seekableinput, int i)
			throws IOException, ImageFormatException {
		switch (colorType) {
		case 0:
			transparentColor = new byte[1];
			if (bitDepth == 16) {
				transparentColor[0] = seekableinput.readByte();
				seekableinput.skipBytes(1);
			} else {
				seekableinput.skipBytes(1);
				transparentColor[0] = seekableinput.readByte();
			}
			if (!rightRange(transparentColor[0]))
				transparentColor = null;
			break;
		case 2:
			transparentColor = new byte[3];
			if (bitDepth == 16) {
				transparentColor[0] = seekableinput.readByte();
				seekableinput.skipBytes(1);
				transparentColor[1] = seekableinput.readByte();
				seekableinput.skipBytes(1);
				transparentColor[2] = seekableinput.readByte();
				seekableinput.skipBytes(1);
			} else {
				seekableinput.skipBytes(1);
				transparentColor[0] = seekableinput.readByte();
				seekableinput.skipBytes(1);
				transparentColor[1] = seekableinput.readByte();
				seekableinput.skipBytes(1);
				transparentColor[2] = seekableinput.readByte();
			}
			if (!rightRange(transparentColor[0])
					|| !rightRange(transparentColor[1])
					|| !rightRange(transparentColor[2]))
				transparentColor = null;
			break;
		case 3:
			for (int i_20_ = 0; i_20_ < i; i_20_++) {
				if (i_20_ >= 256)
					break;
				if (seekableinput.readByte() == 0) {
					transparentColor = new byte[1];
					transparentColor[0] = (byte) i_20_;
					seekableinput.skipBytes(i - i_20_ - 1);
					break;
				}
			}
			break;
		case 4:
		case 6:
			seekableinput.skipBytes(i);
			break;
		}
		seekableinput.skipBytes(4);
	}

	private void parse_pHYs_chunk(SeekableInput seekableinput, int i)
			throws IOException, ImageFormatException {
		double d = (double) ((long) seekableinput.readInt() & 0xffffffffL);
		if (d == 0.0)
			throw new ImageFormatException(
					"Zero horizontal resolution in PNG image "
							+ this.toDisplayString());
		double d_21_ = (double) ((long) seekableinput.readInt() & 0xffffffffL);
		if (d_21_ == 0.0)
			throw new ImageFormatException(
					"Zero vertical resolution in PNG image "
							+ this.toDisplayString());
		int i_22_ = seekableinput.readUnsignedByte();
		if (i_22_ == 0) {
			double d_23_ = Math.sqrt(d_21_ / d);
			horzResolution /= d_23_;
			vertResolution *= d_23_;
		} else if (i_22_ == 1) {
			horzResolution = d * 0.0254;
			vertResolution = d_21_ * 0.0254;
		} else
			throw new ImageFormatException("Invalid unit type " + i_22_
					+ " in PNG image " + this.toDisplayString());
		seekableinput.skipBytes(4);
	}

	public void copyData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		if (!canCopyData)
			throw new ImageFormatException(
					"Cannot copy compressed stream with alpha channel and/or 16-bit color from PNG image "
							+ this.toDisplayString());
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(dataOffset);
			byte[] is = new byte[8192];
			for (;;) {
				int i = seekableinput.readInt();
				byte[] is_24_ = new byte[4];
				seekableinput.readFully(is_24_);
				if (is_24_[0] != 73 || is_24_[1] != 68 || is_24_[2] != 65
						|| is_24_[3] != 84)
					break;
				int i_25_;
				for (/**/; i > 0; i -= i_25_) {
					i_25_ = i > is.length ? is.length : i;
					seekableinput.readFully(is, 0, i_25_);
					outputstream.write(is, 0, i_25_);
				}
				seekableinput.skipBytes(4);
			}
		} finally {
			seekableinput.close();
		}
	}

	public void expandData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		SeekableInput seekableinput = this.openSeekableImageStream();
		Inflater inflater = new Inflater(false);
		inflater.reset();
		try {
			seekableinput.seek(dataOffset);
			Object object = null;
			PNGDecoder pngdecoder;
			if (interlaced)
				pngdecoder = new PNGInterlacedDecoder(outputstream);
			else
				pngdecoder = new PNGSequentialDecoder(outputstream);
			boolean bool = false;
			byte[] is = new byte[4];
			byte[] is_26_ = new byte[8192];
			byte[] is_27_ = new byte[8192];
			for (;;) {
				int i = seekableinput.readInt();
				seekableinput.readFully(is);
				if (is[0] != 73 || is[1] != 68 || is[2] != 65 || is[3] != 84)
					break;
				int i_28_;
				for (/**/; i > 0; i -= i_28_) {
					i_28_ = i > is_26_.length ? is_26_.length : i;
					seekableinput.readFully(is_26_, 0, i_28_);
					if (inflater.finished())
						throw new ImageFormatException(
								"Too much data in PNG image "
										+ this.toDisplayString());
					if (inflater.needsDictionary())
						throw new ImageFormatException(
								"Incorrect compressed data format (dictionary required) in PNG image "
										+ this.toDisplayString());
					inflater.setInput(is_26_, 0, i_28_);
					boolean bool_29_ = false;
					try {
						int i_30_;
						while ((i_30_ = inflater.inflate(is_27_)) > 0)
							pngdecoder.write(is_27_, 0, i_30_);
					} catch (DataFormatException dataformatexception) {
						throw new ImageFormatException(
								"Bad data stream in PNG image "
										+ this.toDisplayString() + ": "
										+ dataformatexception.toString());
					}
				}
				seekableinput.skipBytes(4);
			}
			if (!inflater.finished())
				throw new ImageFormatException(
						"Premature end of data in PNG image "
								+ this.toDisplayString());
		} finally {
			seekableinput.close();
			inflater.end();
		}
	}

	protected boolean rightRange(int i) {
		return (i & 0xff) >> bitDepth == 0;
	}

	public static void main(String[] strings) {
		PNGImage pngimage = new PNGImage();
		if (strings.length == 0)
			System.err.println("Usage: " + pngimage.getClass().getName()
					+ " <image file name>");
		else {
			try {
				pngimage.source = new URLSpec(strings[0]);
				pngimage.parse();
				System.out.println(pngimage.toString());
			} catch (IOException ioexception) {
				System.err.println(ioexception);
			} catch (ImageFormatException imageformatexception) {
				System.err.println(imageformatexception);
			}
		}
	}
}