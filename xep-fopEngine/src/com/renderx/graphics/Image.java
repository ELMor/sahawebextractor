/*
 * Image - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.InputStream;

import com.renderx.util.SeekableInput;
import com.renderx.util.URLSpec;

public abstract class Image {
	public URLSpec source = null;

	public String mimetype = null;

	public double width = 0.0;

	public double height = 0.0;

	protected ImageFactory factory;

	public abstract void parse() throws IOException, ImageFormatException;

	public String toString() {
		return (this.getClass().getName() + ":" + "source='" + source + "'"
				+ "," + "mimetype='" + mimetype + "'" + "," + "width=" + width
				+ " pt" + "," + "height=" + height + " pt");
	}

	public String toDisplayString() {
		if (source == null)
			return "[null]";
		if (source.scheme == 1)
			return "[instream]";
		return source.toString();
	}

	public InputStream openImageStream() throws IOException {
		return factory.openStream(source);
	}

	public SeekableInput openSeekableImageStream() throws IOException {
		return factory.openSeekableStream(source);
	}

	public static void main(String[] strings) {
		if (strings.length <= 1)
			System.err
					.println("Usage: com.renderx.graphics.Image <image handler class> {<image file name>}");
		else {
			for (int i = 1; i != strings.length; i++) {
				try {
					Image image = (Image) Class.forName(strings[0])
							.newInstance();
					image.source = new URLSpec(strings[i]);
					image.parse();
					System.out.println(image);
				} catch (Exception exception) {
					System.err.println(exception);
				}
			}
		}
	}
}