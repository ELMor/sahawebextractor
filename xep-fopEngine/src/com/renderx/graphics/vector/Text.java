/*
 * Text - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public class Text extends Contour {
	public String value = null;

	public FontSpec font = null;

	public double x = 0.0;

	public double y = 0.0;

	public Text() {
		/* empty */
	}

	public Text(PaintSpec paintspec, StrokeSpec strokespec, int i, int i_0_,
			double d, double d_1_, FontSpec fontspec, String string,
			double d_2_, double d_3_) {
		super(paintspec, strokespec, i, i_0_, d, d_1_);
		font = fontspec;
		value = string;
		x = d_2_;
		y = d_3_;
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		if (value != null) {
			printstream.println(string + "%<text fill-opacity=\"" + fillOpacity
					+ "\" stroke-opacity=\"" + strokeOpacity + "\" value=\""
					+ value + "\">");
			if (fill != null) {
				printstream.println(string + "  gsave");
				printstream.println(string + "  %<fill>");
				fill.dump(printstream, string + "    ");
				printstream.println(string + "  %</fill>");
				printstream.println(string + "  " + x + " " + y + " moveto");
				printstream.println(string + "  (" + value + ") show");
				printstream.println(string + "  grestore");
			}
			if (stroke != null && stroke.paint != null) {
				printstream.println(string + "  gsave");
				stroke.dump(printstream, string + "  ");
				printstream.println(string + "  " + x + " " + y + " moveto");
				printstream
						.println(string + "  (" + value + ") false charpath");
				printstream.println(string + "  stroke");
				printstream.println(string + "  grestore");
			}
			printstream.println(string + "%</text>");
		}
	}
}