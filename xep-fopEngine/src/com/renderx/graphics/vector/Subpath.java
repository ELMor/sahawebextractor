/*
 * Subpath - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;

import com.renderx.util.List;

public class Subpath extends GraphicObject {
	public double startX = 0.0;

	public double startY = 0.0;

	public boolean closed = false;

	public List segs = new List();

	public static class Curve extends Segment {
		public double cpx1 = 0.0;

		public double cpy1 = 0.0;

		public double cpx2 = 0.0;

		public double cpy2 = 0.0;

		public Curve(double d, double d_0_, double d_1_, double d_2_,
				double d_3_, double d_4_, double d_5_, double d_6_) {
			super(d, d_0_, d_5_, d_6_);
			cpx1 = d_1_;
			cpy1 = d_2_;
			cpx2 = d_3_;
			cpy2 = d_4_;
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<curve x-from=\"" + xbeg + "\""
					+ " y-from=\"" + ybeg + "\"" + " cpx1=\"" + cpx1 + "\""
					+ " cpy1=\"" + cpy1 + "\"" + " cpx2=\"" + cpx2 + "\""
					+ " cpy2=\"" + cpy2 + "\"" + " x-to=\"" + xend + "\""
					+ " y-to=\"" + yend + "\">");
			printstream.println(string + "  " + cpx1 + " " + cpy1 + " " + cpx2
					+ " " + cpy2 + " " + xend + " " + yend + " curveto");
			printstream.println(string + "%</curve>");
		}

		public double startSlopeX() {
			if (cpx1 != xbeg || cpy1 != ybeg)
				return cpx1 - xbeg;
			if (cpx2 != xbeg || cpy2 != ybeg)
				return cpx2 - xbeg;
			return xend - xbeg;
		}

		public double startSlopeY() {
			if (cpx1 != xbeg || cpy1 != ybeg)
				return cpy1 - ybeg;
			if (cpx2 != xbeg || cpy2 != ybeg)
				return cpy2 - ybeg;
			return cpy1 - ybeg;
		}

		public double endSlopeX() {
			if (xend != cpx2 || yend != cpy2)
				return xend - cpx2;
			if (xend != cpx1 || yend != cpy1)
				return xend - cpx1;
			return xend - xbeg;
		}

		public double endSlopeY() {
			if (xend != cpx2 || yend != cpy2)
				return yend - cpy2;
			if (xend != cpx1 || yend != cpy1)
				return yend - cpy1;
			return yend - ybeg;
		}
	}

	public static class Line extends Segment {
		public Line(double d, double d_7_, double d_8_, double d_9_) {
			super(d, d_7_, d_8_, d_9_);
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<line x-from=\"" + xbeg + "\""
					+ " y-from=\"" + ybeg + "\"" + " x-to=\"" + xend + "\""
					+ " y-to=\"" + yend + "\">");
			printstream.println(string + "  " + xend + " " + yend + " lineto");
			printstream.println(string + "%</line>");
		}

		public double startSlopeX() {
			return xend - xbeg;
		}

		public double startSlopeY() {
			return yend - ybeg;
		}

		public double endSlopeX() {
			return xend - xbeg;
		}

		public double endSlopeY() {
			return yend - ybeg;
		}
	}

	public abstract static class Segment extends GraphicObject {
		public double xbeg = 0.0;

		public double ybeg = 0.0;

		public double xend = 0.0;

		public double yend = 0.0;

		public boolean beginsArc = true;

		public boolean endsArc = true;

		public Segment(double d, double d_10_, double d_11_, double d_12_) {
			xbeg = d;
			ybeg = d_10_;
			xend = d_11_;
			yend = d_12_;
		}

		public abstract void dump(PrintStream printstream, String string)
				throws IOException;

		public abstract double startSlopeX();

		public abstract double startSlopeY();

		public abstract double endSlopeX();

		public abstract double endSlopeY();
	}

	public void append(Segment segment) {
		segs.append(segment);
	}

	public Subpath(double d, double d_13_) {
		startX = d;
		startY = d_13_;
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println(string + "%<subpath closed=\"" + closed + "\">");
		printstream.println(string + "  " + startX + " " + startY + " moveto");
		Enumeration enumeration = segs.elements();
		while (enumeration.hasMoreElements())
			((Segment) enumeration.nextElement()).dump(printstream, string
					+ "  ");
		if (closed)
			printstream.println(string + "  closepath");
		printstream.println(string + "%</subpath>");
	}
}