/*
 * Contour - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public abstract class Contour extends GraphicObject {
	public static final int RULE_NONZERO = 0;

	public static final int RULE_EVENODD = 1;

	public int fillRule = 0;

	public int clipRule = 0;

	public PaintSpec fill = null;

	public StrokeSpec stroke = null;

	public double fillOpacity = 1.0;

	public double strokeOpacity = 1.0;

	public Contour() {
		/* empty */
	}

	public Contour(PaintSpec paintspec, StrokeSpec strokespec, int i, int i_0_) {
		this();
		fill = paintspec;
		stroke = strokespec;
		fillRule = i;
		clipRule = i_0_;
	}

	public Contour(PaintSpec paintspec, StrokeSpec strokespec, int i, int i_1_,
			double d, double d_2_) {
		this();
		fill = paintspec;
		stroke = strokespec;
		fillRule = i;
		clipRule = i_1_;
		fillOpacity = d;
		strokeOpacity = d_2_;
	}

	public abstract void dump(PrintStream printstream, String string)
			throws IOException;
}