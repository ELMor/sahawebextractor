/*
 * Path - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;

import com.renderx.util.List;

public class Path extends Contour {
	public List subs;

	private Subpath currentSubpath;

	public double curX;

	public double curY;

	public Path() {
		subs = new List();
		currentSubpath = null;
		curX = 0.0;
		curY = 0.0;
	}

	public Path(PaintSpec paintspec, StrokeSpec strokespec, int i, int i_0_,
			double d, double d_1_) {
		super(paintspec, strokespec, i, i_0_, d, d_1_);
		subs = new List();
		currentSubpath = null;
		curX = 0.0;
		curY = 0.0;
	}

	public void moveto(double d, double d_2_) {
		currentSubpath = null;
		curX = d;
		curY = d_2_;
	}

	public void lineto(double d, double d_3_) {
		if (currentSubpath == null)
			subs.append(currentSubpath = new Subpath(curX, curY));
		currentSubpath.append(new Subpath.Line(curX, curY, d, d_3_));
		curX = d;
		curY = d_3_;
	}

	public void curveto(double d, double d_4_, double d_5_, double d_6_,
			double d_7_, double d_8_) {
		if (currentSubpath == null)
			subs.append(currentSubpath = new Subpath(curX, curY));
		currentSubpath.append(new Subpath.Curve(curX, curY, d, d_4_, d_5_,
				d_6_, d_7_, d_8_));
		curX = d_7_;
		curY = d_8_;
	}

	public void closepath() {
		if (currentSubpath != null) {
			currentSubpath.closed = true;
			curX = currentSubpath.startX;
			curY = currentSubpath.startY;
			currentSubpath = null;
		}
	}

	public void quadcurveto(double d, double d_9_, double d_10_, double d_11_) {
		curveto((2.0 * d + curX) / 3.0, (2.0 * d_9_ + curY) / 3.0,
				(2.0 * d + d_10_) / 3.0, (2.0 * d_9_ + d_11_) / 3.0, d_10_,
				d_11_);
	}

	public void arcto(double d, double d_12_, double d_13_, boolean bool,
			boolean bool_14_, double d_15_, double d_16_) {
		if (curX != d_15_ || curY != d_16_) {
			if (d == 0.0 || d_12_ == 0.0)
				lineto(d_15_, d_16_);
			else {
				if (d < 0.0)
					d = -d;
				if (d_12_ < 0.0)
					d_12_ = -d_12_;
				double d_17_ = Math.sin(d_13_);
				double d_18_ = Math.cos(d_13_);
				if (d_17_ == 1.0 || d_17_ == -1.0)
					d_18_ = 0.0;
				if (d_18_ == 1.0 || d_18_ == -1.0)
					d_17_ = 0.0;
				double d_19_ = (curX - d_15_) / 2.0;
				double d_20_ = (curY - d_16_) / 2.0;
				double d_21_ = d_18_ * d_19_ + d_17_ * d_20_;
				double d_22_ = -d_17_ * d_19_ + d_18_ * d_20_;
				double d_23_ = d * d;
				double d_24_ = d_12_ * d_12_;
				double d_25_ = d_21_ * d_21_;
				double d_26_ = d_22_ * d_22_;
				double d_27_ = 0.0;
				double d_28_ = 0.0;
				double d_29_ = d_25_ / d_23_ + d_26_ / d_24_;
				if (d_29_ >= 1.0) {
					d *= Math.sqrt(d_29_);
					d_12_ *= Math.sqrt(d_29_);
				} else {
					double d_30_ = bool == bool_14_ ? -1.0 : 1.0;
					double d_31_ = d_30_
							* Math.sqrt((d_23_ * d_24_ - d_23_ * d_26_ - d_24_
									* d_25_)
									/ (d_23_ * d_26_ + d_24_ * d_25_));
					d_27_ = d_31_ * (d * d_22_ / d_12_);
					d_28_ = -d_31_ * (d_12_ * d_21_ / d);
				}
				double d_32_ = (curX + d_15_) / 2.0;
				double d_33_ = (curY + d_16_) / 2.0;
				double d_34_ = d_18_ * d_27_ - d_17_ * d_28_ + d_32_;
				double d_35_ = d_17_ * d_27_ + d_18_ * d_28_ + d_33_;
				double d_36_ = curX - d_34_;
				double d_37_ = curY - d_35_;
				double d_38_ = d_15_ - d_34_;
				double d_39_ = d_16_ - d_35_;
				double d_40_ = (d_18_ * d_36_ + d_17_ * d_37_) / d;
				double d_41_ = (-d_17_ * d_36_ + d_18_ * d_37_) / d_12_;
				double d_42_ = (d_18_ * d_38_ + d_17_ * d_39_) / d;
				double d_43_ = (-d_17_ * d_38_ + d_18_ * d_39_) / d_12_;
				if (d_40_ >= 1.0) {
					d_40_ = 1.0;
					d_41_ = 0.0;
				}
				if (d_40_ <= -1.0) {
					d_40_ = -1.0;
					d_41_ = 0.0;
				}
				if (d_42_ >= 1.0) {
					d_42_ = 1.0;
					d_43_ = 0.0;
				}
				if (d_42_ <= -1.0) {
					d_42_ = -1.0;
					d_43_ = 0.0;
				}
				if (d_41_ >= 1.0) {
					d_40_ = 0.0;
					d_41_ = 1.0;
				}
				if (d_41_ <= -1.0) {
					d_40_ = 0.0;
					d_41_ = -1.0;
				}
				if (d_43_ >= 1.0) {
					d_42_ = 0.0;
					d_43_ = 1.0;
				}
				if (d_43_ <= -1.0) {
					d_42_ = 0.0;
					d_43_ = -1.0;
				}
				double d_44_ = d_41_ >= 0.0 ? Math.acos(d_40_) : -Math
						.acos(d_40_);
				double d_45_ = d_43_ >= 0.0 ? Math.acos(d_42_) : -Math
						.acos(d_42_);
				double d_46_ = d_45_ - d_44_;
				if (bool_14_) {
					for (/**/; d_46_ < 0.0; d_46_ += 6.283185307179586) {
						/* empty */
					}
				} else {
					for (/**/; d_46_ > 0.0; d_46_ -= 6.283185307179586) {
						/* empty */
					}
				}
				int i = (int) Math
						.ceil(Math.abs(2.0 * d_46_) / 3.141592653589793);
				double d_47_ = d_46_ / (double) i;
				double d_48_ = 1.3333333333333333 * Math.tan(d_47_ / 4.0);
				double d_49_ = d_44_;
				double d_50_ = d_41_;
				double d_51_ = d_40_;
				for (int i_52_ = 0; i_52_ < i; i_52_++) {
					boolean bool_53_ = i_52_ == 0;
					boolean bool_54_ = i_52_ == i - 1;
					double d_55_ = d_45_;
					double d_56_ = d_43_;
					double d_57_ = d_42_;
					if (!bool_54_) {
						d_55_ = d_49_ + d_47_;
						d_56_ = Math.sin(d_55_);
						d_57_ = Math.cos(d_55_);
					}
					double d_58_ = d_51_ - d_48_ * d_50_;
					double d_59_ = d_50_ + d_48_ * d_51_;
					double d_60_ = d_57_ + d_48_ * d_56_;
					double d_61_ = d_56_ - d_48_ * d_57_;
					double d_62_ = d_58_ * d * d_18_ - d_59_ * d_12_ * d_17_
							+ d_34_;
					double d_63_ = d_58_ * d * d_17_ + d_59_ * d_12_ * d_18_
							+ d_35_;
					double d_64_ = d_60_ * d * d_18_ - d_61_ * d_12_ * d_17_
							+ d_34_;
					double d_65_ = d_60_ * d * d_17_ + d_61_ * d_12_ * d_18_
							+ d_35_;
					double d_66_ = d_15_;
					double d_67_ = d_16_;
					if (!bool_54_) {
						d_66_ = (d_57_ * d * d_18_ - d_56_ * d_12_ * d_17_ + d_34_);
						d_67_ = (d_57_ * d * d_17_ + d_56_ * d_12_ * d_18_ + d_35_);
					}
					curveto(d_62_, d_63_, d_64_, d_65_, d_66_, d_67_);
					((Subpath.Segment) currentSubpath.segs.last()).beginsArc = bool_53_;
					((Subpath.Segment) currentSubpath.segs.last()).endsArc = bool_54_;
					d_49_ = d_55_;
					d_50_ = d_56_;
					d_51_ = d_57_;
				}
			}
		}
	}

	public void circle(double d, double d_68_, double d_69_) {
		ellipse(d, d_68_, d_69_, d_69_);
	}

	public void ellipse(double d, double d_70_, double d_71_, double d_72_) {
		moveto(d + d_71_, d_70_);
		quarterarcto(d, d_70_ + d_72_, false);
		quarterarcto(d - d_71_, d_70_, true);
		quarterarcto(d, d_70_ - d_72_, false);
		quarterarcto(d + d_71_, d_70_, true);
		closepath();
	}

	public void quarterarcto(double d, double d_73_, boolean bool) {
		double d_74_ = (d - curX) * 0.55228475;
		double d_75_ = (d_73_ - curY) * 0.55228475;
		if (bool)
			curveto(curX + d_74_, curY, d, d_73_ - d_75_, d, d_73_);
		else
			curveto(curX, curY + d_75_, d - d_74_, d_73_, d, d_73_);
	}

	public void square(double d, double d_76_, double d_77_) {
		rectangle(d, d_76_, d_77_, d_77_);
	}

	public void rectangle(double d, double d_78_, double d_79_, double d_80_) {
		moveto(d, d_78_);
		lineto(d, d_78_ + d_80_);
		lineto(d + d_79_, d_78_ + d_80_);
		lineto(d + d_79_, d_78_);
		lineto(d, d_78_);
		closepath();
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println(string + "%<path fill-rule=\""
				+ (fillRule == 0 ? "nonzero" : "evenodd") + "\" clip-rule=\""
				+ (clipRule == 0 ? "nonzero" : "evenodd")
				+ "\" fill-opacity=\"" + fillOpacity + "\" stroke-opacity=\""
				+ strokeOpacity + "\">");
		printstream.println(string + "  newpath");
		Enumeration enumeration = subs.elements();
		while (enumeration.hasMoreElements())
			((Subpath) enumeration.nextElement()).dump(printstream, string
					+ "  ");
		if (fill != null) {
			printstream.println(string + "  gsave");
			printstream.println(string + "  %<fill>");
			fill.dump(printstream, string + "    ");
			printstream.println(string + "    "
					+ (fillRule == 0 ? "fill" : "eofill"));
			printstream.println(string + "  %</fill>");
			printstream.println(string + "  grestore");
		}
		if (stroke != null && stroke.paint != null) {
			printstream.println(string + "  gsave");
			stroke.dump(printstream, string + "  ");
			printstream.println(string + "  stroke");
			printstream.println(string + "  grestore");
		}
		printstream.println(string + "%</path>");
	}
}