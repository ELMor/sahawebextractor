/*
 * FontSpec - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public class FontSpec {
	public String family = null;

	public int weight = -2147483648;

	public String style = null;

	public String variant = null;

	public double size = 0.0;

	public double stretch = 1.0;

	public FontSpec() {
		/* empty */
	}

	public FontSpec(String string, int i, String string_0_, String string_1_,
			double d, double d_2_) {
		family = string;
		weight = i;
		style = string_0_;
		variant = string_1_;
		size = d;
		stretch = d_2_;
	}

	public FontSpec cc() {
		return new FontSpec(family, weight, style, variant, size, stretch);
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println(string + "%<font family=\"" + family + "\" "
				+ "weight=\"" + weight + "\" " + "style=\"" + style + "\" "
				+ "variant=\"" + variant + "\" " + "size=\"" + size + "\" "
				+ "stretch=\"" + stretch + "\">");
		if (stretch == 1.0)
			printstream.println(string + "  /" + family + " " + size
					+ " selectfont");
		else
			printstream.println(string + "  /" + family + " [" + size * stretch
					+ " 0 0 " + size + " 0 0] selectfont");
		printstream.println(string + "%</font>");
	}
}