/*
 * ExternalImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

import com.renderx.graphics.Image;

public class ExternalImage extends GraphicObject {
	public final Image image;

	public ExternalImage(Image image) {
		this.image = image;
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.print(string + "%<image source=\"" + image.source + "\" "
				+ "mimetype=\"" + image.mimetype + "\" " + "width=\""
				+ image.width + "\" " + "height=\"" + image.height + "\">");
		printstream.println(string + "  gsave");
		printstream.println(string + "    0.75 setgray 0 0 " + image.width
				+ " " + image.height + " rectfill");
		printstream.println(string + "  grestore");
		printstream.println(string + "%</image>");
	}
}