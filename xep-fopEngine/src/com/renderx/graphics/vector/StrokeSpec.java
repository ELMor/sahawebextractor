/*
 * StrokeSpec - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public class StrokeSpec extends GraphicObject {
	public PaintSpec paint = null;

	public double thickness = 0.0;

	public static final int LINECAP_BUTT = 0;

	public static final int LINECAP_ROUND = 1;

	public static final int LINECAP_SQUARE = 2;

	public int lineCap = 0;

	public static final int LINEJOIN_MITER = 0;

	public static final int LINEJOIN_ROUND = 1;

	public static final int LINEJOIN_BEVEL = 2;

	public int lineJoin = 0;

	public double miterLimit = 10.0;

	public double[] dashArray = null;

	public double dashPhase = 0.0;

	public StrokeSpec(PaintSpec paintspec, double d) {
		paint = paintspec;
		thickness = d;
	}

	public StrokeSpec cc() {
		StrokeSpec strokespec_0_ = new StrokeSpec(paint, thickness);
		strokespec_0_.lineCap = lineCap;
		strokespec_0_.lineJoin = lineJoin;
		strokespec_0_.miterLimit = miterLimit;
		strokespec_0_.dashArray = dashArray;
		strokespec_0_.dashPhase = dashPhase;
		return strokespec_0_;
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		if (paint != null) {
			printstream.print(string + "%<stroke thickness=\"" + thickness
					+ "\"");
			switch (lineCap) {
			case 0:
				printstream.print(" line-cap=\"butt\"");
				break;
			case 1:
				printstream.print(" line-cap=\"round\"");
				break;
			case 2:
				printstream.print(" line-cap=\"square\"");
				break;
			}
			switch (lineJoin) {
			case 0:
				printstream.print(" line-join=\"miter\" miter-limit=\""
						+ miterLimit + "\"");
				break;
			case 1:
				printstream.print(" line-join=\"round\"");
				break;
			case 2:
				printstream.print(" line-join=\"bevel\"");
				break;
			}
			if (dashArray != null) {
				printstream.print(" dash-array=\"");
				for (int i = 0; i < dashArray.length; i++) {
					if (i > 0)
						printstream.print(" ");
					printstream.print("" + dashArray[i]);
				}
				printstream.print("\" dash-phase=\"" + dashPhase + "\"");
			}
			printstream.println(">");
			printstream.println(string + "  " + thickness + " setlinewidth");
			printstream.println(string + "  " + lineCap + " setlinecap");
			printstream.println(string + "  " + lineJoin + " setlinejoin");
			if (lineJoin == 0)
				printstream.println(string + "  " + miterLimit
						+ " setmiterlimit");
			paint.dump(printstream, string + "  ");
			if (dashArray != null) {
				printstream.print(string + "  [");
				for (int i = 0; i < dashArray.length; i++) {
					if (i > 0)
						printstream.print(" ");
					printstream.print("" + dashArray[i]);
				}
				printstream.println("] " + dashPhase + " setdash");
			}
			printstream.println(string + "%</stroke>");
		}
	}
}