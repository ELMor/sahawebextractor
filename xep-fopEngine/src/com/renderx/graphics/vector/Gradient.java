/*
 * Gradient - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

import com.renderx.util.List;

public abstract class Gradient extends PaintSpec {
	public List stoplist = new List();

	public static class RadialGradient extends Gradient {
		public double xbeg;

		public double ybeg;

		public double rbeg;

		public double xend;

		public double yend;

		public double rend;

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<radial-gradient/>");
		}
	}

	public static class LinearGradient extends Gradient {
		public double xbeg;

		public double ybeg;

		public double xend;

		public double yend;

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<linear-gradient/>");
		}
	}

	public static class Stop {
		double offset = 0.0;

		OpaqueColor color = null;

		public Stop(double d, OpaqueColor opaquecolor) {
			offset = d;
			color = opaquecolor;
		}
	}
}