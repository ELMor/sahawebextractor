/*
 * ImageTree - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ImageTree {
	public double width;

	public double height;

	public double startX;

	public double startY;

	public GraphicObject root = null;

	public ImageTree(double d, double d_0_, double d_1_, double d_2_) {
		startX = d;
		startY = d_0_;
		width = d_1_;
		height = d_2_;
	}

	public ImageTree(double d, double d_3_) {
		startX = 0.0;
		startY = 0.0;
		width = d;
		height = d_3_;
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println("%<imagetree width=\"" + width + "\" height=\""
				+ height + "\">");
		printstream.println("  <</PageSize [" + width + " " + height
				+ "]>> setpagedevice");
		root.dump(printstream, "  ");
		printstream.println("  showpage");
		printstream.println("%</imagetree>");
	}

	public static void main(String[] strings) throws Exception {
		FileOutputStream fileoutputstream = null;
		if (strings.length > 0)
			fileoutputstream = new FileOutputStream(strings[0]);
		ImageTree imagetree = new ImageTree(576.0, 792.0);
		Path path = new Path();
		path.stroke = new StrokeSpec(new OpaqueColor.RGB(0.0, 0.33, 0.33), 2.0);
		path.moveto(50.0, 350.0);
		path.lineto(100.0, 325.0);
		path.arcto(25.0, 25.0, -0.5235987755982988, false, true, 150.0, 300.0);
		path.lineto(200.0, 275.0);
		path.arcto(25.0, 50.0, -0.5235987755982988, false, true, 250.0, 250.0);
		path.lineto(300.0, 225.0);
		path.arcto(25.0, 75.0, -0.5235987755982988, false, true, 350.0, 200.0);
		path.lineto(400.0, 175.0);
		path.arcto(25.0, 100.0, -0.5235987755982988, false, true, 450.0, 150.0);
		path.lineto(500.0, 125.0);
		imagetree.root = path;
		imagetree.dump((fileoutputstream == null ? System.out
				: new PrintStream(fileoutputstream)), "");
	}
}