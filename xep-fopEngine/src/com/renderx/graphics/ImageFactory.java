/*
 * ImageFactory - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.fonts.FontCatalog;
import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.MIMEType;
import com.renderx.util.MIMETypeParseException;
import com.renderx.util.Magic;
import com.renderx.util.SeekableInput;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class ImageFactory {
	private Hashtable imageClasses = new Hashtable();

	private Hashtable namespaces = new Hashtable();

	public final ErrorHandler errHandler;

	public final FontCatalog fontCatalog;

	public final URLCache cache;

	protected Hashtable imageCache = new Hashtable();

	private class Config extends DefaultHandler {
		private static final String NSURI = "http://www.renderx.com/graphics/formats";

		private Config() {
			/* empty */
		}

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes) {
			if ("".equals(string)
					|| "http://www.renderx.com/graphics/formats".equals(string)) {
				if ("format".equals(string_0_))
					registerFormat(attributes);
				else if ("namespace".equals(string_0_))
					registerNamespace(attributes);
			}
		}

		private void registerFormat(Attributes attributes) {
			String string = attributes.getValue("mime-type");
			if (string == null)
				printError("'format' descriptor has no 'mime-type' attribute.");
			else {
				StringTokenizer stringtokenizer = new StringTokenizer(string);
				if (!stringtokenizer.hasMoreTokens())
					printError("'format' descriptor has an empty 'mime-type' attribute.");
				else {
					String string_2_ = attributes.getValue("parser");
					if (string_2_ == null)
						printError("'format' descriptor for MIME type '"
								+ string + "' has no 'parser' attribute.");
					else {
						Class var_class = null;
						try {
							var_class = Class.forName(string_2_.trim());
						} catch (ClassNotFoundException classnotfoundexception) {
							printError("cannot find image parser class "
									+ string_2_.trim() + " for MIME type '"
									+ string + "'");
						}
						while (stringtokenizer.hasMoreTokens()) {
							String string_3_ = stringtokenizer.nextToken();
							Object object = null;
							MIMEType mimetype;
							try {
								mimetype = new MIMEType(string_3_);
							} catch (MIMETypeParseException mimetypeparseexception) {
								printError("invalid MIME type specifier '"
										+ string_3_ + "': "
										+ mimetypeparseexception.getMessage());
								continue;
							}
							try {
								ImageFactory.this.registerImageClass(mimetype,
										var_class);
							} catch (InvalidImageClassException invalidimageclassexception) {
								printError("invalid image parser class "
										+ var_class.getName()
										+ " for MIME type '" + string + "'");
								break;
							}
						}
					}
				}
			}
		}

		private void registerNamespace(Attributes attributes) {
			String string = attributes.getValue("uri");
			if (string == null)
				printError("'namespace' descriptor has no 'uri' attribute.");
			else {
				String string_4_ = attributes.getValue("mime-type");
				if (string_4_ == null)
					printError("'namespace' descriptor has no 'mime-type' attribute.");
				else if (string_4_.trim().length() == 0)
					printError("'namespace' descriptor has an empty 'mime-type' attribute.");
				else {
					Object object = null;
					MIMEType mimetype;
					try {
						mimetype = new MIMEType(string_4_);
					} catch (MIMETypeParseException mimetypeparseexception) {
						printError("invalid MIME type specifier '" + string_4_
								+ "': " + mimetypeparseexception.getMessage());
						return;
					}
					namespaces.put(string, mimetype);
				}
			}
		}

		private void printError(String string) {
			if (errHandler != null)
				errHandler.error("Error in image factory configuration file: "
						+ string);
		}
	}

	public ImageFactory(FontCatalog fontcatalog, URLCache urlcache,
			ErrorHandler errorhandler) {
		fontCatalog = fontcatalog;
		cache = urlcache;
		errHandler = errorhandler;
		String string = User.getProperty("com.renderx.graphics.formats");
		if (string == null)
			string = "resource:com/renderx/graphics/formats.xml";
		try {
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			xmlreader.setContentHandler(new Config());
			xmlreader.parse(new InputSource(string));
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			throw new RuntimeException("error initializing ImageFactory:"
					+ exception.toString());
		}
		Magic.init();
	}

	private void registerImageClass(MIMEType mimetype, Class var_class)
			throws InvalidImageClassException {
		if (!Image.class.isAssignableFrom(var_class))
			throw new InvalidImageClassException(var_class);
		imageClasses.put(mimetype, var_class);
	}

	private Class getImageClass(MIMEType mimetype) {
		Enumeration enumeration = imageClasses.keys();
		while (enumeration.hasMoreElements()) {
			MIMEType mimetype_5_ = (MIMEType) enumeration.nextElement();
			if (mimetype.matches(mimetype_5_))
				return (Class) imageClasses.get(mimetype_5_);
		}
		return null;
	}

	protected Image fromCache(URLSpec urlspec) {
		synchronized (imageCache) {
			return (Image) imageCache.get(makeCacheKey(urlspec));
		}
	}

	protected void toCache(URLSpec urlspec, Image image) {
		synchronized (imageCache) {
			imageCache.put(makeCacheKey(urlspec), image);
		}
	}

	public void clearCache() {
		synchronized (imageCache) {
			imageCache.clear();
		}
	}

	private static final String makeCacheKey(URLSpec urlspec) {
		if (urlspec.scheme == 1) {
			URLSpec urlspec_6_ = urlspec.getBase();
			if (urlspec_6_ != null)
				return urlspec_6_.toString() + " <!&!> " + urlspec.toString();
		}
		return urlspec.toString();
	}

	public Image makeImage(URLSpec urlspec) throws IOException,
			ImageFormatException, UnregisteredMIMETypeException {
		return makeImage(urlspec, null);
	}

	public Image makeImage(URLSpec urlspec, String string) throws IOException,
			ImageFormatException, UnregisteredMIMETypeException {
		Image image = fromCache(urlspec);
		if (string == null) {
			if (image != null)
				return image;
			string = cache.getContentType(urlspec, errHandler);
			if (string == null) {
				SeekableInput seekableinput = openSeekableStream(urlspec);
				try {
					string = Magic.MIME(urlspec.toString(), seekableinput);
				} catch (Magic.UnknownMIMETypeException unknownmimetypeexception) {
					throw new ImageFormatException(unknownmimetypeexception
							.toString());
				} finally {
					seekableinput.close();
				}
			}
		}
		Object object = null;
		MIMEType mimetype;
		try {
			mimetype = new MIMEType(string);
		} catch (MIMETypeParseException mimetypeparseexception) {
			throw new UnregisteredMIMETypeException(string,
					mimetypeparseexception.getMessage());
		}
		if (image != null && image.mimetype.equals(mimetype.toString()))
			return image;
		Class var_class = getImageClass(mimetype);
		if (var_class == null)
			throw new UnregisteredMIMETypeException(string);
		try {
			image = (Image) var_class.newInstance();
		} catch (InstantiationException instantiationexception) {
			instantiationexception.printStackTrace();
			throw new RuntimeException(instantiationexception.getMessage());
		} catch (IllegalAccessException illegalaccessexception) {
			illegalaccessexception.printStackTrace();
			throw new RuntimeException(illegalaccessexception.getMessage());
		}
		image.factory = this;
		image.source = urlspec;
		image.mimetype = mimetype.toString();
		image.parse();
		toCache(urlspec, image);
		return image;
	}

	InputStream openStream(URLSpec urlspec) throws IOException {
		return cache.openStream(urlspec, errHandler);
	}

	SeekableInput openSeekableStream(URLSpec urlspec) throws IOException {
		return cache.openSeekableStream(urlspec, errHandler);
	}

	public static void main(String[] strings) throws Exception {
		DefaultErrorHandler defaulterrorhandler = new DefaultErrorHandler();
		URLCache urlcache = new URLCache(defaulterrorhandler);
		ImageFactory imagefactory = new ImageFactory(new FontCatalog(urlcache,
				defaulterrorhandler), urlcache, defaulterrorhandler);
		if (strings.length == 2)
			System.out.println(imagefactory.makeImage(new URLSpec(strings[0]),
					strings[1]));
		else
			System.err
					.println("Usage: com.renderx.graphics.ImageFactory  <image file name> <mime-type>");
	}
}