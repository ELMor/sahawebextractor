/*
 * PDFExternalFileException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFExternalFileException extends RuntimeException {
	public PDFExternalFileException() {
		/* empty */
	}

	public PDFExternalFileException(Exception exception) {
		super(exception.toString());
	}

	public PDFExternalFileException(String string) {
		super(string);
	}
}