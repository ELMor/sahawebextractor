/*
 * Fonts - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.fonts.FontDescriptor;
import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public class Fonts {
	int fontsCount = -1;

	Hashtable fontsName = new Hashtable();

	Hashtable fontsOID = new Hashtable();

	Hashtable fontsDesc = new Hashtable();

	boolean enableFontsSubset = true;

	Fonts() {
		/* empty */
	}

	public void setEnableFontsSubset(boolean bool) {
		enableFontsSubset = bool;
	}

	public boolean getEnableFontsSubset() {
		return enableFontsSubset;
	}

	void addFont(FontDescriptor fontdescriptor, int i,
			IndirectObject indirectobject) {
		fontsCount++;
		String string = fontdescriptor.record.getMetric().fontName + "+" + i;
		fontsName.put(string, "F" + fontsCount);
		fontsOID.put(string, indirectobject);
		fontsDesc.put(string, fontdescriptor);
	}

	String getPDFFontName(FontDescriptor fontdescriptor, int i) {
		return ((String) fontsName
				.get(fontdescriptor.record.getMetric().fontName + "+" + i));
	}

	IndirectObject getFontOID(FontDescriptor fontdescriptor, int i) {
		return ((IndirectObject) fontsOID
				.get(fontdescriptor.record.getMetric().fontName + "+" + i));
	}

	void write(PDFOutputStream pdfoutputstream) {
		write(pdfoutputstream, null);
	}

	void write(PDFOutputStream pdfoutputstream, Hashtable hashtable) {
		Array array = new Array();
		Enumeration enumeration = fontsName.keys();
		int i = 0;
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			boolean bool = false;
			int i_0_;
			for (i_0_ = 0; i_0_ < i; i_0_++) {
				if (array.get(i_0_).toString().compareTo(object.toString()) > 0)
					break;
			}
			for (int i_1_ = i; i_1_ > i_0_; i_1_--)
				array.put(i_1_, array.get(i_1_ - 1));
			array.put(i_0_, object);
			i++;
		}
		for (int i_2_ = 0; i_2_ < array.length(); i_2_++) {
			String string = (String) array.get(i_2_);
			int i_3_ = new Integer(string
					.substring(string.lastIndexOf("+") + 1)).intValue();
			FontDescriptor fontdescriptor = (FontDescriptor) fontsDesc
					.get(string);
			if (fontdescriptor.record.datatype == 1) {
				T1Font t1font = new T1Font((IndirectObject) fontsOID
						.get(string), fontdescriptor, i_3_);
				t1font.setEnableSubset(enableFontsSubset);
				t1font.write(pdfoutputstream);
			} else if (fontdescriptor.record.datatype == 2) {
				if (!fontdescriptor.record.embed) {
					OneByteTTFont onebytettfont = new OneByteTTFont(
							((IndirectObject) fontsOID.get(string)),
							fontdescriptor, i_3_);
					onebytettfont.write(pdfoutputstream);
					if (fontdescriptor.hasUnmappedChars) {
						pdfoutputstream
								.warning("Some characters from '"
										+ fontdescriptor.record.getMetric().fullName
										+ "' font cannot be represented if the font is unembedded.");
						fontdescriptor.hasUnmappedChars = false;
					}
				} else {
					TTFont ttfont = new TTFont((IndirectObject) fontsOID
							.get(string), fontdescriptor, i_3_);
					ttfont.setEnableSubset(enableFontsSubset);
					ttfont.write(pdfoutputstream);
				}
			} else if (fontdescriptor.record.datatype == 3) {
				T1Font t1font = new T1Font((IndirectObject) fontsOID
						.get(string), fontdescriptor, i_3_);
				t1font.setEnableSubset(enableFontsSubset);
				t1font.write(pdfoutputstream);
			} else if (fontdescriptor.record.datatype == 4) {
				TTFont ttfont = new TTFont((IndirectObject) fontsOID
						.get(string), fontdescriptor, i_3_);
				ttfont.setEnableSubset(enableFontsSubset);
				ttfont.write(pdfoutputstream);
			}
		}
	}
}