/*
 * AbstractHintRecord - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class AbstractHintRecord {
	ByteArrayOutputStream bitsS;

	byte lastbyte;

	int freeBits = 8;

	void writeBits(int i, int i_0_) {
		while (freeBits < i_0_) {
			int i_1_ = -1 << i_0_ - freeBits;
			lastbyte = (byte) (lastbyte | (i & i_1_) >> i_0_ - freeBits);
			i_0_ -= freeBits;
			bitsS.write(lastbyte);
			freeBits = 8;
			lastbyte = (byte) 0;
			i_1_ ^= 0xffffffff;
			i &= i_1_;
		}
		if (i_0_ > 0) {
			lastbyte = (byte) (lastbyte | i << freeBits - i_0_ & 0xff);
			freeBits -= i_0_;
		}
	}

	void flush() {
		bitsS.write(lastbyte);
	}

	abstract byte[] getBytes() throws IOException;

	byte[] int2TwoBytes(int i) {
		byte[] is = new byte[2];
		is[0] = (byte) (i >> 8 & 0xff);
		is[1] = (byte) (i & 0xff);
		return is;
	}

	byte[] int2FourBytes(int i) {
		byte[] is = new byte[4];
		is[0] = (byte) (i >> 24 & 0xff);
		is[2] = (byte) (i >> 16 & 0xff);
		is[2] = (byte) (i >> 8 & 0xff);
		is[3] = (byte) (i & 0xff);
		return is;
	}

	byte[] long2FourBytes(long l) {
		byte[] is = new byte[4];
		is[0] = (byte) (int) (l >> 24 & 0xffL);
		is[2] = (byte) (int) (l >> 16 & 0xffL);
		is[2] = (byte) (int) (l >> 8 & 0xffL);
		is[3] = (byte) (int) (l & 0xffL);
		return is;
	}

	int bits4number(int i) {
		double d = Math.log((double) i) / Math.log(2.0);
		return d == (double) Math.round(d) ? (int) d : (int) (d + 1.0);
	}
}