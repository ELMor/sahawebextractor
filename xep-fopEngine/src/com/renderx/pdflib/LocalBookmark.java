/*
 * LocalBookmark - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class LocalBookmark extends Bookmark {
	Destination dest;

	LocalBookmark(IndirectObject indirectobject, String string, int i,
			int i_0_, int i_1_, float f, float f_2_, boolean bool) {
		super(indirectobject, string, i, i_0_, bool);
		dest = new Destination(i_1_, f, f_2_);
	}

	LocalBookmark(String string, int i, int i_3_, int i_4_, float f,
			float f_5_, boolean bool) {
		super((IndirectObject) null, string, i, i_3_, bool);
		dest = new Destination(i_4_, f, f_5_);
	}

	LocalBookmark(IndirectObject indirectobject, String string,
			String string_6_, int i, int i_7_, boolean bool) {
		super(indirectobject, string_6_, i, i_7_, bool);
		dest = new Destination(string);
	}

	LocalBookmark(String string, String string_8_, int i, int i_9_, boolean bool) {
		super((IndirectObject) null, string_8_, i, i_9_, bool);
		dest = new Destination(string);
	}

	LocalBookmark(String string, IndirectObject indirectobject, float f,
			float f_10_, boolean bool) {
		super((IndirectObject) null, "", 0, 0, bool);
		dest = new Destination(string);
		dest.page_id = new IndirectObject(indirectobject.num);
		dest.page_x = f;
		dest.page_y = f_10_;
	}

	void writeBookmarkBody(PDFOutputStream pdfoutputstream) {
		dest.write(pdfoutputstream);
	}

	int getPageNumber() {
		return dest.page_number;
	}

	void setPageID(IndirectObject indirectobject) {
		dest.page_id = indirectobject;
	}
}