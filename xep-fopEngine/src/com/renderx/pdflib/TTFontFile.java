/*
 * TTFontFile - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.InputStream;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.util.SeekableInput;

public class TTFontFile extends PDFFontFile {
	TTFontFile(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			boolean bool) {
		super(indirectobject, fontdescriptor, bool);
	}

	public String pdfFontType() {
		return "FontFile2";
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		this.writeBaseData(pdfoutputstream);
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		do {
			try {
				if (enableSubset && descriptor.record.subset) {
					Metric metric = descriptor.record.getMetric();
					SeekableInput seekableinput = descriptor.record
							.openSeekableFontStream();
					try {
						com.renderx.fonts.TTFont ttfont = new com.renderx.fonts.TTFont(
								seekableinput, (descriptor.record.subfont));
						for (int i_0_ = 0; i_0_ < 65536; i_0_++) {
							if (descriptor.used.checkGlyph((char) i_0_)) {
								CharMetrics charmetrics = ((CharMetrics) metric.uniTable
										.get((char) i_0_));
								ttfont.addGlyph(charmetrics.c);
							}
						}
						byte[] is = ttfont.createSubset();
						length1 = is.length;
						pdfoutputstream.print(is);
						break;
					} finally {
						seekableinput.close();
					}
				}
				InputStream inputstream = descriptor.record.openFontStream();
				try {
					byte[] is = new byte[8192];
					int i_1_;
					while ((i_1_ = inputstream.read(is)) != -1) {
						length1 += i_1_;
						pdfoutputstream.print(is, 0, i_1_);
					}
				} finally {
					inputstream.close();
				}
			} catch (Exception exception) {
				pdfoutputstream.exception(
						("Cannot output subset for True Type font '"
								+ descriptor.record.fontfile + "'"), exception);
				break;
			}
		} while (false);
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}
}