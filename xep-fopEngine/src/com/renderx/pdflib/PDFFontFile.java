/*
 * PDFFontFile - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.FontDescriptor;

public abstract class PDFFontFile extends PDFObject {
	FontDescriptor descriptor;

	boolean enableSubset;

	int length;

	IndirectObject length_id;

	int length1;

	IndirectObject length1_id;

	PDFFontFile(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			boolean bool) {
		super(indirectobject);
		descriptor = fontdescriptor;
		enableSubset = bool;
	}

	public void writeBaseData(PDFOutputStream pdfoutputstream) {
		length_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		length1_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		pdfoutputstream.println("/Length " + length_id.toStringR());
		pdfoutputstream.println("/Length1 " + length1_id.toStringR());
		if (pdfoutputstream.compression)
			pdfoutputstream.println("/Filter /FlateDecode");
	}

	public void writeSubObjects(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.printLength(length_id, length);
		pdfoutputstream.printLength(length1_id, length1);
	}

	public abstract String pdfFontType();
}