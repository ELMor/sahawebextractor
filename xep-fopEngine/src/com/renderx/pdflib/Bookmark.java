/*
 * Bookmark - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

abstract class Bookmark extends PDFObject {
	String label;

	int external_id;

	int ext_parent_id;

	IndirectObject parent_id;

	IndirectObject prev_id;

	IndirectObject next_id;

	IndirectObject first_id;

	IndirectObject last_id;

	int count;

	boolean open;

	Hashtable bookmarks;

	Bookmark(IndirectObject indirectobject, String string, int i, int i_0_,
			boolean bool) {
		super(indirectobject);
		label = string;
		external_id = i;
		ext_parent_id = i_0_;
		parent_id = null;
		prev_id = null;
		next_id = null;
		first_id = null;
		last_id = null;
		count = 0;
		bookmarks = null;
		open = bool;
	}

	void setType() {
		/* empty */
	}

	void addOutline(Bookmark bookmark_1_) {
		bookmarks.put(new Integer(bookmark_1_.getExternalID()), bookmark_1_);
		if (bookmark_1_.getParentID() == null) {
			if (first_id == null || first_id.num > bookmark_1_.getID().num)
				first_id = bookmark_1_.getID();
			if (last_id == null || last_id.num < bookmark_1_.getID().num)
				last_id = bookmark_1_.getID();
			count++;
		}
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.generateCurrentKey(this.getID().num);
		writeBookmarkBody(pdfoutputstream);
		pdfoutputstream.print("/Title ");
		pdfoutputstream.print_annotation(label);
		pdfoutputstream.println("");
		pdfoutputstream.println("/Parent " + parent_id.toStringR());
		if (next_id != null)
			pdfoutputstream.println("/Next " + next_id.toStringR());
		if (prev_id != null)
			pdfoutputstream.println("/Prev " + prev_id.toStringR());
		if (count != 0) {
			pdfoutputstream.println("/First " + first_id.toStringR());
			pdfoutputstream.println("/Last " + last_id.toStringR());
			pdfoutputstream.println("/Count " + (open ? "" : "-") + count);
		}
	}

	void setOpen(boolean bool) {
		open = bool;
	}

	int getExternalID() {
		return external_id;
	}

	int getExtParentID() {
		return ext_parent_id;
	}

	IndirectObject getParentID() {
		return parent_id;
	}

	IndirectObject getNextID() {
		return next_id;
	}

	IndirectObject getPrevID() {
		return prev_id;
	}

	IndirectObject getFirstID() {
		return first_id;
	}

	IndirectObject getLastID() {
		return last_id;
	}

	void setParentID(IndirectObject indirectobject) {
		parent_id = indirectobject;
	}

	void setNextID(IndirectObject indirectobject) {
		next_id = indirectobject;
	}

	void setPrevID(IndirectObject indirectobject) {
		prev_id = indirectobject;
	}

	void setFirstID(IndirectObject indirectobject) {
		first_id = indirectobject;
	}

	void setLastID(IndirectObject indirectobject) {
		last_id = indirectobject;
	}

	abstract void writeBookmarkBody(PDFOutputStream pdfoutputstream);

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (bookmarks != null) {
			Enumeration enumeration = bookmarks.keys();
			while (enumeration.hasMoreElements()) {
				Bookmark bookmark_2_ = (Bookmark) bookmarks.get(enumeration
						.nextElement());
				bookmark_2_.write(pdfoutputstream);
			}
		}
	}

	boolean addBookmark(Bookmark bookmark_3_) {
		if (bookmarks == null)
			bookmarks = new Hashtable();
		if (bookmark_3_.getExtParentID() == external_id) {
			if (first_id == null || first_id.num > bookmark_3_.getID().num)
				first_id = bookmark_3_.getID();
			if (last_id == null || last_id.num < bookmark_3_.getID().num)
				last_id = bookmark_3_.getID();
			count++;
			bookmark_3_.setParentID(obj_id);
			addNext(bookmark_3_);
			return true;
		}
		Bookmark bookmark_4_ = ((Bookmark) bookmarks.get(new Integer(
				bookmark_3_.getExtParentID())));
		if (bookmark_4_ != null) {
			bookmark_4_.addBookmark(bookmark_3_);
			bookmarks
					.put(new Integer(bookmark_4_.getExternalID()), bookmark_4_);
			return true;
		}
		Enumeration enumeration = bookmarks.keys();
		while (enumeration.hasMoreElements()) {
			Bookmark bookmark_5_ = (Bookmark) bookmarks.get(enumeration
					.nextElement());
			if (bookmark_5_.addBookmark(bookmark_3_)) {
				bookmarks.put(new Integer(bookmark_5_.getExternalID()),
						bookmark_5_);
				return true;
			}
		}
		return false;
	}

	void addNext(Bookmark bookmark_6_) {
		Enumeration enumeration = bookmarks.keys();
		IndirectObject indirectobject = null;
		Bookmark bookmark_7_ = null;
		while (enumeration.hasMoreElements() && indirectobject == null) {
			bookmark_7_ = (Bookmark) bookmarks.get(enumeration.nextElement());
			if (bookmark_7_.getNextID() == null)
				indirectobject = bookmark_7_.getID();
		}
		if (indirectobject != null) {
			bookmark_6_.setPrevID(indirectobject);
			bookmark_7_.setNextID(bookmark_6_.getID());
			bookmarks
					.put(new Integer(bookmark_7_.getExternalID()), bookmark_7_);
		}
		bookmarks.put(new Integer(bookmark_6_.getExternalID()), bookmark_6_);
	}

	void finish(Pages pages) {
		if (bookmarks != null) {
			Enumeration enumeration = bookmarks.keys();
			while (enumeration.hasMoreElements()) {
				Bookmark bookmark_8_ = (Bookmark) bookmarks.get(enumeration
						.nextElement());
				bookmark_8_.finish(pages);
				bookmarks.put(new Integer(bookmark_8_.getExternalID()),
						bookmark_8_);
			}
		}
	}
}