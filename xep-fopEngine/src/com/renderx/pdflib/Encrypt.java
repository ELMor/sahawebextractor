/*
 * Encrypt - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.OutputStream;

public class Encrypt extends PDFObject {
	Encryption encryption;

	String userpassword;

	String ownerpassword;

	int userflags;

	byte[] ovalue = new byte[32];

	byte[] uvalue = new byte[32];

	int pvalue;

	Encrypt(IndirectObject indirectobject, String string, String string_0_,
			int i, String string_1_) {
		super(indirectobject);
		userpassword = string;
		ownerpassword = string_0_;
		userflags = i;
		encryption = new Encryption(string, string_0_, i, this.toString());
		ovalue = encryption.ovalue;
		uvalue = encryption.uvalue;
		pvalue = encryption.pnumber;
	}

	void writeBody(PDFOutputStream pdfoutputstream) throws PDFIOException {
		pdfoutputstream.println("/Filter /Standard");
		pdfoutputstream.println("/V 1");
		pdfoutputstream.println("/R 2");
		pdfoutputstream.print("/O (");
		pdfoutputstream.print_quote_bytes(ovalue);
		pdfoutputstream.println(")");
		pdfoutputstream.print("/U (");
		pdfoutputstream.print_quote_bytes(uvalue);
		pdfoutputstream.println(")");
		pdfoutputstream.println("/P " + pvalue);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) throws PDFIOException {
		/* empty */
	}

	OutputStream getCipherOutputStream(OutputStream outputstream) {
		return encryption.getCipherOutputStream(outputstream);
	}

	void generateCurrentKey(int i) {
		encryption.generateCurrentKey(i);
	}

	public String IDtoStr() {
		return encryption.IDtoStr();
	}

	byte[] encryptArray(byte[] is) {
		return encryption.encryptArray(is);
	}
}