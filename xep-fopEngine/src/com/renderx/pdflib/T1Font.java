/*
 * T1Font - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;

public class T1Font extends OneByteFont {
	T1Font(IndirectObject indirectobject, FontDescriptor fontdescriptor, int i) {
		super(indirectobject, fontdescriptor, i);
	}

	String getSubType() {
		return "Type1";
	}

	void writeEncoding(PDFOutputStream pdfoutputstream, Encoding encoding) {
		pdfEncoding = new PDFEncoding(new IndirectObject(
				pdfoutputstream.lastObjectNumber++), encoding);
		pdfoutputstream.println("/Encoding " + pdfEncoding.getID().toStringR());
	}
}