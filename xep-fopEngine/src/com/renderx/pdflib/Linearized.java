/*
 * Linearized - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class Linearized extends PDFObject {
	public long fileLength;

	public int hintStreamObjectOffset;

	public int hintStreamObjectLength;

	public int firstPageID;

	public long firstPageEndOffset;

	public int numberOfPages;

	public long mainXRefFirstLineOffset;

	public static int size = 160;

	public static long smallHeaderSize = (long) (15 + size);

	Linearized(IndirectObject indirectobject) {
		super(indirectobject);
	}

	void setType() {
		/* empty */
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Linearized 1");
		pdfoutputstream.println("/L " + fileLength);
		pdfoutputstream.println("/H [ " + hintStreamObjectOffset + " "
				+ hintStreamObjectLength + " ]");
		pdfoutputstream.println("/O " + firstPageID);
		pdfoutputstream.println("/E " + firstPageEndOffset);
		pdfoutputstream.println("/N " + numberOfPages);
		pdfoutputstream.println("/T " + mainXRefFirstLineOffset);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}

	void writeTail(PDFOutputStream pdfoutputstream) throws PDFIOException {
		super.writeTail(pdfoutputstream);
		long l = (long) pdfoutputstream.size() - start_offset;
		for (long l_0_ = l; l_0_ < (long) size; l_0_++)
			pdfoutputstream.print(" ");
	}
}