/*
 * Links - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class Links {
	int linksCount = -1;

	Hashtable links = new Hashtable();

	Links() {
		/* empty */
	}

	void addLink(Annot annot) {
		linksCount++;
		this.links.put(new Integer(linksCount), annot);
	}

	void write(PDFOutputStream pdfoutputstream, Pages pages) {
		Enumeration enumeration = this.links.elements();
		while (enumeration.hasMoreElements()) {
			Annot annot = (Annot) enumeration.nextElement();
			annot.write(pdfoutputstream);
		}
	}
}