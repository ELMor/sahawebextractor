/*
 * ExtGStates - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class ExtGStates {
	int gsCount = -1;

	Hashtable fillOpacities = new Hashtable();

	Hashtable strokeOpacities = new Hashtable();

	Hashtable AIS = new Hashtable();

	Hashtable fillOpacityOID = new Hashtable();

	Hashtable strokeOpacityOID = new Hashtable();

	Hashtable AIS_OID = new Hashtable();

	public class PDFExtGState extends PDFObject {
		String entryType;

		String entryValue;

		PDFExtGState(IndirectObject indirectobject, String string,
				String string_0_) {
			super(indirectobject);
			entryType = string;
			entryValue = string_0_;
		}

		String getType() {
			return "ExtGState";
		}

		void writeBody(PDFOutputStream pdfoutputstream) {
			pdfoutputstream.println("/" + entryType + " " + entryValue);
		}

		void writeSubObjects(PDFOutputStream pdfoutputstream) {
			/* empty */
		}
	}

	ExtGStates() {
		/* empty */
	}

	String getFillOpacityName(float f) {
		return (String) fillOpacities.get(new Float(f));
	}

	String getStrokeOpacityName(float f) {
		return (String) strokeOpacities.get(new Float(f));
	}

	String getAISName(boolean bool) {
		return (String) AIS.get(new Boolean(bool));
	}

	void addFillOpacity(float f, IndirectObject indirectobject) {
		gsCount++;
		fillOpacities.put(new Float(f), "GS" + gsCount);
		fillOpacityOID.put(new Float(f), indirectobject);
	}

	void addStrokeOpacity(float f, IndirectObject indirectobject) {
		gsCount++;
		strokeOpacities.put(new Float(f), "GS" + gsCount);
		strokeOpacityOID.put(new Float(f), indirectobject);
	}

	void addAIS(boolean bool, IndirectObject indirectobject) {
		gsCount++;
		AIS.put(new Boolean(bool), "GS" + gsCount);
		AIS_OID.put(new Boolean(bool), indirectobject);
	}

	IndirectObject getFillOpacityOID(float f) {
		return (IndirectObject) fillOpacityOID.get(new Float(f));
	}

	IndirectObject getStrokeOpacityOID(float f) {
		return (IndirectObject) strokeOpacityOID.get(new Float(f));
	}

	IndirectObject getAIS_OID(boolean bool) {
		return (IndirectObject) AIS_OID.get(new Boolean(bool));
	}

	void write(PDFOutputStream pdfoutputstream) {
		Object object = null;
		Enumeration enumeration = fillOpacities.keys();
		while (enumeration.hasMoreElements()) {
			Float var_float = (Float) enumeration.nextElement();
			PDFExtGState pdfextgstate = new PDFExtGState(
					((IndirectObject) fillOpacityOID.get(var_float)), "ca",
					var_float.toString());
			pdfextgstate.write(pdfoutputstream);
		}
		enumeration = strokeOpacities.keys();
		while (enumeration.hasMoreElements()) {
			Float var_float = (Float) enumeration.nextElement();
			PDFExtGState pdfextgstate = new PDFExtGState(
					((IndirectObject) strokeOpacityOID.get(var_float)), "CA",
					var_float.toString());
			pdfextgstate.write(pdfoutputstream);
		}
		enumeration = AIS.keys();
		while (enumeration.hasMoreElements()) {
			Boolean var_boolean = (Boolean) enumeration.nextElement();
			PDFExtGState pdfextgstate = new PDFExtGState(
					(IndirectObject) AIS_OID.get(var_boolean), "AIS",
					var_boolean.toString());
			pdfextgstate.write(pdfoutputstream);
		}
	}
}