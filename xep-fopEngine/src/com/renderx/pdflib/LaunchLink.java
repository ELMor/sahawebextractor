/*
 * LaunchLink - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class LaunchLink extends Annot {
	String filename;

	LaunchLink(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_, int i) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
	}

	String getSubType() {
		return "Link";
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/A ");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Type/Action /S /Launch\n");
		pdfoutputstream.println("/F ");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Type/FileSpec\n");
		pdfoutputstream.println("/F (" + filename + ")\n");
		pdfoutputstream.end_dict();
		pdfoutputstream.end_dict();
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}