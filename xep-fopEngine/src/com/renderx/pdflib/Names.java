/*
 * Names - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class Names extends PDFObject {
	public NameTree dests;

	Names(IndirectObject indirectobject) {
		super(indirectobject);
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Dests " + dests.getID().toStringR());
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		dests.write(pdfoutputstream);
	}
}