/*
 * CMap - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.util.UnicodeData;

public class CMap extends PDFObject {
	int length;

	IndirectObject length_id;

	FontDescriptor descriptor;

	Encoding encoding;

	boolean enableSubset;

	CMap(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			Encoding encoding, boolean bool) {
		super(indirectobject);
		descriptor = fontdescriptor;
		this.encoding = encoding;
		enableSubset = bool;
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		length_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		pdfoutputstream.println("/Length " + length_id.toStringR());
		if (pdfoutputstream.compression)
			pdfoutputstream.println("/Filter /FlateDecode");
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		writeCMap(pdfoutputstream);
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.printLength(length_id, length);
	}

	void writeCMap(PDFOutputStream pdfoutputstream) {
		pdfoutputstream
				.println("%!PS-Adobe-3.0 Resource-CMap\n/CIDInit /ProcSet findresource begin\n12 dict begin\nbegincmap\n/CIDSystemInfo\n<< /Registry (Adobe)\n/Ordering (UCS)\n/Supplement 0\n>> def\n/CMapName /AdHoc-UCS def\n/CMapType 2 def");
		Metric metric = descriptor.record.getMetric();
		int i = 0;
		int i_0_ = 0;
		Object object = null;
		int[] is;
		if (metric.isCIDFont) {
			is = new int[65536];
			for (int i_1_ = 0; i_1_ < 65536; i_1_++)
				is[i_1_] = 0;
			Object object_2_ = null;
			for (int i_3_ = 1; i_3_ < 65536; i_3_++) {
				CharMetrics charmetrics = (CharMetrics) metric.uniTable
						.get((char) i_3_);
				if (charmetrics != null
						&& (!enableSubset || i_3_ <= 0 || descriptor.used
								.checkGlyph((char) i_3_))
						&& is[charmetrics.c] == 0) {
					is[charmetrics.c] = i_3_;
					i_0_++;
					if (i < charmetrics.c)
						i = charmetrics.c;
				}
			}
			pdfoutputstream
					.println("1 begincodespacerange\n<0000> <FFFF>\nendcodespacerange");
		} else {
			is = new int[256];
			for (int i_4_ = 0; i_4_ < 256; i_4_++)
				is[i_4_] = 0;
			i = 255;
			for (int i_5_ = 1; i_5_ < 65536; i_5_++) {
				int i_6_ = encoding.getCode((char) i_5_);
				if (i_6_ != -1
						&& (!enableSubset || descriptor.used
								.checkGlyph((char) i_5_)) && is[i_6_] == 0) {
					is[i_6_] = i_5_;
					i_0_++;
				}
			}
			pdfoutputstream
					.println("1 begincodespacerange\n<00> <FF>\nendcodespacerange");
		}
		int i_7_ = 0;
		while (i_7_ <= i && i_0_ > 0) {
			int i_8_ = i_0_ < 100 ? i_0_ : 100;
			i_0_ -= i_8_;
			pdfoutputstream.println("" + i_8_ + " beginbfchar");
			for (/**/; i_8_ > 0 && i_7_ <= i; i_7_++) {
				if (is[i_7_] != 0) {
					pdfoutputstream.print("<"
							+ (metric.isCIDFont ? fourHexDigits(i_7_)
									: twoHexDigits(i_7_)) + "> <");
					char[] cs = (char[]) UnicodeData.LIGATURES
							.get((char) is[i_7_]);
					if (cs != null) {
						for (int i_9_ = 0; i_9_ < cs.length; i_9_++)
							pdfoutputstream.print(fourHexDigits(cs[i_9_]));
					} else
						pdfoutputstream.print(fourHexDigits(is[i_7_]));
					pdfoutputstream.println(">");
					i_8_--;
				}
			}
			pdfoutputstream.println("endbfchar");
		}
		pdfoutputstream
				.println("endcmap\nCMapName currentdict /CMap defineresource pop\nend\nend");
	}

	String fourHexDigits(int i) {
		String string = Integer.toHexString(i);
		string = "0000".substring(0, 4 - string.length()) + string;
		return string;
	}

	String twoHexDigits(int i) {
		String string = Integer.toHexString(i);
		string = "00".substring(0, 2 - string.length()) + string;
		return string;
	}
}