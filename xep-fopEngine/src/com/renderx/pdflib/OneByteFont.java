/*
 * OneByteFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;

public abstract class OneByteFont extends PDFFont {
	CMap cmap;

	PDFFontDescriptor pdfFontDescriptor;

	PDFEncoding pdfEncoding;

	OneByteFont(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			int i) {
		super(indirectobject, fontdescriptor);
		encoding = i;
		if (enableSubset && fontdescriptor.record.subset
				&& fontdescriptor.record.embed)
			this.subsetName();
	}

	abstract String getSubType();

	abstract void writeEncoding(PDFOutputStream pdfoutputstream,
			Encoding encoding);

	void writeAddInfo(PDFOutputStream pdfoutputstream) {
		Metric metric = descriptor.record.getMetric();
		Encoding encoding = (Encoding) descriptor.encodingTable
				.get(this.encoding);
		Encoding encoding_0_ = new Encoding();
		boolean bool = false;
		for (int i = 0; i < 256; i++) {
			char c = encoding.byte2uni[i];
			if (c != 0 && descriptor.used.checkGlyph(c)) {
				encoding_0_.uni2byte.put(c, new Integer(i));
				encoding_0_.table[i] = metric.ucm(c);
				encoding_0_.byte2uni[i] = c;
				bool = true;
			}
		}
		if (!bool) {
			CharMetrics charmetrics = metric.ucm(' ');
			do {
				if (charmetrics == null) {
					charmetrics = metric.missingGlyph;
					if (charmetrics == null)
						break;
				}
			} while (false);
			encoding_0_.assignCode(' ', charmetrics);
		}
		int i = encoding_0_.getFirstCode();
		int i_1_ = encoding_0_.getLastCode();
		if (i == -1 || i_1_ == -1)
			throw new PDFException("Internal error: empty encoding");
		pdfoutputstream.println("/FirstChar " + i);
		pdfoutputstream.println("/LastChar " + i_1_);
		pdfoutputstream.print("/Widths [");
		for (int i_2_ = i; i_2_ <= i_1_; i_2_++) {
			if (i_2_ % 16 == 15)
				pdfoutputstream.println();
			else if (i_2_ > 0)
				pdfoutputstream.print(" ");
			if (encoding_0_.table[i_2_] == null)
				pdfoutputstream.print("0");
			else
				pdfoutputstream.print(PDFOutputStream
						.pdf_float((double) encoding_0_.table[i_2_].wx));
		}
		pdfoutputstream.println("]");
		writeEncoding(pdfoutputstream, encoding_0_);
		pdfFontDescriptor = new PDFFontDescriptor(new IndirectObject(
				pdfoutputstream.lastObjectNumber++), descriptor, subsetName
				+ metric.fontName, this.make_fontflags(), enableSubset);
		cmap = new CMap(new IndirectObject(pdfoutputstream.lastObjectNumber++),
				descriptor, encoding_0_, enableSubset);
		pdfoutputstream.println("/FontDescriptor "
				+ pdfFontDescriptor.getID().toStringR());
		pdfoutputstream.println("/ToUnicode " + cmap.getID().toStringR());
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (pdfFontDescriptor != null) {
			if (pdfEncoding != null)
				pdfEncoding.write(pdfoutputstream);
			pdfFontDescriptor.write(pdfoutputstream);
			cmap.write(pdfoutputstream);
		}
	}
}