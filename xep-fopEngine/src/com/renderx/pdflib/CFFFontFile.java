/*
 * CFFFontFile - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.CFFFont;
import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.fonts.TTMetric;
import com.renderx.util.SeekableByteArrayInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.SeekableInputOffsetFilter;

public class CFFFontFile extends PDFFontFile {
	CFFFontFile(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			boolean bool) {
		super(indirectobject, fontdescriptor, bool);
	}

	public String pdfFontType() {
		return "FontFile3";
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		writeBaseData(pdfoutputstream);
		if (descriptor.record.getMetric().isCIDFont)
			pdfoutputstream.println("/Subtype /CIDFontType0C");
		else
			pdfoutputstream.println("/Subtype /Type1C");
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		try {
			Metric metric = descriptor.record.getMetric();
			if (enableSubset && descriptor.record.subset) {
				SeekableInput seekableinput = descriptor.record
						.openSeekableFontStream();
				byte[] is = new byte[(int) ((TTMetric) metric).cff_length];
				seekableinput.seek(((TTMetric) metric).cff_offset);
				seekableinput.readFully(is);
				SeekableByteArrayInputStream seekablebytearrayinputstream = new SeekableByteArrayInputStream(
						is);
				try {
					CFFFont cfffont = new CFFFont(seekablebytearrayinputstream,
							((TTMetric) metric).getCFFMetric());
					for (int i_0_ = 0; i_0_ < 65536; i_0_++) {
						if (descriptor.used.checkGlyph((char) i_0_)) {
							CharMetrics charmetrics = ((CharMetrics) metric.uniTable
									.get((char) i_0_));
							if (charmetrics != null)
								cfffont.addGlyph(charmetrics.c);
						}
					}
					byte[] is_1_ = cfffont.createSubset();
					pdfoutputstream.print(is_1_);
				} catch (Exception exception) {
					exception.printStackTrace();
				} finally {
					seekablebytearrayinputstream.close();
					seekableinput.close();
				}
			} else {
				SeekableInput seekableinput = descriptor.record
						.openSeekableFontStream();
				SeekableInputOffsetFilter seekableinputoffsetfilter = new SeekableInputOffsetFilter(
						seekableinput, (((TTMetric) metric).cff_offset));
				try {
					long l = ((TTMetric) metric).cff_length;
					byte[] is = new byte[16384];
					while (l > 0L) {
						int i_2_ = l < (long) is.length ? (int) l : is.length;
						l -= (long) i_2_;
						seekableinputoffsetfilter.read(is, 0, i_2_);
						pdfoutputstream.print(is, 0, i_2_);
					}
				} catch (Exception exception) {
					exception.printStackTrace();
				} finally {
					seekableinputoffsetfilter.close();
					seekableinput.close();
				}
			}
		} catch (Exception exception) {
			pdfoutputstream.exception(("Cannot output subset for CFF font '"
					+ descriptor.record.fontfile + "'"), exception);
		}
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	public void writeBaseData(PDFOutputStream pdfoutputstream) {
		length_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		pdfoutputstream.println("/Length " + length_id.toStringR());
		if (pdfoutputstream.compression)
			pdfoutputstream.println("/Filter /FlateDecode");
	}

	public void writeSubObjects(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.printLength(length_id, length);
	}
}