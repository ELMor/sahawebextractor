/*
 * PDFOutputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.StringTokenizer;

import com.renderx.pdflib.parser.PDFString;
import com.renderx.util.DeflaterOutputStream;
import com.renderx.util.Hashtable;
import com.renderx.util.SeekableByteArrayInputStream;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.User;

public class PDFOutputStream extends OutputStream {
	private static final int FLOAT_PRECISION_HIGH = 1000;

	private static final int FLOAT_PRECISION_LOW = 10;

	private static final String pdfEncodingCharset = "\u02c6\u02d9\u02dd\u02db\u02da\u02dc\u2022\u2020\u2021\u2026\u2014\u2013\u0192\u2044\u2039\u203a\u2212\u2030\u201e\u201c\u201d\u2018\u2019\u201a\u2122\ufb01\ufb02\u0141\u0152\u0160\u0178\u017d\u0131\u0142\u0153\u0161\u017e\u20ac\t\r\n\u00a0\u00a1\u00a2\u00a3\u00a4\u00a5\u00a6\u00a7\u00a8\u00a9\u00aa\u00ab\u00ac\u00ad\u00ae\u00af\u00b0\u00b1\u00b2\u00b3\u00b4\u00b5\u00b6\u00b7\u00b8\u00b9\u00ba\u00bb\u00bc\u00bd\u00be\u00bf\u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cc\u00cd\u00ce\u00cf\u00d0\u00d1\u00d2\u00d3\u00d4\u00d5\u00d6\u00d7\u00d8\u00d9\u00da\u00db\u00dc\u00dd\u00de\u00df\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ec\u00ed\u00ee\u00ef\u00f0\u00f1\u00f2\u00f3\u00f4\u00f5\u00f6\u00f7\u00f8\u00f9\u00fa\u00fb\u00fc\u00fd\u00fe\u00ff !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

	public static PrintStream dump = null;

	public int lastObjectNumber;

	OutputStream currentOS;

	DataOutputStream output;

	ByteArrayOutputStream outputBA;

	DeflaterOutputStream deflaterOS;

	OutputStream cipherOS;

	public boolean USE_TEMP_FILE;

	File tmpdir;

	File tmpfile;

	ByteArrayOutputStream tmpbaos;

	DataOutputStream realoutput;

	public int filenumber;

	public PDFTrailer trailer;

	boolean useEncryption;

	boolean saveUseEncryption;

	Encrypt encrypt;

	public boolean compression;

	private boolean linearize;

	public boolean unicodeAnnotations;

	IndirectObject stream_id;

	int stream_start_offset;

	int stream_end_offset;

	boolean printText;

	PDFDocument doc;

	public static synchronized void dumpprintln(String string) {
		dumpprint(string + "\n");
	}

	public static synchronized void dumpprint(String string) {
		dump.print(string);
	}

	File getTempFile() {
		File file;
		do
			file = (new File(tmpdir, ("pdfgen_"
					+ Integer.toHexString((int) System.currentTimeMillis())
					+ "_" + Integer.toHexString(++filenumber) + ".tmp")));
		while (file.exists());
		return file;
	}

	public PDFOutputStream(OutputStream outputstream, PDFDocument pdfdocument)
			throws IOException {
		String string = User.getProperty("com.renderx.pdflib.debug.DUMP");
		String string_0_ = User
				.getProperty("com.renderx.pdflib.debug.DUMP.file");
		if (string != null && string.equalsIgnoreCase("on")
				|| string_0_ != null) {
			if (string_0_ != null) {
				try {
					dump = new PrintStream(
							new FileOutputStream(string_0_, true));
				} catch (Exception exception) {
					System.err.println("Cannot redirect dump to " + string_0_
							+ ": " + exception);
					dump = System.err;
				}
			} else
				dump = System.err;
		}
		USE_TEMP_FILE = true;
		tmpdir = null;
		tmpfile = null;
		tmpbaos = null;
		filenumber = 0;
		stream_id = null;
		output = new DataOutputStream(outputstream);
		doc = pdfdocument;
		currentOS = output;
		compression = true;
		linearize = false;
		useEncryption = false;
		saveUseEncryption = false;
		unicodeAnnotations = true;
		printText = false;
	}

	boolean getLinearize() {
		return linearize;
	}

	void setLinearize(boolean bool) {
		linearize = bool;
		if (linearize) {
			if (useEncryption)
				useEncryption = false;
			realoutput = output;
			try {
				if (USE_TEMP_FILE) {
					tmpfile = getTempFile();
					output = new DataOutputStream(new FileOutputStream(tmpfile));
				} else {
					tmpbaos = new ByteArrayOutputStream();
					output = new DataOutputStream(tmpbaos);
				}
				currentOS = output;
			} catch (IOException ioexception) {
				throw new PDFIOException(ioexception);
			}
		}
	}

	void finish() {
		try {
			output.flush();
			if (this.linearize) {
				output.close();
				Object object = null;
				SeekableInput seekableinput;
				if (USE_TEMP_FILE)
					seekableinput = new SeekableFileInputStream(tmpfile);
				else {
					seekableinput = new SeekableByteArrayInputStream(tmpbaos
							.toByteArray());
					tmpbaos = null;
				}
				Linearize linearize = new Linearize(seekableinput, realoutput,
						doc.imageFactory, doc.fontCatalog, doc.errorHandler);
				linearize.setUseCompression(compression);
				if (saveUseEncryption)
					linearize.setUseEncryption(encrypt);
				linearize.linearize();
				realoutput.flush();
				seekableinput.close();
				if (USE_TEMP_FILE)
					tmpfile.delete();
			}
		} catch (RuntimeException runtimeexception) {
			throw runtimeexception;
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new PDFIOException(exception);
		}
	}

	public void write(int i) throws IOException {
		currentOS.write(i);
	}

	public void setUseEncryption(String string, String string_1_, int i) {
		if (!linearize)
			useEncryption = true;
		saveUseEncryption = true;
		encrypt = new Encrypt(new IndirectObject(lastObjectNumber), string,
				string_1_, i, output.toString());
		if (!linearize)
			lastObjectNumber++;
	}

	public boolean getUseEncryption() {
		return useEncryption;
	}

	public OutputStream getCurrentOutputStream() {
		return currentOS;
	}

	public void startCompressedStream() {
		if (useEncryption) {
			cipherOS = encrypt.getCipherOutputStream(output);
			currentOS = cipherOS;
		}
	}

	public void finishCompressedStream() {
		if (useEncryption) {
			try {
				cipherOS.flush();
			} catch (Exception exception) {
				throw new PDFIOException("Can not flush cipher stream");
			}
			cipherOS = null;
		}
		currentOS = output;
	}

	public boolean streamStarted() {
		if (stream_id != null)
			return true;
		return false;
	}

	public IndirectObject beginStream(IndirectObject indirectobject)
			throws PDFWrongElementException {
		if (streamStarted())
			throw new PDFWrongElementException(
					"You are already sending stream data");
		stream_id = indirectobject;
		IndirectObject indirectobject_2_ = new IndirectObject(stream_id.num + 1);
		trailer.xref.addObjectOffset(stream_id, (long) size());
		generateCurrentKey(stream_id.num);
		println(stream_id + " obj");
		println("<</Length " + indirectobject_2_.toStringR());
		if (compression)
			println("/Filter /FlateDecode");
		println(">>");
		println("stream");
		stream_start_offset = size();
		startStream();
		indirectobject_2_ = new IndirectObject(indirectobject_2_.num + 1);
		return indirectobject_2_;
	}

	public void startStream() {
		if (useEncryption) {
			cipherOS = encrypt.getCipherOutputStream(output);
			currentOS = cipherOS;
		}
		if (compression) {
			deflaterOS = new DeflaterOutputStream(currentOS);
			currentOS = deflaterOS;
		}
	}

	public void finishStream() {
		if (compression) {
			try {
				deflaterOS.finish();
			} catch (Exception exception) {
				throw new PDFIOException("Can not close compressed stream");
			}
			deflaterOS = null;
		}
		if (useEncryption) {
			try {
				cipherOS.flush();
			} catch (Exception exception) {
				throw new PDFIOException("Can not flush cipher stream");
			}
			cipherOS = null;
		}
		currentOS = output;
	}

	public void endStream() throws PDFWrongElementException {
		endText();
		if (stream_id == null)
			throw new PDFWrongElementException(
					"You finished sending stream data");
		finishStream();
		stream_end_offset = size();
		println("endstream");
		println("endobj");
		printLength(new IndirectObject(stream_id.num + 1), stream_end_offset
				- stream_start_offset);
		stream_id = null;
	}

	void printLength(IndirectObject indirectobject, int i) {
		trailer.xref.addObjectOffset(indirectobject, (long) size());
		println(indirectobject + " obj");
		println("" + i);
		println("endobj");
	}

	public void printVector(String string, Hashtable hashtable, int i) {
		if (i >= 0) {
			print("/" + string + " ");
			print("[");
			for (int i_3_ = 0; i_3_ <= i; i_3_++)
				print(((IndirectObject) hashtable.get(new Integer(i_3_)))
						.toStringR()
						+ " ");
			print("]");
			print("\n");
		}
	}

	public void print(char c) {
		try {
			currentOS.write((byte) c & 0xff);
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	public void print(int i) {
		try {
			currentOS.write((byte) i & 0xff);
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	public void print(byte[] is) {
		print(is, 0, is.length);
	}

	public void print(byte[] is, int i, int i_4_) {
		try {
			currentOS.write(is, i, i_4_);
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	public void print(String string) {
		int i = string.length();
		try {
			for (int i_5_ = 0; i_5_ < i; i_5_++)
				print(string.charAt(i_5_));
			currentOS.flush();
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	public void println() {
		print("\n");
	}

	public void println(String string) {
		print(string + "\n");
	}

	public void printByte(byte i) {
		try {
			currentOS.write(i);
			currentOS.flush();
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	void print_quote_bytes(byte[] is) {
		try {
			for (int i = 0; i < is.length; i++) {
				int i_6_ = is[i];
				if (0 <= i_6_ && i_6_ < 32) {
					currentOS.write(92);
					currentOS.write(48 + (i_6_ >> 6 & 0x7));
					currentOS.write(48 + (i_6_ >> 3 & 0x7));
					currentOS.write(48 + (i_6_ & 0x7));
				} else if (i_6_ == 40 || i_6_ == 41 || i_6_ == 92) {
					currentOS.write(92);
					currentOS.write(i_6_);
				} else
					currentOS.write(i_6_);
			}
		} catch (IOException ioexception) {
			throw new PDFIOException(ioexception);
		}
	}

	public void print_annotation(String string) {
		print("(");
		boolean bool = false;
		if (unicodeAnnotations) {
			StringTokenizer stringtokenizer = (new StringTokenizer(
					string,
					"\u02c6\u02d9\u02dd\u02db\u02da\u02dc\u2022\u2020\u2021\u2026\u2014\u2013\u0192\u2044\u2039\u203a\u2212\u2030\u201e\u201c\u201d\u2018\u2019\u201a\u2122\ufb01\ufb02\u0141\u0152\u0160\u0178\u017d\u0131\u0142\u0153\u0161\u017e\u20ac\t\r\n\u00a0\u00a1\u00a2\u00a3\u00a4\u00a5\u00a6\u00a7\u00a8\u00a9\u00aa\u00ab\u00ac\u00ad\u00ae\u00af\u00b0\u00b1\u00b2\u00b3\u00b4\u00b5\u00b6\u00b7\u00b8\u00b9\u00ba\u00bb\u00bc\u00bd\u00be\u00bf\u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cc\u00cd\u00ce\u00cf\u00d0\u00d1\u00d2\u00d3\u00d4\u00d5\u00d6\u00d7\u00d8\u00d9\u00da\u00db\u00dc\u00dd\u00de\u00df\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ec\u00ed\u00ee\u00ef\u00f0\u00f1\u00f2\u00f3\u00f4\u00f5\u00f6\u00f7\u00f8\u00f9\u00fa\u00fb\u00fc\u00fd\u00fe\u00ff !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"));
			bool = stringtokenizer.hasMoreTokens();
		}
		if (bool)
			new PDFString(string).unicode_quote(this);
		else
			new PDFString(string).pdfencoding_quote(this);
		print(")");
	}

	public void printString(String string) {
		print("(");
		new PDFString(string).quote(this);
		print(")");
	}

	public void printStringUnicode(String string) {
		print("(");
		new PDFString(string).unicode_quote(this);
		print(")");
	}

	public void printStringPDFEncoding(String string) {
		print("(");
		new PDFString(string).pdfencoding_quote(this);
		print(")");
	}

	static String pdf_float(double d, int i) {
		if (d == 0.0)
			return "0";
		String string = "";
		if (d < 0.0) {
			d = -d;
			string = "-";
		}
		int i_7_ = (int) Math.floor(d);
		int i_8_ = (int) Math.round((d - (double) i_7_) * (double) i);
		if (i_8_ == i) {
			i_7_++;
			i_8_ = 0;
		}
		if (i_7_ != 0) {
			string += i_7_;
			if (i_8_ != 0) {
				string += ".";
				for (int i_9_ = i / 10; i_8_ / i_9_ == 0; i_9_ /= 10)
					string += "0";
				for (/**/; i_8_ % 10 == 0; i_8_ /= 10) {
					/* empty */
				}
				string += i_8_;
			}
		} else {
			string += "0.";
			for (/**/; d < 0.1; d *= 10.0)
				string += "0";
			for (i_8_ = (int) Math.round(d * (double) i * 10.0); i_8_ % 10 == 0; i_8_ /= 10) {
				/* empty */
			}
			string += i_8_;
		}
		return string;
	}

	static String pdf_float(double d) {
		return pdf_float(d, 1000);
	}

	static String pdf_float_coarse(double d) {
		return pdf_float(d, 10);
	}

	void setTextMatrix(float f, float f_10_, float f_11_, float f_12_) {
		startText();
		print(pdf_float((double) f) + " " + 0 + " " + pdf_float((double) f_10_)
				+ " " + 1 + " ");
		println(pdf_float((double) f_11_) + " " + pdf_float((double) f_12_)
				+ " Tm");
	}

	void begin_dict() {
		print("<<");
	}

	void end_dict() {
		print(">>\n");
	}

	public int size() {
		return output.size();
	}

	void startText() {
		if (!printText) {
			printText = true;
			println("BT");
		}
	}

	void endText() {
		if (printText) {
			printText = false;
			println("ET");
		}
	}

	public void generateCurrentKey(int i) {
		if (useEncryption)
			encrypt.generateCurrentKey(i);
	}

	PDFObject getEncryptObject() {
		return encrypt;
	}

	String getEncryptionID() {
		return encrypt.IDtoStr();
	}

	public byte[] encryptArray(byte[] is) {
		return encrypt.encryptArray(is);
	}

	public void warning(String string) {
		if (doc.errorHandler != null)
			doc.errorHandler.warning(string);
	}

	public void error(String string) {
		if (doc.errorHandler != null)
			doc.errorHandler.error(string);
	}

	public void exception(String string, Exception exception) {
		if (doc.errorHandler != null)
			doc.errorHandler.exception(string, exception);
	}

	public void close() throws IOException {
		output.close();
	}
}