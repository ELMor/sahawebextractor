/*
 * PDFObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.util.List;

public abstract class PDFObject {
	List indirectObjects = null;

	IndirectObject obj_id = null;

	long start_offset;

	PDFObject(IndirectObject indirectobject) {
		obj_id = indirectobject;
	}

	IndirectObject getID() {
		return obj_id;
	}

	String getType() {
		return null;
	}

	abstract void writeBody(PDFOutputStream pdfoutputstream)
			throws PDFIOException;

	public void write(PDFOutputStream pdfoutputstream) throws PDFIOException {
		if (obj_id == null)
			throw new PDFIOException("Object " + getType()
					+ " don`t have obj_id");
		pdfoutputstream.trailer.xref.addObjectOffset(obj_id,
				(long) pdfoutputstream.size());
		pdfoutputstream.generateCurrentKey(getID().num);
		writeHead(pdfoutputstream);
		writeBody(pdfoutputstream);
		writeTail(pdfoutputstream);
		writeSubObjects(pdfoutputstream);
	}

	public void writeHead(PDFOutputStream pdfoutputstream)
			throws PDFIOException {
		start_offset = (long) pdfoutputstream.size();
		pdfoutputstream.println(obj_id + " obj");
		pdfoutputstream.print("<<");
		if (getType() != null)
			pdfoutputstream.println("/Type/" + getType());
	}

	void writeTail(PDFOutputStream pdfoutputstream) throws PDFIOException {
		pdfoutputstream.println(">>\nendobj");
	}

	abstract void writeSubObjects(PDFOutputStream pdfoutputstream)
			throws PDFIOException;
}