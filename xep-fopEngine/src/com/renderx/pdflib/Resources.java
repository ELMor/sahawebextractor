/*
 * Resources - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.List;

public class Resources extends PDFObject {
	private final List fonts = new List();

	private final List images = new List();

	private final List spotcolors = new List();

	private final List extgstates = new List();

	private final Hashtable fonttable = new Hashtable();

	private final Hashtable imagetable = new Hashtable();

	private final Hashtable spotcolortable = new Hashtable();

	private final Hashtable extgstatetable = new Hashtable();

	Resources(IndirectObject indirectobject) {
		super(indirectobject);
	}

	void setType() {
		/* empty */
	}

	void addFont(String string) {
		if (fonttable.get(string) == null) {
			fonttable.put(string, string);
			fonts.append(string);
		}
	}

	void addImage(String string) {
		if (imagetable.get(string) == null) {
			imagetable.put(string, string);
			images.append(string);
		}
	}

	void addSpotColor(String string) {
		if (spotcolortable.get(string) == null) {
			spotcolortable.put(string, string);
			spotcolors.append(string);
		}
	}

	void addFillOpacity(String string) {
		if (extgstatetable.get(string) == null) {
			extgstatetable.put(string, string);
			extgstates.append(string);
		}
	}

	void addStrokeOpacity(String string) {
		if (extgstatetable.get(string) == null) {
			extgstatetable.put(string, string);
			extgstates.append(string);
		}
	}

	void addAIS(String string) {
		if (extgstatetable.get(string) == null) {
			extgstatetable.put(string, string);
			extgstates.append(string);
		}
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.print("/ProcSet [/PDF");
		if (!images.isEmpty())
			pdfoutputstream.print(" /ImageC");
		if (!fonts.isEmpty())
			pdfoutputstream.print(" /Text");
		pdfoutputstream.println("]");
		if (!fonts.isEmpty()) {
			pdfoutputstream.print("/Font <<");
			Enumeration enumeration = fonts.elements();
			while (enumeration.hasMoreElements())
				pdfoutputstream.println((String) enumeration.nextElement());
			pdfoutputstream.println(">>");
		}
		if (!images.isEmpty()) {
			pdfoutputstream.print("/XObject <<");
			Enumeration enumeration = images.elements();
			while (enumeration.hasMoreElements())
				pdfoutputstream.println((String) enumeration.nextElement());
			pdfoutputstream.println(">>");
		}
		if (!spotcolors.isEmpty()) {
			pdfoutputstream.print("/ColorSpace <<");
			Enumeration enumeration = spotcolors.elements();
			while (enumeration.hasMoreElements())
				pdfoutputstream.println((String) enumeration.nextElement());
			pdfoutputstream.println(">>");
		}
		if (!extgstates.isEmpty()) {
			pdfoutputstream.print("/ExtGState <<");
			Enumeration enumeration = extgstates.elements();
			while (enumeration.hasMoreElements())
				pdfoutputstream.println((String) enumeration.nextElement());
			pdfoutputstream.println(">>");
		}
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}