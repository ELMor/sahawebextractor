/*
 * PDFIOException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFIOException extends RuntimeException {
	public PDFIOException() {
		/* empty */
	}

	public PDFIOException(Exception exception) {
		super(exception.toString());
	}

	public PDFIOException(String string) {
		super(string);
	}
}