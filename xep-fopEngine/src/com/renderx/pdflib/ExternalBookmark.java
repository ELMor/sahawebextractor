/*
 * ExternalBookmark - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class ExternalBookmark extends Bookmark {
	String url;

	boolean openNewWindow;

	ExternalBookmark(IndirectObject indirectobject, String string, int i,
			int i_0_, String string_1_, boolean bool, boolean bool_2_) {
		super(indirectobject, string, i, i_0_, bool);
		url = string_1_;
		openNewWindow = bool_2_;
	}

	ExternalBookmark(String string, int i, int i_3_, String string_4_,
			boolean bool, boolean bool_5_) {
		super(null, string, i, i_3_, bool);
		url = string_4_;
		openNewWindow = bool_5_;
	}

	void writeBookmarkBody(PDFOutputStream pdfoutputstream) {
		WebLink.createAction(pdfoutputstream, url, openNewWindow);
	}
}