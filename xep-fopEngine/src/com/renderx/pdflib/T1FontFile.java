/*
 * T1FontFile - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.fonts.PFxFont;
import com.renderx.util.SeekableInput;

public class T1FontFile extends PDFFontFile {
	int length2;

	IndirectObject length2_id;

	int length3;

	IndirectObject length3_id;

	T1FontFile(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			boolean bool) {
		super(indirectobject, fontdescriptor, bool);
	}

	public String pdfFontType() {
		return "FontFile";
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		this.writeBaseData(pdfoutputstream);
		length2_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		length3_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
		pdfoutputstream.println("/Length2 " + length2_id.toStringR());
		pdfoutputstream.println("/Length3 " + length3_id.toStringR());
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		try {
			SeekableInput seekableinput = descriptor.record
					.openSeekableFontStream();
			try {
				PFxFont pfxfont = new PFxFont(seekableinput);
				if (enableSubset && descriptor.record.subset) {
					Metric metric = descriptor.record.getMetric();
					pfxfont.newSubset();
					for (int i_0_ = 0; i_0_ < 65536; i_0_++) {
						if (descriptor.used.checkGlyph((char) i_0_)) {
							CharMetrics charmetrics = metric.ucm((char) i_0_);
							if (charmetrics != null)
								pfxfont.addGlyph(charmetrics.n,
										(Encoding) descriptor.encodingTable
												.get(0));
						}
					}
					pfxfont.endSubset();
				}
				pdfoutputstream.print(pfxfont.head);
				length1 = pfxfont.head.length();
				if (enableSubset && descriptor.record.subset) {
					pdfoutputstream.print(pfxfont.subsetbody);
					length2 = pfxfont.subsetbody.length;
				} else {
					pdfoutputstream.print(pfxfont.body);
					length2 = pfxfont.body.length;
				}
				pdfoutputstream.println(pfxfont.tail);
				length3 = pfxfont.tail.length();
			} finally {
				seekableinput.close();
			}
		} catch (Exception exception) {
			pdfoutputstream.exception(("Cannot output subset for Type 1 font '"
					+ descriptor.record.fontfile + "', the font is broken"),
					exception);
		}
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	public void writeSubObjects(PDFOutputStream pdfoutputstream) {
		super.writeSubObjects(pdfoutputstream);
		pdfoutputstream.printLength(length2_id, length2);
		pdfoutputstream.printLength(length3_id, length3);
	}
}