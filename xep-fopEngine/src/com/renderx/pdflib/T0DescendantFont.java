/*
 * T0DescendantFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.FontRecord;
import com.renderx.fonts.Metric;
import com.renderx.fonts.TTMetric;
import com.renderx.util.Array;
import com.renderx.util.UnicodeTable;

public class T0DescendantFont extends PDFFont {
	PDFFontDescriptor pdfFontDescriptor;

	T0DescendantFont(IndirectObject indirectobject,
			FontDescriptor fontdescriptor, boolean bool) {
		super(indirectobject, fontdescriptor);
		enableSubset = bool;
		if (enableSubset && fontdescriptor.record.subset
				&& fontdescriptor.record.embed)
			this.subsetName();
	}

	String getSubType() {
		if (descriptor.record.getMetric().isCFF)
			return "CIDFontType0";
		return "CIDFontType2";
	}

	void writeAddInfo(PDFOutputStream pdfoutputstream) {
		FontRecord fontrecord = descriptor.record;
		String string = "Adobe";
		String string_0_ = "";
		String string_1_ = "0";
		Metric metric = fontrecord.getMetric();
		if (metric.isCFF) {
			try {
				string = ((TTMetric) metric).getCFFMetric().registry;
				string_0_ = ((TTMetric) metric).getCFFMetric().ordering;
				string_1_ = String
						.valueOf(((TTMetric) metric).getCFFMetric().supplement);
			} catch (Exception exception) {
				pdfoutputstream.exception("Cannot extract CFF data; ",
						exception);
			}
		}
		pdfoutputstream.println("/CIDSystemInfo <<");
		pdfoutputstream.println("/Registry (" + string + ")");
		pdfoutputstream.println("/Ordering (" + string_0_ + ")");
		pdfoutputstream.println("/Supplement " + string_1_ + " >>");
		pdfFontDescriptor = new PDFFontDescriptor(new IndirectObject(
				pdfoutputstream.lastObjectNumber++), descriptor,
				(subsetName + fontrecord.getMetric().fontName), this
						.make_fontflags(), enableSubset);
		pdfoutputstream.println("/FontDescriptor "
				+ pdfFontDescriptor.getID().toStringR());
		writeTTFWidths(pdfoutputstream);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		pdfFontDescriptor.write(pdfoutputstream);
	}

	void writeTTFWidths(PDFOutputStream pdfoutputstream) {
		Metric metric = descriptor.record.getMetric();
		UnicodeTable unicodetable = new UnicodeTable();
		if (metric.missingGlyph != null)
			unicodetable.put((char) metric.missingGlyph.c, new Double(
					(double) metric.missingGlyph.wx));
		for (int i = 0; i < 256; i++) {
			Array array = (Array) metric.uniTable.ranges.get(i);
			if (array != null) {
				for (int i_2_ = 0; i_2_ < 256; i_2_++) {
					CharMetrics charmetrics = (CharMetrics) array.get(i_2_);
					if (charmetrics != null
							&& descriptor.used
									.checkGlyph((char) (i << 8 | i_2_)))
						unicodetable.put((char) charmetrics.c, new Double(
								(double) charmetrics.wx));
				}
			}
		}
		pdfoutputstream.println("/W [");
		boolean bool = false;
		for (int i = 0; i < 256; i++) {
			Array array = (Array) unicodetable.ranges.get(i);
			if (array == null) {
				if (bool) {
					pdfoutputstream.print("]");
					bool = false;
				}
			} else {
				for (int i_3_ = 0; i_3_ < 256; i_3_++) {
					Double var_double = (Double) array.get(i_3_);
					if (var_double == null) {
						if (bool) {
							pdfoutputstream.print("]");
							bool = false;
						}
					} else if (bool)
						pdfoutputstream.print(" "
								+ PDFOutputStream.pdf_float(var_double
										.doubleValue()));
					else {
						pdfoutputstream.print("" + (i << 8 | i_3_) + "["
								+ var_double);
						bool = true;
					}
				}
			}
		}
		if (bool)
			pdfoutputstream.print("]");
		pdfoutputstream.println("\n]");
	}
}