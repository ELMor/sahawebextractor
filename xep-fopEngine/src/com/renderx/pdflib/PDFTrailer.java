/*
 * PDFTrailer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFTrailer {
	public Info info;

	public Integer info_id;

	public Catalog catalog;

	public Integer catalog_id;

	public PDFObject encrypt;

	public PDFXRefTable xref = new PDFXRefTable();

	public long prev = -1L;

	public PDFTrailer() {
		info_id = catalog_id = new Integer(-1);
	}

	void write(PDFOutputStream pdfoutputstream) {
		xref.write(pdfoutputstream);
		pdfoutputstream.println("trailer");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Size " + xref.offsets.length());
		pdfoutputstream.println("/Info "
				+ (info != null ? info.getID().toStringR()
						: new IndirectObject(info_id).toStringR()));
		pdfoutputstream.println("/Root "
				+ (catalog != null ? catalog.getID().toStringR()
						: new IndirectObject(catalog_id).toStringR()));
		if (pdfoutputstream.getUseEncryption()) {
			pdfoutputstream.println("/Encrypt " + encrypt.getID().toStringR());
			pdfoutputstream.println("/ID[<" + pdfoutputstream.getEncryptionID()
					+ "><" + pdfoutputstream.getEncryptionID() + ">]");
		}
		if (prev > 0L)
			pdfoutputstream.println("/Prev " + prev);
		pdfoutputstream.end_dict();
		pdfoutputstream.println("startxref");
		pdfoutputstream.println("" + xref.offset);
		pdfoutputstream.println("%%EOF");
	}
}