/*
 * Token - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

public class Token {
	public static final byte T_NOTDEF = 0;

	public static final byte T_NAME = 1;

	public static final byte T_STRING = 2;

	public static final byte T_UNSIGNED_INTEGER = 3;

	public static final byte T_R_MARKER = 4;

	public static final byte T_START_ARRAY_DELIMITER = 5;

	public static final byte T_END_ARRAY_DELIMITER = 6;

	public static final byte T_START_DICT_DELIMITER = 7;

	public static final byte T_END_DICT_DELIMITER = 8;

	public static final byte T_OBJ_MARKER = 9;

	public static final byte T_ENDOBJ_MARKER = 10;

	public static final byte T_STREAM_MARKER = 11;

	public static final byte T_STARTXREF_MARKER = 12;

	public static final byte T_SIMPLE = 13;

	public byte type = 0;

	public String value = null;

	public Token(Token token_0_) {
		type = token_0_.type;
		value = token_0_.value;
	}

	public Token(byte i, String string) {
		type = i;
		value = string;
	}

	public String toString() {
		switch (type) {
		case 0:
			return "notdef";
		case 1:
			return "Name '" + value + "'";
		case 2:
			return "String '" + value + "'";
		case 3:
			return "UnsignetInteger '" + value + "'";
		case 4:
			return "RMarker";
		case 5:
			return "StartArrayDelimiter";
		case 6:
			return "EndArrayDelimiter";
		case 7:
			return "StartDictDelimiter";
		case 8:
			return "EndDictDelimiter";
		case 9:
			return "StartObjMarker";
		case 10:
			return "EndObjMarker";
		case 11:
			return "StreamMarker";
		case 12:
			return "StartXrefMarker";
		case 13:
			return "Simple '" + value + "'";
		default:
			return "WRONG!";
		}
	}
}