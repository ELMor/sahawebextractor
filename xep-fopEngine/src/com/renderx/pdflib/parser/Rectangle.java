/*
 * Rectangle - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

public class Rectangle {
	public float x1;

	public float y1;

	public float x2;

	public float y2;

	public Rectangle(float f, float f_0_, float f_1_, float f_2_) {
		x1 = f;
		y1 = f_0_;
		x2 = f_1_;
		y2 = f_2_;
	}

	public String toString() {
		return "" + x1 + " " + y1 + " " + x2 + " " + y2;
	}

	public float getWidth() {
		return x2 - x1;
	}

	public float getHeight() {
		return y2 - y1;
	}
}