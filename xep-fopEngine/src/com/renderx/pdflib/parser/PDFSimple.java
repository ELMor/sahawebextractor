/*
 * PDFSimple - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.List;

public class PDFSimple extends PDFElement {
	public String value;

	PDFSimple(String string) {
		value = string;
	}

	public String toString() {
		return value;
	}

	void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException {
		pdfoutputstream.write(value.getBytes());
	}

	List getIndirectObjects() {
		return new List();
	}
}