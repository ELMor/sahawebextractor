/*
 * ParseUtils - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.graphics.ImageFormatException;
import com.renderx.util.Hashtable;

class ParseUtils {
	public static synchronized boolean isDigit(char c) {
		if (c >= '0' && c <= '9')
			return true;
		return false;
	}

	public static synchronized boolean isNumber(char c) {
		if (isDigit(c) || c == '.' || c == '+' || c == '-')
			return true;
		return false;
	}

	public static synchronized long readDigits(String string) {
		String string_0_ = "";
		int i = 0;
		int i_1_ = string.length();
		char c = string.charAt(0);
		while (i < i_1_) {
			if (isDigit(c))
				break;
			i++;
			c = string.charAt(i);
		}
		while (i < i_1_ && isDigit(c)) {
			string_0_ += c;
			if (++i < i_1_)
				c = string.charAt(i);
		}
		return new Long(string_0_).longValue();
	}

	public static synchronized int getIntFromIndirectObject(
			PDFElement pdfelement) throws IOException, ImageFormatException {
		if (pdfelement instanceof PDFIndirectObject)
			return ((PDFIndirectObject) pdfelement).intValue();
		throw new ImageFormatException("Broken indicert object "
				+ pdfelement.toString());
	}

	public static synchronized int getIntFromHash(Hashtable hashtable, int i) {
		return ((Integer) hashtable.get(new Integer(i))).intValue();
	}

	public static synchronized boolean isSpace(char c) {
		if (c == '\t' || c == '\n' || c == '\r' || c == ' ')
			return true;
		return false;
	}

	public static synchronized boolean isTerminal(char c) {
		if (c == '}' || c == '{' || c == '%' || c == '[' || c == ']'
				|| c == '/' || c == '(' || c == '<' || c == '>' || isSpace(c))
			return true;
		return false;
	}
}