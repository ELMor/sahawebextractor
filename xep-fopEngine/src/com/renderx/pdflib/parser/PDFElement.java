/*
 * PDFElement - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.List;

public abstract class PDFElement {
	abstract void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException;

	abstract List getIndirectObjects();
}