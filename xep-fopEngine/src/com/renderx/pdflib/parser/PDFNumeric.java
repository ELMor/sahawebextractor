/*
 * PDFNumeric - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

public class PDFNumeric extends PDFSimple {
	public boolean isUnsignedInteger;

	public PDFNumeric(String string, boolean bool) {
		super(string);
		isUnsignedInteger = bool;
	}
}