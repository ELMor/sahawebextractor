/*
 * PDFStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

import com.renderx.pdflib.PDFOutputStream;

public class PDFStream extends PDFDictionary {
	public long streamOffset = -1L;

	public long streamLength = -1L;

	PDFElement filters;

	byte[] array;

	public long stream_size;

	public PDFStream(PDFDictionary pdfdictionary) {
		dict = pdfdictionary.dict;
	}

	void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance, int i)
			throws IOException {
		this.writeDict(pdfoutputstream, pdfinstance);
		pdfoutputstream.println("stream");
		pdfoutputstream.generateCurrentKey(i);
		long l = (long) pdfoutputstream.size();
		pdfoutputstream.startCompressedStream();
		pdfoutputstream.print(array);
		pdfoutputstream.finishCompressedStream();
		stream_size = (long) pdfoutputstream.size() - l;
		pdfoutputstream.print("endstream");
	}

	String minFilter() throws IOException {
		if (filters == null)
			return "";
		if (filters instanceof PDFName) {
			String string = ((PDFName) filters).value;
			return uncompressTill(string);
		}
		if (filters instanceof PDFArray) {
			String string = "";
			for (int i = 0; i < ((PDFArray) filters).array.length(); i++)
				string += uncompressTill(((PDFName) ((PDFArray) filters).array
						.get(i)).value);
			return string;
		}
		throw new IOException("Broken filter for page");
	}

	String uncompressTill(String string) {
		if (string.equals("/FlateDecode"))
			return "";
		return string;
	}

	void decompress() throws IOException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<decompress>");
		if (filters == null) {
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("</decompress>");
		} else if (filters instanceof PDFName) {
			String string = ((PDFName) filters).value;
			decompress(string);
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("</decompress>");
		} else if (filters instanceof PDFArray) {
			String string = "";
			for (int i = 0; (i < ((PDFArray) filters).array.length() && decompress(((PDFName) ((PDFArray) filters).array
					.get(i)).value)); i++) {
				/* empty */
			}
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("</decompress>");
		} else
			throw new IOException("Broken filter for page");
	}

	boolean decompress(String string) throws IOException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<filter name=\"" + string + "\"/>");
		if (string.equals("/FlateDecode")) {
			decompressFlate();
			return true;
		}
		if (string.equals("/ASCIIHEXDecode")) {
			decompressASCIIHEX();
			return true;
		}
		if (string.equals("/ASCII85Decode")) {
			decompressASCII85();
			return true;
		}
		if (string.equals("/RunLengthDecode")) {
			decompressRunLength();
			return true;
		}
		if (string.equals("/LZWDecode"))
			return true;
		return false;
	}

	void decompressFlate() throws IOException {
		ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(
				array);
		InflaterInputStream inflaterinputstream = new InflaterInputStream(
				bytearrayinputstream);
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		byte[] is = new byte[1024];
		boolean bool = false;
		int i;
		while ((i = inflaterinputstream.read(is)) > 0)
			bytearrayoutputstream.write(is, 0, i);
		inflaterinputstream.close();
		array = bytearrayoutputstream.toByteArray();
		bytearrayoutputstream.close();
	}

	void decompressASCIIHEX() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		int i = -1;
		for (int i_0_ = 0; i_0_ < array.length; i_0_++) {
			int i_1_;
			if ((i_1_ = hexval((char) array[i_0_])) != -1) {
				if (i == -1)
					i = i_1_;
				else {
					bytearrayoutputstream.write((i << 4) + i_1_);
					i = -1;
				}
			}
		}
		array = bytearrayoutputstream.toByteArray();
		bytearrayoutputstream.close();
	}

	private int hexval(char c) {
		switch (c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return c - '0';
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
			return c - 'a' + '\n';
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
			return c - 'A' + '\n';
		default:
			return -1;
		}
	}

	private int ascii85val(char c) {
		if (c < '!' || c > 'u')
			return -1;
		return c - '!';
	}

	void decompressASCII85() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		int i = 0;
		int[] is = new int[5];
		for (int i_2_ = 0; i_2_ < array.length; i_2_++) {
			if (array[i_2_] == 126)
				break;
			if (array[i_2_] == 122 && i == 0) {
				bytearrayoutputstream.write(0);
				bytearrayoutputstream.write(0);
				bytearrayoutputstream.write(0);
				bytearrayoutputstream.write(0);
			} else {
				int i_3_;
				if ((i_3_ = ascii85val((char) array[i_2_])) != -1) {
					is[i] = i_3_;
					if (++i == 5) {
						i = 0;
						int i_4_ = 0;
						for (int i_5_ = 0; i_5_ < 5; i_5_++)
							i_4_ = i_4_ * 85 + is[i_5_];
						bytearrayoutputstream.write((byte) (i_4_ >> 24));
						bytearrayoutputstream.write((byte) (i_4_ >> 16));
						bytearrayoutputstream.write((byte) (i_4_ >> 8));
						bytearrayoutputstream.write((byte) i_4_);
					}
				}
			}
		}
		if (i == 1)
			throw new IOException("Illegal length in ASCII85Decode.");
		for (int i_6_ = i; i_6_ < 5; i_6_++)
			is[i_6_] = 84;
		int i_7_ = 0;
		for (int i_8_ = 0; i_8_ < 5; i_8_++)
			i_7_ = i_7_ * 85 + is[i_8_];
		if (i == 2)
			bytearrayoutputstream.write((byte) (i_7_ >> 24));
		else if (i == 3) {
			bytearrayoutputstream.write((byte) (i_7_ >> 24));
			bytearrayoutputstream.write((byte) (i_7_ >> 16));
		} else if (i == 4) {
			bytearrayoutputstream.write((byte) (i_7_ >> 24));
			bytearrayoutputstream.write((byte) (i_7_ >> 16));
			bytearrayoutputstream.write((byte) (i_7_ >> 8));
		}
		array = bytearrayoutputstream.toByteArray();
		bytearrayoutputstream.close();
	}

	void decompressRunLength() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		int i = -1;
		for (int i_9_ = 0; i_9_ < array.length; i_9_++) {
			if (i == -1) {
				i = array[i_9_] < 0 ? (int) (256 + array[i_9_]) : array[i_9_];
				if (i == 128)
					break;
			} else if (i > 128) {
				i = 257 - i;
				for (int i_10_ = 0; i_10_ <= i; i_10_++)
					bytearrayoutputstream.write(array[i_9_]);
				i = -1;
			} else {
				bytearrayoutputstream.write(array[i_9_]);
				i--;
			}
		}
		array = bytearrayoutputstream.toByteArray();
		bytearrayoutputstream.close();
	}
}