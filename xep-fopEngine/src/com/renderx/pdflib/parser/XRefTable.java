/*
 * XRefTable - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.graphics.ImageFormatException;
import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.Hashtable;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;

import junit.framework.Assert;
import junit.framework.TestCase;

public class XRefTable {
	int root_id = 0;

	int info_id = 0;

	public Hashtable obj_offset = new Hashtable();

	public static final class Test extends TestCase {
		class IntegerHashtable {
			Hashtable ht;

			IntegerHashtable(String string) throws Exception {
				DataInputStream datainputstream = (new DataInputStream(
						ClassLoader.getSystemResourceAsStream(string)));
				ht = new Hashtable();
				String string_0_;
				while ((string_0_ = datainputstream.readLine()) != null)
					ht.put(new Integer(string_0_.substring(0, string_0_
							.indexOf(" "))), new Long(string_0_
							.substring(string_0_.indexOf(" ") + 1)));
				datainputstream.close();
			}

			boolean equals(Hashtable hashtable) {
				if (ht.size() != hashtable.size())
					return false;
				Enumeration enumeration = ht.keys();
				while (enumeration.hasMoreElements()) {
					Integer integer = (Integer) enumeration.nextElement();
					if (!hashtable.containsKey(integer))
						return false;
					Long var_long = (Long) ht.get(integer);
					Long var_long_1_ = (Long) hashtable.get(integer);
					if (var_long.longValue() != var_long_1_.longValue())
						return false;
				}
				return true;
			}
		}

		class NameHashtable {
			Hashtable ht;

			NameHashtable(String string) throws Exception {
				DataInputStream datainputstream = (new DataInputStream(
						ClassLoader.getSystemResourceAsStream(string)));
				ht = new Hashtable();
				String string_2_;
				while ((string_2_ = datainputstream.readLine()) != null)
					ht.put(string_2_.substring(0, string_2_.indexOf(" ")),
							string_2_.substring(string_2_.indexOf(" ") + 1));
				datainputstream.close();
			}

			boolean equals(Hashtable hashtable) {
				if (ht.size() != hashtable.size())
					return false;
				Enumeration enumeration = ht.keys();
				while (enumeration.hasMoreElements()) {
					String string = (String) enumeration.nextElement();
					if (!hashtable.containsKey(string))
						return false;
					String string_3_ = (String) ht.get(string);
					String string_4_ = (String) hashtable.get(string);
					if (!string_3_.equals(string_4_))
						return false;
				}
				return true;
			}
		}

		protected void setUp() {
			/* empty */
		}

		public void testOneXrefHashtable() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/oneXRT.pdf",
						"1.tst");
				XRefTable xreftable = new XRefTable(
						new SeekableFileInputStream("1.tst"));
				IntegerHashtable integerhashtable = (new IntegerHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/oneXRT.test"));
				Assert
						.assertTrue(integerhashtable
								.equals(xreftable.obj_offset));
				Object object = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		public void testOneXrefRoot() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/oneXRT.pdf",
						"1.tst");
				XRefTable xreftable = new XRefTable(
						new SeekableFileInputStream("1.tst"));
				NameHashtable namehashtable = (new NameHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/oneRoot.test"));
				Hashtable hashtable = new Hashtable();
				hashtable.put("Root", "" + xreftable.getRoot());
				Assert.assertTrue(namehashtable.equals(hashtable));
				Object object = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		public void testManyXrefHashtable() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/manyXRT.pdf",
						"1.tst");
				XRefTable xreftable = new XRefTable(
						new SeekableFileInputStream("1.tst"));
				IntegerHashtable integerhashtable = (new IntegerHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/manyXRT.test"));
				Assert
						.assertTrue(integerhashtable
								.equals(xreftable.obj_offset));
				Object object = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		private void resourceToFile(String string, String string_5_)
				throws IOException {
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string);
			FileOutputStream fileoutputstream = new FileOutputStream(string_5_);
			byte[] is = new byte[1024];
			int i;
			while ((i = inputstream.read(is, 0, 1024)) >= 0)
				fileoutputstream.write(is, 0, i);
			fileoutputstream.close();
		}
	}

	public XRefTable(SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		readCrossTables(getStartXref(seekableinput), seekableinput);
	}

	public int getRoot() {
		return root_id;
	}

	public int getInfo() {
		return info_id;
	}

	public long getOffset(String string) {
		return getOffset(new Integer(string).intValue());
	}

	public long getOffset(int i) {
		return getLongFromHash(obj_offset, i);
	}

	private int getStartXref(SeekableInput seekableinput) throws IOException {
		long l = seekableinput.length();
		long l_6_ = l < 64L ? 0L : l - 64L;
		byte[] is = new byte[64];
		seekableinput.seek(l_6_);
		seekableinput.readFully(is);
		int i;
		for (i = 63; i >= 0 && (is[i] < 48 || is[i] > 57); i--) {
			/* empty */
		}
		String string = "";
		for (/**/; i >= 0 && is[i] >= 48 && is[i] <= 57; i--)
			string = (char) is[i] + string;
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getStartXref value=\"" + string
					+ "\"/>");
		return new Integer(string).intValue();
	}

	private void readCrossTables(int i, SeekableInput seekableinput)
			throws IOException, ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream
					.dumpprintln("<readCrossTable offset=\"" + i + "\">");
		seekableinput.seek((long) i);
		if (!seekableinput.readLine().equals("xref"))
			throw new ImageFormatException("!Wrong xref offset " + i);
		Object object = null;
		String string;
		while (!(string = seekableinput.readLine()).startsWith("trailer")) {
			int i_7_ = (int) ParseUtils.readDigits(string);
			int i_8_ = (int) (ParseUtils.readDigits(string.substring(string
					.indexOf(" ") + 1)));
			for (int i_9_ = 0; i_9_ < i_8_; i_9_++) {
				StringTokenizer stringtokenizer = new StringTokenizer(
						seekableinput.readLine(), " ");
				long l = ParseUtils.readDigits(stringtokenizer.nextToken());
				int i_10_ = (int) ParseUtils.readDigits(stringtokenizer
						.nextToken());
				String string_11_ = stringtokenizer.nextToken();
				if (string_11_.startsWith("n")
						&& !obj_offset.containsKey(new Integer(i_7_ + i_9_))) {
					obj_offset.put(new Integer(i_7_ + i_9_), new Long(l));
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<object number=\""
								+ (i_7_ + i_9_) + "\" offset=\"" + l + "\"/>");
				}
			}
		}
		PDFDictionary pdfdictionary = (PDFDictionary) ElementParser
				.parse(new Lexer(seekableinput));
		if (pdfdictionary.dict.containsKey("/Encrypt"))
			throw new ImageFormatException(
					"Encrypted pdf files weren`t supported");
		if (pdfdictionary.dict.containsKey("/Root") && root_id == 0) {
			root_id = ParseUtils
					.getIntFromIndirectObject((PDFElement) pdfdictionary.dict
							.get("/Root"));
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("<Root number=\"" + root_id
						+ "\"/>");
		}
		if (pdfdictionary.dict.containsKey("/Info") && info_id == 0) {
			info_id = ParseUtils
					.getIntFromIndirectObject((PDFElement) pdfdictionary.dict
							.get("/Info"));
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("<Info number=\"" + info_id
						+ "\"/>");
		}
		if (pdfdictionary.dict.containsKey("/Prev"))
			readCrossTables(new Integer(((PDFNumeric) pdfdictionary.dict
					.get("/Prev")).value).intValue(), seekableinput);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</readCrossTable>");
	}

	public long getLongFromHash(Hashtable hashtable, int i) {
		return ((Long) hashtable.get(new Integer(i))).longValue();
	}

	public String toString() {
		Object object = null;
		if (obj_offset == null)
			return null;
		String string = "<xref>\n";
		Enumeration enumeration = obj_offset.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			string += ("<object number=\"" + integer + "\" offset=\""
					+ getOffset(integer.intValue()) + "\">\n");
		}
		string += "</xref>\n";
		return string;
	}

	public static void main(String[] strings) throws Exception {
		System.out.println(new XRefTable(
				new SeekableFileInputStream(strings[0])));
	}
}