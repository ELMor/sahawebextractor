/*
 * PDFArray - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.Array;
import com.renderx.util.List;

public class PDFArray extends PDFElement {
	public Array array = new Array();

	void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException {
		pdfoutputstream.write(91);
		for (int i = 0; i < array.length(); i++) {
			((PDFElement) array.get(i)).write(pdfoutputstream, pdfinstance);
			pdfoutputstream.write(32);
		}
		pdfoutputstream.write(93);
	}

	public List getIndirectObjects() {
		List list = new List();
		for (int i = 0; i < array.length(); i++) {
			List list_0_ = ((PDFElement) array.get(i)).getIndirectObjects();
			if (list_0_ != null)
				list.append(list_0_);
		}
		return list;
	}

	public String toString() {
		String string = "[";
		for (int i = 0; i < array.length(); i++)
			string += (PDFElement) array.get(i) + " ";
		string += "]";
		return string;
	}
}