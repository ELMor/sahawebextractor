/*
 * PageOffsetHintRecord - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public class PageOffsetHintRecord extends AbstractHintRecord {
	public int leastNumber;

	public long firstObjectLocation;

	public int bits4number;

	public int leastLength;

	public int bits4length;

	public int contentStreamOffset;

	public int bits4content;

	public int contentStreamLength;

	public int bits4contentLength;

	public int bits4sharedNumber;

	public int bits4sharedIdentifer;

	public int bits4numerator;

	public int denomenator;

	Array records;

	class PageOffsetHintString {
		int objects;

		long length;

		int shared;

		int sharedid;

		int numerator;

		int contentstreamoffset;

		int contentstreamlength;

		PageOffsetHintString(int i, long l, int i_0_, int i_1_, int i_2_,
				int i_3_, int i_4_) {
			/* empty */
		}
	}

	PageOffsetHintRecord(Array array, Hashtable hashtable,
			Hashtable hashtable_5_, Hashtable hashtable_6_) {
		int i = -2147483648;
		int i_7_ = 2147483647;
		Array array_8_ = new Array();
		leastNumber = 2147483647;
		int i_9_ = -2147483648;
		leastLength = 2147483647;
		int i_10_ = -2147483648;
		for (int i_11_ = 0; i_11_ < array.length(); i_11_++) {
			int i_12_ = 1;
			int i_13_ = 0;
			Enumeration enumeration = null;
			if (i_11_ == 0)
				enumeration = hashtable.keys();
			else {
				Hashtable hashtable_14_ = (Hashtable) hashtable_6_
						.get(new Integer(i_11_));
				if (hashtable_14_ != null)
					enumeration = hashtable_14_.keys();
			}
			if (enumeration != null) {
				while (enumeration.hasMoreElements()) {
					Integer integer = (Integer) enumeration.nextElement();
					if (i < integer.intValue())
						i = integer.intValue();
					if (i_7_ > integer.intValue())
						i_7_ = integer.intValue();
					i_13_ += ((Long) hashtable_5_.get(integer)).intValue();
					i_12_++;
				}
			}
			i_13_ += ((Long) hashtable_5_.get(array.get(i_11_))).intValue();
			if (leastNumber > i_12_)
				leastNumber = i_12_;
			if (i_9_ < i_12_)
				i_9_ = i_12_;
			if (leastLength > i_12_)
				leastLength = i_13_;
			if (i_10_ < i_12_)
				i_10_ = i_12_;
			array_8_.put(i_11_, new Long((long) i_13_));
		}
		bits4number = this.bits4number(i_9_ - leastNumber);
		bits4length = this.bits4number(i_10_ - leastLength);
		contentStreamOffset = 0;
		bits4content = 1;
		contentStreamLength = 0;
		bits4contentLength = bits4length;
		bits4sharedNumber = 0;
		bits4sharedIdentifer = 0;
		bits4numerator = 0;
		denomenator = 255;
		records = null;
		if (array.length() > 1) {
			records = new Array();
			for (int i_15_ = 0; i_15_ < array.length(); i_15_++) {
				int i_16_ = 1;
				Enumeration enumeration = null;
				if (i_15_ == 0)
					enumeration = hashtable.keys();
				else {
					Hashtable hashtable_17_ = (Hashtable) hashtable_6_
							.get(new Integer(i_15_));
					if (hashtable_17_ != null)
						enumeration = hashtable_17_.keys();
				}
				if (enumeration != null) {
					while (enumeration.hasMoreElements()) {
						Integer integer = (Integer) enumeration.nextElement();
						if (i < integer.intValue())
							i = integer.intValue();
						if (i_7_ > integer.intValue())
							i_7_ = integer.intValue();
						i_16_++;
					}
				}
				addrecord(i_16_, ((Long) array_8_.get(i_15_)).longValue(), 0,
						0, 0, 0, 0);
			}
		}
	}

	void addrecord(int i, long l, int i_18_, int i_19_, int i_20_, int i_21_,
			int i_22_) {
		records.put(records.length(), new PageOffsetHintString(i, l, i_18_,
				i_19_, i_20_, i_21_, i_22_));
	}

	byte[] getBytes() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		bytearrayoutputstream.write(this.int2FourBytes(leastNumber));
		bytearrayoutputstream.write(this.long2FourBytes(firstObjectLocation));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4number));
		bytearrayoutputstream.write(this.int2FourBytes(leastLength));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4length));
		bytearrayoutputstream.write(this.int2FourBytes(contentStreamOffset));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4content));
		bytearrayoutputstream.write(this.int2FourBytes(contentStreamLength));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4contentLength));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4sharedNumber));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4sharedIdentifer));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4numerator));
		bytearrayoutputstream.write(this.int2TwoBytes(denomenator));
		if (records != null) {
			for (int i = 0; i < records.length(); i++) {
				PageOffsetHintString pageoffsethintstring = (PageOffsetHintString) records
						.get(i);
			}
		}
		return bytearrayoutputstream.toByteArray();
	}
}