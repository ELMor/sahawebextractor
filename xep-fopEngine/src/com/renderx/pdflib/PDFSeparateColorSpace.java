/*
 * PDFSeparateColorSpace - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFSeparateColorSpace extends PDFObject {
	String name;

	float[] altcolor;

	String altcolorName;

	PDFSeparateColorSpace(IndirectObject indirectobject, String string,
			float[] fs) {
		super(indirectobject);
		altcolor = fs;
		name = "";
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c == '\t' || c == '\n' || c == '\r' || c == '\u00a0')
				c = ' ';
			if (c == ' ') {
				StringBuffer stringbuffer = new StringBuffer();
				PDFSeparateColorSpace pdfseparatecolorspace_0_ = this;
				pdfseparatecolorspace_0_.name = stringbuffer.append(
						pdfseparatecolorspace_0_.name).append("#20").toString();
			} else if (c == '(' || c == ')' || c == '<' || c == '>' || c == '['
					|| c == ']' || c == '{' || c == '}' || c == '/' || c == '%') {
				StringBuffer stringbuffer = new StringBuffer();
				PDFSeparateColorSpace pdfseparatecolorspace_1_ = this;
				pdfseparatecolorspace_1_.name = stringbuffer.append(
						pdfseparatecolorspace_1_.name).append("#").toString();
				StringBuffer stringbuffer_2_ = new StringBuffer();
				PDFSeparateColorSpace pdfseparatecolorspace_3_ = this;
				pdfseparatecolorspace_3_.name = stringbuffer_2_.append(
						pdfseparatecolorspace_3_.name).append(
						Integer.toHexString(c)).toString();
			} else {
				StringBuffer stringbuffer = new StringBuffer();
				PDFSeparateColorSpace pdfseparatecolorspace_4_ = this;
				pdfseparatecolorspace_4_.name = stringbuffer.append(
						pdfseparatecolorspace_4_.name).append((char) c)
						.toString();
			}
		}
		if (fs == null) {
			altcolorName = "DeviceGray";
			altcolor = new float[] { 0.0F };
		}
		switch (altcolor.length) {
		case 1:
			altcolorName = "DeviceGray";
			break;
		case 3:
			altcolorName = "DeviceRGB";
			break;
		case 4:
			altcolorName = "DeviceCMYK";
			break;
		default:
			altcolorName = "DeviceGray";
		}
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("[/Separation");
		pdfoutputstream.println("/" + name);
		pdfoutputstream.print("/" + altcolorName);
		pdfoutputstream.begin_dict();
		writeAltColorTransformFunction(pdfoutputstream, altcolor);
		pdfoutputstream.end_dict();
		pdfoutputstream.print("]");
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}

	public void writeHead(PDFOutputStream pdfoutputstream)
			throws PDFIOException {
		start_offset = (long) pdfoutputstream.size();
		pdfoutputstream.println(obj_id + " obj");
	}

	void writeTail(PDFOutputStream pdfoutputstream) throws PDFIOException {
		pdfoutputstream.println("\nendobj");
	}

	private void writeAltColorTransformFunction(
			PDFOutputStream pdfoutputstream, float[] fs) {
		int i = fs.length;
		pdfoutputstream.print("/FunctionType 2 /N 1.0 /Domain[0 1] /Range[");
		for (int i_5_ = 0; i_5_ < i; i_5_++) {
			pdfoutputstream.print("0.0 1.0");
			if (i_5_ < i - 1)
				pdfoutputstream.print(" ");
		}
		pdfoutputstream.print("]");
		pdfoutputstream.print(" /C0[");
		if (i == 1 || i == 3) {
			for (int i_6_ = 0; i_6_ < i; i_6_++) {
				pdfoutputstream.print("1.0");
				if (i_6_ < i - 1)
					pdfoutputstream.print(" ");
			}
		} else {
			for (int i_7_ = 0; i_7_ < i; i_7_++) {
				pdfoutputstream.print("0.0");
				if (i_7_ < i - 1)
					pdfoutputstream.print(" ");
			}
		}
		pdfoutputstream.print("] ");
		pdfoutputstream.print("/C1[");
		for (int i_8_ = 0; i_8_ < i; i_8_++) {
			pdfoutputstream.print(PDFOutputStream.pdf_float((double) fs[i_8_]));
			if (i_8_ < i - 1)
				pdfoutputstream.print(" ");
		}
		pdfoutputstream.print("]");
	}
}