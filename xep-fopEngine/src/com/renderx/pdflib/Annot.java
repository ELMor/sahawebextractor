/*
 * Annot - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

abstract class Annot extends PDFObject {
	static final int BORDER_SOLID = 0;

	static final int BORDER_DASHED = 1;

	static final int BORDER_BEVELED = 2;

	static final int BORDER_INSET = 3;

	static final int BORDER_UNDERLINED = 4;

	static final int icon_file_graph = 0;

	static final int icon_file_paperclip = 1;

	static final int icon_file_pushpin = 2;

	static final int icon_file_tag = 3;

	static final int icon_text_comment = 4;

	static final int icon_text_insert = 5;

	static final int ICON_TEXT_NOTE = 6;

	static final int icon_text_paragraph = 7;

	static final int icon_text_newparagraph = 8;

	static final int icon_text_key = 9;

	static final int icon_text_help = 10;

	static final String[] icon_names = { "Graph", "Paperclip", "Pushpin",
			"Tag", "Comment", "Insert", "Note", "Paragraph", "NewParagraph",
			"Key", "Help" };

	static final int flag_invisible = 1;

	static final int flag_hidden = 2;

	static final int flag_print = 4;

	static final int flag_nozoom = 8;

	static final int flag_norotate = 16;

	static final int flag_noview = 32;

	static final int flag_readonly = 64;

	static final String[] border_style_names = { "S", "D", "B", "I", "U" };

	int type;

	float[] rect = new float[4];

	int border_style;

	float border_width;

	float border_red;

	float border_green;

	float border_blue;

	float border_dash1;

	float border_dash2;

	Annot(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_) {
		super(indirectobject);
		rect[0] = f;
		rect[1] = f_0_;
		rect[2] = f_1_;
		rect[3] = f_2_;
	}

	abstract String getSubType();

	abstract void writeAnnotBody(PDFOutputStream pdfoutputstream);

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.print("/Subtype/" + getSubType());
		write_border_style(pdfoutputstream);
		pdfoutputstream.println("/Rect ["
				+ PDFOutputStream.pdf_float_coarse((double) rect[0]) + " "
				+ PDFOutputStream.pdf_float_coarse((double) rect[1]) + " "
				+ PDFOutputStream.pdf_float_coarse((double) rect[2]) + " "
				+ PDFOutputStream.pdf_float_coarse((double) rect[3]) + "]");
		writeAnnotBody(pdfoutputstream);
	}

	void write_border_style(PDFOutputStream pdfoutputstream) {
		if (border_style != 0 || border_width != 1.0F || border_red != 0.0F
				|| border_green != 0.0F || border_blue != 0.0F
				|| border_dash1 != 3.0F || border_dash2 != 3.0F) {
			pdfoutputstream.print("/Border[0 0 "
					+ PDFOutputStream.pdf_float((double) border_width));
			if (border_width != 0.0F && border_style == 1) {
				pdfoutputstream.print("["
						+ PDFOutputStream.pdf_float((double) border_dash1));
				if (border_dash1 != border_dash2)
					pdfoutputstream
							.print(" "
									+ (PDFOutputStream
											.pdf_float((double) border_dash2)));
				pdfoutputstream.print("]");
			}
			pdfoutputstream.print("]");
			if (!"Link".equals(getSubType()) || border_width != 0.0F)
				pdfoutputstream.print("/C ["
						+ PDFOutputStream.pdf_float((double) border_red) + " "
						+ PDFOutputStream.pdf_float((double) border_green)
						+ " " + PDFOutputStream.pdf_float((double) border_blue)
						+ "]");
		}
	}
}