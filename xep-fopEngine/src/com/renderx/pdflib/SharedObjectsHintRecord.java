/*
 * SharedObjectsHintRecord - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;

import com.renderx.pdflib.parser.PDFInstance;
import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public class SharedObjectsHintRecord extends AbstractHintRecord {
	public int firstID;

	public int firstLocation;

	public int number4firstPage;

	public int number4shared;

	public int bits4number;

	public int leastLength;

	public int bits4length;

	public SharedObjectsHintRecord(Array array, Hashtable hashtable,
			Hashtable hashtable_0_, Hashtable hashtable_1_,
			PDFInstance pdfinstance) {
		Enumeration enumeration = hashtable.keys();
		firstID = pdfinstance.newID((Integer) array.get(0)).intValue();
		leastLength = 2147483647;
		int i = -2147483648;
		int i_2_ = 0;
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			if (integer.intValue() < firstID)
				firstID = integer.intValue();
			if (integer.intValue() > i_2_)
				i_2_ = integer.intValue();
			int i_3_ = ((Long) hashtable_1_.get(integer)).intValue();
			if (leastLength > i_3_)
				leastLength = i_3_;
			if (i < i_3_)
				i = i_3_;
		}
		enumeration = hashtable_0_.keys();
		number4firstPage = 1;
		number4shared = 0;
		int i_4_ = ((Long) hashtable_1_.get(array.get(0))).intValue();
		if (leastLength > i_4_)
			leastLength = i_4_;
		if (i < i_4_)
			i = i_4_;
		Array array_5_ = new Array();
		Array array_6_ = new Array();
		array_5_.put(((Integer) array.get(0)).intValue(), new Integer(i_4_));
		array_6_.put(pdfinstance.newID((Integer) array.get(0)).intValue(),
				array.get(0));
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			number4firstPage++;
			i_4_ = ((Long) hashtable_1_.get(integer)).intValue();
			array_5_.put(integer.intValue(), new Integer(i_4_));
			array_6_.put(pdfinstance.newID(integer).intValue(), integer);
			if (leastLength > i_4_)
				leastLength = i_4_;
			if (i < i_4_)
				i = i_4_;
		}
		number4shared = number4firstPage;
		bits4number = 1;
		bits4length = this.bits4number(i - leastLength);
		bitsS = new ByteArrayOutputStream();
		int i_7_ = pdfinstance.newID((Integer) array.get(0)).intValue();
		for (int i_8_ = i_7_; i_8_ < i_7_ + hashtable_0_.size(); i_8_++)
			this.writeBits(((Integer) array_5_.get(((Integer) array_6_
					.get(i_8_)).intValue())).intValue()
					- leastLength, bits4length);
		this.writeBits(0, 1);
		this.writeBits(1, bits4number);
	}

	byte[] getBytes() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		bytearrayoutputstream.write(this.int2FourBytes(firstID));
		bytearrayoutputstream.write(this.int2FourBytes(firstLocation));
		bytearrayoutputstream.write(this.int2FourBytes(number4firstPage));
		bytearrayoutputstream.write(this.int2FourBytes(number4shared));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4number));
		bytearrayoutputstream.write(this.int2FourBytes(leastLength));
		bytearrayoutputstream.write(this.int2TwoBytes(bits4length));
		this.flush();
		bytearrayoutputstream.write(bitsS.toByteArray());
		return bytearrayoutputstream.toByteArray();
	}
}