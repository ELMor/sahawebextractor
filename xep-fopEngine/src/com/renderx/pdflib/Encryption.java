/*
 * Encryption - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.OutputStream;
import java.util.Date;
import java.util.Random;

import com.renderx.crypto.Hex;
import com.renderx.crypto.MD5;
import com.renderx.crypto.RC4;
import com.renderx.crypto.RC4OutputStream;

class Encryption {
	static byte[] ps = { 40, -65, 78, 94, 78, 117, -118, 65, 100, 0, 78, 86,
			-1, -6, 1, 8, 46, 46, 0, -74, -48, 104, 62, -128, 47, 12, -87, -2,
			100, 83, 105, 122 };

	byte[] obuf = new byte[32];

	byte[] ubuf = new byte[32];

	byte[] ovalue = new byte[32];

	byte[] uvalue = new byte[32];

	byte[] basickey = new byte[32];

	byte[] currentkey = new byte[10];

	int pnumber = -64;

	byte[] pvalue = { -64, -1, -1, -1 };

	byte[] ID = null;

	public Encryption(String string, String string_0_, int i, String string_1_) {
		generateP(i);
		generateID(string_1_);
		generateOU(string_0_, string);
		basickey = genKeyStd(ubuf);
	}

	void generateP(int i) {
		pnumber = i;
		pvalue[0] = (byte) (i & 0xff);
		pvalue[1] = (byte) (i >> 8 & 0xff);
		pvalue[2] = (byte) (i >> 16 & 0xff);
		pvalue[3] = (byte) (i >> 24 & 0xff);
	}

	byte[] prepareStr(String string) {
		byte[] is = new byte[32];
		int i = string.length();
		boolean bool = false;
		if (i < 32) {
			int i_2_ = 32 - i;
		} else
			i = 32;
		for (int i_3_ = 0; i_3_ < i; i_3_++)
			is[i_3_] = (byte) string.charAt(i_3_);
		System.arraycopy(ps, 0, is, i, 32 - i);
		return is;
	}

	void generateOU(String string, String string_4_) {
		obuf = prepareStr(string);
		MD5 md5 = new MD5();
		byte[] is = new byte[32];
		byte[] is_5_ = new byte[5];
		is = md5.digest(obuf);
		System.arraycopy(is, 0, is_5_, 0, 5);
		ubuf = prepareStr(string_4_);
		ovalue = encrypt(is_5_, ubuf);
		generateU();
	}

	void generateU() {
		byte[] is = genKeyStd(ubuf);
		uvalue = encrypt(is, ps);
	}

	byte[] genKeyStd(byte[] is) {
		byte[] is_6_ = (new byte[ubuf.length + ovalue.length + pvalue.length
				+ ID.length]);
		System.arraycopy(ubuf, 0, is_6_, 0, ubuf.length);
		System.arraycopy(ovalue, 0, is_6_, ubuf.length, ovalue.length);
		System.arraycopy(pvalue, 0, is_6_, ubuf.length + ovalue.length,
				pvalue.length);
		System.arraycopy(ID, 0, is_6_, ubuf.length + ovalue.length
				+ pvalue.length, ID.length);
		MD5 md5 = new MD5();
		byte[] is_7_ = md5.digest(is_6_);
		byte[] is_8_ = new byte[5];
		System.arraycopy(is_7_, 0, is_8_, 0, 5);
		return is_8_;
	}

	void generateCurrentKey(int i) {
		byte[] is = new byte[10];
		System.arraycopy(basickey, 0, is, 0, 5);
		byte[] is_9_ = new byte[4];
		is[5] = (byte) (i & 0xff);
		is[6] = (byte) (i >> 8 & 0xff);
		is[7] = (byte) (i >> 16 & 0xff);
		is[8] = (byte) 0;
		is[9] = (byte) 0;
		MD5 md5 = new MD5();
		System.arraycopy(md5.digest(is), 0, currentkey, 0, 10);
	}

	OutputStream getCipherOutputStream(OutputStream outputstream) {
		try {
			return new RC4OutputStream(outputstream, currentkey);
		} catch (Exception exception) {
			throw new PDFException(exception);
		}
	}

	byte[] encrypt(byte[] is, byte[] is_10_) {
		Object object = null;
		byte[] is_11_;
		try {
			RC4 rc4 = new RC4(is);
			is_11_ = rc4.crypt(is_10_);
		} catch (Exception exception) {
			throw new PDFException(exception);
		}
		return is_11_;
	}

	byte[] encryptArray(byte[] is) {
		return encrypt(currentkey, is);
	}

	byte[] encryptStr(String string) {
		int i = string.length();
		byte[] is = new byte[i];
		for (int i_12_ = 0; i_12_ < i; i_12_++)
			is[i_12_] = (byte) string.charAt(i_12_);
		byte[] is_13_ = encrypt(currentkey, is);
		return is_13_;
	}

	public static void main(String[] strings) {
		/* empty */
	}

	void generateID(String string) {
		MD5 md5 = new MD5();
		String string_14_ = new Date().toString() + string
				+ new Random().nextFloat();
		int i = string_14_.length();
		byte[] is = new byte[i];
		for (int i_15_ = 0; i_15_ < i; i_15_++)
			is[i_15_] = (byte) string_14_.charAt(i_15_);
		ID = md5.digest(is);
	}

	public String IDtoStr() {
		return Hex.toString(ID);
	}

	static void print_array(String string, byte[] is) {
		System.out.println("--- " + string + " ---");
		System.out.println(Hex.toString(is));
	}
}