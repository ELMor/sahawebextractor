/*
 * PDFWrongElementException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFWrongElementException extends RuntimeException {
	public PDFWrongElementException() {
		/* empty */
	}

	public PDFWrongElementException(Exception exception) {
		super(exception.toString());
	}

	public PDFWrongElementException(String string) {
		super(string);
	}
}