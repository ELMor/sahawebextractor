/*
 * Destination - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class Destination {
	String dest_id;

	int page_number;

	IndirectObject page_id;

	float page_x;

	float page_y;

	Destination(int i, float f, float f_0_) {
		page_number = i;
		page_x = f;
		page_y = f_0_;
		page_id = null;
	}

	Destination(String string) {
		page_id = null;
		dest_id = string;
	}

	Destination(String string, IndirectObject indirectobject, float f,
			float f_1_) {
		dest_id = string;
		page_id = new IndirectObject(indirectobject.num);
		page_x = f;
		page_y = f_1_;
	}

	public void write(PDFOutputStream pdfoutputstream) {
		if (page_id != null && dest_id == null)
			pdfoutputstream.println("/Dest [" + page_id.toStringR() + " /XYZ "
					+ PDFOutputStream.pdf_float((double) page_x) + " "
					+ PDFOutputStream.pdf_float((double) page_y) + " 0]");
		else if (dest_id != null) {
			pdfoutputstream.print("/Dest ");
			pdfoutputstream.print_annotation(dest_id);
			pdfoutputstream.println("");
		}
	}
}