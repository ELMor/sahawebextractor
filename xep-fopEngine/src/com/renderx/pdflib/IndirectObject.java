/*
 * IndirectObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class IndirectObject {
	int num;

	public IndirectObject(int i) {
		num = i;
	}

	public IndirectObject(Integer integer) {
		num = integer.intValue();
	}

	public String toString() {
		return "" + num + " 0";
	}

	public String toStringR() {
		return toString() + " R";
	}
}