/*
 * PDFEncoding - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.Encoding;

public class PDFEncoding extends PDFObject {
	Encoding enc;

	String getType() {
		return "Encoding";
	}

	PDFEncoding(IndirectObject indirectobject, Encoding encoding) {
		super(indirectobject);
		enc = encoding;
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		int i = enc.getFirstCode();
		int i_0_ = enc.getLastCode();
		pdfoutputstream.print("/Differences [" + i);
		int i_1_ = 1;
		for (int i_2_ = i; i_2_ <= i_0_; i_2_++) {
			if (enc.table[i_2_] == null)
				i_1_ = 0;
			else {
				if (i_1_ == 0)
					pdfoutputstream.print("\n" + i_2_);
				pdfoutputstream.print(++i_1_ % 8 == 0 ? "\n" : " ");
				pdfoutputstream.print("/" + enc.table[i_2_].n);
			}
		}
		pdfoutputstream.println("]");
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}