/*
 * Outlines - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class Outlines extends PDFObject {
	IndirectObject first_id;

	IndirectObject last_id;

	int count;

	Hashtable bookmarks = new Hashtable();

	Hashtable bookmarkIDs = new Hashtable();

	Outlines(IndirectObject indirectobject) {
		super(indirectobject);
		last_id = null;
		first_id = null;
	}

	void addBookmark(Bookmark bookmark, PDFOutputStream pdfoutputstream) {
		if (bookmarkIDs.containsKey(new Integer(bookmark.getExternalID())))
			pdfoutputstream.error("Bookmark with id "
					+ bookmark.getExternalID() + " already exists.");
		else {
			bookmark.obj_id = new IndirectObject(
					pdfoutputstream.lastObjectNumber++);
			bookmarkIDs.put(new Integer(bookmark.getExternalID()), "");
			if (bookmark.getExtParentID() == 0) {
				if (first_id == null || first_id.num > bookmark.getID().num)
					first_id = bookmark.getID();
				if (last_id == null || last_id.num < bookmark.getID().num)
					last_id = bookmark.getID();
				count++;
				bookmark.setParentID(obj_id);
				addNext(bookmark);
			} else {
				Bookmark bookmark_0_ = ((Bookmark) bookmarks.get(new Integer(
						bookmark.getExtParentID())));
				if (bookmark_0_ != null) {
					bookmark_0_.addBookmark(bookmark);
					bookmarks.put(new Integer(bookmark_0_.getExternalID()),
							bookmark_0_);
				} else {
					Enumeration enumeration = bookmarks.keys();
					while (enumeration.hasMoreElements()) {
						Bookmark bookmark_1_ = ((Bookmark) bookmarks
								.get(enumeration.nextElement()));
						if (bookmark_1_.addBookmark(bookmark))
							bookmarks.put(new Integer(bookmark_1_
									.getExternalID()), bookmark_1_);
					}
				}
			}
		}
	}

	void addNext(Bookmark bookmark) {
		Enumeration enumeration = bookmarks.keys();
		IndirectObject indirectobject = null;
		Bookmark bookmark_2_ = null;
		while (enumeration.hasMoreElements() && indirectobject == null) {
			bookmark_2_ = (Bookmark) bookmarks.get(enumeration.nextElement());
			if (bookmark_2_.getNextID() == null)
				indirectobject = bookmark_2_.getID();
		}
		if (indirectobject != null) {
			bookmark.setPrevID(indirectobject);
			bookmark_2_.setNextID(bookmark.getID());
			bookmarks
					.put(new Integer(bookmark_2_.getExternalID()), bookmark_2_);
		}
		bookmarks.put(new Integer(bookmark.getExternalID()), bookmark);
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Count " + count);
		pdfoutputstream.println("/First " + first_id.toStringR());
		pdfoutputstream.println("/Last " + last_id.toStringR());
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		Enumeration enumeration = bookmarks.keys();
		while (enumeration.hasMoreElements()) {
			Bookmark bookmark = (Bookmark) bookmarks.get(enumeration
					.nextElement());
			bookmark.write(pdfoutputstream);
		}
	}

	void finish(Pages pages) {
		Enumeration enumeration = bookmarks.keys();
		while (enumeration.hasMoreElements()) {
			Bookmark bookmark = (Bookmark) bookmarks.get(enumeration
					.nextElement());
			bookmark.finish(pages);
			bookmarks.put(new Integer(bookmark.getExternalID()), bookmark);
		}
	}
}