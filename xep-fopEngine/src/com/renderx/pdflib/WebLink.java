/*
 * WebLink - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.util.URLUtil;

class WebLink extends Annot {
	int dest_type;

	int dest_page;

	String url;

	boolean openNewWindow;

	String getSubType() {
		return "Link";
	}

	WebLink(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_, int i, String string, boolean bool) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
		url = string;
		openNewWindow = bool;
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		createAction(pdfoutputstream, url, openNewWindow);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}

	public static void createAction(PDFOutputStream pdfoutputstream,
			String string, boolean bool) {
		string = string.trim();
		if (string.length() >= 5
				&& string.toLowerCase().substring(0, 5).equals("file:")) {
			String string_3_ = string.substring(5);
			if (string_3_.startsWith("//"))
				string_3_ = string_3_.substring(2);
			String string_4_ = "";
			int i = string_3_.lastIndexOf('#');
			if (i != -1) {
				string_4_ = string_3_.substring(i + 1);
				string_3_ = string_3_.substring(0, i);
			}
			createRemoteGotoAction(pdfoutputstream, URLUtil
					.urlDecode(string_3_), URLUtil.urlDecode(string_4_), bool);
		} else
			createURIAction(pdfoutputstream, string);
	}

	public static void createURIAction(PDFOutputStream pdfoutputstream,
			String string) {
		pdfoutputstream.print("/A<</S/URI/URI ");
		pdfoutputstream.printStringPDFEncoding(string);
		pdfoutputstream.println(">>");
	}

	public static void createRemoteGotoAction(PDFOutputStream pdfoutputstream,
			String string, String string_5_, boolean bool) {
		pdfoutputstream.print("/A<</S/GoToR");
		pdfoutputstream.print("/NewWindow " + (bool ? "true" : "false"));
		pdfoutputstream.print("/D ");
		if (string_5_ == null || "".equals(string_5_))
			pdfoutputstream.print("[0/Fit]");
		else
			pdfoutputstream.printStringPDFEncoding(string_5_);
		pdfoutputstream.print("/F ");
		pdfoutputstream.printStringPDFEncoding(string);
		pdfoutputstream.println(">>");
	}
}