package isf.tools.testing.dataSet;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;

import isf.tools.testing.dataSet.swing.GUIWizard;

public class Main {
	public static void cargaDriver(String drv, String rdbms, String maker) {
		// System.out.print("Intentando cargar "+rdbms+" de "+maker+":");
		try {
			Class.forName(drv);
			// System.out.println("OK.");
		} catch (Exception e) {
			 System.out.println("Fallo.");
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// Cargamos los controladores conocidos...
			Environment env=new Environment(args);
			loadJDBCDrivers(env.propDrivers);
			env.connect();
			if(args.length>0){
				Procedimiento p=new Procedimiento(env);
				env.log=new SystemOut();
				p.procesaExtraccion();	
			}else{
				GUIWizard gw=new GUIWizard();
				gw.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				gw.setVisible(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void loadJDBCDrivers(String jdbcPrp) throws Exception {
		Properties prp=new Properties();
		prp.load(new FileInputStream(new File(jdbcPrp)));
		int num=Integer.parseInt(prp.getProperty("numdrivers"));
		for(int i=1;i<=num;i++) {
			System.out.println("Registering "+prp.getProperty("driver."+i+".name"));
			String root="jar:file://"+prp.getProperty("driver."+i+".jar")+"!/";
			URL u=new URL(root);
			String cn=prp.getProperty("driver."+i+".class");
			URLClassLoader ucl=new URLClassLoader(new URL[] {u});
			Driver d=(Driver)Class.forName(cn,true,ucl).newInstance();
			DriverManager.registerDriver(new SREDriver(d));
		}
		Class.forName("org.h2.Driver");
	}	
}
