package isf.tools.testing.dataSet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Mapeador implements Sustituyente {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3724215157603863250L;
	private transient PreparedStatement psSelect = null;
	private transient PreparedStatement psInsert = null;
	private transient Connection c = null;
	private transient boolean inicializado = false;
	protected long strNumber = 0;
	private final String zeros = "00000000";

	public String sustituye(String orig) {
		try {
			psSelect.setString(1, orig);
			ResultSet rs = psSelect.executeQuery();
			if (rs.next())
				return rs.getString(1);
			String relevantNumber = "" + (++strNumber);
			String ret = "str"
					+ zeros.substring(0, zeros.length()
							- relevantNumber.length()) + relevantNumber;
			psInsert.setString(1, orig);
			psInsert.setString(2, ret);
			psInsert.execute();
			c.commit();
			return ret;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void inicializa() throws Exception {
		// Se crea la tabla destinada a la encriptaci�n/sustitucion de cadenas
		// de texto
		Statement st = c.createStatement();
		String sql = "create text table ssfsustencript"
				+ "(vdest varchar(4096), vori varchar(128) primary key )";
		try {
			st.execute(sql);
		} catch (SQLException e) {
		}
		sql = "set table ssfsustencript source \"ssfsustencript.txt;fs=|\"";
		try {
			st.execute(sql);
		} catch (SQLException e) {
		}
		psSelect = c
				.prepareStatement("select vdest from ssfsustencript where vori=?");
		psInsert = c
				.prepareStatement("insert into ssfsustencript(vori,vdest) values(?,?)");
		inicializado = true;
	}

	public Mapeador(Connection conn) {
		c = conn;
	}

	public boolean isInicializado() {
		return inicializado;
	}
}
