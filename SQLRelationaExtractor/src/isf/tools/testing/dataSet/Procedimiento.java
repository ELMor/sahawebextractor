package isf.tools.testing.dataSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public class Procedimiento {
	Environment env=null;
	
	public Procedimiento(Environment e) { env=e; };
	
	
	public  void procesaExtraccion() {
		Sustituyente s = null;
		if(env.jobname==null)
			proc01_SaveSchemaToJOBFile();
		if (env.recrearTablas)
			proc02_DropTablasEnDest();
		if (env.recrearTablas || env.crearTablas) {
			proc03_CreaReplicaEnDEST();
		}
		try {
			if (!env.encripta.equals("")) {
				s = new Mapeador(env.dest);
				s.inicializa();
				proc04_MarcaTablasEncriptadas(s);
			}
			if (env.ddbbCompleta) {
				proc80_ddbbCompleta();
			} else {
				proc80_recursivo(env.dsTabla, env.dsWhere, 0, "");
				if (env.loadChildrens || env.loadAllChildrens)
					proc90_recursivo(env.dsTabla, 1, "");
			}
			// Se procede a la encriptacion de los campos marcados
			procA0_encripta();
			// Finalmente se emiten sentencias SQL Create e Insert para las
			// tablas de dest.
			if (env.emitirSQL)
				procB0_emitSQL();
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			procFF_CloseALL();
		}
	}

	public void proc01_SaveSchemaToJOBFile() {
		try {
			// Salvamos la definicion de dbOrig. Se puede continuar el proceso
			// mas adelante de esta manera
			File f=new File(env.datadir, env.schema + ".job");
			env.jobname=f.getCanonicalPath();
			env.schOrig.grabar(f);
			if (env.dumpXML) {
				PrintStream ps = new PrintStream(new FileOutputStream(new File(
						env.datadir, env.schema + ".xml")));
				env.schOrig.dump(ps);
				ps.close();
			}
		} catch (FileNotFoundException fnfe1) {
			env.log.println("Atención: " + fnfe1.getMessage() + " "
					+ env.datadir.toString() + "/" + env.schema + ".job");
		} catch (IOException ioe1) {
			env.log.println("Atención: " + ioe1.getMessage());
		}
	}

	public  void proc02_DropTablasEnDest() {
		env.schOrig.dropTablas(env.dest);
	}

	public void proc03_CreaReplicaEnDEST() {
		try {
			// Creamos una replica en la BBDD de hypersonic
			env.schOrig.replica(env.dest, false);
		} catch (SQLException sqle1) {
			sqle1.printStackTrace();
			System.exit(-1);
		} catch (IOException ioe1) {
			ioe1.printStackTrace();
			System.exit(-1);
		}
	}

	public  void proc04_MarcaTablasEncriptadas(Sustituyente s) {
		StringTokenizer st = new StringTokenizer(env.encripta, ", ");
		while (st.hasMoreElements()) {
			String el = st.nextToken();
			if (el.equals(""))
				continue;
			int isField = el.indexOf(".");
			if (isField > 0) {
				String tabla = el.substring(0, isField);
				Tabla t = env.schOrig.getTabla(tabla);
				if (t == null) {
					env.log.println("tabla " + tabla + " no existe");
					continue;
				}
				t.setSustituyente(s);
				String field = el.substring(isField + 1);
				Campo c = t.getCampo(field);
				if (c == null) {
					env.log.println("campo " + el + " no existe");
					continue;
				}
				env.log.println(el + " marcado como encriptado.");
				c.setEncriptar(true);
			} else {
				Tabla t = env.schOrig.getTabla(el);
				if (t == null) {
					env.log.println("tabla " + el + " no existe");
					continue;
				}
				t.setSustituyente(s);
				for (Enumeration<Campo> k = t.getEnumCampos(); k.hasMoreElements();) {
					Campo c = (Campo) k.nextElement();
					env.log.println(el + "." + c.nombre
							+ " marcado como encriptado");
					c.setEncriptar(true);
				}
			}
		}
	}

	public void proc80_ddbbCompleta()
			throws Exception {
		for (Enumeration<String> enuT = env.schOrig.getTablas(); enuT.hasMoreElements();) {
			Tabla t = env.schOrig.getTabla((String) enuT.nextElement());
			env.log.flush();
			env.log.println("Replicando " + t.nombre + ":"
					+ t.replica(env.schOrig, env.dest, "") + " reg.");
		}
	}

	public int proc80_recursivo(String tHijaNombre, String sqlwo, int level, String logString) {
		// En primer lugar se insertan los registros del where
		Tabla hij = env.schOrig.getTabla(tHijaNombre);
		int inserted = 0;
		try {
			// Si no hay inserciones, se vuelve.
			inserted = hij.replica(env.schOrig, env.dest, sqlwo + " and ("+ hij.niCond(false, env.dest) + ")");
			env.log.println(getTL(level) + hij.nombre + ":" + inserted + logString);
			if (inserted == 0)
				return 0;
			Hashtable<String,Vector<Campo>> foreignKeys = hij.getFKs();
			for (Enumeration<String> en = foreignKeys.keys(); en.hasMoreElements();) {
				String fkNombre =  en.nextElement();
				Vector<Campo> comp = foreignKeys.get(fkNombre);
				// Seleccionamos los distincts values de fields
				// Que existan sobre la tabla t para insertar sus padres
				Tabla fkTab = env.schOrig.getTabla(hij.getFKForeignTable(comp));
				String sqlWhere = "Where " + "(" + hij.getFKForeignFields(comp)
						+ ") in " + "(select distinct "
						+ hij.getFKOwnFields(comp) + " from " + hij.nombre
						+ " " + sqlwo + " and (" + hij.niCond(true, env.dest) + ")"
						+ ")";
				// A jugar...
				proc80_recursivo(fkTab.nombre, sqlWhere, level + 1, " <-" + tHijaNombre + "." + fkNombre);
			}
		} catch (Exception e) {
			env.log.println("No se pueden copiar los registros de "
					+ hij.nombre + " con '" + sqlwo + "'");
			e.printStackTrace();
			System.exit(-1);
		}
		return inserted;
	}

	public void proc90_recursivo(String stt, int level, String logString) {
		// Ahora vamos con los hijos. Mucho mas complicado.
		Tabla master = env.schOrig.getTabla(stt);
		// Cogemos los nombres de las fks...
		for (Enumeration<String> relMaster = master.fkb.keys(); relMaster
				.hasMoreElements();) {
			try {
				String rel = (String) relMaster.nextElement();
				Tabla h = (Tabla) master.fkb.get(rel);
				Vector<Campo> compHija = h.fkf.get(rel);
				String masterF = h.getFKForeignFields(compHija);
				String slaveF = h.getFKOwnFields(compHija);
				String sqlWhere = "where "
						+ master.innerGroupCond(masterF, slaveF, true, env.dest)
								.toString();
				int inserted = proc80_recursivo(h.nombre, sqlWhere, level + 1, " ->" + stt + "." + rel);
				env.log.println(getTL(level) + h.nombre + ":" + inserted
						+ logString);
				if (inserted > 0 && env.loadAllChildrens)
					proc90_recursivo(h.nombre, level + 1, " ->" + stt + "." + rel);
			} catch (SQLException sqle1) {
				sqle1.printStackTrace();
				System.exit(-1);
			}
		}
	}

	public void procA0_encripta()
			throws Exception {
		env.log.println("Encriptando...");
		for (Enumeration<String> enuT = env.schOrig.getTablas(); enuT.hasMoreElements();) {
			Tabla t = env.schOrig.getTabla((String) enuT.nextElement());
			for (Enumeration<Campo> enuC = t.getEnumCampos(); enuC != null
					&& enuC.hasMoreElements();) {
				Campo c = (Campo) enuC.nextElement();
				if (c.getEncriptar().booleanValue()) {
					t.encripta(env.dest,env.log);
					enuC = null;
				}
			}
		}
	}

	public void procB0_emitSQL() throws Exception {
		for (Enumeration<String> enuT = env.schOrig.getTablas(); enuT.hasMoreElements();) {
			env.schOrig.getTabla((String) enuT.nextElement()).dumpSQL(env.dest, env.fwSQL);
		}
		for (Enumeration<String> enuT = env.schOrig.getTablas(); enuT.hasMoreElements();) {
			env.schOrig.getTabla((String) enuT.nextElement()).dumpSQLPKConstraint(env.fwSQL);
		}
		for (Enumeration<String> enuT = env.schOrig.getTablas(); enuT.hasMoreElements();) {
			env.schOrig.getTabla((String) enuT.nextElement()).dumpSQLConstraints(env.fwSQL);
		}
	}

	public  void procFF_CloseALL() {
		try {
			env.log.println("\nFin de la extracci�n.");
			if (env.fwSQL != null)
				env.fwSQL.close();
			env.schOrig.getConexion().close();
		} catch (Exception e) {
		}
		try {
			env.dest.close();
		} catch (SQLException e) {
		}
	}

	public static final String espacios = "                                                                                                                     ";

	public static String getTL(int l) {
		return espacios.substring(0, l);
	}

}
