package isf.tools.testing.dataSet.swing;

import javax.swing.table.AbstractTableModel;

import isf.tools.testing.dataSet.EsquemaDB;
import isf.tools.testing.dataSet.Tabla;

public class TablasTableModel extends AbstractTableModel {


	private static final long serialVersionUID = -7979788889154888578L;
	private EsquemaDB sch=null;
	private Boolean selected[]=null;
	private String where[]=null;
	private Boolean encripta[]=null;
	private Boolean loadChildren[]=null;
	
	public TablasTableModel(EsquemaDB inp){
		sch=inp;
		selected=new Boolean[sch.numTablas()];
		where=new String[sch.numTablas()];
		encripta = new Boolean[sch.numTablas()];
		loadChildren = new Boolean[sch.numTablas()];
		for(int i=0;i<sch.numTablas();i++){
			selected[i]=new Boolean(false);
			encripta[i]=new Boolean(false);
			loadChildren[i]=new Boolean(false);
			where[i]=new String("");
		}
	}
	
	public int getColumnCount() {
		//TABLA,SELECCIONAR,WHERE,ENCRIPTAR,children
		return 5;
	}

	public int getRowCount() {
		return sch.numTablas();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Tabla t=sch.getTablaAt(rowIndex);
		switch(columnIndex){
		case 0: return t.getNombre(); 
		case 1: return selected[rowIndex];
		case 2: return where[rowIndex];
		case 3: return encripta[rowIndex];
		case 4: return loadChildren[rowIndex];
		}
		
		return null;
	}

	public String getColumnName(int column) {
		switch(column){
		case 0: return "Tabla";
		case 1: return "Extraer?";
		case 2: return "WHERE";
		case 3: return "Encriptar?";
		case 4: return "Extraer Hijos?";
		}
		return "Hello Mojo";
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex==0)
			return false;
		return true;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0: break;
		case 1: selected[rowIndex]=(Boolean)aValue; break;
		case 2: where[rowIndex]=(String)aValue; break;
		case 3: encripta[rowIndex]=(Boolean)aValue; break;
		case 4: loadChildren[rowIndex]=(Boolean)aValue; break;
		}
		if(columnIndex>0)
			fireTableCellUpdated(rowIndex,columnIndex);
		
	}

	public Class getColumnClass(int columnIndex) {
		switch(columnIndex){
		case 0: return String.class;
		case 1: return Boolean.class;
		case 2: return String.class;
		case 3: return Boolean.class;
		case 4: return Boolean.class;
		}
		return String.class;
	}

}
