package isf.tools.testing.dataSet.swing;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import isf.tools.testing.dataSet.RELogger;

public class SwingLogger implements RELogger {
	JTextPane jta=null;
	JScrollPane jsp;
	
	public SwingLogger(JScrollPane jsp, JTextPane jtp){
		this.jsp=jsp;
		jta=jtp;
	}
	
	public void print(String msg) {
		String actual=jta.getText();
		if(actual.length()>1024)
			actual=actual.substring(msg.length());
		jta.setText(actual+msg);
		JScrollBar jsb=jsp.getVerticalScrollBar();
		try {
			jsb.setValue(jsb.getMaximum()-jsb.getVisibleAmount());
		} catch (RuntimeException e) {
		}
	}

	public void println(String msg) {
		print(msg+"\n");
	}

	public void flush() {
		
	}

}
