package isf.tools.testing.dataSet.swing;


import isf.tools.testing.dataSet.Campo;
import isf.tools.testing.dataSet.EsquemaDB;
import isf.tools.testing.dataSet.Tabla;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class CamposTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -7707634287765420441L;
	private JTable master=null;
	private EsquemaDB sch=null;
	
	public CamposTableModel(JTable master,EsquemaDB sch){
		this.master=master;
		this.sch=sch;
	}
	
	public int getColumnCount() {
		return 4;
	}

	public int getRowCount() {
		int masterSelectedRow=master.getSelectedRow();
		if(masterSelectedRow<0)
			return 0;
		String sqlTableName=(String)master.getValueAt(masterSelectedRow,0);
		Tabla t=sch.getTabla(sqlTableName);
		return t.getCampos().size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		int masterSelectedRow=master.getSelectedRow();
		if(masterSelectedRow<0)
			return null;
		String sqlTableName=(String)master.getValueAt(masterSelectedRow,0);
		Tabla t=sch.getTabla(sqlTableName);
		String nombreCampo=t.getCampoAt(rowIndex);
		Campo c=t.getCampo(nombreCampo);
		int pkPosition=t.pkPosition(c);
		switch(columnIndex){
		case 0: return nombreCampo;
		case 1: return c.getNombreTipo();
		case 2: return pkPosition>0? ""+pkPosition:"";
		case 3: return c.getEncriptar();
		}
		return null;
	}

	public Class getColumnClass(int columnIndex) {
		if(columnIndex<3)
			return String.class;
		return Boolean.class;
	}

	public String getColumnName(int column) {
		switch(column){
		case 0:return "Nombre";
		case 1:return "Tipo";
		case 2:return "PK";
		case 3:return "Encriptar";
		}
		return null;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex==3)
			return true;
		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		int masterSelectedRow=master.getSelectedRow();
		String sqlTableName=(String)master.getValueAt(masterSelectedRow,0);
		Tabla t=sch.getTabla(sqlTableName);
		String nombreCampo=t.getCampoAt(rowIndex);
		Campo c=t.getCampo(nombreCampo);
		c.setEncriptar(((Boolean)aValue).booleanValue());
	}

}
