package ssf.sescam.lesp.regcen;

import java.util.Properties;

import ssf.sescam.lesp.regcen.synch.Sescam;
import ssf.sescam.lesp.regcen.monitor.MonitorSched;
import ssf.utils.Global;
import org.apache.log4j.Logger;
import java.io.*;

/**
 * <p>
 * Title: Recopilacion Listas de Espera SESCAM
 * </p><p>
 * Description: Sistema de recopilacion de Informix HP-HIS de listas de espera
 * </p><p>
 * Copyright: Copyright (c) 2002 Soluziona
 * </p><p>
 * Company: Soluziona
 * </p>
 *
 * @author Eladio Linares
 *
 * @version 1.0
 */

public class Main {
  private Properties prop = new Properties();
  private Logger log = Logger.getLogger(getClass());

  /**
   * Entrada principal de ejecucion
   * @param args Argumentos de la linea de comandos
   */
  public static void main(String[] args) {
    new Main(args);
  }

  /**
   * Constructor
   * @param args Argumentos de linea de comandos
   */
  public Main(String[] args) {
    try {
      Global.initLog4J("ssf/sescam/lesp/regcen/logging.properties");
      //Se carga la configuracion de config.properties
      cargaConfiguracion();
      log.info("Centralizacion de registros de Listas de Espera");
      log.info("Quirurgica, CEX y Pruebas Diagnosticas.");
      log.info("(c) 2002 Soluziona");
      log.info("Leyendo la configuracion de 'config.properties'");
      //Se muestra dicha configuracion
      dumpConfig();
      //Ahora comprobar la conexion y el esquema de servicios centrales.
      if (!Sescam.check(prop)) {
        log.error("No existe conexion con SESCAM -- abortando");
        System.exit( -1);
      }
      //Monitoriza los schedules
      Thread monitorSch = new Thread(new MonitorSched(prop),"Scheduler");
      monitorSch.start();
    }
    catch (Exception e) {
      System.out.flush();
      System.err.flush();
      e.printStackTrace();
    }
  }

  /**
   * Carga la configuracion desde un archivo 'properties'
   * @throws Exception
   */
  public void cargaConfiguracion() throws Exception {
    //Cargamos la configuracion de usuario
    String cfgp = "config.properties";
    FileInputStream fis = new FileInputStream(cfgp);
    prop.load(fis);
    //Le a�adimos la configuracion interna
    cfgp = "ssf/sescam/lesp/regcen/internal.properties";
    prop.load(ClassLoader.getSystemResourceAsStream(cfgp));
    //Carga de los drivers JDBC
    int numDrivers = Integer.parseInt(prop.getProperty("jdbc.driver.size"));
    for (int i = 1; i <= numDrivers; i++) {
      Class.forName(prop.getProperty("jdbc.driver." + i));
    }
  }

  /**
   * Muestra en pantalla la configuracion actual
   */
  void dumpConfig() {
    log.debug(prop.toString());
  }
	

 
}