package ssf.sescam.lesp.regcen.db.Centros;

import ssf.sescam.lesp.regcen.db.*;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class DropSchema extends SchemaBase {

  public DropSchema(String[] args) throws Exception {
    super(args);
    SQLFileRunner.MRInternal(
        true,
        c,
        "Borrando Schema",
        "Centros/undo",
        null);
  }
}