
drop trigger "informix".trigins_lista_espe;

drop trigger "informix".trigdel_lista_espe;

drop trigger "informix".trigupd_lista_espe;


create  procedure logtolog
  (
     tipope            smallint
    ,fecope            datetime year to second
    ,numerohc          integer
    ,servreal          char(4)
    ,cod_medico        integer
    ,fecha_en_lista    date
    ,codidiag_ingreso  char(6)
    ,codiproc_ingreso  char(6)
    ,prioridad         char(1)
    ,servpeti          char(4)
    ,medipeti          integer
    ,aviso_urg         char(1)
    ,preingresado      char(1)
    ,fecha_ingreso     date
    ,observaciones     char(70)
    ,tipfinancia       smallint
    ,financia          char(6)
    ,ambito            char(1)
    ,descproc          char(60)
    ,descdiag          char(60)
    ,tipoanest         char(1)
    ,norden            integer
    ,tipolis           smallint
    ,procedede         smallint
    ,fechaqui          date
    ,numproce          integer
    ,hemoterapia       char(1)
    ,complicacion      char(1)
    ,inclusion         smallint
    ,codidiag2         char(6)
    ,descdiag2         char(60)
    ,ingresado         char(1)
    ,bajalabor         char(1)
    ,numpreop          integer
    ,fecrechazo        date
    ,tipoacti          smallint
    ,fec_prevista      date
    ,hora_prevista     char(5)
    ,codiproc2         char(6)
    ,descproc2         char(60)
    ,derivable         char(1)
    ,situacion         smallint
    ,est_preop         char(1)
    ,aptociru          char(1)
    ,fecapto           date
    ,feccaduca         date
    ,asa               char(3)
    ,localizado        char(1)
    ,fecdemora         date
    ,tpodemora         smallint
    ,fecderiva         date
    ,hospderiva        char(4)
    ,fecconfirma       date
    ,grabauser         char(20)
    ,grabadia          date
    ,grabahora         char(5)
    ,motrechazo        smallint
    ,f_baja            date
  )

if tipope=1 then
  insert into "informix".cht_lista_espe
    (
    	numerohc,
    	norden,
    	fecha_en_lista,
    	grabauser,
    	grabadia,
    	grabahora
    )
    values (
    	numerohc ,
    	norden ,
    	fecha_en_lista ,
    	grabauser ,
    	grabadia ,
    	grabahora
    	);
end if;

if tipope=5 then
  update "informix".cht_lista_espe
  set
  	"informix".cht_lista_espe.numerohc = numerohc
  where
  	"informix".cht_lista_espe.norden = norden;
end if;

if tipope=10 then
  delete from "informix".cht_lista_espe
  where
  	"informix".cht_lista_espe.norden = norden;
end if;


insert into lesplogger
  (
     tipope
    ,fecope
    ,procesada
    ,numerohc
    ,servreal
    ,cod_medico
    ,fecha_en_lista
    ,codidiag_ingreso
    ,codiproc_ingreso
    ,prioridad
    ,servpeti
    ,medipeti
    ,aviso_urg
    ,preingresado
    ,fecha_ingreso
    ,observaciones
    ,tipfinancia
    ,financia
    ,ambito
    ,descproc
    ,descdiag
    ,tipoanest
    ,norden
    ,tipolis
    ,procedede
    ,fechaqui
    ,numproce
    ,hemoterapia
    ,complicacion
    ,inclusion
    ,codidiag2
    ,descdiag2
    ,ingresado
    ,bajalabor
    ,numpreop
    ,fecrechazo
    ,tipoacti
    ,fec_prevista
    ,hora_prevista
    ,codiproc2
    ,descproc2
    ,derivable
    ,situacion
    ,est_preop
    ,aptociru
    ,fecapto
    ,feccaduca
    ,asa
    ,localizado
    ,fecdemora
    ,tpodemora
    ,fecderiva
    ,hospderiva
    ,fecconfirma
    ,grabauser
    ,grabadia
    ,grabahora
    ,motrechazo
    ,fecha_baja
  )
 values
  (
     tipope
    ,fecope
    ,0
    ,numerohc
    ,servreal
    ,cod_medico
    ,fecha_en_lista
    ,codidiag_ingreso
    ,codiproc_ingreso
    ,prioridad
    ,servpeti
    ,medipeti
    ,aviso_urg
    ,preingresado
    ,fecha_ingreso
    ,observaciones
    ,tipfinancia
    ,financia
    ,ambito
    ,descproc
    ,descdiag
    ,tipoanest
    ,norden
    ,tipolis
    ,procedede
    ,fechaqui
    ,numproce
    ,hemoterapia
    ,complicacion
    ,inclusion
    ,codidiag2
    ,descdiag2
    ,ingresado
    ,bajalabor
    ,numpreop
    ,fecrechazo
    ,tipoacti
    ,fec_prevista
    ,hora_prevista
    ,codiproc2
    ,descproc2
    ,derivable
    ,situacion
    ,est_preop
    ,aptociru
    ,fecapto
    ,feccaduca
    ,asa
    ,localizado
    ,fecdemora
    ,tpodemora
    ,fecderiva
    ,hospderiva
    ,fecconfirma
    ,grabauser
    ,grabadia
    ,grabahora
    ,motrechazo
    ,f_baja
  );

end procedure;

create trigger le02
  insert on lista_espe
  referencing new as newmov
  for each row
  (execute procedure
    logtolog(
       1
      ,current year to second
      ,newmov.numerohc
      ,newmov.servreal
      ,newmov.cod_medico
      ,newmov.fecha_en_lista
      ,newmov.codidiag_ingreso
      ,newmov.codiproc_ingreso
      ,newmov.prioridad
      ,newmov.servpeti
      ,newmov.medipeti
      ,newmov.aviso_urg
      ,newmov.preingresado
      ,newmov.fecha_ingreso
      ,newmov.observaciones
      ,newmov.tipfinancia
      ,newmov.financia
      ,newmov.ambito
      ,newmov.descproc
      ,newmov.descdiag
      ,newmov.tipoanest
      ,newmov.norden
      ,newmov.tipolis
      ,newmov.procedede
      ,newmov.fechaqui
      ,newmov.numproce
      ,newmov.hemoterapia
      ,newmov.complicacion
      ,newmov.inclusion
      ,newmov.codidiag2
      ,newmov.descdiag2
      ,newmov.ingresado
      ,newmov.bajalabor
      ,newmov.numpreop
      ,newmov.fecrechazo
      ,newmov.tipoacti
      ,newmov.fec_prevista
      ,newmov.hora_prevista
      ,newmov.codiproc2
      ,newmov.descproc2
      ,newmov.derivable
      ,newmov.situacion
      ,newmov.est_preop
      ,newmov.aptociru
      ,newmov.fecapto
      ,newmov.feccaduca
      ,newmov.asa
      ,newmov.localizado
      ,newmov.fecdemora
      ,newmov.tpodemora
      ,newmov.fecderiva
      ,newmov.hospderiva
      ,newmov.fecconfirma
      ,newmov.grabauser
      ,newmov.grabadia
      ,newmov.grabahora
      ,newmov.motrechazo
      ,null
    )
  );

create trigger le03
  update on lista_espe
  referencing old as oldmov new as newmov
  for each row
  (execute procedure
    logtolog(
       5
      ,current year to second
      ,newmov.numerohc
      ,newmov.servreal
      ,newmov.cod_medico
      ,newmov.fecha_en_lista
      ,newmov.codidiag_ingreso
      ,newmov.codiproc_ingreso
      ,newmov.prioridad
      ,newmov.servpeti
      ,newmov.medipeti
      ,newmov.aviso_urg
      ,newmov.preingresado
      ,newmov.fecha_ingreso
      ,newmov.observaciones
      ,newmov.tipfinancia
      ,newmov.financia
      ,newmov.ambito
      ,newmov.descproc
      ,newmov.descdiag
      ,newmov.tipoanest
      ,newmov.norden
      ,newmov.tipolis
      ,newmov.procedede
      ,newmov.fechaqui
      ,newmov.numproce
      ,newmov.hemoterapia
      ,newmov.complicacion
      ,newmov.inclusion
      ,newmov.codidiag2
      ,newmov.descdiag2
      ,newmov.ingresado
      ,newmov.bajalabor
      ,newmov.numpreop
      ,newmov.fecrechazo
      ,newmov.tipoacti
      ,newmov.fec_prevista
      ,newmov.hora_prevista
      ,newmov.codiproc2
      ,newmov.descproc2
      ,newmov.derivable
      ,newmov.situacion
      ,newmov.est_preop
      ,newmov.aptociru
      ,newmov.fecapto
      ,newmov.feccaduca
      ,newmov.asa
      ,newmov.localizado
      ,newmov.fecdemora
      ,newmov.tpodemora
      ,newmov.fecderiva
      ,newmov.hospderiva
      ,newmov.fecconfirma
      ,newmov.grabauser
      ,newmov.grabadia
      ,newmov.grabahora
      ,newmov.motrechazo
      ,null
    )
  );

create trigger le01
	delete on lista_espe
	referencing old as oldmov
  for each row
  (execute procedure
  	logtolog(
  	   10
  	  ,current year to second
      ,oldmov.numerohc
      ,oldmov.servreal
      ,oldmov.cod_medico
      ,oldmov.fecha_en_lista
      ,oldmov.codidiag_ingreso
      ,oldmov.codiproc_ingreso
      ,oldmov.prioridad
      ,oldmov.servpeti
      ,oldmov.medipeti
      ,oldmov.aviso_urg
      ,oldmov.preingresado
      ,oldmov.fecha_ingreso
      ,oldmov.observaciones
      ,oldmov.tipfinancia
      ,oldmov.financia
      ,oldmov.ambito
      ,oldmov.descproc
      ,oldmov.descdiag
      ,oldmov.tipoanest
      ,oldmov.norden
      ,oldmov.tipolis
      ,oldmov.procedede
      ,oldmov.fechaqui
      ,oldmov.numproce
      ,oldmov.hemoterapia
      ,oldmov.complicacion
      ,oldmov.inclusion
      ,oldmov.codidiag2
      ,oldmov.descdiag2
      ,oldmov.ingresado
      ,oldmov.bajalabor
      ,oldmov.numpreop
      ,oldmov.fecrechazo
      ,oldmov.tipoacti
      ,oldmov.fec_prevista
      ,oldmov.hora_prevista
      ,oldmov.codiproc2
      ,oldmov.descproc2
      ,oldmov.derivable
      ,oldmov.situacion
      ,oldmov.est_preop
      ,oldmov.aptociru
      ,oldmov.fecapto
      ,oldmov.feccaduca
      ,oldmov.asa
      ,oldmov.localizado
      ,oldmov.fecdemora
      ,oldmov.tpodemora
      ,oldmov.fecderiva
      ,oldmov.hospderiva
      ,oldmov.fecconfirma
      ,oldmov.grabauser
      ,oldmov.grabadia
      ,oldmov.grabahora
      ,oldmov.motrechazo
      ,null
    )
  );

