package ssf.sescam.lesp.regcen.db.Centros.patcher.p2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import ssf.sescam.lesp.regcen.db.patcher.ClassPatcher;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class FincHos
    extends ClassPatcher {
  public boolean isDoubleConnectionRequired() {
    return true;
  }

  public boolean patchDoubleConnection(Hashtable params) {
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log = Logger.getLogger(getClass());

    Statement stCentro = null;
    ResultSet rsCentro = null;
    PreparedStatement pstSescam = null;
    try {

      String sql2 = "Update lesp set finchos=? where centro=" + CEGA +
          " and norden=?";
      pstSescam = toSescam.prepareStatement(sql2);

      stCentro = toCentro.createStatement();

      log.info("Corrigiendo fechas desde lista_espe");
      String sql1 = "select norden,fecha_en_lista from lista_espe";
      rsCentro = stCentro.executeQuery(sql1);

      while (rsCentro.next()) {
        pstSescam.setDate(1, rsCentro.getDate(2));
        pstSescam.setInt(2, rsCentro.getInt(1));
        pstSescam.execute();
      }
      rsCentro.close();

      log.info("Parche 2: Corrigiendo fechas desde h_lespadm");
      sql1 = "select nfila,f_inclu from h_lespadm";
      rsCentro = stCentro.executeQuery(sql1);

      while (rsCentro.next()) {
        pstSescam.setDate(1, rsCentro.getDate(2));
        pstSescam.setInt(2, rsCentro.getInt(1));
        pstSescam.execute();
      }
      rsCentro.close();

      sql2 = "Update lespcex set finclusihos=? where cega=" + CEGA +
          " and ncita=?";
      pstSescam = toSescam.prepareStatement(sql2);

      log.info("Corrigiendo fechas desde actividad,citas y anulacion");
      sql1 = "select ncita,grabadia from actividad where grabadia is not null " +
          "union all select ncita,grabadia from citas where grabadia is not null " +
          "union all select ncita,grabadia from anulacion where grabadia is not null ";
      rsCentro = stCentro.executeQuery(sql1);

      while (rsCentro.next()) {
        pstSescam.setDate(1, rsCentro.getDate(2));
        pstSescam.setInt(2, rsCentro.getInt(1));
        pstSescam.execute();
      }
      log.info("Correccion de fechas finchos y finclusihos terminada");

    }
    catch (Exception e) {
      log.error("Parche 2: Fallo el parche FincHos!", e);
      return false;
    }
    return true;
  }
}