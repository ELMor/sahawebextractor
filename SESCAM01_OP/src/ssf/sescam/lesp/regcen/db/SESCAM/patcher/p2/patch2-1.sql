create table masterclave(
  centro       number,
  tipo         number(1),
  clave        number,
  estado       number(2) default 1
)
/

alter table masterclave add constraint masterclave_pk
  primary key (centro,tipo,clave) using index tablespace pro1legidx01
/

create table solapes(
  centro number,
  tipo number,
  claveinicial number,
  clave number,
  signo number,
  fi date,
  ff date,
  situ number
)
/

create index solapes01 on solapes(centro,tipo,clave) tablespace pro1legidx01
/

create table continuos(
  centro number,
  clave number,
  claveinicial number,
  tipo number,
  signo number,
  fi date,
  ff date,
  spac varchar2(6),
  acu number
)
/

create index continuos01 on continuos(centro,tipo,clave) tablespace pro1legidx01
/

create index continuos02 on continuos(centro,tipo,claveinicial) tablespace pro1legidx01
/

create or replace view oltle(
  centro,
  tipo,
  claveinicial,
  tle,
  dem,
  activo) as
select
  centro,
  tipo,
  claveinicial,
  acu+decode(sign(signo),1,trunc(sysdate)-fi,0),
  spac,
  decode(sign(signo),1,1,0)
from
  continuos
where
 trunc(sysdate) between fi and decode(ff,null,trunc(sysdate),ff-1)
/

Create Or Replace View catalogacion (centro, tipo, claveinicial, catalog) As
   Select centro, 1, norden, catalog
     From lesp
   Union All
   Select cega, 2, ncita, catalog
     From lespcex
/

Create Or Replace View oltlec As
   Select oltle.*, catalogacion.CATALOG
     From oltle, catalogacion
    Where oltle.centro = catalogacion.centro
      And oltle.tipo = catalogacion.tipo
      And oltle.claveinicial = catalogacion.claveinicial
/

CREATE TABLE LEHPDEMO
(
  CENTRO      INTEGER                           NOT NULL,
  NORDEN      INTEGER                           NOT NULL,
  FEC_INICIO  DATE                              NOT NULL,
  NDIAS       INTEGER                           NOT NULL,
  TIPO        INTEGER                           NOT NULL,
  ENSITU      INTEGER,
  GRABADIA    DATE,
  GRABAHORA   CHAR(5),
  GRABAUSER   CHAR(20)
)
/

ALTER TABLE LEHPDEMO ADD CONSTRAINT LEHPDEMO_PK
  PRIMARY KEY (CENTRO, NORDEN, FEC_INICIO, NDIAS, TIPO)
  USING INDEX TABLESPACE PRO1LEGIDX01
/

CREATE TABLE CEXINCID
(
  CENTRO     INTEGER                            NOT NULL,
  CLAVE      INTEGER                            NOT NULL,
  NCITA      INTEGER,
  TIPINCI    CHAR(1),
  FEC_INCI   DATE,
  DIAS_DEMO  INTEGER,
  HOSPDER    CHAR(4),
  PERDIDA    CHAR(1),
  GRABADIA   DATE,
  GRABAHORA  CHAR(5),
  GRABAUSER  CHAR(20)
)
/

ALTER TABLE CEXINCID ADD CONSTRAINT CEXINCID_PK
  PRIMARY KEY (CENTRO, CLAVE)
  USING INDEX TABLESPACE PRO1LEGIDX01
/
