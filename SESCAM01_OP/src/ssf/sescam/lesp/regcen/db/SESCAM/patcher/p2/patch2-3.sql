Create Or Replace Trigger lesp02_tle
   Before Update Or Delete Or Insert
   On lesp
   For Each Row
Begin
   If Updating Or Inserting Then
      legase.SYNCMASTERCLAVE (:New.centro, 1, :New.norden);
   Else
      legase.SYNCMASTERCLAVE (:Old.centro, 1, :Old.norden);
   End If;
End;
/

Create Or Replace Trigger lespcex02_tle
   Before Update Or Delete Or Insert
   On lespcex
   For Each Row
Begin
   If Updating Or Inserting Then
      legase.SYNCMASTERCLAVE (:New.cega, 2, :New.ncita);
   Else
      legase.SYNCMASTERCLAVE (:Old.cega, 2, :Old.ncita);
   End If;
End;
/

Create Or Replace Trigger lehpdemo01_tle
   Before Update Or Delete Or Insert
   On lehpdemo
   For Each Row
Declare
   exist   Number;
Begin
   If Updating Or Inserting Then
      Select Count (*)
        Into EXIST
        From LESP
       Where CENTRO = :New.centro And NORDEN = :New.norden;

      If exist > 0 Then
         legase.SYNCMASTERCLAVE (:New.centro, 1, :New.norden);
      End If;
   Else
      Select Count (*)
        Into EXIST
        From LESP
       Where CENTRO = :Old.centro And NORDEN = :Old.norden;

      If exist > 0 Then
         legase.SYNCMASTERCLAVE (:Old.centro, 1, :Old.norden);
      End If;
   End If;
End;
/

Create Or Replace Trigger cexincid01_tle
   Before Update Or Delete Or Insert
   On cexincid
   For Each Row
Declare
   exist   Number;
Begin
   If Updating Or Inserting Then
      Select Count (*)
        Into EXIST
        From LESPCEX
       Where CEGA = :New.centro And ncita = :New.ncita;

      If exist > 0 Then
         legase.SYNCMASTERCLAVE (:New.centro, 2, :New.ncita);
      End If;
   Else
      Select Count (*)
        Into EXIST
        From LESPCEX
       Where CEGA = :Old.centro And ncita = :Old.ncita;

      If exist > 0 Then
         legase.SYNCMASTERCLAVE (:Old.centro, 2, :Old.ncita);
      End If;
   End If;
End;
/
