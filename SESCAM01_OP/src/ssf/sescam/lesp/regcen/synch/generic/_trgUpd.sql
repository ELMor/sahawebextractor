create trigger {nombre}02
  update on {nombre}
  referencing old as oldmov new as newmov
  for each row
  (execute procedure
    {nombre}_pro2(
       current year to second
      ,{oldmov.campos}
      ,{newmov.campos}
    )
  );

