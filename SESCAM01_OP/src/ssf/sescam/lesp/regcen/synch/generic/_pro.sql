create  procedure {nombre}_pro
  (
     tipope            smallint
    ,fecope            datetime year to second
    ,{defCampos}
  )
insert into {nombre}_log
  (
     tipope
    ,fecope
    ,estado
    ,{campos}
  )
 values
  (
     tipope
    ,fecope
    ,0
    ,{campos}
  );

end procedure;
