package ssf.sescam.lesp.regcen.synch.generic;

import java.sql.*;
import java.util.Hashtable;
import ssf.utils.Global;
import org.apache.log4j.Logger;
import java.sql.ResultSetMetaData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Tabla {
  private Connection from, to;
  private String name;
  private String condicion;
  private String defCampos[];
  private String campos[];
  private String primaryKey[];
  Hashtable params = new Hashtable();
  private static Logger log = null;
  private int CEGA;

  public Tabla(Connection from, Connection to, String name) throws Exception {
    this.name = name;
    this.from = from;
    this.to = to;
    log = Logger.getLogger(getClass());
    String cega =
        "select valclave from constantes where codclav=6 and aplicacion='CLI'";
    ResultSet rs = from.createStatement().executeQuery(cega);
    if (rs.next()) {
      CEGA = rs.getInt("valclave");
    }
  }

  public Tabla(Connection from, Connection to, String name, String pk[]) throws
      Exception {
    this(from, to, name);
    primaryKey = pk;
  }

  public Tabla(Connection from, Connection to, String name, String where) throws
		Exception {
	  this(from, to, name);
	  condicion = where;
 }
 
  public Tabla(Connection from, Connection to, String name, String where, String pk[]) throws
	   Exception {
	 this(from, to, name,where);
	 primaryKey = pk;
   }



  private boolean checkRemoteSchema() throws Exception {
    log.info("Comprobando esquema sincronizacion entidad:" + name);
    DatabaseMetaData remDBMD = from.getMetaData();
    ResultSet rsTablas = remDBMD.getTables(null, null, name.toUpperCase() + "%", null);
    boolean existsName = false,
        existsName_log = false;
    while (rsTablas.next()) {
      String tname = rsTablas.getString("TABLE_NAME").trim();
      if (tname.equalsIgnoreCase(name)) {
        existsName = true;
      }
      if (tname.equalsIgnoreCase(name + "_log")) {
        existsName_log = true;
      }
    }
    rsTablas.close();
    //Ahora chequeamos que el esquema est� bien
    if (!existsName) {
      throw new Exception(name + " no existe en la base de datos remota");
    }
    fields();
    if (!existsName_log) {
      return false;
    }
    return true;
  }

  private synchronized boolean checkLocalSchema() throws Exception {
    boolean ret = true;
    log.info("Comprobando esquema local entidad " + name);
    DatabaseMetaData locDBMD = to.getMetaData();
    ResultSet rsTablas = locDBMD.getTables(null, null, name.toUpperCase(), null);
    if (!rsTablas.next()) {
      ret = false;
    }
    rsTablas.close();
    log.info("Comprobada " + name);
    return ret;
  }

  private synchronized void createLocalName() throws Exception {
    log.info("Creando esquema local para entidad " + name);
    try {
      executeSQL(to, "ssf/sescam/lesp/regcen/synch/generic/_locTbl.sql", false);
    }
    catch (SQLException e) {
      log.warn("Ignorando error de creacion:" + e.getMessage());
      try {
        String sql = "Delete from {nombre} where centro=" + CEGA;
        sql = Global.multipleReplace(sql, params);
        to.createStatement().execute(sql);
      }
      catch (SQLException ex) {
        log.warn("Ignorando error de borrado:", ex);
      }
    }
  }

  public boolean synchronize() {
    try {
      if (!checkRemoteSchema() || !checkLocalSchema()) {
        createRemoteSchema();
        createLocalName();
      }
      masterLoop();
      purgeLogger();
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return false;
    }
    return true;
  }

  private void fields() throws Exception {
    DatabaseMetaData remDBMD = from.getMetaData();
    ResultSet rsFields = remDBMD.getColumns(null, null, name, null);
    int numFields = 0;
    while (rsFields.next()) {
      String cname = rsFields.getString("COLUMN_NAME").trim(); 
      if (!(cname.equalsIgnoreCase("centro")) 
      		&& !(cname.equalsIgnoreCase("estado"))) {
        numFields++;
      }
    }
    rsFields.close();
    defCampos = new String[numFields];
    campos = new String[numFields];
    rsFields = remDBMD.getColumns(null, null, name, null);
    numFields = 0;
    while (rsFields.next()) {
      String cname = rsFields.getString("COLUMN_NAME").trim();
      String tname = rsFields.getString("TYPE_NAME").trim();
      if (tname.equalsIgnoreCase("serial")) {
        tname = "int";
      }
      if (cname.equalsIgnoreCase("centro") 
      			|| cname.equalsIgnoreCase("estado")) {
        continue;
      }
      campos[numFields] = cname;
      defCampos[numFields] = tname;
      if (defCampos[numFields].equalsIgnoreCase("CHAR")) {
        defCampos[numFields] += "(" + rsFields.getString("COLUMN_SIZE").trim() +
            ")";
      }
      numFields++;
    }
    rsFields.close();
    if (primaryKey == null) {
      rsFields = remDBMD.getPrimaryKeys(null, null, name);
      numFields = 0;
      while (rsFields.next()) {
        numFields++;
      }
      rsFields.close();
      if (numFields == 0) {
        throw new Exception(name +
                            " no tiene primary key y no ha sido especificada");
      }
      rsFields = remDBMD.getPrimaryKeys(null, null, name);
      primaryKey = new String[numFields];
      numFields = 0;
      while (rsFields.next()) {
        String pkcname = rsFields.getString("COLUMN_NAME").trim();
        if (pkcname.equalsIgnoreCase("centro")) {
          throw new Exception("Tabla remota tiene como pk un campo 'centro'");
        }
        primaryKey[numFields] = pkcname;
        numFields++;
      }
      rsFields.close();
    }
    String cam = "", oldcam = "";
    String defcam = "", olddefcam = "";
    String oldmov = "", newmov = "";
    String pk = "";

    for (int i = 0; i < campos.length; i++) {
      String ap = (i == 0 ? "" : ",");
      if ( ( (float) i) % ( (float) 5) == 0) {
        ap += "\n";
      }
      cam += ap + campos[i];
      oldcam += ap + "_" + campos[i];
      defcam += ap + campos[i] + " " + defCampos[i];
      olddefcam += ap + "_" + campos[i] + " " + defCampos[i];
      oldmov += ap + "oldmov." + campos[i];
      newmov += ap + "newmov." + campos[i];
    }
    for (int i = 0; i < primaryKey.length; i++) {
      String ap = (i == 0 ? "" : ",");
      pk += ap + primaryKey[i];
    }

    params.put("{nombre}", name);
    params.put("{defCampos}", defcam);
    params.put("{oldDefCampos}", olddefcam);
    params.put("{campos}", cam);
    params.put("{oldCampos}", oldcam);
    params.put("{oldmov.campos}", oldmov);
    params.put("{newmov.campos}", newmov);
    params.put("{primaryKey}", pk);
    params.put("{uncampocualquiera}", campos[1]);

  }

  private void createRemoteSchema() throws Exception {
    log.warn("Borrando esquema remoto entidad " + name + " para sincronizacion");
    dropRemoteSchema();
    log.warn("Creando esquema remoto entidad " + name + " para sincronizacion");
    try {
      log.warn("colocando lock mode a wait.");
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_wait.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_log.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_pro.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_pro2.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_trgIns.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_trgUpd.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_trgDel.sql", false);
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_init.sql", false);
      log.warn("colocando lock mode a no wait.");
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_nowait.sql", false);
    }
    catch (Exception ex) {
      log.warn("No puedo crear esquema para tabla " + name + ",borrando...", ex);
      dropRemoteSchema();
      throw new Exception(name + " No puedo sincronizar:" + ex.getMessage());
    }
  }

  private void dropRemoteSchema() {
    try {
      log.warn("colocando lock mode a wait.");
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_wait.sql", false);
    }
    catch (Exception ex1) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_log.sql", true);
    }
    catch (Exception ex1) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_pro.sql", true);
    }
    catch (Exception ex2) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_pro2.sql", true);
    }
    catch (Exception ex2) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_trgIns.sql", true);
    }
    catch (Exception ex3) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_trgUpd.sql", true);
    }
    catch (Exception ex4) {
    }
    try {
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/u_trgDel.sql", true);
    }
    catch (Exception ex5) {
    }
    try {
      log.warn("colocando lock mode a not wait.");
      executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_nowait.sql", false);
    }
    catch (Exception ex1) {
    }
  }

  private void executeSQL(Connection conn, String fname, boolean ignore) throws
      Exception {
    String fileContent = Global.getContentOfFile(fname);
    fileContent = Global.multipleReplace(fileContent, params);
    Statement st = conn.createStatement();
    try {
      st.execute(fileContent);
    }
    catch (Exception e) {
      if (!ignore) {
        log.warn("El script ha fallado:" + e.getMessage() + "\n---------\n" +
                 fileContent + "\n---------\n");
      }else{
        log.warn("Ignorando el fallo:" + e.getMessage() + "\n---------\n" +
                 fileContent + "\n---------\n");
      }
      throw e;
    }
    st.close();
  }

  private void masterLoop() throws Exception {
    log.info("Iniciando refresco entidad " + name);
    Statement st = null, stFrom = null, stTo = null;
    String sql = null;
    ResultSet rs = null;
    ResultSetMetaData rsmd = null;
    try {
      st = from.createStatement();
      stTo = to.createStatement();
      stFrom = from.createStatement();
      sql = "select * from " + name + "_log where estado=0 ";
      sql += (!condicion.equals(""))? " and " + condicion : "";
      sql +=" order by fecope,tipope desc";
      rs = st.executeQuery(sql);
      rsmd = rs.getMetaData();
    }
    catch (SQLException ex) {
      log.error("Error:", ex);
    }
    try {
      int contadorRegistros[] = new int[] {
          0, 0, 0};
      while (rs.next()) {
        switch (rs.getInt("tipope")) {
          case 0: //Insertar nuevo
          case 1:
            contadorRegistros[0]++;
            sql = replace(
                "Insert into {nombre}(centro,{campos}) " +
                "values({CEGA},{values})",
                rs, rsmd);
            stTo.execute(sql);
            break;
          case 5: //Modificar
            contadorRegistros[1]++;
            sql = replace(
                "delete from {nombre} " +
                "where centro={CEGA} and {primaryKeyCondition}",
                rs, rsmd);
            stTo.execute(sql);
            sql = replace(
                "Insert into {nombre}(centro,{campos}) " +
                "values({CEGA},{values})",
                rs, rsmd);
            stTo.execute(sql);
            break;
          case 10: //Borrar
            contadorRegistros[2]++;
            sql = replace(
                "delete from {nombre} " +
                "where centro={CEGA} and {primaryKeyCondition}",
                rs, rsmd);
            stTo.execute(sql);
            break;
        }
        sql = replace("Update {nombre}_log set estado=1 " +
                      "where tipope={rs.tipope} " +
                      " and fecope={rs.fecope} " +
                      " and {primaryKeyCondition}", rs, rsmd);
        stFrom.execute(sql);
      }
      log.info("Terminado el refresco de la entidad " + name);
      log.info("-->Inserciones en " + name + "    :" + contadorRegistros[0]);
      log.info("-->Modificaciones en " + name + " :" + contadorRegistros[1]);
      log.info("-->Borrados en " + name + "       :" + contadorRegistros[2]);
    }
    catch (Exception ex1) {
      log.error("Error sincronizando " + name, ex1);
      try {
        sql = replace("Update {nombre}_log set estado=-1 " +
                      "where tipope={rs.tipope} " +
                      " and fecope={rs.fecope} " +
                      " and {primaryKeyCondition}", rs, rsmd);
        stFrom.execute(sql);
      }
      catch (Exception ex2) {
        log.warn("No puedo marcar el registro como erroneo en el log remoto",
                 ex2);
        throw new Exception(
            "No puedo marcar el registro como erroneo en el log remoto");
      }
    }
  }

  private String replace(String main, ResultSet rs, ResultSetMetaData rsmd) throws
      Exception {
      	
    //Reemplazar los parametros globales.
    main = Global.multipleReplace(main, params);
    //Reemplazar parametros dependientes del ResultSet
    if (main.indexOf("{values}") >= 0) {
      String values = "";
      for (int i = 0; i < campos.length; i++) {
      	String val = formatField(rs, rsmd, campos[i]);
        values += (i == 0 ? "" : ",") + val.trim();
      }
      main = Global.replaceString(main, "{values}", values);
    }
    if (main.indexOf("{primaryKeyCondition}") >= 0) {
      String conditionPK = "";
      for (int i = 0; i < primaryKey.length; i++) {
        String val = formatField(rs, rsmd, primaryKey[i]);
        conditionPK += (i == 0 ? "" : " and ") + primaryKey[i] + "=" + val;
      }
      main = Global.replaceString(main, "{primaryKeyCondition}", conditionPK);
    }
    if (main.indexOf("{valuesPK}") >= 0) {
      String valuesPK = "";
      for (int i = 0; i < primaryKey.length; i++) {
        String val = formatField(rs, rsmd, primaryKey[i]);
        valuesPK += (i == 0 ? "" : ",") + val.trim() + "";
      }
      main = Global.replaceString(main, "{valuesPK}", valuesPK);
    }
    if (main.indexOf("{rs.tipope}") >= 0) {
      String val = Integer.toString(rs.getInt("tipope"));
      main = Global.replaceString(main, "{rs.tipope}", val);
    }
    if (main.indexOf("{rs.fecope}") >= 0) {
      main = Global.replaceString(main, "{rs.fecope}",
                                  "datetime(" + rs.getTimestamp("fecope") +
                                  ") year to fraction");
    }
    if (main.indexOf("{CEGA}") >= 0) {
      main = Global.replaceString(main, "{CEGA}", Integer.toString(CEGA));
    }
    return main;
  }

  private String formatField(ResultSet rs, ResultSetMetaData rsmd, String field) throws
      Exception {
    Object genVal = null;

    switch (rsmd.getColumnType(rs.findColumn(field))) {
      case java.sql.Types.DATE:
        genVal = rs.getDate(field);
        break;
      case java.sql.Types.TIMESTAMP:
        genVal = rs.getTimestamp(field);
        break;
      default:
        genVal = rs.getString(field);
    }
    if (rs.wasNull()) {
      return "null";
    }
    String val = genVal.toString();
    switch (rsmd.getColumnType(rs.findColumn(field))) {
      case java.sql.Types.DATE:
        val = "{d '" + val + "'}";
        break;
      case java.sql.Types.CHAR:
        val = "'" + val + "'";
        break;
      case java.sql.Types.TIMESTAMP:
        val = "{ts '" + val + "'}";
        break;
    }
    return val;
  }

  private void purgeLogger() throws Exception {
    executeSQL(from, "ssf/sescam/lesp/regcen/synch/generic/_purge.sql", false);
  }
}