package ssf.sescam.lesp.regcen.synch;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;

import ssf.sescam.lesp.regcen.db.SQLFileRunner;
import ssf.sescam.lesp.regcen.db.patcher.Patcher;
import org.apache.log4j.Logger;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Sescam {
  private static boolean sescamSchemaComprobacion = false;
  private static Connection toSescam = null;

  private static boolean checkConnection(Properties p) {
    Logger log = Logger.getLogger("ssf.sescam.lesp.regcen.synch.Sescam");
    try {
      toSescam = DriverManager.getConnection(
          p.getProperty("sescam.url"),
          p.getProperty("sescam.usuario"),
          p.getProperty("sescam.password"));
    }
    catch (SQLException e) {
      log.error("Error obteniendo conexion:", e);
      toSescam = null;
      return false;
    }
    return true;
  }

  private static boolean checkSchema(Properties p) {
    try {
      DatabaseMetaData mdSescam = toSescam.getMetaData();
      ResultSet rsCat1 = mdSescam.getTables(null, null, "LESP", null);
      ResultSet rsCat2 = mdSescam.getTables(null, null, "LESPLOGGER", null);
      boolean ret = rsCat1.next() && rsCat2.next();
      rsCat2.close();
      rsCat1.close();
      return ret;
    }
    catch (Exception E) {
      return false;
    }
  }

  private static void createSchema(Properties p) {
    dropSchema(p);
    try {
      SQLFileRunner.multipleRun(false, toSescam, p, "sescam.sql.do",
                                "SESCAM",null);
    }
    catch (Exception e) {
      e.printStackTrace();
      try {
        dropSchema(p);
      }
      catch (Exception ee) {
      }
    }
  }

  private static void dropSchema(Properties p) {
    Logger log = Logger.getLogger("ssf.sescam.lesp.regcen.synch.Sescam");
    try {
      SQLFileRunner.multipleRun(true, toSescam, p, "sescam.sql.undo",
                                "SESCAM",null
                                );
    }
    catch (Exception e) {
      log.debug("Drop schema:", e);
    }
  }

  /**
   * Chequea la conexion con SESCAM y crea el esquema de datos si es necesario
   * @param p Archivo de configuracion 'properties'
   * @param log Logger de salida de mensajes
   * @return true si esta OK
   */
  public static boolean check(Properties p) {
    Logger log = Logger.getLogger("ssf.sescam.lesp.regcen.synch.Sescam");
    boolean ret = true;
    if (!checkConnection(p)) {
      return false;
    }
    else {
      log.info("Conexion con SESCAM comprobada");
    }
    if (!checkSchema(p)) {
      log.info("No existe esquema, creando tablas");
      createSchema(p);
      if (!checkSchema(p)) {
        log.info("Hubo algun error en la creacion del esquema, abortando");
        dropSchema(p);
        ret = false;
      }
      else {
        ret = true;
      }
    }
    if (ret) {
      Hashtable patcherParams = new Hashtable();
      patcherParams.put("rootDir", "ssf/sescam/lesp/regcen/db/SESCAM/");
      patcherParams.put("toSescam", toSescam);
      Patcher patch = new Patcher(patcherParams);
      ret=patch.upgradeLevel();
    }
    try {
      toSescam.close();
    }
    catch (Exception e) {
      ret=false;
      log.debug("Excepcion", e);
    }
    return ret;
  }

}