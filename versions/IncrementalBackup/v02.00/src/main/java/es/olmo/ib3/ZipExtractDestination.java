package es.olmo.ib3;

import java.io.FileOutputStream;
import java.io.InputStream;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class ZipExtractDestination extends ExtractManager {
	ZipOutputStream zos;
	
	public ZipExtractDestination(Progress p, String name) throws Exception{
		super(p);
		zos=new ZipOutputStream(new FileOutputStream(name));
	}
	
	@Override
	public void itemExtract(String relPath, InputStream fis, String msg) throws Exception {
		//prog.startPrepareItem();
		zos.putNextEntry(new ZipEntry(relPath));
		//prog.endPrepareItem();
		save(fis,zos,msg);
	}

	@Override
	public void finishExtract() throws Exception {
		zos.close();
	}

}
