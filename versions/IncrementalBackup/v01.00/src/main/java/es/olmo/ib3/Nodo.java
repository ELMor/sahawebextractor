package es.olmo.ib3;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Nodo implements Serializable, Comparable<Nodo>, Iterable<Nodo> {
	private static final long serialVersionUID = 1709310092884694300L;

	private Nodo parent=null;
	private Vector<Nodo> childs=new Vector<>();
	
	private String nombre="";
	private long date;
	private long size=0;
	private boolean isDeleted=false;
	private boolean dir;
	private byte status=1; 
	public History history=new History();

	private transient File fileRef=null;

	@Override
	public String toString(){
		return nombre + (isDir()?"/":"");
	}
	
	public Nodo(){
		dir=true;
	}
	
	public Nodo(File file, Progress p){
		fileRef=file;
		nombre=file.getName();
		if(file.isDirectory()){
			setDate(0);
			dir=true;
			File list[] = incBackupMgr(file) ;
			if(list!=null)
				for(File child:list)
					new Nodo(child,this,p);
			size=0;
		}else{
			setDate(file.lastModified());
			size=file.length();
			dir=false;
			p.reportAdvance(1,file.getAbsolutePath());
		}
		
	}
	
	private Nodo(File child, Nodo parent, Progress p){
		this(child,p);
		parent.addHijo(this);
	}
	
	public void addHijo(Nodo child){
		int pos=Collections.binarySearch(childs, child);
		if(pos<0){
			childs.insertElementAt(child, -pos-1);
		}else{
			if(pos==childs.size()){
				childs.add(child);
			}else{
				childs.setElementAt(child, pos);
			}
		}
		child.parent=this;
	}
	
	public Nodo getChildWithSameName(Nodo other){
		int pos=Collections.binarySearch(childs, other);
		if(pos<0)
			return null;
		return childs.elementAt(pos);
	}
		
	public static final byte opUntouched=1;
	public static final byte opCreated=2;
	public static final byte opUpdated=4;
	public static final byte opDeleted=8;
	public static final byte opMatch=16;
	public static final byte opAll=31;
	
	class HistoryAction implements Serializable {
		private static final long serialVersionUID = 1L;
		int typeOfAction=0; // 1: Added, 2: Updated, 3:deleted
		long date=0; //Time of operation
		String zipRef;
		public HistoryAction(int tipo, long fecha, String zr){
			typeOfAction=tipo;
			date=fecha;
			zipRef=zr;
		}
	}
	
	class History implements Serializable{
		private static final long serialVersionUID = 1L;
		Vector<HistoryAction> ops=new Vector<>();
		int selectedZipNumber;
		public void register(int op, long date, String zr){
			ops.add(new HistoryAction(op,date,zr));
		}
		public int whichZip(String zr){
			if(zr==null)
				return ops.size()-1;
			return whichZip(parseDate(zr));
		}
		public int whichZip(long date){
			long lastOpDate=ops.get(0).date;
			for(int i=0;i<ops.size();i++){
				HistoryAction ha=ops.elementAt(i);
				if(ha.date==date)
					return i;
				if(lastOpDate<date&&date<ha.date)
					return i;
				lastOpDate=ha.date;
			}
			return ops.size()-1;
		}
		public void selectZip(int zr){
			selectedZipNumber=whichZip(zr);
		}
		public void selectZip(long date){
			selectedZipNumber=whichZip(date);
		}
	}

	@Override
	public int compareTo(Nodo o) {
		return nombre.compareTo(o.nombre);
	}

	@Override
	public Iterator<Nodo> iterator() {
		return childs.iterator();
	}

	public boolean isDir() {
		return dir;
	}

	public String getName(){
		return nombre;
	}
	
	public String getFullName(){
		String ret=nombre;
		for(Nodo p=parent;p!=null;p=p.parent)
			ret=p.nombre+"/"+ret;
		return ret;
	}
	
	public String getRelativeName(){
		if(parent==null)
			return "";
		String pre=parent.getRelativeName();
		if(pre.equals(""))
			return nombre;
		else
			return pre+File.separator+nombre;
	}
	
	/**
	 * Change status of this node and its children
	 * @param status New status (see op---)
	 * @param registerHistory If true, append an entry to history
	 * @param zipRef Name of the snapshot that holds the version
	 */
	public void resetStatus(long fecha, byte status, boolean registerHistory, String zipRef){
		this.status=status;
		if(registerHistory && status!=opUntouched){
			history.register(status,fecha,zipRef);
		}
		if(isDir())
			for(Nodo child:childs)
				child.resetStatus(fecha,status,registerHistory,zipRef);
	}
	/**
	 * CAlculate th sum of the sizes
	 * @param mask select only status that mask with it
	 * @return total size
	 */
	public long getSize(byte mask){
		boolean select=(status & mask)!=0;
		long ret= select ? size : 0;
		if(isDir()){
			ret=0;
			for(Nodo child:childs)
				ret+=child.getSize(mask);
		}
		return ret;
	}
	
	/**
	 * Get number of sleeves 
	 * @param mask
	 * @return
	 */
	public long getNumberOfFiles(byte mask){
		if((status & mask)!=0){
			long ret=0;
			if(isDir()){
				for(Nodo child:childs){
					if(child.isDir())
						ret+=child.getNumberOfFiles(mask);
					else
						ret++;
				}
				return ret;
			}else{
				return 1;
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * Get number of dirs
	 * @return
	 */
	public long getNumberOfDirs(){
		long ret= 0 ;
		if(isDir()){
			for(Nodo child:childs)
				ret+=(child.isDir()?1:0)+child.getNumberOfDirs();
		}
		return ret;
	}

	public int getStatus() {
		return status;
	}


	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}
	
	public void setTFRef(File r){
		fileRef=r;
	}
	
	public File getFileRef(){
		return fileRef;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public void matchWalker(Pattern pat,String dsel){
		String tom=getFullName();
		Matcher matcher=pat.matcher(tom);
		if(isDir()){
			if(matcher.matches())
				status=opMatch;
			else
				for(Nodo child:childs)
					child.matchWalker(pat,dsel);
		}else{
			if(matcher.matches()){
				status=opMatch;
				history.whichZip(dsel);
			}
		}
	}
	/**
	 * Show a filetree like picture
	 * @param depth of current dir
	 */
	public void showWalkerTree(int depth){
		if(isAnyChildMatched()){
			for(int i=0;i<depth;i++)
				System.out.print("|  ");
			String nameOfFile=(nombre==null||nombre.length()==0 ? "Backup":nombre);
			if(isDir())
				nameOfFile+=File.separator;
			System.out.println("+--"+ nameOfFile);
			for(Nodo child:childs)
				if(child.isDir())
					child.showWalkerTree(depth+1);
			for(Nodo child:childs){
				if(!child.isDir() && child.status==opMatch){
					Vector<String> lines=child.showEntryHistory();
					for(int i=0;i<lines.size();i++){
						for(int j=0;j<=depth;j++){
							System.out.print("|  ");
						}
						if(i==0)
							System.out.println("+--"  +lines.get(i));
						else if(i==1)
							System.out.println(" \\  "+lines.get(i));
						else
							System.out.println("  | " +lines.get(i));
					}
				}
			}
		}
	}
	
	private boolean isAnyChildMatched(){
		if(status==opMatch)
			return true;
		boolean toRet=false;
		for(Nodo child:childs){
			if(toRet)
				break;
			else
				toRet|=child.isAnyChildMatched();
		}
		return toRet;
	}
	
	public Vector<String> showEntryHistory(){
		Vector<String> ret=new Vector<>();
		ret.add(nombre + (isDir() ? File.separator : ""));
		SimpleDateFormat sdf=new SimpleDateFormat();
		sdf.applyPattern(RootBackupManager.dateFormat);
		for(HistoryAction ha:history.ops){
			String d=sdf.format(new Date(ha.date));
			switch(ha.typeOfAction){
			case opCreated: ret.add(ha.zipRef+": Created "+d); break;
			case opUpdated: ret.add(ha.zipRef+": Updated "+d); break;
			case opDeleted: ret.add(ha.zipRef+": Deleted "); break;
			}
		}
		return ret;
	}

	public void setName(String n) {
		nombre=n;
	}

	public static long parseDate(String s){
		long ret=0;
		try {
			SimpleDateFormat sdf=new SimpleDateFormat(RootBackupManager.dateFormat);
			Date d=sdf.parse(s);
			ret=d.getTime();
		}catch(Exception e){
			return 0;
		}
		return ret;
	}	
	public String getSelZip(){
		return history.ops.elementAt(history.selectedZipNumber).zipRef;
	}
	
	private File[] incBackupMgr(File dir) {
		File pr=new File(dir,".IncBackup.properties");
		if(!pr.exists()){
			return dir.listFiles();
		}
		try {
			Properties prop=new Properties();
			FileInputStream fis=new FileInputStream(pr);
			prop.load(fis);
			fis.close();
			if(prop.getProperty("Ignore").equalsIgnoreCase("true"))
				return new File[0];
			return dir.listFiles(new IncBackupProp(prop));
		} catch (Exception e) {
		}
		return dir.listFiles();
	}
	
	class IncBackupProp implements FileFilter {
		Properties prop;
		Vector<Pattern> includes;
		Vector<Pattern> excludes;
		
		public IncBackupProp(Properties p){
			prop=p;
			String includeMatch=prop.getProperty("Include","*");
			String excludeMatch=prop.getProperty("Exclude",null);
			includes=tokenPart(includeMatch);
			excludes=tokenPart(excludeMatch);
		}
		
		private Vector<Pattern> tokenPart(String lst){
			Vector<String> ret=new Vector<>();
			StringTokenizer st=new StringTokenizer(lst,File.pathSeparator);
			while(st.hasMoreElements()){
				ret.add(st.nextToken());
			}
			Vector<Pattern> toret=new Vector<>();
			for(String s:ret)
				toret.add(BackupManager.substitutePatternChars(s));
			return toret;
		}
		
		@Override
		public boolean accept(File file) {
			//Includes
			for(Pattern p:includes){
				Matcher m=p.matcher(file.getName());
				if(m.matches())
					return true;
			}
			for(Pattern p:excludes){
				Matcher m=p.matcher(file.getName());
				if(m.matches())
					return false;
			}
			if(includes.size()>0)
				return false;
			if(excludes.size()>0)
				return true;
			return true;
		}
		
	}
	
}
