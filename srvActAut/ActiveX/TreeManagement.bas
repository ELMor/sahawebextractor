Attribute VB_Name = "TreeManagement"
Option Explicit

Public Function AppendBranch(tv1 As TreeView, inp$, img%, level%) As Node
    Dim outp$(), nl&, i&, j&, acu$, n As Node
    On Error Resume Next
    If Err.Number = 0 Then
        nl = DecompBranch(inp, outp)
        If level > 0 Then
            If nl > level Then
                Exit Function
            End If
        End If
        If tv1.Nodes(1).Key <> outp(1) Then
            Call tv1.Nodes.Add(, , "\" & UCase(outp(1)), outp(1), img)
        End If
        For i = nl To 1 Step -1
            Err.Clear
            acu = ""
            For j = 1 To i
                acu = acu & "\" & UCase(outp(j))
            Next
            Set n = tv1.Nodes(acu)
            If Err.Number = 0 Then
                For j = i + 1 To nl
                    acu = acu & "\" & UCase(outp(j))
                    Set n = tv1.Nodes.Add(n, tvwChild, acu, outp(j), img)
                Next
                i = 1
            End If
        Next
        Err.Clear
    End If
    On Error GoTo 0
    If nl > 2 And img > 0 Then
        If img > tv1.Nodes(UCase("\" & outp(1) & "\" & outp(2))).Image Then
            tv1.Nodes(UCase("\" & outp(1) & "\" & outp(2))).Image = img
        End If
    End If
    Set AppendBranch = n
End Function

Public Function DecompBranch(inpg$, outp$()) As Long
    Dim ret%, pos%, lastpos%, inp$
    ReDim outp(1 To 1)
    inp = inpg
    'Obviamos los "\" iniciales
    While Left(inp, 1) = "\"
        inp = Right(inp, Len(inp) - 1)
    Wend
    pos = InStr(1, inp, "\")
    If pos = 0 Then
        outp(1) = inp
        ret = 0
    Else
        outp(1) = Left(inp, pos - 1)
        ret = 1
    End If
    lastpos = pos + 1
    pos = InStr(lastpos, inp, "\")
    While pos > 0
        ret = ret + 1
        ReDim Preserve outp(1 To ret)
        outp(ret) = Mid$(inp, lastpos, pos - lastpos)
        lastpos = pos + 1
        pos = InStr(lastpos, inp, "\")
    Wend
    ret = ret + 1
    ReDim Preserve outp(1 To ret)
    outp(ret) = Mid$(inp, lastpos, Len(inp) - lastpos + 1)
    DecompBranch = ret
End Function


Public Sub SaveTree(tv1 As TreeView, fname$)
    Dim i&, nm&
    Open fname For Output Access Write As #1
    nm = 0
    For i = 1 To tv1.Nodes.Count
        If tv1.Nodes(i).Children = 0 Then
            nm = nm + 1
            Write #1, nm, tv1.Nodes(i).FullPath
        End If
    Next
    On Error GoTo 0
    Close #1
End Sub

Public Sub LoadTree(tv1 As TreeView, fname$, strs$(), level%)
    Dim nm&, inp$, imgndx%, i%
    Open fname For Input Access Read As #1
    On Error Resume Next
    While Err.Number = 0
        Input #1, nm, inp
        If Err.Number = 0 Then
            If UBound(strs, 1) > 0 Then
                For i = 1 To UBound(strs, 1)
                    If InStr(1, inp, strs(i)) <> 0 Then
                        imgndx = i
                    End If
                Next
            Else
                imgndx = 0
            End If
            On Error GoTo 0
            Call AppendBranch(tv1, inp, imgndx, level%)
            On Error Resume Next
            imgndx = 0
        End If
    Wend
    Close #1
    On Error GoTo 0
End Sub
