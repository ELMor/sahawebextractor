VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl LVComputers 
   ClientHeight    =   4260
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5325
   ScaleHeight     =   4260
   ScaleWidth      =   5325
   ToolboxBitmap   =   "LVComputers.ctx":0000
   Begin VB.TextBox txt1 
      Height          =   285
      Left            =   840
      TabIndex        =   3
      Text            =   "*"
      Top             =   3840
      Width           =   1815
   End
   Begin MSComDlg.CommonDialog cd1 
      Left            =   4320
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin ComctlLib.TreeView tv1 
      Height          =   3015
      Left            =   0
      TabIndex        =   1
      Top             =   120
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   5318
      _Version        =   327682
      HideSelection   =   0   'False
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.ListView lv1 
      DragIcon        =   "LVComputers.ctx":0312
      Height          =   3135
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   5530
      SortKey         =   1
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      Icons           =   "il1"
      SmallIcons      =   "il1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   "nombre"
         Object.Tag             =   ""
         Text            =   "Nombre"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   "Descripcion"
         Object.Tag             =   ""
         Text            =   "Descripcion"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Cumple el filtro"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label lbl1 
      Caption         =   "Filtro"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   3840
      Width           =   495
   End
   Begin ComctlLib.ImageList il1 
      Left            =   4320
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "LVComputers.ctx":0754
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichero 
      Caption         =   "Fichero"
      Begin VB.Menu Cargar 
         Caption         =   "&Cargar"
         Shortcut        =   ^S
      End
      Begin VB.Menu GrabarComo 
         Caption         =   "Grabar como..."
      End
   End
End
Attribute VB_Name = "LVComputers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Declare Function InitThunk _
    Lib "netthunk.dll" _
    Alias "_InitThunk@0" () As Long
Private Declare Sub EndThunk _
    Lib "netthunk.dll" _
    Alias "_EndThunk@0" ()
Private Declare Function ReadComputer _
    Lib "netthunk.dll" _
    Alias "_ReadComputer@12" (i&, ByVal comp As Long, ByVal comment As Long) As Integer
    
Public Event NewMaster(cname As String)

Private Sub tv1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        PopupMenu mnuFichero, , x, y
    End If
End Sub

Private Sub txt1_Change()
    Dim i&
    For i = 1 To lv1.ListItems.Count
        If UCase(lv1.ListItems(i).Text & lv1.ListItems(i).SubItems(1)) Like "*" & UCase(txt1.Text) & "*" Then
            lv1.ListItems(i).SubItems(2) = "*"
        Else
            lv1.ListItems(i).SubItems(2) = ""
        End If
    Next i
    lv1.Refresh
End Sub

Private Sub UserControl_Initialize()
    Dim nc&, i&, comp As String, come As String, ret&, li As ListItem
    Dim n As Node
    nc = InitThunk()
    Screen.MousePointer = 11
    For i = 0 To nc - 1
        comp = Space$(128)
        come = Space$(256)
        ret = ReadComputer(i, StrPtr(comp), StrPtr(come))
        comp = Trim(comp)
        come = Trim(come)
        Set li = lv1.ListItems.Add(, comp, comp, 1, 1)
        li.SubItems(1) = come
    Next
    Call EndThunk
    come = Environ("COMPUTERNAME")
    Set n = AppendBranch(tv1, come, 0, 0)
    n.Expanded = True
    n.Selected = True
    Call lv1.ListItems.Remove(come)
    Screen.MousePointer = 0
End Sub

Private Sub lv1_ItemClick(ByVal Item As ComctlLib.ListItem)
    lv1.Drag vbBeginDrag
End Sub

Private Sub tv1_DragDrop(Source As Control, x As Single, y As Single)
    Dim i%, n As Node
    If tv1.Nodes.Count = 0 Then
        For i = 1 To lv1.ListItems.Count
            If lv1.ListItems(i).Selected = True Then
                Set n = AppendBranch(tv1, lv1.ListItems(i), 0, 0)
                n.Tag = lv1.ListItems(i).SubItems(1)
                n.Expanded = True
                n.Selected = True
                i = lv1.ListItems.Count
                RaiseEvent NewMaster(n.Key)
            End If
        Next
        i = 1
    Else
        Set n = tv1.SelectedItem
        For i = 1 To lv1.ListItems.Count
            If lv1.ListItems(i).Selected = True Then
                Set n = AppendBranch(tv1, tv1.SelectedItem.FullPath & "\" & lv1.ListItems(i), 0, 0)
                n.Tag = lv1.ListItems(i).SubItems(1)
                n.Expanded = True
            End If
        Next
        i = 1
    End If
    While i <= lv1.ListItems.Count
        If lv1.ListItems(i).Selected = True Then
            Call lv1.ListItems.Remove(i)
            i = i - 1
        End If
        i = i + 1
    Wend
End Sub

Private Sub RestoreNode(n As Node)
    Dim li As ListItem
    While n.Children > 0
        Call RestoreNode(n.Child)
    Wend
    Set li = lv1.ListItems.Add(, n.Text, n.Text, 1, 1)
    li.SubItems(1) = n.Tag
    Call tv1.Nodes.Remove(n.Key)
End Sub

Private Sub tv1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        Call RestoreNode(tv1.SelectedItem)
    End If
End Sub

Private Sub UserControl_Resize()
    tv1.Top = 0
    tv1.Left = 0
    tv1.Height = (UserControl.Height - lbl1.Height) / 2
    tv1.Width = UserControl.Width
    lbl1.Top = tv1.Height
    lbl1.Left = 0
    txt1.Top = lbl1.Top
    txt1.Left = lbl1.Width
    lv1.Top = tv1.Height + lbl1.Height
    lv1.Left = 0
    lv1.Height = (UserControl.Height - lbl1.Height) / 2
    lv1.Width = UserControl.Width
End Sub

Private Sub lv1_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
    If ColumnHeader.Index = 1 Then
        If lv1.SortKey = 0 Then
            lv1.SortOrder = IIf(lv1.SortOrder = lvwAscending, lvwDescending, lvwAscending)
        Else
            lv1.SortKey = 0
            lv1.SortOrder = lvwAscending
        End If
    ElseIf ColumnHeader.Index = 2 Then
        If lv1.SortKey = 1 Then
            lv1.SortOrder = IIf(lv1.SortOrder = lvwAscending, lvwDescending, lvwAscending)
        Else
            lv1.SortKey = 1
            lv1.SortOrder = lvwAscending
        End If
    ElseIf ColumnHeader.Index = 3 Then
        If lv1.SortKey = 2 Then
            lv1.SortOrder = IIf(lv1.SortOrder = lvwAscending, lvwDescending, lvwAscending)
        Else
            lv1.SortKey = 2
            lv1.SortOrder = lvwAscending
        End If
    End If
    lv1.Sorted = True
End Sub

Public Property Get getCompTree() As String
    If tv1.Nodes.Count < 2 Then
        getCompTree = ""
    Else
        getCompTree = CompTree(tv1.Nodes(1))
    End If
End Property

Private Function CompTree(n As Node) As String
    Dim i&, s$, nl$, nh&
    nl = Chr(13) & Chr(10)
    If n = tv1.Nodes(1) Then
        nh = 1
    Else
        nh = n.Parent.Children
    End If
    For i = 1 To nh
        s = s & n.Text & nl
        If n.Children > 0 Then s = s & " { " & CompTree(n.Child) & " }" & nl
        Set n = n.Next
    Next
    CompTree = s
End Function


Public Property Get Master() As String
    Dim ret$
    If tv1.Nodes.Count > 0 Then
        ret = tv1.Nodes(1).Key
    End If
    Master = ret
End Property

Private Sub Cargar_Click()
    Dim i&, strs$(0)
    cd1.ShowOpen
    If cd1.filename <> "" Then
        If tv1.Nodes.Count > 0 Then Call RestoreNode(tv1.Nodes(1))
        On Error Resume Next
        Call LoadTree(tv1, cd1.filename, strs, 0)
        For i = 1 To tv1.Nodes.Count
            Call lv1.ListItems.Remove(tv1.Nodes(i).Text)
        Next
        On Error GoTo 0
    End If
    If tv1.Nodes.Count > 0 Then RaiseEvent NewMaster(tv1.Nodes(1).Key)
End Sub

Private Sub GrabarComo_Click()
    cd1.ShowSave
    If cd1.filename <> "" Then
        Call SaveTree(tv1, cd1.filename)
    End If
    
End Sub


