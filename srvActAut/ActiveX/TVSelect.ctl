VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl TVSelect 
   Alignable       =   -1  'True
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4650
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4410
   ControlContainer=   -1  'True
   ScaleHeight     =   4650
   ScaleWidth      =   4410
   ToolboxBitmap   =   "TVSelect.ctx":0000
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   840
      TabIndex        =   4
      Top             =   4200
      Width           =   3255
   End
   Begin MSComDlg.CommonDialog cd1 
      Left            =   3840
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   2040
      TabIndex        =   2
      Top             =   2520
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.DirListBox Dir1 
      Height          =   315
      Left            =   2040
      TabIndex        =   1
      Top             =   3000
      Visible         =   0   'False
      Width           =   615
   End
   Begin ComctlLib.TreeView tv1 
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   7011
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   88
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "il1"
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Caption         =   "Comando:"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   4080
      Width           =   735
   End
   Begin ComctlLib.ImageList il1 
      Left            =   2880
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   5
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TVSelect.ctx":0312
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TVSelect.ctx":062C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TVSelect.ctx":0946
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TVSelect.ctx":0C60
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "TVSelect.ctx":0F7A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFichero 
      Caption         =   "Fichero"
      Begin VB.Menu Cargar 
         Caption         =   "Cargar"
      End
      Begin VB.Menu GrabarComo 
         Caption         =   "Grabar como..."
      End
   End
End
Attribute VB_Name = "TVSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private drv As String
Private cname As String
Private nod As Node

Public Property Get Ficheros() As String
    Dim n As Node, s$
    Set n = tv1.Nodes.Item(2)
    If n.Image = 4 Then Call tv1_Expand(n)
    Call getNodeFiles(n, s)
    Ficheros = s
End Property

Private Function getRootNodeText() As String
    Dim s As String
    If Left(cname, 1) = "\" Then
        s = Right(cname, Len(cname) - 1) & "\" & drv & "$"
    Else
        s = cname & "\" & drv & "$"
    End If
    getRootNodeText = s
End Function

Private Sub ReloadAll()
    Dim n As Node, s As String
    Screen.MousePointer = 11
    Err.Number = 0
    Dir1.Path = "\\" & getRootNodeText()
    File1.Path = Dir1.Path
    If Err.Number <> 0 Then
        MsgBox "Esa computadora debe tener compartido los shares C$ y ADMIN$"
    End If
    tv1.Nodes.Clear
    Set n = AppendBranch(tv1, getRootNodeText(), 0, 0)
    n.Image = 3
    Call AppendBranch(tv1, getRootNodeText() & "\...", 0, 0)
    Screen.MousePointer = 0
End Sub

Private Sub RefreshDir(n As Node, sele As Boolean)
    Dim n2 As Node, s$, imgFile%, imgDir%, i%
    imgFile = IIf(sele, 2, 1)
    imgDir = IIf(sele, 4, 3)
    Err.Number = 0
    Dir1.Path = "\\" & n.fullPath
    File1.Path = "\\" & n.fullPath
    If Err.Number <> 0 Then Exit Sub
    For i = 0 To Dir1.ListCount - 1
        s = lastDir(Dir1.List(i))
        Set n2 = AppendBranch(tv1, n.fullPath & "\" & s, 0, 0)
        n2.Image = imgDir
        Call AppendBranch(tv1, n2.fullPath & "\...", 0, 0)
    Next i
    For i = 0 To File1.ListCount - 1
        s = File1.List(i)
        If s <> "?" Then
            Call AppendBranch(tv1, n.fullPath & "\" & s, imgFile, 0)
        End If
    Next i
End Sub

Private Function lastDir(s As String) As String
    Dim i As Long, j As Long, k As Long
    i = InStr(s, "\"): k = Len(s)
    While i > j
        j = i
        i = i + InStr(Mid(s, i + 1, k - i), "\")
    Wend
    lastDir = Mid(s, j + 1, k - i)
End Function

Private Function SelectTree(n As Node)
    Call SelectTreeRec(n)
    Call ConfirmNode(n)
End Function

Private Function UnselectTree(n As Node)
    Call UnselectTreeRec(n)
    Call ConfirmNode(n)
End Function

Private Function SelectTreeRec(n2 As Node)
    Dim n As Node, i As Integer
    Set n = n2.Child
    For i = 0 To n2.Children - 1
        Select Case n.Image
            Case 1: n.Image = 2
            Case 2:
            Case 3: n.Image = 4: Call SelectTreeRec(n)
            Case 4:
            Case 5: n.Image = 4: Call SelectTreeRec(n)
        End Select
        Set n = n.Next
    Next i
End Function

Private Function UnselectTreeRec(n2 As Node)
    Dim n As Node, i As Integer
    Set n = n2.Child
    For i = 0 To n2.Children - 1
        Select Case n.Image
            Case 1:
            Case 2: n.Image = 1
            Case 3:
            Case 4: n.Image = 3: Call UnselectTreeRec(n)
            Case 5: n.Image = 3: Call UnselectTreeRec(n)
        End Select
        Set n = n.Next
    Next i
End Function

Private Function ConfirmNode(n As Node)
    Dim nodo As Node, nch As Integer, nse%, nun%, i%
    If n.Key = tv1.Nodes(1).Key Then Exit Function
    nch = n.Parent.Children
    Set nodo = n.Parent.Child
    For i = 0 To nch - 1
        If nodo.Image = 2 Or nodo.Image = 4 Then nse = nse + 1
        If nodo.Image = 1 Or nodo.Image = 3 Then nun = nun + 1
        Set nodo = nodo.Next
    Next
    If nun = nch Then
        n.Parent.Image = 3
    ElseIf nse = nch Then
        n.Parent.Image = 4
    Else
        n.Parent.Image = 5
    End If
    If n.Parent.Key <> tv1.Nodes(1).Key Then Call ConfirmNode(n.Parent)
End Function

Private Sub Text1_Change()
    On Error Resume Next
    nod.Tag = Text1.Text
    On Error GoTo 0
End Sub

Private Sub tv1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        PopupMenu mnuFichero, , x, y
    End If
End Sub

Private Sub UserControl_Initialize()
    cname = Environ("COMPUTERNAME")
End Sub

Private Sub UserControl_Resize()
    tv1.Width = UserControl.Width
    tv1.Height = UserControl.Height - Label1.Height
    Label1.Left = 0
    Label1.Top = tv1.Height
    Text1.Left = Label1.Width
    Text1.Top = Label1.Top
    Text1.Width = UserControl.Width - Label1.Width
End Sub

Private Sub getNodeFiles(n As Node, s As String)
    Dim h As Node, nch%, i%, nl$, glMaster$, glDisk$, fullPath$
    Set h = n.Child
    nch = n.Children
    nl = Chr(13) & Chr(10)
    For i = 0 To nch - 1
        DoEvents
        Select Case h.Image
            Case 2
                glMaster = tv1.Nodes(1).Text
                glDisk = tv1.Nodes(2).Text
                tv1.Nodes(1).Text = ""
                tv1.Nodes(2).Text = drv & ":"
                fullPath = Right(h.Parent.fullPath, Len(h.Parent.fullPath) - 1)
                s = s & Chr(34) & h.Text & Chr(34) & _
                    " { " & Chr(34) & fullPath & Chr(34) & " " & _
                    Chr(34) & h.Tag & Chr(34) & " }" & nl
                tv1.Nodes(1).Text = glMaster
                tv1.Nodes(2).Text = glDisk
            Case 4
                Call tv1_Expand(h)
                Call getNodeFiles(h, s)
            Case 5
                Call getNodeFiles(h, s)
        End Select
        Set h = h.Next
    Next
End Sub

Private Sub tv1_NodeClick(ByVal Node As ComctlLib.Node)
    Select Case Node.Image
        Case 1: Node.Image = 2: Call ConfirmNode(Node)
        Case 2: Node.Image = 1: Call ConfirmNode(Node)
        Case 3: Node.Image = 4: Call SelectTree(Node)
        Case 4: Node.Image = 3: Call UnselectTree(Node)
        Case 5: Node.Image = 3: Call UnselectTree(Node)
    End Select
    tv1.Refresh
    Set nod = Node
    Text1.Text = Node.Tag
End Sub

Private Sub tv1_Expand(ByVal Node As ComctlLib.Node)
    Dim n As Node
    Set n = Node.Child
    If Right(n.Text, 3) = "..." Then
        Call tv1.Nodes.Remove(n.Key)
        If Node.Image = 4 Then
            Call RefreshDir(Node, True)
        Else
            Call RefreshDir(Node, False)
        End If
    End If
End Sub

Public Property Let Drive(ByVal vNewValue As String)
    drv = Left(vNewValue, 1)
    Call ReloadAll
End Property

Public Property Get Drive() As String
    Drive = drv
End Property

Public Property Get Computer() As String
    Computer = cname
End Property

Public Property Let Computer(ByVal vNewValue As String)
    cname = vNewValue
    Call ReloadAll
End Property

Private Sub Cargar_Click()
    Dim pth$, out$(), nle&, i%
    cd1.ShowOpen
    If cd1.filename <> "" Then
        On Error Resume Next
        MousePointer = 11
        Open cd1.filename For Input As #1
        Input #1, pth
        While Err.Number = 0
            nle = DecompBranch(pth, out)
            pth = "\"
            For i = 1 To nle - 1
                pth = pth & UCase(out(i))
                tv1.Nodes(pth).Expanded = True
                pth = pth & "\"
            Next
            pth = pth & UCase(out(i))
            Call tv1_NodeClick(tv1.Nodes(pth))
            Input #1, pth
        Wend
        Close #1
        MousePointer = 0
        On Error GoTo 0
    End If
End Sub

Private Sub NodeGrab(nn As Node)
    Dim i&, nch&, n As Node
    Set n = nn
    Select Case n.Image
        Case 2:
            Write #1, n.fullPath
        Case 4:
            Write #1, n.fullPath
        Case 5:
            nch = n.Children
            Set n = n.Child
            For i = 1 To nch
                Call NodeGrab(n)
                Set n = n.Next
            Next
    End Select
End Sub

Private Sub GrabarComo_Click()
    Dim i&
    cd1.ShowSave
    If cd1.filename <> "" Then
        Open cd1.filename For Output As #1
        Call NodeGrab(tv1.Nodes(2))
        Close #1
    End If
End Sub


