#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;

//QrBatch\0Comandos\0
int QrBatch(SOCKET sock, char *cmd){
	int j,NeedReboot;
	LisAct lis;
	char computername[128];
	if(!Decompile(cmd+strlen(cmd)+1,&lis)){
		sprintf(Msg,"Error Decompile en QrBatch: %s",cmd+strlen(cmd)+1);
		SendMessageTo(&lis,"","",Msg);
		return 0;
	}
	SendMessageTo(&lis,"","","Orden de actualizacion recibida.Comprobando...");
	//Copiar los archivos remotos (Aquellos que est�n out-of-date
	GetRemoteFiles(&lis);
	//Actualizar archivos en cola. Puede pedir reiniciar.
	int OOfDate=0;
	for(j=0;!OOfDate && j<lis.fic->carfic;j++)
		if( lis.fic->downloaded[j] )
			OOfDate=1;
	if( OOfDate ){
		NeedReboot=CopyQueue( &lis );//Ahora se sabe si se pospuso alguna copia
		RegistrarArchivos(&lis);	//Se registran 
	}else{
		NeedReboot=0;
		RegistrarArchivos(&lis);	//Se registran 
		SendMessageTo(&lis,"","","Todos los ficheros estan actualizados.");
	}
	SendMessageTo(&lis,"","","Actualizacion Completada");
	//Proceder con los subgrupos. Si NeedReboot, sustituir a esta maquina como
	//jefe de grupo y poner a su padre.
	gethostname(computername,127);
	for(j=0;lis.cpu->cpus && j<lis.cpu->cpus->ncpu;j++){
		//Buscar esta maquina
		if(!stricmp(computername,((CPU*)(lis.cpu->cpus->cpu[j]))->cname) &&
			//Tiene hijas?
			((CPU*)(lis.cpu->cpus->cpu[j]))->cpus ){
			//Si no se va a rearrancar esta maquina, proceder normalmente
			if( !NeedReboot )
				SendCommandToCPU(&lis,(CPU*)(lis.cpu->cpus->cpu[j]),szRsBatch);
			else{
				SendMessageTo(&lis,"","","Cambio de maestro de grupo por el abuelo.");
				//Poner al padre para hacer el trabajo del hijo.
				free(lis.cpu->cpus->cpu[j]->cname);
				lis.cpu->cpus->cpu[j]->cname=strdup( lis.cpu->cname );
				SendCommandToCPU(&lis,(CPU*)(lis.cpu->cpus->cpu[j]),szRsBatch);
			}
		}
	}
	FreeAll(&lis);
	closesocket(sock);
	return 0;
}
