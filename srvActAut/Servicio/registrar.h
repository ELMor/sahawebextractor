/*
 * Fichero registrar.h
 */


#ifndef __REGISTRAR_H__
#define __REGISTRAR_H__

#include "decompile.h"

void RegistrarArchivos( LisAct* plis);
void RegistrarArchivos( LisAct* plis, char *fname, char *path, char *cmd, int delay );
void ProcDelayedFic();
void DelayFic( LisAct* plis, char *fname, char *path, char *cmd );

void ConvertUCtoS(char* dst, const char* org);

#define szPathReg 	"System\\CurrentControlSet\\Services\\ActAutService\\PendingFiles"

#endif