
#include <direct.h>
#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"
#include "filecrc32.h"
#include "direx.h"
#include "configdir.h"

extern BOOL bDebug;

void ReceiveFile(SOCKET sck, Fics* from, int pos){
	char tmpf[512],*command;
	unsigned long leidos;
	//Utilizar un nombre de fichero temporal, para que no se pisen!!!!
	strcpy(tmpf,TEMP_DEST);			//TEMP_DEST ya tiene la barra de directorio al final
	strcat(tmpf,from->path[pos]+3);	//A�adir parte del directorio destino
	strcat(tmpf,"\\");				//Barra final
	mkdirex( tmpf );				//Asegurar que existe
	strcat(tmpf,from->fname[pos]);	//Finalmente a�adir el archivo
	FILE *fp=fopen(tmpf,"wb");
	from->downloaded[pos]=1;
	command=(char*)malloc(SizeXMitBuf);
	//Recepcion de multiples paquetes desde la computadora remota
	while((leidos=recv(sck,command,SizeXMitBuf,0))>0)
		fwrite(command,1,leidos,fp);
	fclose(fp);
	free(command);
}

void GetRemoteFiles(LisAct* la){
	char *locfname,*buf,*command;
	Fics *fic=la->fic;
	unsigned long lcom,crc;
	struct _stat st1;
	long fictam;
	SOCKET sck;
	int j,ret;

	locfname=(char*)malloc(1024);
	command=(char*)malloc(SizeXMitBuf);
	mkdirex(TEMP_DEST);
	for(j=0;j<fic->carfic;j++){
		fic->downloaded[j]=0;
		//Crear path del archivo local (por si existe y est� actualizado)
		mkdirex( fic->path[j] ); //Si ya existe no lo toca.
		strcat(strcat(strcpy(locfname,fic->path[j]),"\\"),fic->fname[j]);
		//Estadisticas del archivo
		fictam = _stat(locfname,&st1)==0 ? st1.st_size : -1 ;
		//Comando al Host: Comando, Nombre, Path y tama�o.
		buf=command;				lcom=0;
		strcpy(buf,szQrGetFile);	lcom+=strlen(buf)+1;	buf=command+lcom;
		strcpy(buf,fic->path[j] );	lcom+=strlen(buf)+1;	buf=command+lcom;
		strcpy(buf,fic->fname[j]);	lcom+=strlen(buf)+1;	buf=command+lcom;
		memcpy(buf,&fictam,4);		lcom+=4;				buf=command+lcom;
		//Envio.
		sck=SendCommandToCPU(la->cpu->cname,command,lcom);
		//Respuesta.
		ret=recv(sck,command,SizeXMitBuf,0);
		if(!stricmp(command,szFileNotFound)){
			SendMessageTo(la,fic->path[j],fic->fname[j],
				"El servidor no encuentra el archivo");
		}else if(!stricmp(command,szSendFile)){	//El host quiere enviar el archivo:
			ReceiveFile(sck,fic,j);
			SendMessageTo(la,fic->path[j],fic->fname[j],
				"Fichero copiado temporalmente.");
		}else if(!stricmp(command,szFileCRC32) ){	//El host quiere que calculemos el CRC
			SendMessageTo(la,fic->path[j],fic->fname[j],
				"Tama�o igual, calculando CRC32...");
			GetFileCRC32(locfname,&crc);
			//envio
			send(sck,(const char*)&crc,sizeof(crc),0);
			//recepcion
			recv(sck,command,SizeXMitBuf,0);
			if(!stricmp(command,szSendFile)){	//El host quiere enviar el archivo:
				ReceiveFile(sck,fic,j);
				SendMessageTo(la,fic->path[j],fic->fname[j],
					"Fichero copiado temporalmente.");
			}else{
				SendMessageTo(la,fic->path[j],fic->fname[j],
					"Fichero NO Necesita actualizacion.");
			}
		}
		closesocket( sck );
	}
	free(locfname);
	free(command);
}
