
#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

HANDLE ThreadsCI[1024];
unsigned long nTCI;

void CInstalacion(void *vp){
	int ret;
	char msg[512];
	void **p=(void**)vp;

	LisAct *la=(LisAct*) (p[0]);
	CPU    *pc=(CPU*)    (p[1]);

	printf("Ping a %s...\n",pc->cname);
	ret=Ping( pc->cname );
	if(ret==WSAETIMEDOUT){
		sprintf(msg,"%s ESTA APAGADO!!!",pc->cname);
		SendMessageTo(la,"","",msg);
	}else if(ret==WSAECONNREFUSED){
		sprintf(msg,"Instalando servicio a %s",pc->cname);
		SendMessageTo(la,"","",msg);
		if(CmdInstallService(pc->cname,"srvactaut.exe"))
			CmdStartService(pc->cname);
	}else if( ret==OLDER_REMOTE_VERSION ){
		sprintf(msg,"Actualizando servicio a %s",pc->cname);
		SendMessageTo(la,"","",msg);
        CmdRemoveService(pc->cname);
		if(CmdInstallService(pc->cname,"srvactaut.exe"))
			CmdStartService(pc->cname);
	}else if( ret==NEWER_REMOTE_VERSION || ret==SAME_REMOTE_VERSION){
		sprintf(msg,"%s Tiene la versi�n correcta de srvActAut",pc->cname);
		SendMessageTo(la,"","",msg);
	}
	free(vp);
	ExitThread(0);
}

void CInstalaciones(LisAct* la, CPU* pc){
	void **p;
	int i;

	p=(void**)malloc(2*sizeof(void*));
	p[0]=(void*)(la);
	p[1]=(void*)(pc);
	ThreadsCI[nTCI++]=(HANDLE)_beginthread(CInstalacion,0,(void*)p);

	for(i=0;pc->cpus && i<pc->cpus->ncpu;i++)
		CInstalaciones( la, pc->cpus->cpu[i] );
}

void CompruebaInstalaciones(LisAct *la, CPU *pc){
	unsigned long i;
	nTCI=0;
	DWORD Status;
	int Continue=1;

	CInstalaciones(la,pc);

	while(Continue){
		Sleep(1000);
		Continue=0;
		for(i=0;i<nTCI;i++){
			GetExitCodeThread(ThreadsCI[i],&Status);
			if(Status==STILL_ACTIVE)
				Continue=1;
		}
	}
}