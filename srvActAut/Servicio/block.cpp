#include "block.h"

Nodo::Nodo(){
	nombre=NULL;
	tamCont=0;
	contenido=NULL;
	padre=NULL;
	numHijos=0;
	hijos=NULL;
}

void
Nodo::setNom(char *s){
	if(nombre) 
		free(nombre);
	if(s) 
		nombre=strdup(s);
	else  
		nombre=NULL;
}

void
Nodo::getNom(){
	return nombre;
}

void 
Nodo::setTamCont(long tam)
	tamCont=tam;
}

void 
Nodo::setCont(voip *pc){
	contenido=pc;
}


long
Nodo::getNumHijos(){
	return numHijos;
}

void
Nodo::addHijo(Nodo& nod){
	hijos=(Nodo**)realloc(hijos,(1+numHijos)*sizeof(Nodo*));
	nod.setPadre(this);
	hijos[numHijos++]=&nod;
}

Nodo*
Nodo::addHijo(char *s){
	char *out,*gout;
	long npaths=trocea(s,gout=out=strdup(s)),i,j,notfound;
	Nodo *este=this;
	while(npaths){
		notfound=1;
		for(j=0;notfound && j<este->getNumHijos();j++){
			if(!stricmp(out,este->getNod(j)->getNom())){
				este=este->getNod(j);
				out+=strlen(out)+1;
				npaths--;
				notfound=0;
			}
		}
		if(notfound){
			for(i=0;i<npaths;i++){
				Nodo *estehijo=new Nodo();
				estehijo->setNom(out);
				estehijo->setpadre(este);
				este=estehijo;
			}
			npaths=0;
		}
	}
	free(gout);
	return este;
}

Nodo*
Nodo::getNod(long index){
	if(index>=0 && index<numHijos)
		return hijos[index];
	return NULL;
}

Nodo*
Nodo::busca(char *spath){
	char *out,*gout;
	long npaths=trocea(s,gout=out=strdup(s)),i,j,notfound;
	Nodo *este=this;
	while(npaths){
		notfound=1;
		for(j=0;notfound && j<este->getNumHijos();j++){
			if(!stricmp(out,este->getNod(j)->getNom())){
				este=este->getNod(j);
				out+=strlen(out)+1;
				npaths--;
				notfound=0;
			}
		}
		if(notfound){
			npaths=0;
			este=NULL;
		}
	}
	free(gout);
	return este;
}

void //Reserva memoria: el que llama debe liberar la memoria una vez acabado
Nodo::serial(void *&p){

}


long
Nodo::trocea(char *s, char *troceado){
	long ret=1;
	char *aux;
	troceado=strdup(s);
	while(NULL!=(troceado=strchr(troceado,SEPCHAR))){
		*troceado=0;
		ret++;
	}
	return ret;
}

