/*
 * Fichero dump.h
 */


#ifndef __DUMP_H__
#define __DUMP_H__

#include <stdio.h>
#include "decompile.h"

inline void dumpCPUs(CPUs *cpus, char *s);

inline void dumpCPU(CPU *cpu, char *s){
	strcat(strcpy(s,cpu->cname)," ");
	if(cpu->cpus){
		strcat(s,"{ ");
		dumpCPUs(cpu->cpus,s+strlen(s));
		strcat(s,"} ");
	}
}

inline void dumpCPUs(CPUs *cpus, char *s){
	int i;
	if( cpus )
		for(i=0;i<cpus->ncpu;i++)
			dumpCPU((CPU*)(cpus->cpu[i]),s+strlen(s));
}

int CompruebaFic(Fics *fic);

#endif