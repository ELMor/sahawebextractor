/*
 * Fichero socksrv.cpp
 */

int ServiceTryingToStop=0;

DispatchTable dsptab[]={
	{szQrGetFile,QrGetFile},
	{szQrBatch	,QrBatch  },
	{szRsBatch	,RsBatch  },
	{szRsPing	,RsPing   },
	{NULL       ,NULL     }
};

#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;

