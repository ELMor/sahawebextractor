/*
 * Fichero servicio.cpp
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <tchar.h>
#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "monitor.h"

extern int ServiceTryingToStop;
HANDLE  hServerStopEvent = NULL;

VOID ServiceStart (DWORD dwArgc, LPTSTR *lpszArgv)
{
    HANDLE                  hEvents[2] = {NULL, NULL};

    if (!ReportStatusToSCMgr(SERVICE_START_PENDING,NO_ERROR,3000))
        goto cleanup;
    hServerStopEvent = CreateEvent(NULL,TRUE,FALSE,NULL);
    if ( hServerStopEvent == NULL)
        goto cleanup;
    hEvents[0] = hServerStopEvent;
    if (!ReportStatusToSCMgr(SERVICE_START_PENDING,NO_ERROR,3000))
        goto cleanup;
    hEvents[1] = CreateEvent(NULL,TRUE,FALSE,NULL);
    if ( hEvents[1] == NULL)
        goto cleanup;
    if (!ReportStatusToSCMgr(SERVICE_RUNNING,NO_ERROR,0))
        goto cleanup;
	//Inicio de un Thread de Monitorizaci�n de logon
	_beginthread( MonitorLogon, 0, NULL);
	//Inicio de un thread de escucha socket
	_beginthread( MainSock,0,NULL );
	//Inicio de thread para recepcion de mensajes
	InitMensajesSrvr();
	do {
		Sleep( 5000 );  // 5 segundos. Si se intenta detener desde Panel de control fallar�
						// porque lo m�ximo que espera son 3 segundos. Si se pone 3 segundos 
						// se carga la m�quina innecesariamente.
	}while( !ServiceTryingToStop );
cleanup:
    if (hServerStopEvent)
        CloseHandle(hServerStopEvent);
    if (hEvents[1]) // overlapped i/o event
        CloseHandle(hEvents[1]);

}

VOID ServiceStop()
{
	ServiceTryingToStop=1;
    if ( hServerStopEvent )
        SetEvent(hServerStopEvent);
}
