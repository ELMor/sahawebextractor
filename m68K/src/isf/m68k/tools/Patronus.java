package isf.m68k.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

public class Patronus {
	String fileName=null;
	File file=null;
	
	public Patronus(String a[]){
		if(a.length==0)
			fileName="code0001.bin.s";
		else
			fileName=a[0];
		file=new File(fileName);
	}
	public void process() throws Exception{
		BufferedReader br=new BufferedReader(new FileReader(file));
		String line[]={null,null,null};
		long numbers[][]={{0,0,0},{0,0,0},{0,0,0}};
		int linenum=0;
		while( null!=(line[linenum%3]=br.readLine()) ){
			int current=linenum%3;
			int ayer=(linenum-1)%3;
			int anteayer=(linenum-2)%3;
			StringTokenizer st=new StringTokenizer(line[current],"\t ");
			numbers[current][0]=Long.parseLong(st.nextToken(),16);
			String op=st.nextToken().toUpperCase();
			if(op.length()>=4 && op.length()<=12){
				String opcode=op.substring(0,4);
				String opvalu=op.substring(4);
				numbers[current][1]=Long.parseLong(opcode,16);
				numbers[current][2]=opvalu.length()==0 ? 0 : Long.parseLong(opvalu,16);
			}
			if(		numbers[current][1]==0x4E75 
				&&  numbers[ayer][1]==0x0697
				&&  numbers[anteayer][1]==0x487a
				){
				long branch=numbers[anteayer][2]+6+numbers[anteayer][0]+numbers[ayer][2];
				if(branch>=0x100000000L)
					branch-=0x100000000L;
				System.out.println(line[current]+"; ->-> hidden jsr to "+
						Long.toHexString(branch));
			} else if(linenum>2 && numbers[ayer][1]==0x4e4f && numbers[current][2]>0){
				System.out.println(line[current]+";  unknown trap");
			}else{
				System.out.println(line[current]);
			}
			linenum++;
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Patronus(args).process();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
