--Script para borrar la instalación de hl7 del servidor

drop table hl06;
drop table hl05;
drop table hl04;
drop table hl03;
drop table hl02;
drop table hl01;

drop sequence hl04NumEvento_seq;

drop Package hl7msgbld;
drop package hl7Notif;

drop view SEGMENT_EVN;
drop view SEGMENT_IN1;
drop view SEGMENT_MRG;
drop view SEGMENT_MSH;
drop view SEGMENT_ORC;
drop view SEGMENT_OBR;
drop view SEGMENT_PID;
drop view SEGMENT_PV1;

drop trigger HL7_CLIENTES_ADTA31;
drop trigger HL7_FUSION_ADTA34;
drop trigger HL7_HC_ADTA28;
drop trigger HL7_ORDER_ORMO01;

