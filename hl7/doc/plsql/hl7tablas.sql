-- Creacion del modelo de datos de OpenHL7
-- 1. Activar Logging
SPOOL hl7tablas.log
-- 2. Borrar la posible instalaci�n existente
@dropHL7Tablas.sql
-- 3. Crear Tablas

CREATE TABLE hl01
  (hl01codevento NUMBER PRIMARY KEY,
   hl01ident       VARCHAR2(7) UNIQUE NOT NULL,
   hl01nomevento VARCHAR2(100),
   hl01bldfunc  VARCHAR2(100),
   hl01sendproc  VARCHAR2(100),
   hl01recvproc  VARCHAR2(100)
  );

COMMENT ON TABLE  hl01               IS 'Tipos de eventos y Procesado salida/entrada';
COMMENT ON COLUMN hl01.hl01codevento IS 'Codigo del evento';
COMMENT ON COLUMN hl01.hl01ident     IS 'Identificacion segun MSH';
COMMENT ON COLUMN hl01.hl01nomevento IS 'Nombre del evento';
COMMENT ON COLUMN hl01.hl01nomevento IS 'Stored Function que construye el mensaje';
COMMENT ON COLUMN hl01.hl01sendproc  IS 'Clase java para enviar. Debe implementar ssf.hl7.opensic.Procesador';
COMMENT ON COLUMN hl01.hl01recvproc  IS 'Clase java para procesar recepcion. Debe implementar ssf.hl7.opensic.Procesador';

--Eventos definidos
INSERT INTO hl01
     VALUES (1, 'ADT^A28', 'Nuevo Paciente', 'hl7msgbld.getADTA28', 'ssf.hl7.opensic.out.addPac', 'ssf.hl7.opensic.in.addPac');
INSERT INTO hl01
     VALUES (2, 'ADT^A31', 'Cambio en Paciente', 'hl7msgbld.getADTA31', 'ssf.hl7.opensic.out.updPac', 'ssf.hl7.opensic.in.updPac');
INSERT INTO hl01
     VALUES (3, 'ADT^A34', 'Fusion Pacientes', 'hl7msgbld.getADTA34', 'ssf.hl7.opensic.out.fusPac', 'ssf.hl7.opensic.in.fusPac');
INSERT INTO hl01
     VALUES (4, 'ORM^O01', 'Pedido General', 'hl7msgbld.getORMO01', 'ssf.hl7.opensic.out.envSol', NULL);
INSERT INTO hl01
     VALUES (5, 'ORR^O02', 'Confirmacion Realizacion', NULL, NULL, 'ssf.hl7.opensic.in.recRes');
INSERT INTO hl01
     VALUES (6, 'ORU^R01', 'Tx Observaciones', NULL, NULL, 'ssf.hl7.opensic.in.txrObs');
INSERT INTO hl01
     VALUES (7, 'DFT^P03', 'Tx Financiera', NULL, NULL, 'ssf.hl7.opensic.in.txrFin');
COMMIT ;

CREATE TABLE hl02
  (hl02codsys NUMBER PRIMARY KEY,
   hl02nomsys VARCHAR2(100)
  );

COMMENT ON TABLE hl02 IS 'Sistemas involucrados en el trafico HL7';
COMMENT ON COLUMN hl02.hl02codsys IS 'Codigo del sistema';
COMMENT ON COLUMN hl02.hl02nomsys IS 'Nombre del sistema';

INSERT INTO hl02
     VALUES (1, 'OpenSIC');
INSERT INTO hl02
     VALUES (2, 'Medora');
INSERT INTO hl02
     VALUES (3, 'LIS');
COMMIT ;

CREATE TABLE hl03
  (hl02codsys_ori NUMBER REFERENCES hl02(hl02codsys),
   hl02codsys_des NUMBER REFERENCES hl02(hl02codsys),
   hl03numhandler NUMBER DEFAULT 0,
   hl03sessionid  VARCHAR2(24),
   hl03segundos    NUMBER DEFAULT 5,
   hl03path        VARCHAR2(256),
   hl03filepref     VARCHAR2(256),
   hl03filesuff      VARCHAR2(256),
   hl03delfile   NUMBER DEFAULT 0,
   hl03movedir   VARCHAR2(256),
   hl03server      VARCHAR2(256),
   hl03port        NUMBER DEFAULT 0,
   hl03seqmsg        NUMBER DEFAULT 1,
   hl03begmsgchar VARCHAR2(10),
   hl03endmsgchar VARCHAR2(10)
  );

ALTER TABLE hl03 ADD CONSTRAINT hl0301
  PRIMARY KEY(hl02codsys_ori,hl02codsys_des);

COMMENT ON TABLE hl03 IS 'Forma de Comunicaciones entre sistemas (File/TCP)';
COMMENT ON COLUMN hl03.hl02codsys_ori IS 'codigo del sistema emisor';
COMMENT ON COLUMN hl03.hl02codsys_des IS 'codigo del sistema receptor';
COMMENT ON COLUMN hl03.hl03numhandler IS 'Numero de manejador (argumento ssf.hl7.Main)';
COMMENT ON COLUMN hl03.hl03sessionid IS 'Sesion oracle que maneja la conexion';
COMMENT ON COLUMN hl03.hl03segundos IS 'Intervalo en Segundos de los chequeos';
COMMENT ON COLUMN hl03.hl03path IS 'Directorio (del servidor) destino del archivo';
COMMENT ON COLUMN hl03.hl03filepref IS 'Prefijo de fichero (se buscan los path||pref*suff)';
COMMENT ON COLUMN hl03.hl03filesuff IS 'Sufijo de fichero (se buscan los path||pref*suff)';
COMMENT ON COLUMN hl03.hl03delfile IS 'Indica si se borra el archivo cuando se procesa';
COMMENT ON COLUMN hl03.hl03movedir IS 'Indica el directorio donde se mueve el fichero procesado';
COMMENT ON COLUMN hl03.hl03server IS 'Nombre o direccion IP del servidor';
COMMENT ON COLUMN hl03.hl03port IS 'Puerto TCP de comunicaciones';
COMMENT ON COLUMN hl03.hl03seqmsg IS 'Requiere mensajes secuenciales, NULL no requiere, otro secuencia';
COMMENT ON COLUMN hl03.hl03begmsgchar IS 'Caracteres de inicio de mensaje en transmision TCP/IP';
COMMENT ON COLUMN hl03.hl03endmsgchar IS 'Caracteres de fin    de mensaje en transmision TCP/IP';

--Ejemplo de registros:
--OpenSIC a Medora
INSERT INTO hl03
     VALUES (1, 2, 1, NULL, 5, NULL, NULL, NULL, 0, NULL, '127.0.0.1', 19199, 1, CHR (
                                                                                    11
                                                                                 ),    CHR (
                                                                                          28
                                                                                       )
                                                                                    || CHR (
                                                                                          13
                                                                                       ));
--OpenSIC a LAB
INSERT INTO hl03
     VALUES (1, 3, 1, NULL, 5, 'c:\tmp\toLab', 'msg', '.dat', 0, 'c:\tmp\toLab\proc', NULL, NULL, 1, NULL, CHR (
                                                                                                              13
                                                                                                           ));
--Medora a OpenSIC
INSERT INTO hl03
     VALUES (2, 1, 1, NULL, 5, NULL, NULL, NULL, 0, NULL, '127.0.0.1', 19199, 1, CHR (
                                                                                    11
                                                                                 ),    CHR (
                                                                                          28
                                                                                       )
                                                                                    || CHR (
                                                                                          13
                                                                                       ));
--LAB a OpenSIC
INSERT INTO hl03
     VALUES (3, 1, 1, NULL, 5, 'c:\tmp\toLab', 'msg', '.dat', 0, 'c:\tmp\toLab\proc', NULL, NULL, 1, NULL, CHR (
                                                                                                              13
                                                                                                           ));
COMMIT ;

CREATE TABLE hl04
  (hl04numevento  NUMBER PRIMARY KEY,
   hl02codsys_ori NUMBER REFERENCES hl02(hl02codsys),
   hl02codsys_des NUMBER REFERENCES hl02(hl02codsys),
   hl04transid    NUMBER,
   hl01codevento  NUMBER REFERENCES hl01(hl01codevento),
   hl04fecha      DATE,
   hl04bldmsg        NUMBER DEFAULT 1,
   hl04bldcmd        VARCHAR2(512),
   hl04procstatus NUMBER DEFAULT 0,
   hl04errtext    VARCHAR2(128),
   hl04rawmsg        VARCHAR2(4000)
   );

COMMENT ON TABLE hl04 IS 'Mensajes hl7 enviados/recibidos';
COMMENT ON COLUMN hl04.hl04numevento IS 'numero del evento (pk)';
COMMENT ON COLUMN hl04.hl02codsys_ori IS 'Sistema emisor';
COMMENT ON COLUMN hl04.hl02codsys_des IS 'Sistema receptor';
COMMENT ON COLUMN hl04.hl04transid IS 'Contador de eventos del link (transaction ID)';
COMMENT ON COLUMN hl04.hl01codevento IS 'Tipo del evento';
COMMENT ON COLUMN hl04.hl04fecha IS 'Fecha de evento';
COMMENT ON COLUMN hl04.hl04bldmsg IS 'Indica si se ha de construir el mensaje (en hl04rawMsg)';
COMMENT ON COLUMN hl04.hl04bldcmd IS 'LLamada a Stored Function que retorna el mensaje';
COMMENT ON COLUMN hl04.hl04procstatus IS 'Estado del proceso 0:noproc,1:proc+OK,2:proc+Err';
COMMENT ON COLUMN hl04.hl04errtext IS 'Error';
COMMENT ON COLUMN hl04.hl04rawmsg IS 'Mensaje en formato RAW';

COMMIT ;

CREATE TABLE hl05
   (hl05actdetpk NUMBER PRIMARY KEY,
    hl05rptcode VARCHAR2(40),
    hl05rptfecha DATE,
    hl05rptverfecha DATE,
    hl05status VARCHAR2(10),
    hl05verificador VARCHAR2(40),
    hl05rptdata CLOB);

COMMENT ON TABLE hl05 IS 'Informes';
COMMENT ON COLUMN hl05.hl05actdetpk IS 'Codigo de actividad';
COMMENT ON COLUMN hl05.hl05rptcode IS 'Codigo del informe en Medora';
COMMENT ON COLUMN hl05.hl05rptfecha IS 'Fecha de creacion del informe';
COMMENT ON COLUMN hl05.hl05rptverfecha IS 'Fecha de Verficacion del informe';
COMMENT ON COLUMN hl05.hl05status  IS 'Estado del informe';
COMMENT ON COLUMN hl05.hl05verificador IS 'Codigo de la persona que lo verific�';
COMMENT ON COLUMN hl05.hl05rptdata IS 'Contenido del informe (texto)';

CREATE TABLE hl06
   (hl06codigo NUMBER PRIMARY KEY,
    hl06segmentmrg VARCHAR2(400));

COMMENT ON TABLE hl06 IS 'Guardar segmentos Merge aqu� cuando se borren pacientes';
COMMENT ON COLUMN hl06.hl06codigo IS 'Codigo del paciente que se borr�';
COMMENT ON COLUMN hl06.hl06segmentmrg IS 'Segmento MERGE';

-- -------------------------- Fin de Tablas ----------------------------
-- 4. Crear secuencia de eventos
CREATE SEQUENCE hl04numevento_seq START WITH 1 INCREMENT BY 1 NOCACHE;

-- 5. Creaci�n de Stored Procedures
CREATE OR REPLACE PACKAGE hl7msgbld
AS
   --Obtencion de Datos
   FUNCTION getdirciupro (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getclasepaciente (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getubicacion (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getmedicoresp (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getservicioresp (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getplansalud (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getfinanciadorid (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getfinanciadornombre (cc NUMBER)
      RETURN VARCHAR2;

   FUNCTION getnumeroafiliado (cc NUMBER)
      RETURN VARCHAR2;

   --Obtencion de segmentos HL7 Completos
   FUNCTION getmsh (
      cc   NUMBER, --Codigo de cliente
      so   NUMBER, --Sistema origen
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2;

   FUNCTION getevn
      RETURN VARCHAR2;

   FUNCTION getpid (cc NUMBER --Codigo de cliente
                             )
      RETURN VARCHAR2;

   FUNCTION getmrg (cc NUMBER --Codigo cliente antiguo
                             )
      RETURN VARCHAR2;

   FUNCTION getpv1 (cc NUMBER --Codigo cliente
                             )
      RETURN VARCHAR2;

   FUNCTION getin1 (cc NUMBER --Codigo cliente
                             )
      RETURN VARCHAR2;

   FUNCTION getorc (cd NUMBER --Actividad_det_pk
                             )
      RETURN VARCHAR2;

   FUNCTION getobr (cd NUMBER --Actividad_det_pk
                             )
      RETURN VARCHAR2;

   --Obtencion de Mensajes HL7 completos

   FUNCTION getadta28 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2;

   FUNCTION getadta31 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2;

   FUNCTION getadta34 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER, --Codigo de evento
      oh   NUMBER --Historia antigua
   )
      RETURN VARCHAR2;

   FUNCTION getormo01 (
      ca   NUMBER, --Actividad_pk
      sd   NUMBER, --Sistema destino
      ce   NUMBER, --codigo de evento
      ld   VARCHAR2 --Lista de actividad_det_pk
   )
      RETURN VARCHAR2;
END hl7msgbld;
/

-- 6. Creacion de vistas de construccion de segmentos
--Implementacion de Segmentos hl7 mediante vistas
CREATE OR REPLACE VIEW segment_msh
AS
   SELECT clientes.codigo_cliente, hl0201.hl02codsys hl02codsys_ori,
          hl0202.hl02codsys hl02codsys_des, hl01.hl01codevento,
             'MSH|^~\&'
          || --Head
            '|'
          || hl0201.hl02nomsys
          || --SendingApp
            '|'
          || --SendingFaci
            '|'
          || hl0202.hl02nomsys
          || --ReceivingApp
            '|'
          || --ReceivingFaci
            '|'
          || TO_CHAR (SYSDATE, 'YYYYMMDDHH24MI')
          || --Fecha
            '|'
          || --Security
            '|'
          || hl01.hl01ident
          || --GRP^TIPO
            '|'
          || hl03.hl03seqmsg
          || --Secuencia
            '|'
          || 'P'
          || '|'
          || '2.2'
          || '|'
          || '|'
          || '|'
          || 'AL'
          || '|'
          || 'AL'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM clientes, hl03, hl02 hl0201, hl02 hl0202, hl01
    WHERE hl0201.hl02codsys = hl03.hl02codsys_ori
      AND hl0202.hl02codsys = hl03.hl02codsys_des;


CREATE OR REPLACE VIEW segment_evn
AS
   SELECT    'EVN'
          || '|<eventtypecode>'
          || '|'
          || TO_CHAR (SYSDATE, 'YYYYMMDDHH24MI')
          || '|'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM DUAL;

CREATE OR REPLACE VIEW segment_pid
AS
   SELECT c.codigo_cliente,
             'PID'
          || '|<pidsecuencia>'
          || '|'
          || '|'
          || c.codigo_cliente
          || '|'
          || '|'
          || RTRIM (c.apellido1)
          || ' '
          || RTRIM (c.apellido2)
          || '^'
          || RTRIM (c.nombre)
          || '|'
          || '|'
          || TO_CHAR (c.nac_fecha, 'YYYYMMDD')
          || '|'
          || DECODE (c.codigo_sexo, 3, 'M', 2, 'F', 1, 'U', 'O')
          || '|'
          || '|'
          || '|'
          || hl7msgbld.getdirciupro (c.codigo_cliente)
          || '|'
          || '|'
          || c.telefono1
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || TO_CHAR (c.exitus_fecha, 'YYYYMMDD')
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM clientes c;

CREATE OR REPLACE VIEW segment_mrg
AS
   SELECT c.codigo_cliente,
             'MRG'
          || '|'
          || c.codigo_cliente
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          --       || '|' || RTRIM (c.apellido1) || ' ' || RTRIM (c.apellido2) || '^' || RTRIM (c.nombre)
          || '|'
          || RTRIM (c.apellido1)
          || ' '
          || RTRIM (c.apellido2)
          || '^'
          || RTRIM (c.nombre)
          || '[ENDOFSEGMENT]' rawmsg
     FROM clientes c;

CREATE OR REPLACE VIEW segment_pv1
AS
   SELECT c.codigo_cliente,
             'PV1'
          || '|'
          || '0001'
          || '|'
          || 'O'

--        || hl7msgbld.getclasepaciente (c.codigo_cliente)
          || '|'
          || hl7msgbld.getubicacion (c.codigo_cliente)
          || '|'
          || 'O'
          || '|'
          || '|'
          || '|'
          || hl7msgbld.getmedicoresp (c.codigo_cliente)
          || '|'
          || '|'
          || '|'
          || hl7msgbld.getservicioresp (c.codigo_cliente)
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM clientes c;

CREATE OR REPLACE VIEW segment_in1
AS
   SELECT c.codigo_cliente,
             'IN1'
          || '|'
          || '0001'
          || '|'
          || hl7msgbld.getplansalud (c.codigo_cliente)
          || '|'
          || hl7msgbld.getfinanciadorid (c.codigo_cliente)
          || '|'
          || hl7msgbld.getfinanciadornombre (c.codigo_cliente)
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || hl7msgbld.getnumeroafiliado (c.codigo_cliente)
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM clientes c;

CREATE OR REPLACE VIEW segment_orc
AS
   SELECT ad.actividad_det_pk cad,
             'ORC'
          || '|'
          || DECODE (ad.actdet_estado, 'A', 'OC', 'NW')
          || '|'
          || ad.actividad_det_pk
          || '|'
          || '|'
          || a.actividad_pk
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || a.codigo_personal
          || '^'
          || TRIM (fp.apellido1)
          || '^'
          || TRIM (fp.nombre)
          || '^^^'
          || cat.cod_categ
          || '|'
          || '|'
          || '|'
          || '|'
          || fp.telefono
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM actividad a,
          actividad_det ad,
          fpersona fp,
          tcategor cat,
          prest_item pi
    WHERE a.actividad_pk = ad.actividad_pk
      AND a.codigo_personal = fp.codigo_personal
      AND fp.codigo_categoria = cat.codigo_categoria
      AND pi.prest_item_pk = ad.prest_item_pk;


CREATE OR REPLACE VIEW segment_obr
AS
   SELECT ad.actividad_det_pk cad,
             'OBR'
          || '|'
          || '|'
          || ad.actividad_det_pk
          || '|'
          || '|'
          || TRIM (pi.prest_item_cod)
          || '^'
          || prest_item_desc
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || a.actividad_pk
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '|'
          || '[ENDOFSEGMENT]' rawmsg
     FROM actividad a,
          actividad_det ad,
          fpersona fp,
          tcategor cat,
          prest_item pi
    WHERE a.actividad_pk = ad.actividad_pk
      AND a.codigo_personal = fp.codigo_personal
      AND fp.codigo_categoria = cat.codigo_categoria
      AND pi.prest_item_pk = ad.prest_item_pk;

-- 7. Implementacion de los Stored Procedures
CREATE OR REPLACE PACKAGE BODY hl7msgbld
AS
   FUNCTION getdirciupro (cc IN NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (150);
   BEGIN
      SELECT    c.dom_direccion
             || '^'
             || ld5.nombre_ld5
             || '^'
             || ld3.nombre_ld3
             || '^'
             || '^'
             || c.dom_cp
             || '^'
             || ld4.nombre_ld4
        INTO ret
        FROM clientes c, ld5, ld3, ld4
       WHERE c.codigo_cliente = cc
         AND c.dom_ld5 = ld5.codigo_ld5
         AND c.dom_ld4 = ld5.codigo_ld4
         AND c.dom_ld3 = ld3.codigo_ld3
         AND ROWNUM < 2;

      RETURN ret;
   END;

   FUNCTION getclasepaciente (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getubicacion (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getmedicoresp (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getservicioresp (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getplansalud (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getfinanciadorid (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getfinanciadornombre (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getnumeroafiliado (cc NUMBER)
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2);
   BEGIN
      RETURN ret;
   END;

   FUNCTION getmsh (
      cc   NUMBER, --Codigo de cliente
      so   NUMBER, --Sistema origen
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_msh
       WHERE codigo_cliente = cc
         AND hl02codsys_ori = so
         AND hl02codsys_des = sd
         AND hl01codevento = ce;

      RETURN ret;
   END;

   FUNCTION getevn
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_evn;

      RETURN ret;
   END;

   FUNCTION getpid (cc NUMBER --Codigo de cliente
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_pid
       WHERE codigo_cliente = cc;

      RETURN ret;
   END;

   FUNCTION getmrg (cc NUMBER --codigo de cliente antiguo
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (2000);
      cnt   NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO cnt
        FROM hl06
       WHERE hl06codigo = cc;

      IF cnt > 0
      THEN
         SELECT hl06segmentmrg
           INTO ret
           FROM hl06
          WHERE hl06codigo = cc;
      ELSE
         SELECT rawmsg
           INTO ret
           FROM segment_mrg
          WHERE codigo_cliente = cc;
      END IF;

      RETURN ret;
   END;

   FUNCTION getpv1 (cc NUMBER --Codigo cliente
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_pv1
       WHERE codigo_cliente = cc;

      RETURN ret;
   END;

   FUNCTION getin1 (cc NUMBER --Codigo cliente
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_in1
       WHERE codigo_cliente = cc;

      RETURN ret;
   END;

   FUNCTION getorc (cd NUMBER --actividad_det_pk
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_orc
       WHERE cad = cd;

      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      RETURN ret;
   END;

   FUNCTION getobr (cd NUMBER --actividad_det_pk
                             )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      SELECT rawmsg
        INTO ret
        FROM segment_obr
       WHERE cad = cd;

      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      RETURN ret;
   END;

   FUNCTION getadta28 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      ret := getmsh (cc, 1, sd, ce);
      ret :=    ret
             || getevn;
      ret :=    ret
             || getpid (cc);
      ret :=    ret
             || getpv1 (cc);
      ret :=    ret
             || getin1 (cc);
      ret := REPLACE (ret, '<eventtypecode>', 'A28');
      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      RETURN ret;
   END;

   FUNCTION getadta31 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER --Codigo de evento
   )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      ret := getmsh (cc, 1, sd, ce);
      ret :=    ret
             || getevn;
      ret :=    ret
             || getpid (cc);
      ret :=    ret
             || getpv1 (cc);
      ret :=    ret
             || getin1 (cc);
      ret := REPLACE (ret, '<eventtypecode>', 'A31');
      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      RETURN ret;
   END;

   FUNCTION getadta34 (
      cc   NUMBER, --Codigo de cliente
      sd   NUMBER, --Sistema destino
      ce   NUMBER, --Codigo de evento
      oh   NUMBER --Codigo Cliente antiguo
   )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
   BEGIN
      ret := getmsh (cc, 1, sd, ce);
      ret :=    ret
             || getevn;
      ret :=    ret
             || getpid (cc);
      ret :=    ret
             || getmrg (oh);
      ret := REPLACE (ret, '<eventtypecode>', 'A34');
      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      RETURN ret;
   END;

   FUNCTION getormo01 (
      ca   NUMBER, --Actividad_pk,
      sd   NUMBER, --Sistema destino
      ce   NUMBER, --Codigo de Evento
      ld   VARCHAR2 --Lista de actividad_det_pk
   )
      RETURN VARCHAR2
   IS
      ret   VARCHAR2 (4000);
      cc    NUMBER;
      aux   VARCHAR2 (4000);
      cpi   NUMBER; --Codigo de Prestacion Inicial
      cpf   NUMBER; --Codigo de Prestacion Final
   BEGIN
      --Buscar el codigocliente
      SELECT codigo_cliente
        INTO cc
        FROM actividad a, episodios e
       WHERE a.actividad_pk = ca AND a.epis_pk = e.epis_pk;

      ret := getmsh (cc, 1, sd, ce);
      ret :=    ret
             || getpid (cc);
      ret := REPLACE (ret, '<pidsecuencia>', '0001');
      ret :=    ret
             || getpv1 (cc);
      --Ahora se separan ORC y OBR (1 ORC para varios OBR)

      cpi := 1;
      cpf := INSTR (ld, ',', 1, 1);
      IF cpf = 0
      THEN
         cpf := LENGTH( ld ) + 1;
      END IF;
      ret :=    ret
             || getorc (TO_NUMBER (SUBSTR (ld, 1,   cpf
                                                  - 1)));

      WHILE cpi <= LENGTH (ld)
      LOOP
         IF cpf > 0
         THEN
            aux := SUBSTR (ld, cpi,   cpf
                                    - cpi);
         ELSE
            aux := SUBSTR (ld, cpi);
         END IF;

         ret :=    ret
                || getobr (TO_NUMBER (aux));
         cpi :=   cpi
                + LENGTH (aux)
                + 1;
         cpf := INSTR (ld, ',', cpi, 1);
      END LOOP;

      RETURN ret;
   END;
END hl7msgbld;
/

SHOW err

--Package para Notificacion
CREATE OR REPLACE PACKAGE hl7notif
AS
   PROCEDURE todos (bldcmd VARCHAR2, codeve NUMBER, cc NUMBER);

   PROCEDURE sistema (bldcmd VARCHAR2, codeve NUMBER, cc NUMBER, sd NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY hl7notif
AS
   PROCEDURE sistema (bldcmd VARCHAR2, codeve NUMBER, cc NUMBER, sd NUMBER)
   IS
      rechazar    NUMBER          := 0;
      numevento   NUMBER;
      bldcmd2     VARCHAR2 (512);
      mycursor    INTEGER;
      rowsaff     INTEGER;
      hl7msg      VARCHAR2 (4095);
   BEGIN
      --Si esta siendo insertado/modificado desde el exterior,
      --no Notificar (medora->opensic->medora evitar)
      SELECT COUNT (*)
        INTO rechazar
        FROM hl03
       WHERE hl03sessionid = DBMS_SESSION.unique_session_id ()
         AND hl02codsys_des = 1;

      IF rechazar > 0
      THEN
         RETURN; --No notificar
      END IF;

      --Siguiente evento del link
      SELECT hl04numevento_seq.NEXTVAL
        INTO numevento
        FROM DUAL;

      --Substitucion de parametros
      --<hl01CodSys_des>:
      bldcmd2 := REPLACE (bldcmd, '<hl02CodSys_des>', TO_CHAR (sd));

         --ELM 11/12/01 Vamos a calcular bldcmd2 in-transaction: BEGIN
         --mycursor:=dbms_sql.open_cursor(); --abrimos un cursor
         --bldcmd2    := 'SELECT '||bldcmd2||' DATOS FROM DUAL';
         --dbms_sql.parse(mycursor,bldcmd2,dbms_sql.NATIVE); --Parsing
         --dbms_sql.define_column(mycursor,1,'',4095); --Definir valor devuelto
         --rowsaff:=dbms_sql.execute_and_fetch(mycursor,true);
         --dbms_sql.column_value(mycursor,1,hl7msg); --Obtengo el mensaje
         --dbms_sql.close_cursor(mycursor); --Ciao cursor
         --ELM 11/12/01 Vamos a calcular bldcmd2 in-transaction: END
      --Insertar
      INSERT INTO hl04
                  (hl04numevento, hl01codevento, hl02codsys_ori,
                   hl02codsys_des, hl04fecha, hl04procstatus, hl04rawmsg,
                   hl04bldmsg, hl04bldcmd)
           VALUES (numevento, codeve, 1,
                   sd, SYSDATE, 0, NULL,
                   1, bldcmd2);
   END;

   PROCEDURE todos (bldcmd VARCHAR2, codeve NUMBER, cc NUMBER)
   IS
      CURSOR c1
      IS
         SELECT hl02codsys
           FROM hl02
          WHERE hl02codsys > 1;
   BEGIN
      FOR r1 IN c1
      LOOP
         hl7notif.sistema (bldcmd, codeve, cc, r1.hl02codsys);
      END LOOP;
   END;
END;
/

SHOW err

-- 8. Triggers de creacion de mensajeria HL7
--Notificacion al exterior de creacion de paciente
CREATE OR REPLACE TRIGGER hl7_hc_adta28
   AFTER INSERT
   ON clientes
   FOR EACH ROW
DECLARE
   codeve   NUMBER;
   bldcmd   VARCHAR2 (512);
BEGIN
   SELECT hl01codevento, hl01bldfunc
     INTO codeve, bldcmd
     FROM hl01
    WHERE hl01ident = 'ADT^A28';

   bldcmd :=    bldcmd
             || '('
             || TO_CHAR (:NEW.codigo_cliente)
             || ',<hl02CodSys_des>,'
             || TO_CHAR (codeve)
             || ')';
   hl7notif.todos (bldcmd, codeve, :NEW.codigo_cliente);
END;
/

--Notificacion al exterior de Modificacion de clientes
CREATE OR REPLACE TRIGGER hl7_clientes_adta31
   AFTER UPDATE
   ON clientes
   FOR EACH ROW
DECLARE
   codeve   NUMBER;
   bldcmd   VARCHAR2 (512);
BEGIN
   SELECT hl01codevento, hl01bldfunc
     INTO codeve, bldcmd
     FROM hl01
    WHERE hl01ident = 'ADT^A31';

   bldcmd :=    bldcmd
             || '('
             || TO_CHAR (:NEW.codigo_cliente)
             || ',<hl02CodSys_des>,'
             || TO_CHAR (codeve)
             || ')';
   hl7notif.todos (bldcmd, codeve, :NEW.codigo_cliente);
END;
/
--Guardar segmentos Merge
CREATE OR REPLACE TRIGGER hl7_fusion_adta34_aux
   BEFORE DELETE
   ON clientes
   FOR EACH ROW
BEGIN
   INSERT INTO hl06
               (hl06codigo,
                hl06segmentmrg)
        VALUES (:OLD.codigo_cliente,
                   'MRG'
                || '|'
                || :OLD.codigo_cliente
                || '|'
                || '|'
                || '|'
                || '|'
                || '|'
                || '|'
                || RTRIM (:OLD.apellido1)
                || ' '
                || RTRIM (:OLD.apellido2)
                || '^'
                || RTRIM (:OLD.nombre)
                || '[ENDOFSEGMENT]');
END;
/

--Notificacion al exterior de Fusion de Pacientes
CREATE OR REPLACE TRIGGER hl7_fusion_adta34
   BEFORE UPDATE
   ON hc
   FOR EACH ROW
DECLARE
   nhc_id1   NUMBER;
   nhc_id2   NUMBER;
   codeve    NUMBER;
   bldcmd    VARCHAR2 (512);
   cc        NUMBER;
   ca        NUMBER;
BEGIN
   IF    :OLD.codigo_cliente IS NULL
      OR :NEW.codigo_cliente IS NULL
      OR :OLD.codigo_cliente = :NEW.codigo_cliente
   THEN
      RETURN; --No Notificar
   END IF;

   --Codigo de evento y funcion que obtiene el texto del mensaje
   SELECT hl01codevento, hl01bldfunc
     INTO codeve, bldcmd
     FROM hl01
    WHERE hl01ident = 'ADT^A34';

   cc := :NEW.codigo_cliente;
   ca := :OLD.codigo_cliente;
   bldcmd :=    bldcmd
             || '('
             || TO_CHAR (cc)
             || ',<hl02CodSys_des>,'
             || TO_CHAR (codeve)
             || ','
             || TO_CHAR (ca)
             || ')';
   hl7notif.todos (bldcmd, codeve, cc);
END;
/

--Notificacion de peticiones de pruebas a Radiologia
CREATE OR REPLACE TRIGGER hl7_order_ormo01
   AFTER INSERT OR UPDATE
   ON actividad_det
   FOR EACH ROW
DECLARE
   codeve   NUMBER;
   bldcmd   VARCHAR2 (512);
   ne       NUMBER;
   ne1      NUMBER;
BEGIN
   --Comprobar si es de radiologia (Imagen)
   SELECT tipo_actividad_pk
     INTO ne1
     FROM actividad
    WHERE actividad_pk = :NEW.actividad_pk;

   --Si no lo es, no hacer nada
   IF  ne1 <> 4 AND ne1 <> 2
   THEN
      RETURN;
   END IF;

   --si es radiologia:
   SELECT hl01codevento, hl01bldfunc
     INTO codeve, bldcmd
     FROM hl01
    WHERE hl01ident = 'ORM^O01';

   --Comprobar si no hay programado ya un ORM.
   SELECT COUNT (*)
     INTO ne
     FROM hl04
    WHERE hl01codevento = codeve
      AND hl04bldmsg = 1
      AND hl04procstatus = 0
      AND hl04bldcmd LIKE    bldcmd
                          || '('
                          || :NEW.actividad_pk
                          || ',%';

   IF ne1 = 4
   THEN
      IF ne = 0
      THEN
         --No lo hay:incluir el mensaje una nueva peticion
         SELECT hl04numevento_seq.NEXTVAL
           INTO ne
           FROM DUAL;

         INSERT INTO hl04
                     (hl04numevento, hl02codsys_ori, hl02codsys_des,
                      hl04transid, hl01codevento, hl04fecha, hl04bldmsg,
                      hl04bldcmd,
                      hl04procstatus, hl04errtext, hl04rawmsg)
              VALUES (ne, 1, 2,
                      NULL, codeve, SYSDATE, 1,
                         bldcmd
                      || '('
                      || :NEW.actividad_pk
                      || ',2,'
                      || codeve
                      || ','''
                      || :NEW.actividad_det_pk
                      || ''')',
                      0, NULL, NULL);
      ELSE
         --Si lo hay: incluir la nueva prueba en la peticion existente
         SELECT MIN (hl04numevento)
           INTO ne
           FROM hl04
          WHERE hl01codevento = codeve
            AND hl04bldmsg = 1
            AND hl04procstatus = 0
            AND hl04bldcmd LIKE    bldcmd
                                || '('
                                || :NEW.actividad_pk
                                || ',%';

         UPDATE hl04
            SET hl04bldcmd =    SUBSTR (
                                   hl04bldcmd,
                                   1,
                                     LENGTH (hl04bldcmd)
                                   - 2
                                )
                             || ','
                             || :NEW.actividad_det_pk
                             || ''')'
          WHERE hl04numevento = ne;
      END IF;
   ELSE
      IF ne = 0
      THEN
         --No lo hay:incluir el mensaje una nueva peticion
         SELECT hl04numevento_seq.NEXTVAL
           INTO ne
           FROM DUAL;

         INSERT INTO hl04
                     (hl04numevento, hl02codsys_ori, hl02codsys_des,
                      hl04transid, hl01codevento, hl04fecha, hl04bldmsg,
                      hl04bldcmd,
                      hl04procstatus, hl04errtext, hl04rawmsg)
              VALUES (ne, 1, 3,
                      NULL, codeve, SYSDATE, 1,
                         bldcmd
                      || '('
                      || :NEW.actividad_pk
                      || ',3,'
                      || codeve
                      || ','''
                      || :NEW.actividad_det_pk
                      || ''')',
                      0, NULL, NULL);
      ELSE
         --Si lo hay: incluir la nueva prueba en la peticion existente
         SELECT MIN (hl04numevento)
           INTO ne
           FROM hl04
          WHERE hl01codevento = codeve
            AND hl04bldmsg = 1
            AND hl04procstatus = 0
            AND hl04bldcmd LIKE    bldcmd
                                || '('
                                || :NEW.actividad_pk
                                || ',%';

         UPDATE hl04
            SET hl04bldcmd =    SUBSTR (
                                   hl04bldcmd,
                                   1,
                                     LENGTH (hl04bldcmd)
                                   - 2
                                )
                             || ','
                             || :NEW.actividad_det_pk
                             || ''')'
          WHERE hl04numevento = ne;
      END IF;
   END IF;
END;
/

SPOOL off
EXIT
