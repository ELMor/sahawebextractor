The JAVA_STRUCT example demonstrates the Oracle 9i SQLJ Object Type
feature (using ORAData representation). 

It requires a Java-enabled database.

This example creates the SQLJ object type JAVA_STRUCT_PERSON_T
in the database.  This type is based on the Java class
JavaStructPerson which implements the the oracle.sql.ORAData
interface.  Note that the _SQL_TYPECODE of this class is
OracleTypes.JAVA_STRUCT.
 
This example also shows how to invoke member functions on
SQLJ object types (these are implemented in Java), and how
to insert and subsequently retrieve instances of such types.


(1) Source files: 
    JavaStructDemo.sql     - script to publish SQLJ type in database 
    JavaStructPersonT.sqlj - ORAData-based implementation of a
                             SQLJ object type
    JavaStructDemo.sqlj    - test driver to exercise the SQLJ type

(2) Run the JAVA_STRUCT type demo. 

    % sqlplus scott/tiger @JavaStructDemo.sql
    % loadjava -u scott/tiger -r -f JavaStructPersonT.sqlj
    % sqlj JavaStructPersonT.sqlj JavaStructDemo.sqlj
    % java JavaStructDemo

    Expected output is:
   
    Creating p = Scott Tiger
    Method calls: p.getDbFirstName() = ScottDB   p.getDbLastName() = TigerDB
    ** Inserting objects... **
    ** Selecting as objects **
    12345 - Scott - Tiger - 1999-12-31 00:00:00.0
    31415 - Albert - Einstein - 1986-04-01 00:00:00.0
    ** Selecting as attributes **
    12345 - Scott Tiger - 1999-12-31 00:00:00.0
    31415 - Albert Einstein - 1986-04-01 00:00:00.0
