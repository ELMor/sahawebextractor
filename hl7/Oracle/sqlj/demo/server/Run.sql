
-- SQL*PLUS script to run the SQLJ demo in server 

connect scott/tiger

set echo on
set serveroutput on
set termout on
set flush on

execute dbms_java.set_output(10000);


create or replace procedure SQLJ_SERVER_DEMO as language java
 name 'ServerDemo.main (java.lang.String[])';
/

call SQLJ_SERVER_DEMO()
/

exit
/
