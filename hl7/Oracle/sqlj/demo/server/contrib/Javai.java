import java.util.Vector;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class Javai
{

   private static boolean LDEBUG = false;
   private static boolean m_printError = false;

   public static void main(String[] args)
   {
      if (args==null || args.length==0 || args[0]==null)
      {
        System.out.println("Usage: Javai <JavaExpression>");
        System.out.println(" a primitive Java interpreter where <JavaExpression> can contain:");
        System.out.println("   field access: x.y.z");
        System.out.println("   method invocation: f(g(),h())");
        System.out.println("   String, char, and int constants:  -1, \"the\", 'b'");
        System.out.println("   arrays: {\"a\",\"b\"}, {}");
        System.out.println("   constructor invocations: (new Foo())");
        return;
     }

     m_printError = true;

     for (int i=0; i<args.length; i++) {
      try
      {
        System.out.println("Evaluating: "+args[i]+"...");

        if (args[i]!=null)
        {
          Javai parser = new Javai(args[i]);
          JObject.init(parser);

          JObject jo = parser.javaExpr(null);

          if (parser.peek() != Token.TokEOF) parser.error(parser.peek(), "[1] expected EOF"); 

          System.out.println("Returns   : ("+printClass(jo.getType())+") "+printValue(jo.getValue()));
        }
        else
        {
          System.out.println("Returns   : (java.lang.String) null");
        }
      }
      catch (IllegalArgumentException exn) { }
      catch (Throwable t)
      {
        t = unwrapException(t);
        System.out.println("Unexpected error: "+t);
      }
     }
   }

   public static String fun(String arg) throws SQLException
   {
     if (arg==null) return null;

     m_printError = false;

     Javai parser = new Javai(arg);
     JObject.init(parser);
     JObject jo = parser.javaExpr(null);

     if (parser.peek() != Token.TokEOF) parser.error(parser.peek(), "[1] expected EOF");

     if (jo.getValue()==null) return null;

     try
     {
       return printValue(jo.getValue());
     }
     catch (IllegalArgumentException exn)
     {
        return "Unexpected error: "+exn;
     }
     catch (Throwable t)
     {
        t = unwrapException(t);
        return "Unexpected error: "+t;
     }
   }

   public Javai(String s)
   {
     m_stream = s;
     m_idx = 0;
     m_max = s.length();
   }
   private String m_stream;
   private int m_idx;
   private int m_max;

   private void error(Token t, String msg)
   {
     String m = "Error "+msg+" at: "+m_stream.substring(0,t.m_start)+
                ">>>"+ t.getImage()+ "<<<" + m_stream.substring(t.m_end);

     if (m_printError) System.out.println(m);

     throw new IllegalArgumentException(m);
   }

   private void error(String msg)
   {
     String m = "Error "+msg;

     if (m_printError) System.out.println(m);

     throw new IllegalArgumentException(m);
   }


   private static String printClass(Class c)
   {
     if (c==null) return "void";

     String res = "";
     while (c.isArray()) { res = res + "[]"; c = c.getComponentType(); }
     return c.getName() + res;
   }

   private static String printValue(Object o)
   {
     if (o==null) return "null";

     if (o.getClass().isArray()) {
        
        int l = Array.getLength(o);
        String s = "{";

        for (int i=0; i<l; i++) {
            s = s + printValue(Array.get(o,i));
            if (i<l-1) s = s +",";
        }
        return s + "}";
     }
     return o.toString();
   }
     


   private Token getToken()
   {
     Token t= getToken2();
     if (LDEBUG) {
       System.out.println("getToken(): "+t.toString());
     }
     return t;
   }
   private Token getToken2()
   {
     if (m_idx>=m_max) return Token.TokEOF;

     // Skip whitespace, if any
     char ch = m_stream.charAt(m_idx);
     while (isSpace(ch) && m_idx<m_max) { m_idx++; if (m_idx<m_max) ch=m_stream.charAt(m_idx); }

     if (m_idx==m_max) return Token.TokEOF;

     int start=m_idx;
     int end = start;

     if (ch=='"') { 
       end = start+1;
       while ( end<m_max && ((ch=m_stream.charAt(end)) != '"') ) end++;

       m_idx=end+1;
       return new Token(start+1, end, Token.STRING, m_stream);
     } else if (ch=='\'') {

       end = start+1;
       while ( end<m_max && ((ch=m_stream.charAt(end)) != '\'') ) end++;

       m_idx=end+1;
       return new Token(start+1, end, Token.CHARACTER, m_stream);

     } else if (ch=='-' || ('0' <= ch && ch <='9')) {
       // Note: this will falsely recognize "-" as a numeric constant
       char sch = ch;
       end = start+1;
       while ( end<m_max && (ch=m_stream.charAt(end)) >= '0' && ch <= '9') end++;
       m_idx=end;

       if (sch=='-' && end==start+1) return new Token(start, end, Token.SYMBOL, m_stream);

       return new Token(start, end, Token.NUMBER, m_stream);

     } else if (Character.isLetter(ch) || ch=='_' || ch=='$') {
       end = start+1;
       while ( end<m_max && (Character.isLetter(ch=m_stream.charAt(end)) || Character.isDigit(ch)) || ch=='_' || ch=='$' ) end++;

       m_idx=end;
       return new Token(start, end, Token.IDENTIFIER, m_stream);
     } else {
       m_idx=start+1;
       return new Token(start, start+1, Token.SYMBOL, m_stream); 
     }
   } 

   private static boolean isSpace(char ch)
   { return ch==' ' || ch=='\n' || ch == '\r' || ch=='\t'; }

   private static class Token
   {
     public static final int EOF = 0;
     public static final int NUMBER = 1;
     public static final int STRING = 2;
     public static final int CHARACTER = 3;
     public static final int SYMBOL = 4;
     public static final int IDENTIFIER = 5;

     public String toString() {
       String s = getImage();
       s = "'" + s + "':"+m_start+":"+m_end+":";
       if (m_tkind==EOF)        { s = s + "EOF"; } else
       if (m_tkind==NUMBER)     { s = s + "NUMBER"; } else
       if (m_tkind==STRING)     { s = s + "STRING"; } else
       if (m_tkind==CHARACTER)  { s = s + "CHARACTER"; } else
       if (m_tkind==SYMBOL)     { s = s + "SYMBOL"; } else
       if (m_tkind==IDENTIFIER) { s = s + "IDENTIFIER"; } else { s= s + "?"; };
       return s;
     }

     Token(int start, int end, int kind, String stream)
     {
       m_start=start; m_end=end; m_tkind=kind;
       m_stream=stream;
     }

     public String getImage() {
      if (m_tkind == EOF) return null;
      return m_stream.substring(m_start, m_end);
     }

     public boolean isEOF() { return m_tkind==EOF; }

     public boolean isNUMBER() { return m_tkind==NUMBER; }
     public boolean isSTRING() { return m_tkind==STRING; }
     public boolean isCHARACTER() { return m_tkind==CHARACTER; }
     public boolean isConstant() { return isNUMBER() || isSTRING() || isCHARACTER()
                                          || (isSYMBOL() && getImage().equals("{")); }

     public boolean isSYMBOL() { return m_tkind==SYMBOL; }
     public boolean isIDENTIFIER() { return m_tkind==IDENTIFIER; }

     private Token(int kind) { this(0,0, kind, null); }

     private int m_start;
     private int m_end;
     private int m_tkind;
     private String m_stream;

     public static final Token TokEOF = new Token(EOF);
   }


   public Token read()
   {
     Token t = nextToken;
     if (nextToken==null) t = getToken();
     nextToken = null;

     return t;
   }   

   public Token peek()
   {
     if (nextToken==null) nextToken = getToken();
     return nextToken;
   }
   private Token nextToken=null;


   private static class JObject
   {
     static void init(Javai parser) { m_parser=parser; }
     private static Javai m_parser = null;

     JObject(Token t) {
       if (t.isSTRING()) {
          this.setType(String.class);
          this.setValue(t.getImage());
       } else if (t.isNUMBER()) {
          this.setType(int.class);
          this.setValue(new Integer(Integer.parseInt(t.getImage())));
       } else if (t.isCHARACTER()) {
          this.setType(char.class);
          this.setValue(new Character(t.getImage().charAt(0)));
       } else {
         m_parser.error(t,"[2] Expected a constant");
       }
     }

     JObject(Class type, Object value) {
       m_type = type; m_value=value;
     }

     Class getType() { return m_type; }
     void setType(Class t) { m_type=t; }

     Object getValue() { return m_value; }
     void setValue(Object o) { m_value = o; }

     private Class m_type=null;
     private Object m_value=null;
   }


   private JObject javaExpr(JObject instance)
   {
     JObject o=null;
     Token t=peek();

     if (t.isConstant() && !t.isSYMBOL())
     {
        t=read();
        o = new JObject(t);
     }
     else if (t.isIDENTIFIER() && t.getImage().equals("new"))
     {
       t = read();

       String n = qualifiedName();
 
       t = peek();

       if (!(t.isSYMBOL() && t.getImage().equals("("/*)*/))) error(t, "[3] Expected \"(\""/*)*/);
       t = read();

       JObject[] args = new JObject[]{};
       t = peek();
       if (t.isSYMBOL() && t.getImage().equals(/*(*/")"))
       {
          // empty argument list
       }
       else
       {
         args = argumentList();
       }

       t = peek();
       if (!(t.isSYMBOL() && t.getImage().equals(/*(*/")"))) error(t, /*(*/"[4] Expected \")\"");
       read();

       if (instance!=null) error(t, "[5] Cannot call constructor on an instance");
       return callConstructor(n, args);

     }
     else if (t.isIDENTIFIER())
     {

       String n = qualifiedName();
       if (instance!=null) n = instance.getType().getName() + "." + n;

       t = peek();
       if (t.isSYMBOL() && t.getImage().equals("("/*)*/))
       {
         t = read();

         JObject[] args = new JObject[]{};

         t = peek();
         if (t.isSYMBOL() && t.getImage().equals(/*(*/")"))
         {
           // empty argument list
         } else {
           args = argumentList();
         }

         t = peek();
         if (!(t.isSYMBOL() && t.getImage().equals(/*(*/")"))) error(t, /*(*/"[6] Expected \")\"");
         read();

         o = callMethod(n, instance, args);
       }
       else
       {
         o = getField(n, instance);
       }
     }
     else if (t.isSYMBOL() && t.getImage().equals("("/*)*/))
     {
       // if (instance!=null) error(t, "[7] Syntax error - \"(\" unexpected.");
       t = read();
      
       o = javaExpr(instance);
       t = peek();
       if (!(t.isSYMBOL() && t.getImage().equals(/*(*/")"))) error(t, /*(*/"[8] Expected \")\"");
       t = read();

     }
     else if (t.isSYMBOL() && t.getImage().equals("{"/*}*/))
     {
       // if (instance!=null) error(t, "[9] Syntax error - \"{\" unexpected.");
       t = read();
      
       JObject[] args = new JObject[]{};

       t = peek();
       if (t.isSYMBOL() && t.getImage().equals(/*{*/"}"))
       {
          // empty argument list
          o = new JObject( String[].class, new String[]{} );
       } else {
           args = argumentList();
           Object[] vals = new Object[args.length];
           Class elems = args[0].getType();
           vals[0] = args[0].getValue();

           for (int i=1; i<args.length; i++) {
             vals[i] = args[i].getValue();
             Class comp=args[i].getType();
             if (elems.isAssignableFrom(comp)) {
                // keep element type
             } else if (comp.isAssignableFrom(elems)) {
                elems = comp;
             } else {
                // incompatible types
                elems = Object.class;
             }
           }

           Object theValue = null;

           if (elems == int.class)
           {
             int[] ia = new int[args.length];
             for (int i=0; i<ia.length; i++) ia[i] = ((Integer)vals[i]).intValue();
             theValue = ia;
           }
           else if (elems == char.class)
           {
             char[] ca = new char[args.length];
             for (int i=0; i<ca.length; i++) ca[i] = ((Character)vals[i]).charValue();
             theValue = ca;
           }
           else
           {
             theValue = Array.newInstance(elems, args.length);
             System.arraycopy(vals,0, theValue, 0, args.length);
           }
           Class  theClass = theValue.getClass();

           o = new JObject(theClass, theValue);
       }

       t = peek();
       if (!(t.isSYMBOL() && t.getImage().equals(/*{*/"}"))) error(t, /*(*/"[10] Expected \"}\"");
       t = read();
     }
     else
     {
       error(t, "[11] expected <constant>, <identifier>, \"new\", or \"(\"");
       // Dummy return
       return null;
     }

     t = peek();
     if (t.isSYMBOL() && t.getImage().equals("."))
     {
       t = read();
       o = javaExpr(o);
     }
     return o;
  }

  private JObject[] argumentList()
  {
    Vector v = new Vector();

    Token t=peek();

    while (t.isConstant() || t.isIDENTIFIER() 
           || (t.isSYMBOL() && t.getImage().equals("("/*)*/)) )
    {
      v.addElement(javaExpr(null));
      t = peek();
      if (t.isSYMBOL() && t.getImage().equals(",")) {
         t = read();
         t = peek();
      } else break; 
    }
    JObject[] jary = new JObject[v.size()];
    v.copyInto(jary);
    return jary;
  }

  private String qualifiedName() {
    Token t=peek();
    String res="";
    while (t.isIDENTIFIER()) {
      t =read();
      res = res + t.getImage();
      t = peek();
      if (t.isSYMBOL() && t.getImage().equals(".")) {
        res = res+".";
        t = read();
        t = peek();
      } else {
        return res;
      }
    }

    error(t,"[12] expected identifier");
    // Dummy return
    return null;
  } 


  private JObject getField(String n, JObject instance)
  {
    try
    {
      int pos = n.lastIndexOf(".");
      Class c = classForName(n.substring(0,pos));
      Field f = c.getField(n.substring(pos+1));

      return new JObject(f.getType(), f.get( (instance==null) ? null : instance.getValue()));
    }
    catch (Throwable t)
    {
      t = unwrapException(t);
      error("[13] Unable to evaluate field "+n+": "+t.toString());
      // Dummy return
      return null;
    }
  }

  private JObject callConstructor(String n, JObject[] args)
  {
    try
    {
      Class k = classForName(n);
      Class[] cary=new Class[args.length];
      Object[] oary=new Object[args.length];
      for (int i=0; i<args.length; i++) { cary[i]=args[i].getType(); oary[i]=args[i].getValue(); };

      Constructor c = k.getConstructor(cary);

      return new JObject(k, c.newInstance(oary));
    }
    catch (Throwable t)
    {
      t = unwrapException(t);
      error("[14] Unable to apply constructor "+n+": "+t.toString());
      // Dummy return
      return null;
    }
  }

  private JObject callMethod(String n, JObject instance, JObject[] args)
  {
    try
    {
      int pos = n.lastIndexOf(".");

      Class c = classForName(n.substring(0,pos));
      Class[] cary=new Class[args.length];
      Object[] oary=new Object[args.length];
      for (int i=0; i<args.length; i++) { cary[i]=args[i].getType(); oary[i]=args[i].getValue(); };

      Method m = c.getMethod(n.substring(pos+1), cary);

      return new JObject(m.getReturnType(), m.invoke( (instance==null) ? null : instance.getValue(), oary));
    }
    catch (Throwable t)
    {
      t = unwrapException(t);
      error("[15] Unable to call method "+n+": "+t.toString());
      // Dummy return
      return null;
    }
  }

  private static Class classForName(String s) throws ClassNotFoundException
  {
    Class c = null;
    if (s==null || s.equals("void")) return c;

    try
    {
      c = Class.forName(s);
    }
    catch (ClassNotFoundException exn)
    {
      c = Class.forName("java.lang."+s);
    }

    return c;
  }

  private static Throwable unwrapException(Throwable t)
  {
    if (t instanceof InvocationTargetException)
    {
      return ((InvocationTargetException)t).getTargetException();
    }
    return t;
  }
}
