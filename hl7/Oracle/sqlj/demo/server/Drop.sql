
-- SQL*PLUS script to drop demo classes

set echo on
set serveroutput on
set termout on
set flush on

drop java source "ServerDemo"
/
drop java class "ServerDemo"
/
drop java class "ServerDemo_SJProfileKeys"
/
drop java resource "ServerDemo_SJProfile0.ser"
/
exit
/
