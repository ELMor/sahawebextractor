#
# Sample script for server load of sqlj source
#

# Drop loaded classes, if any
sqlplus scott/tiger @Drop.sql

# Load the SQLJ source directly 
loadjava -oci8 -resolve -force -user scott/tiger ServerDemo.sqlj

# Publish SQL wrapper and run the demo on the server using SQL*Plus 
sqlplus scott/tiger @Run.sql
 
# In sqlplus, it should print
# Hello! I'm SQLJ in server!
# Today is <date>
 



