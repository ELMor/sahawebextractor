import java.sql.Connection;
import sqlj.framework.error.ErrorLog;
import sqlj.framework.error.JSError;
import sqlj.framework.error.Warning;
import sqlj.framework.checker.SQLOperation;
import sqlj.semantics.sql.CheckerOptions;
import sqlj.semantics.sql.SimpleCheckerImpl;
 
public class ParsingSimpleCheckerImpl extends SimpleCheckerImpl
{

  public void parse(ErrorLog errlog,
                    Connection conn,
                    SQLOperation op,
                    short[]  sql_types)
  {
     String stmt = op.getSQLString();
     SQLUtil su = new SQLUtil(stmt);

     if (m_opt.getVerbose())
     {
       su.enable_tracing();
     }
     else
     {
       su.disable_tracing();
     }

     try
     {
       if (op.getOperationCode() == SQLOperation.CALL
           || op.getOperationCode() == SQLOperation.VALUES)
       {
         su.call_expr();
       }
       else
       {
         su.sql_statement();
       }
     }
     catch (ParseException exn)
     {
        CustomParseException cpe = new CustomParseException(exn);

        if (m_opt.getPortable())
        {
          errlog.addEntry(new JSError(cpe.getMessage(stmt)));
        }
        else
        {
          errlog.addEntry(new Warning(cpe.getMessage(stmt)));
        }
     }  
     catch (TokenMgrError exn)
     {
        if (m_opt.getPortable())
        {
          errlog.addEntry(new JSError(exn.getMessage()));
        }
        else
        {
          errlog.addEntry(new Warning(exn.getMessage()));
        }
     }
  }

  public void setCheckerOptions(CheckerOptions opt)
  { m_opt = opt; super.setCheckerOptions(opt); } 
  private CheckerOptions m_opt;
}
