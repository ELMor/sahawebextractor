import sqlj.framework.checker.SQLChecker;
import sqlj.semantics.OfflineCheckerBuilder;
import sqlj.semantics.TypeProperties;
import sqlj.semantics.sql.SimpleCheckerImpl;

/**
Implementation of a SQLChecker that works entirely offline.

This SQLChecker uses a SQL grammar similar to the one published by DATE.
**/
public class ParsingOfflineChecker extends OfflineCheckerBuilder
                  implements SQLChecker
{
  public ParsingOfflineChecker()
  {
    this(new TypeProperties());
  }

  public ParsingOfflineChecker(TypeProperties tp)
  {
    super(tp, new ParsingSimpleCheckerImpl());
    ((SimpleCheckerImpl) m_simple_checker).setCheckerOptions(this);
    ((SimpleCheckerImpl) m_simple_checker).setSQLTypeProperties(this.m_tp);
  }
}
