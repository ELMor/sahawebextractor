import sqlj.framework.checker.SQLChecker;
import sqlj.semantics.SQLCheckerBuilder;
import sqlj.semantics.TypeProperties;
import sqlj.semantics.sql.SimpleCheckerImpl;

/**
Implementation of a parsing SQLChecker that uses JDBC.

This SQLChecker relies on a database connection. It is built on top of an
implementation of the SimpleChecker interface.
**/
public class ParsingJdbcChecker extends SQLCheckerBuilder
                  implements SQLChecker
{
  public ParsingJdbcChecker()
  {
    this(new TypeProperties());
  }

  public ParsingJdbcChecker(TypeProperties tp)
  {
    super(new ParsingSimpleCheckerImpl(), tp);
    ((SimpleCheckerImpl) m_simple_checker).setCheckerOptions(this);
    ((SimpleCheckerImpl) m_simple_checker).setSQLTypeProperties(this.m_tp);
  }
}
