Sample SQLJ translator components
*********************************

The SQLJ translator is very modular.  It is straighforward to
plug in new SQLChecker components for checking SQL syntax and
semantics, as well as new customizer/customization components
for adding to or modifying runtime behavior.
This directory .../sqlj/demo/components contains examples
of the following:
  - a SQLChecker that uses a full SQL grammar for parsing SQL statements
  - a SQLJ customizer that transforms SQL statements into stored
    procedures and calls to these at runtime
These components demonstrate advanced functionality of the SQLJ
translator and are supplied _as_ _is_ for illustration purposes only.
They are not supported as part of the Oracle SQLJ product in any way.

The compiled class files for these components can be found in
components.zip.  Thus, you can simply add this file to your CLASSPATH
to run the SQLJ examples. Additionally, we also give instructions
below for rebuilding these files. 


Parsing SQL checkers
--------------------

These checkers performs more in-depth SQL syntax parsing than
the default sqlj.semantics.JdbcChecker and sqlj.semantics.OfflineChecker
that are automatically used for non-Oracle JDBC connections.
You could use the associated SQL grammar (or a variant of it) for
example, to ensure that only portable SQL constructs occur in your SQLJ
application.

Source code for the ParsingJdbcChecker and the ParsingOfflineChecker,
as well as a SQL grammar can be found in the subdirectory
.../sqlj/demo/components/checker.  The SQL grammar is loosely based
on the book by Hughes and Darwen on the ANSI SQL Standard. However,
at this point the grammar requires a good deal more work. - Feel free
to experiment with this grammar, or build your own grammar instead.

(1) Note: you do not need to build these checkers - just put 
    components.zip in your CLASSPATH, for example:
             setenv CLASSPATH components.zip:${CLASSPATH}

    To rebuild do the following in the checker subdirectory:
    javacc sql.jj   -- this re-generates the grammar-dependent Java files
    javac  *.java   -- this compiles the grammar files and the
                       SQLChecker glue files
    Now simply put the resulting class files in a directory on your
    CLASSPATH, or refresh components.zip with them.

(2) Specify that you want to use these SQLCheckers instead of the default
    ones.  This is most easily achieved by adding the following lines to
    your sqlj.properties files (this has already been done to the 
    sqlj.properties files in this directory).

       sqlj.offline=ParsingOfflineChecker
       sqlj.online=ParsingJdbcChecker

    Additionally, you may want to turn portability warnings on as follows
    (this has also been already done).

       sqlj.warn=portable,noverbose

(3) Now run sqlj as follows:

      sqlj TestChecker1.sqlj   -- this runs the SQL parser on a number of 
                                  SQL statements. Nothing should be reported.
      sqlj TestChecker2.sqlj   -- this runs the SQL parser on a set of erroneous
                                  SQL statements. You get many errors, and you
                                  also see why the grammar can use some more work.

    If you perform online checking, you will see additional error and warning
    messages on the SELECT statements - unless you define appropriate table
    structures (but not all of the SQL statements are consistent with one another).
    


PL/SQL Customizer
-----------------

This demonstrates the implementation of customizers and of customizations.

The PL/SQL customizer converts SQL statements into PL/SQL procedures.
At customization time, it creates PL/SQL procedures in the database
and modifies the SQL statements in the SQLJ profile to call these.
At runtime, the PL/SQL customization ensures that these stored
procedures are actually being called.

(1) Note: you do not need to build the PL/SQL customizer - just put 
    components.zip in your CLASSPATH, for example:
             setenv CLASSPATH components.zip:${CLASSPATH}

    To rebuild do the following in the customizer subdirectory:
    javac  *.java   -- this compiles the customizer and the
                       customization classes
    Now simply put the resulting class files in a directory on your
    CLASSPATH, or refresh components.zip with them.

(2) Invoke the PL/SQL customizer on one of more serialized profiles
    as follows (all one line).

       java PLSQLCustomizer -user=<user>/<password> -url=<url>
                            <file1>.ser <fileN>.jar ...

    This is equivalent to the following invocation of the SQLJ translator.

       sqlj -P-customizer=PLSQLCustomizer 
            -P-user=<user>/<password> -P-url=<url>
            <file1>.ser <fileN>.jar ...

    Note that database connection information is required, since at
    customization time new PL/SQL store procedures are created in the
    database.

(3) When you run the PL/SQL customized program you should also 
    have components.zip in your CLASSPATH.
    Alternatively, it is sufficient to add the class
    PLSQLCustomization.class to your SQLJ runtime environment.
