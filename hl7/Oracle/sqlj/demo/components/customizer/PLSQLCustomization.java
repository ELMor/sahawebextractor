/* Copyright (c) 1997 Oracle Corporation */
import sqlj.runtime.profile.Customization;
import sqlj.runtime.profile.ConnectedProfile;
import sqlj.runtime.profile.Profile;
import sqlj.runtime.profile.ref.ProfileImpl;
import sqlj.runtime.profile.ProfileData;
import sqlj.runtime.ConnectionContext;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
  A PLSQL customization contains a customized version of a profile
  data object.  The customized version of the data uses calls to
  PLSQL stored procedures instead of the client's original SQL
  operations.
  **/
public class PLSQLCustomization implements Customization
{
  private ProfileData m_customData;

  /**
    Creates a new customization using the passed custom data.  The
    custom data will be serialized with this customization, and can thus
    be used at runtime when a custom connected profile is created 
    by this customization.
    **/
  public PLSQLCustomization(ProfileData customData)
  {
    m_customData = customData;
  }

  /**
    Returns true if the connection's database product name is ORACLE,
    false otherwise.
    **/
  public boolean acceptsConnection(Connection conn)
  {
    try {
      DatabaseMetaData dbmd = conn.getMetaData();
      String dbName = dbmd.getDatabaseProductName();
      return dbName.equalsIgnoreCase("ORACLE");
    } catch (SQLException e) {
      return false;
    }
  }
 
  /**
    Returns a connected profile for the baseProfile on the given sqlj
    connection context (with an underlying JDBC connection).
    The connected profile makes calls to PLSQL stored
    procedures, as described by the underlying custom profile data.
    Note that we reuse the "default" connected profile implementation,
    creating it with a custom profile data object rather than the
    default profile data.

    @exception java.sql.SQLException if a default connected profile 
      cannot be created.
    **/
  public ConnectedProfile getProfile(ConnectionContext ctx, Profile baseProfile)
    throws SQLException
  {
    System.out.println("creating plsql custom profile");
    return ProfileImpl.getDefaultProfile(ctx, m_customData);
  }

}
