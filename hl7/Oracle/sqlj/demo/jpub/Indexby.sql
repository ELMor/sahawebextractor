create or replace package indexby as

--  jpub.addtypemap=SCOTT.INDEXBY.VARCHAR_ARY:String[10](255)
--  jpub.addtypemap=SCOTT.INDEXBY.INTEGER_ARY:int[10]
--  jpub.addtypemap=SCOTT.INDEXBY.FLOAT_ARY:double[10]

 type varchar_ary IS TABLE OF VARCHAR2(255) INDEX BY BINARY_INTEGER;
 type integer_ary IS TABLE OF INTEGER       INDEX BY BINARY_INTEGER;
 type float_ary   IS TABLE OF NUMBER        INDEX BY BINARY_INTEGER;

 function get_float_ary RETURN float_ary;
 procedure pow_integer_ary(x integer_ary, y OUT integer_ary);
 procedure xform_varchar_ary(x IN OUT varchar_ary);

end indexby;
/


create or replace package body indexby is

 function get_float_ary RETURN float_ary is
   tab float_ary;
 begin
   tab(5) := 5.0;
   tab(1) := 1.0;
   tab(3) := 3.0;
   tab(2) := 2.0;
   tab(4) := 4.0;
   return tab;
 end get_float_ary;

 procedure pow_integer_ary(x integer_ary, y OUT integer_ary) is
 begin
   y(1) := x(1) * x(1); 
   y(2) := x(2) * x(2); 
   y(3) := x(3) * x(3); 
   y(4) := x(4) * x(4); 
   y(5) := x(5) * x(5); 
 end pow_integer_ary;

 procedure xform_varchar_ary(x IN OUT varchar_ary) is
 begin
   x(2) := x(2) || '2' || x(2);
   x(3) := x(3) || '3' || x(3) || '3' || x(3);
   x(4) := x(4) || '4' || x(4) || '4' || x(4) || '4' || x(4);
   x(5) := x(5) || '5' || x(5) || '5' || x(5) || '5' || x(5) || '5' || x(5);
 end xform_varchar_ary;

end indexby;
/
commit;
exit
