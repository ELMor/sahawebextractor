
/* Copyright (c) Oracle Corporation 1999, 2000. All Rights Reserved. */

import sqlj.runtime.ref.DefaultContext;
import oracle.sqlj.runtime.Oracle;


public class TestRationalO
{

  public static void main(String[] args) throws java.sql.SQLException
  {
    DefaultContext dc;
    dc = Oracle.connect(new TestRationalO().getClass(),
                        "connect.properties");

    if (dc == null)
    {
       System.err.println("Can't connect!");
       return;
    }

    Integer n = new Integer(5);
    Integer d = new Integer(10);
    RationalO r = new RationalO(dc);
    r.setNumerator(n);
    r.setDenominator(d);

    Integer g = r.gcd(n, d);
    System.out.println("gcd: " + g);

    Float f = r.toreal();
    System.out.println("real value: " + f);

    RationalO s = r.plus(r); 
    System.out.println("sum: " + s.getNumerator() + "/" + 
                                 s.getDenominator());

    s = s.normalize();
    System.out.println("sum: " + s.getNumerator() + "/" + 
                                 s.getDenominator());
  } 
}
