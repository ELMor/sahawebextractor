set echo on
drop table GraduateStudent_tab;
drop table Student_tab;
drop table Person_tab;

drop type GraduateStudent_t force;
drop type Student_t force;
drop type Person_t force;

create type Person_t as object (ssn number, name varchar2(20), dob date)
 not final;
/

create type Student_t under Person_t (deptid number, major varchar2(20)) not final;
/

create type GraduateStudent_t under Student_t (hours number);
/

create table Person_tab of Person_t;

create table Student_tab of Student_t;

create table GraduateStudent_tab of GraduateStudent_t;

commit;
exit
