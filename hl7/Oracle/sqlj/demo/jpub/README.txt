/* Copyright (c) Oracle Corporation 1999-2002. */
/* All rights reserved */

This file is the README file for sample programs in the JPub demo
directory.  It specifies the steps required to run each of the
programs in this directory, and their expected behavior.
All demos use the scott/tiger schema.

0. First, check your environment setup
**************************************

0.1 Modify the entries in "connect.properties" and "jpub.properties"
    to reflect the logon strings used for your JDBC driver and database
    account.

    The demo programs in the demo directory use information contained
    in the file "connect.properties" to establish a runtime connection.
    In addition, the JPublisher utility uses information contained in
    the file "jpub.properties" to establish a connection.


0.2 Make sure the JPub utility can be invoked from the command line,
    and produces the expected output:

% jpub
JPub: Java Object Type Publisher, version 9.2.0.0.0 Production
Copyright (c) 1997, 2002, Oracle Corporation. All Rights Reserved.

JPub generates Java or SQLJ source code for the following SQL entities:
   object types, collection types, and packages.

Invocation:
   jpub <options>

The following option is required: 
   -user=<username>/<password>

Other options are:
   -input=<input file>
   Types and packages to be processed by JPub may be listed in the -input file
   The contents of the -input file may be as simple as:
      SQL Person
      SQL Employee

   -sql=<sql entity list>
   Types and packages to be processed by JPub may also be listed using the -sql option
   For example, -sql=a,b:c,d:e:f is equivalent to the -input file entries:
      SQL a
      SQL b AS c
      SQL d GENERATE e AS f

   The following options determine which Java types are generated
   to represent SQL user-defined, numeric, lob, and other types:
   -usertypes=oracle|jdbc
   -numbertypes=objectjdbc|jdbc|bigdecimal|oracle
   -lobtypes=oracle|jdbc
   -builtintypes=jdbc|oracle
   -addtypemap=<sql opaque type>:<java wrapper class>
   -addtypemap=<plsql type>:<java>:<sql type>:<sql2plsql>:<plsql2sql>
   -plsqlmap=true|false|always   - control generation of PL/SQL wrappers
   -plsqlfile=<file for generated PL/SQL wrapper code>
   -plsqlpackage=<package for generated PL/SQL code>

   -case=mixed|same|lower|upper
   -dir=<base directory for generated Java files>
   -driver=<JDBC-Driver>
      (default: oracle.jdbc.driver.OracleDriver)
   -encoding=<Java encoding of -input file and generated files>
   -gensubclass=true|force|call-super|false
   -methods=none|named|all
   -omit_schema_names
   -package=<package-name>
   -access=public|protected|package
   -compatible=ORAData|CustomDatum
   -context=generated|DefaultContext
   -serializable=true|false
   -tostring=true|false
   -props=<properties file from which to load options>
   -url=<JDBC-URL>
      (default: jdbc:oracle:oci8:@)


    If this expected output is not obtained, please check your environment
    settings for PATH and CLASSPATH:
       * PATH should include the directory .../sqlj/bin
       * CLASSPATH should include the file .../sqlj/lib/translator.jar

    In addition, a JDK 1.1-compatible Java compiler (assumed to be javac)
    and the java interpreter (assumed java) must be correctly installed.


0.3 Make sure that the SQLJ translator can be invoked from the command
    line prompt, and produces the expected output:

% sqlj
Usage:  sqlj [options] file1.sqlj [file2.java] ...
   or   sqlj [options] file1.ser  [file2.jar]  ...
where options include:
     -d=<directory>           root directory for generated binary files
     -encoding=<encoding>     Java encoding for source files
     -user=<user>/<password>  enable online checking
     -url=<url>               specify URL for online checking
     -status                  print status during translation
     -compile=false           do not compile generated Java files
     -linemap                 instrument compiled class files from sqlj source
     -profile=false           do not customize generated *.ser profile files
     -ser2class               convert generated *.ser files to *.class files
     -P-<option> -C-<option>  pass -<option> to profile customizer or compiler
     -P-help  -C-help         get help on profile customizer or compiler
     -J-<option>              pass -<option> to the JavaVM running SQLJ
     -version                 get SQLJ version
     -help-alias              get help on command-line aliases
     -help-long               get full help on all front-end options

Note:  place -<key>=<value> in sqlj.properties as sqlj.<key>=<value>

    If this expected output is not obtained, please check your environment
    settings, as described in section 0.2.


1. Next, run the sample programs in this directory.
***************************************************

Four rational number examples are provided, demonstrating three different
ways to do the same thing:

* "MyRationalC" uses a rational number object type.  The class JPub
  generates has no methods other than "get" and "set" accessor methods.
  The user extends the class JPub generates and adds additional methods.

The other examples involve client wrapper methods that invoke server-side
PL/SQL methods:

* "RationalO" uses a rational number object type, with a few methods for
  manipulating the rational numbers.

* "RationalP" puts the rational number methods in a PL/SQL package.  A
  rational number type without methods provides arguments to the PL/SQL
  methods in the package.

* "MyRationalO" shows another example of extending a JPub-generated
  rational number class.

* "MyRationalO8i" shows how to use the JPublisher 8i compatibility
  mode. This may be useful if you wrote an application with JPublisher
  version 8.1.7 or earlier, and do not want to change you application
  to take advantage of the changed APIs in JPublisher release 9i.


These examples are kept deliberately simple, so as not to obscure the
principles involved.

This directory contains the following files for demos 1.1 through 1.7.
Additional files are listed below with the demo that uses it.

  README.txt            the file you are now reading

  Rational.sql          SQL script to create the object types Rational and
                        RationalO and the PL/SQL package RationalP

  MyRationalC.java      Source code for the class MyRationalC

  TestMyRationalC.java  Test program to demonstrate use of the
                        class MyRationalC, a hand-written class that
                        extends the JPub-generated class JPubRationalC

  TestRationalO.java    Test program to demonstrate use of the class RationalO,
                        which JPub generates to represent the RationalO type

  TestRationalP.java    Test program to demonstrate use of the class RationalP,
                        which JPub generates to represent the PL/SQL package
                        RationalP. This test also uses the Rational class,
                        which JPub generates to represent the Rational type

  TestMyRationalO.java  Test program to demonstrate use of the class
                        MyRationalO, a hand-written class that extends the
                        JPub-generated class JPubRationalO.

  MyRationalO.java      Source code for the class MyRationalO

  MyRationalO8i.java    Source code for the class MyRationalO8i

  connect.properties    used by the test programs when they connect

  jpub.properties       used by JPub when it connects


1.1 Run the Rational.sql SQL script to create the object types and
    the PL/SQL package used in these examples. (replace scott/tiger
    with your username/password values).

    % sqlplus scott/tiger @Rational.sql

1.2 Run the MyRationalC test.  In this test, JPub generates
    JPubRationalC.java.
    This class represents the database object type RationalO.
    The hand-written class MyRationalC.java extends JPubRationalC
    by adding methods such as toString() and plus().

    % jpub -props=jpub.properties -sql=RationalO:JPubRationalC:MyRationalC -mapping=jdbc -methods=none
    % javac JPubRationalC.java MyRationalC.java TestMyRationalC.java
    % java TestMyRationalC
 
    The expected output is:

    gcd: 5
    real value: 0.5
    sum: 100/100
    sum: 1/1

1.3 Run the RationalO test.  In this test, JPub generates
    RationalO.java.  This class invokes server methods of the
    database object type RationalO, which represents rational numbers.

    % jpub -props=jpub.properties -sql=RationalO -methods=true
    % sqlj RationalO.sqlj TestRationalO.java
    % java TestRationalO

    The expected output is:

    gcd: 5
    real value: 0.5
    sum: 100/100
    sum: 1/1

1.4 Run the RationalP test.  This time, JPub generates a .java file
    for the class Rational, which represents the database object type
    of the same name.
    This Rational class has numerator and denominator attributes, but
    no methods.
    JPub also generates a .java file for the Java class RationalP,
    which represents the PL/SQL package of the same name.
    This package has methods to manipulate rational numbers.

    % jpub -props=jpub.properties -sql=RationalP,Rational -mapping=oracle -methods=true
    % sqlj RationalP.sqlj Rational.java TestRationalP.java
    % java TestRationalP

    The expected output is:

    real value: 0.5
    gcd: 5
    sum: 100/100
    sum: 1/1

1.5 Run the MyRationalO test.  This test demonstrates how to extend a
    class generated by JPub.  This time, JPub generates
    JPubRationalO.java.  This class represents the database object
    type RationalO.  The hand-written class MyRationalO.java extends
    JPubRationalO by adding a toString() method.

    % jpub -props=jpub.properties -sql=RationalO:JPubRationalO:MyRationalO -methods=true
    % sqlj JPubRationalO.sqlj MyRationalO.java TestMyRationalO.java
    % java TestMyRationalO

    The expected output is:

    gcd: 5
    real value: 0.5
    sum: 100/100
    sum: 1/1


1.6 Run the MyRationalO8i test. This test is very similar to MyRationalO,
    except that JPublisher is run in 8i compatibility mode. In this mode
    JPublisher generates sources that will be compatible with code that
    you wrote for subclassing classes generated with JPublisher 8.1.7 or
    earlier.

    % jpub -props=jpub.properties -sql=RationalO:JPubRationalO8i:MyRationalO8i -methods=true -compatible=8i
    % sqlj JPubRationalO8i.sqlj MyRationalO8i.java TestMyRationalO8i.java
    % java TestMyRationalO8i

    The expected output is:

    gcd: 5
    real value: 0.5
    sum: 100/100
    sum: 1/1


1.7 Run the inheritance test. This test shows how you can use JPublisher
    to generate code for a SQL object type inheritance hierarchy.
    The TestInh test driver is written in SQLJ and also shows the
    use of meta bind variable to write dynamic SQL code in SQLJ.
    
    First you need to create the types PERSON_T, STUDENT_T and
    GRADUATESTUDENT_T, as well as three associated extent tables in
    the database.

    % sqlplus scott/tiger @Inherit.sql

    Now you can use JPublisher to generate code, SQLJ to compile
    it and the test driver, and run it.

    % jpub -props=jpub.properties -sql=PERSON_T:InhPerson,STUDENT_T:InhStudent,GRADUATESTUDENT_T:InhGraduate
    % sqlj TestInh.sqlj InhPerson*.java InhStudent*.java InhGraduate*.java 
    % java TestInh

    The expected output is:

    Insert InhPerson Jerry P. into PERSON_TAB
    Insert InhStudent George P. into PERSON_TAB
    Insert InhGraduate Robert P. into PERSON_TAB
    List the values selected from PERSON_TAB
    PERSON: 1002, Jerry P.
    STUDENT: 1202, George P., Chemistry
    GRADUATESTUDENT: 1222, Robert P., Biology, 30
    Insert InhStudent George S. into STUDENT_TAB
    Insert InhGraduate Robert S. into STUDENT_TAB
    List the values selected from STUDENT_TAB
    STUDENT: 1202, George S., Chemistry
    GRADUATESTUDENT: 1222, Robert S., Biology, 30
    Insert InhGraduate Robert G. into GRADUATESTUDENT_TAB
    List the values selected from GRADUATESTUDENT_TAB
    GRADUATESTUDENT: 1222, Robert G., Biology, 30


1.8 The next example creates a SQL object type BOOLEANS with object
    methods that use the PL/SQL type BOOLEAN.

    Source files:
      Booleans.sql
      TestBooleans.sqlj.

    Generated files:
      Booleans.sqlj
      BooleansWrap.sql

    Run the Booleans.sql SQL script to create the BOOLEAN type
    (replace scott/tiger with your username/password).
  
    % sqlplus scott/tiger @Booleans.sql

    Use JPublisher to publish the SQL object type BOOLEANS.
    Note that the JPublisher invocation below must be entered
    as a single command line.

    % jpub -props=jpub.properties -sql=BOOLEANS:Booleans \
           -plsqlfile=BooleansWrap.sql -plsqlpackage=BOOLEANS_WRAP \
           -tostring=true

    Run the SQL*Plus to load the generated PL/SQL wrapper
    code in the BOOLEANS_WRAP package.

    % sqlplus scott/tiger @BooleansWrap.sql

    Now use SQLJ to compile everything and then run it.

    % sqlj TestBooleans.sqlj Booleans.sqlj
    % java TestBooleans

    The expected output is:

    SCOTT.BOOLEANS(0,1,0).f(false) = true
    SCOTT.BOOLEANS(0,1,0).p(true,false -> true,-> true)
      returns new state SCOTT.BOOLEANS(0,null,0)


1.9 Run the index-by table demo. This demo shows JPub support for
    scalar index-by table types when using the JDBC-OCI driver.
    Some properties used for JPub are included in Indexby.sql,
    prefixed by "--  ".  They are used to set up typemap entries
    that map PL/SQL index-by table types to Java array types.

    Source files:
      Indexby.sql
      TestIndexby.sqlj

    Generated files:
      Indexby.sqlj

    First create the PL/SQL package INDEXBY with scalar
    index-by table types and procedures that use those types.

    % sqlplus scott/tiger @Indexby.sql

    Now publish this package to Java. Note that we have to specify
    the Indexby.sql script as a JPub properties files, since
    the type correspondences are defined there.

    % jpub -u scott/tiger -s Indexby -props=Indexby.sql

    Translate the generated SQL code and execute the test driver.

    % sqlj Indexby.sqlj TestIndexby.sqlj
    % java TestIndexby

    The expected output is:

    Connected.
    getFloatAry() = [1.0,2.0,3.0,4.0,5.0]
    powIntegerAry([0,1,2,3,4,5,6], ->[0,1,4,9,16])
    xformVarcharAry([a,B,ce,day,aye] -> [a,B2B,ce3ce3ce,day4day4day4day,aye5aye5aye5aye5aye])
    Done.


1.10 Run the PL/SQL type demo. This test demonstrates JPublisher
     support for PL/SQL types and packages. The demo creates
     the necessary SQL types to publish a PL/SQL package - here
     SYS.DBMS_UTIL - to Java.
    
    Source files: 
      WrDbmsUtil.sql   - creates supporting SQL types and conversions

    Generated files:
      WrDbmsUtil.sqlj                  - actual wrapper for package
      WrDbmsUtilInstanceTable.java     - for supporting SQL type
      WrDbmsutilInstanceRecord.java    - for supporting SQL type
      WrDbmsutilInstanceRecordRef.java
      WrDbmsUtilPlsql.sql              - PL/SQL wrapper 

    First create SQL types that correspond to PL/SQL record
    types and to PL/SQL index-by table types, as well as PL/SQL
    conversion functions that map between the two.

    % sqlplus scott/tiger @WrDbmsUtil.sql

    Now invoke JPublisher and do two things at the same time:
     - publish the SQL types defined in WrDbmsUtil.sql
     - publish the package SYS.DBMS_UTILITY using the typemap
       entries and conversion functions we just defined.
    The typemap entries that map PL/SQL types from the
    DBMS_UTILITY package into these SQL types are embedded
    into the script as SQL comments.

    The following is a single JPublisher command line.

    % jpub -u scott/tiger -props=WrDbmsUtil.sql \
           -s SYS.DBMS_UTILITY:WrDbmsUtil

    Now load WR_DBMSUTIL_PLSQL into the database.

    % sqlplus scott/tiger @WrDbmsUtilPlsql.sql

    Translate and compile all of the Java code.

    % sqlj WrDbmsUtil*.sqlj WrDbmsUtil*.java

    Now you can use the class WrDbmsUtil and the generated
    Java types to call functions and procedures in the
    SYS.DBMS_UTIL package.
