CONNECT SCOTT/TIGER;

drop TABLE struct_tab;
drop TYPE struct_ty force;

CREATE TYPE struct_ty AS OBJECT
( 
  datum        DATE,
  num     NUMBER
);
/

CREATE TABLE struct_tab
( 
  objVal       struct_ty
);

INSERT INTO struct_tab VALUES (struct_ty(TO_DATE('01-01-2000','MM-DD-YY'),1));
COMMIT;

CREATE OR REPLACE PROCEDURE STRUCT_TY_PROC(o1 in struct_ty, o2 out struct_ty) AS
BEGIN
  o2 := struct_ty(o1.datum, o1.num-999);
END;
/

EXIT;
