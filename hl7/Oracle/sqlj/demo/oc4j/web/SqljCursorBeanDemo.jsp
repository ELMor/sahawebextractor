
<%@ page import="java.sql.* , oracle.jsp.dbutil.*, oracle.sqlj.runtime.*" %>

<!------------------------------------------------------------------
 * This is a basic JavaServer Page that uses a DB Access Bean and queries
 * emp table in schema scott and outputs the result in an html table.
 *  
--------------------------------------------------------------------!>

<jsp:useBean id="connbean" class="oracle.sqlj.runtime.SqljConnBean" scope="session">
<jsp:setProperty name="connbean" property="User" value="scott"/>
<jsp:setProperty name="connbean" property="Password" value="tiger"/>
<jsp:setProperty name="connbean" property="URL" value="jdbc:oracle:thin:@quwang-sun:1521:orcl"/>
</jsp:useBean>
<jsp:useBean id="curbean" class="oracle.jsp.dbutil.CursorBean" scope="session">
<jsp:setProperty name="curbean" property="PreFetch" value="10"/>
<jsp:setProperty name="curbean" property="ExecuteBatch" value="2"/>
</jsp:useBean>

<HTML> 
  <HEAD> 
    <TITLE>
 CursorBean Demo JSP
    </TITLE>
  </HEAD>
 <BODY BGCOLOR=EOFFFO> 
 <H1> Hello 
  <%= (request.getRemoteUser() != null? ", " + request.getRemoteUser() : "") %>
 !  I am Cursor Bean  JSP.
 </H1>
 <HR>
 <B> I`m using curbean and i'm quering EMP names from EMP table in schema SCOTT..
 </B> 

 <P>
<%
 
    try {

      // Make the Connection
      connbean.connect();

      String sql = "BEGIN OPEN ? FOR SELECT ename FROM emp; END;";
 
      // Create a Callable Statement
      curbean.create ( connbean, CursorBean.CALL_STMT, sql);
      curbean.registerOutParameter(1,oracle.jdbc.driver.OracleTypes.CURSOR);

      // Execute the PLSQL
      curbean.executeUpdate ();
 
      // Get the Ref Cursor
      ResultSet rset = curbean.getCursor(1);

      out.println(oracle.jsp.dbutil.BeanUtil.translateToHTMLTable (rset));

      // Close the RefCursor
      rset.close();

      // Close the Bean
      curbean.close();

      // Close the connection
      connbean.close();

    } catch (SQLException e) {
      out.println("<P>" + "There was an error doing the query:");
      out.println ("<PRE>" + e + "</PRE> \n <P>");
    }
%>

 </BODY>
</HTML>
