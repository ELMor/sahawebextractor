
<%@ page import="java.sql.* , oracle.sqlj.runtime.*, oracle.jsp.dbutil.CursorBean" %>

<!------------------------------------------------------------------
 * This is a basic JavaServer Page that uses a Sqlj Connection Bean and queries
 * emp table in schema scott and outputs the result in an html table.
 *  
--------------------------------------------------------------------!>

<jsp:useBean id="cbean" class="oracle.sqlj.runtime.SqljConnBean" scope="session"> 
<jsp:setProperty name="cbean" property="User" value="scott"/>
<jsp:setProperty name="cbean" property="Password" value="tiger"/>
<jsp:setProperty name="cbean" property="URL" value="jdbc:oracle:thin:@quwang-sun:1521:orcl"/>
<jsp:setProperty name="cbean" property="PreFetch" value="5"/>
<jsp:setProperty name="cbean" property="StmtCacheSize" value="2"/>
</jsp:useBean>

<HTML> 
  <HEAD> 
    <TITLE>
  SQLJ Connection Bean Demo JSP
    </TITLE>
  </HEAD>
 <BODY BGCOLOR=EOFFFO> 
 <H1> Hello 
  <%= (request.getRemoteUser() != null? ", " + request.getRemoteUser() : "") %>
 !  I am SQLJ Connection Bean Demo JSP.
 </H1>
 <HR>
 <B> I`m using connection and a query bean and i'm quering EMP names and Salary from EMP table in schema SCOTT..
 </B> 
 <P>
<%
    try {
 
      // Make the Connection
      cbean.connect();

      String sql = "SELECT ename, sal FROM scott.emp ORDER BY ename";

      // get a Cursor Bean
      CursorBean cb = cbean.getCursorBean  (CursorBean.PREP_STMT, sql);

      out.println(cb.getResultAsHTMLTable());

      // Close the cursor bean
      cb.close();
      // Close the Bean to close the connection
      cbean.close();
    } catch (SQLException e) {
      out.println("<P>" + "There was an error doing the query:");
      out.println ("<PRE>" + e + "</PRE> \n <P>");
    }
%>

 </BODY>
</HTML>
