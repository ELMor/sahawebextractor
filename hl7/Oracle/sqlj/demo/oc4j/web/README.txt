This directory contains files that demonstrate the usage of SQLJ-specific connection beans, namely, oracle.sqlj.runtime.SqljConnBean and oracle.sqlj.runtime.SqljConnCacheBean. 

(0) Modify OC4J config files. 
For "config/global-web-application.xml":
	<servlet>
		<servlet-name>sqljsp</servlet-name>
		<servlet-class>oracle.jsp.runtimev2.JspServlet</servlet-class>
	</servlet>
	<servlet-mapping>
		<servlet-name>sqljsp</servlet-name>
		<url-pattern>/*.sqljsp</url-pattern>
	</servlet-mapping>
For config/mime.types:
	application/sqljsp                      sqljsp

(1) Start OC4J,

% cd j2ee/home
% java -jar oc4j.jar

(2) Copy JSP and SQLJ jar files :

% cp ${SRCHOME}/sqlj/runtime12ee.jar j2ee/home/lib
% cp ${SRCHOME}/sqlj/translator.zip j2ee/home/lib
% cp -f ${SRCHOME}/jsp/lib/ojsputil.jar j2ee/home/lib

(3) Copy .jsp or .sqljsp files:
% cd {SRCHOME}/sqlj/demo/oc4j/ojsp
% cp *.jsp *.sqljsp j2ee/home/default-web-app/examples/jsp/

(4) SqljConnBeanDemo 

http://localhost:8888/examples/jsp/SqljConnBeanDemo.jsp

Results
-------
Hello ! I am SQLJ Connection Bean Demo JSP. 

I`m using connection and a query bean and i'm quering EMP names and Salary from EMP table in schema SCOTT.. 

 ENAME SAL
 ADAMS 1100
 ALLEN 1600
 BLAKE 2850
 CLARK 2450
 FORD 3000
 JAMES 950
 JONES 2975
 KING 5000
 MARTIN 1250
 MILLER 1300
 SCOTT 3000
 SMITH 800
 TURNER 1500
 WARD 1250

(5) SqljConnCacheBeanDemo

http://localhost:8888/examples/jsp/SqljConnCacheBeanDemo.jsp

Results
-------
Hello ! I am Connection Cache Demo Bean 


I will do a basic JDBC query to get employee data from EMP table in schema SCOTT. The connection is obtained from
the Connection Cache. 

        Employee Name Salary 
           ADAMS $1100.0 
            ALLEN $1600.0 
            BLAKE $2850.0 
            CLARK $2450.0 
             FORD $3000.0 
            JAMES $950.0 
            JONES $2975.0 
             KING $5000.0 
           MARTIN $1250.0 
           MILLER $1300.0 
            SCOTT $3000.0 
            SMITH $800.0 
           TURNER $1500.0 
            WARD $1250.0 

(6)SqljCursorBeanDemo

http://localhost:8888/examples/jsp/SqljCursorBeanDemo.jsp

Results
-------
Hello ! I am Cursor Bean JSP. 


I`m using cbean and i'm quering EMP names from EMP table in schema SCOTT.. 

 ENAME
 SMITH
 ALLEN
 WARD
 JONES
 MARTIN
 BLAKE
 CLARK
 SCOTT
 KING
 TURNER
 ADAMS
 JAMES
 FORD
 MILLER

(7) SqljIterator.sqljsp

http://localhost:8888/examples/jsp/SqljIterator.jsp

Results
-------
Hello, SCOTT! 
I will use a SQLJ iterator to get employee data from EMP table in schema SCOTT.. 

Employee Name Salary 
 ADAMS 1100.0 
 ALLEN 1600.0 
 BLAKE 2850.0 
 CLARK 2450.0 
 FORD 3000.0 
 JAMES 950.0 
 JONES 2975.0 
 KING 5000.0 
 MARTIN 1250.0 
 MILLER 1300.0 
 SCOTT 3000.0 
 SMITH 800.0 
 TURNER 1500.0 
 WARD 1250.0 

(8) SqljSelectInto.sqljsp

http://localhost:8888/examples/jsp/SqljSelectInto.sqljsp?empno=7902

Results
-------
Employee # 7902 Details: 

      Name       : FORD
      Salary     : 3000.0
      Date hired : 03-DEC-1981


              
