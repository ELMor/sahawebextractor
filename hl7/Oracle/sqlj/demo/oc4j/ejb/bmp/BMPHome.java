import java.util.Collection;
import javax.ejb.*;
import java.rmi.RemoteException;

public interface BMPHome extends EJBHome
{
	public BMP create(String name, Float quantity) throws CreateException, RemoteException;

	public BMP findByPrimaryKey(BMPPK pk) throws RemoteException, FinderException;
	public Collection findAll() throws RemoteException, FinderException;
}
