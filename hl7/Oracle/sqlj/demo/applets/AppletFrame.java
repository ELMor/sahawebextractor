import java.awt.Frame;
import java.awt.Image;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.applet.Applet;
import java.applet.AppletStub;
import java.applet.AppletContext;
import java.applet.AudioClip;
import java.net.URL;
import java.util.Enumeration;

public class AppletFrame extends Frame
                         implements AppletStub, AppletContext
{
  //
  // Utility class that permits to call an applet as an application.
  // See p. 506ff. in:  C.S. Horstmann, G. Cornell, "core JAVA 1.1",
  //                    vol.I, Sunsoft Press, 1997
  // 

  AppletFrame(Applet a, int width, int height)
  {
    m_a = a;

    setTitle(a.getClass().getName());
    setSize(width, height);
    add("Center", a);
    a.init();
    show();
    a.start();

    addWindowListener(
      new WindowAdapter() {
          public void windowClosing(WindowEvent e)
          {
            m_a.stop();
            System.exit(0);
          }
      });
  }

  private Applet m_a = null;

  // AppletStub methods
  public boolean isActive() { return true; };
  public URL getDocumentBase() { return null; };
  public URL getCodeBase() { return null; };
  public String getParameter(String name) { return ""; };
  public AppletContext getAppletContext() { return this; };
  public void appletResize(int width, int height) { };

  // AppletContext methods
  public AudioClip getAudioClip(URL url) { return null; }
  public Image getImage(URL url) { return null; }
  public Applet getApplet(String name) { return null; }
  public Enumeration getApplets() { return null; }
  public void showDocument(URL url) { }
  public void showDocument(URL url, String target) { }
  public void showStatus(String status) { }

}
