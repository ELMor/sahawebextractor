REM  table creation for demo BatchDemo.sqlj

DROP TABLE BATCH_DEMO;

CREATE TABLE BATCH_DEMO
            (EMPNO     NUMBER(7), 
             ENAME     VARCHAR2(20),
             HIREDATE  DATE,
             SAL       NUMBER(10, 2)
            );
/
EXIT;
/
