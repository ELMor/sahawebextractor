
-- connect / as sysdba;

create or replace package sqljutl as

      -- The following is required at translate-time for SQLJ
      function has_default(oid number,
                           proc char,
                           seq number,
                           ovr number) return number;

      -- The following is required at translate-time for JPublisher
      procedure get_typecode(tid raw, code OUT number,
                             class OUT varchar2, typ OUT number);

      -- The following might be used at runtime for converting
      -- between SQL and PL/SQL types 
      function bool2int(b boolean) return integer;
      function int2bool(i integer) return boolean;
      function ids2char(iv DSINTERVAL_UNCONSTRAINED) return CHAR;
      function char2ids(ch CHAR) return DSINTERVAL_UNCONSTRAINED;
      function iym2char(iv YMINTERVAL_UNCONSTRAINED) return CHAR;
      function char2iym(ch CHAR) return YMINTERVAL_UNCONSTRAINED;

end sqljutl;
/

create or replace package body sqljutl is

      function has_default(oid number,
                           proc char,
                           seq number,
                           ovr number) return number is
               def number;
      begin
         if proc IS NULL
         then
            select DEFAULT# INTO def FROM ARGUMENT$
                   WHERE PROCEDURE$ IS NULL AND OBJ# = oid
                         AND SEQUENCE# = seq AND OVERLOAD# = ovr;
         else 
            select DEFAULT# INTO def FROM ARGUMENT$
                   WHERE PROCEDURE$ = proc AND OBJ# = oid
                         AND SEQUENCE# = seq AND OVERLOAD# = ovr;
         end if;

         if def IS NULL
         then return 0;
         else return 1;
         end if;
      end has_default;


      procedure get_typecode
                  (tid raw, code OUT number,
                   class OUT varchar2, typ OUT number) is
         m NUMBER;
      begin
         SELECT typecode, externname, externtype INTO code, class, typ
         FROM TYPE$ WHERE toid = tid;
      exception
         WHEN TOO_MANY_ROWS
         THEN
         begin
           SELECT max(version#) INTO m FROM TYPE$ WHERE toid = tid;
           SELECT typecode, externname, externtype INTO code, class, typ
           FROM TYPE$ WHERE toid = tid AND version# = m;
         end;
      end get_typecode;

      function bool2int(b BOOLEAN) return INTEGER is
      begin if b is null then return null;
            elsif b then return 1;
            else return 0; end if;
      end bool2int;

      function int2bool(i INTEGER) return BOOLEAN is
      begin if i is null then return null;
            else return i<>0;
            end if;
      end int2bool;

      function ids2char(iv DSINTERVAL_UNCONSTRAINED) return CHAR is
         res CHAR(19);
      begin
         res := iv;
      end ids2char;

      function char2ids(ch CHAR) return DSINTERVAL_UNCONSTRAINED is
         iv DSINTERVAL_UNCONSTRAINED;
      begin
         iv := ch;
         return iv;
      end char2ids;

      function iym2char(iv YMINTERVAL_UNCONSTRAINED) return CHAR is
         res CHAR(9);
      begin
         res := iv;
      end iym2char;

      function char2iym(ch CHAR) return YMINTERVAL_UNCONSTRAINED is
         iv YMINTERVAL_UNCONSTRAINED;
      begin
         iv := ch;
         return iv;
      end char2iym;

end sqljutl;
/

grant execute on sqljutl to public ;

