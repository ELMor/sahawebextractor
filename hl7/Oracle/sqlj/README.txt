Release Notes for Oracle SQLJ Release 9.2.0.1.0 Production


CONTENTS OF THIS README

  1. WHAT IS SQLJ?
  2. WHAT IS IN THIS DISTRIBUTION?
  3. INSTALLATION
  4. WHAT'S NEW?
  5. PLATFORM-SPECIFIC INFORMATION
  6. BUGS FIXED
  7. KNOWN RESTRICTIONS AND PROBLEMS
  8. USING SQLJ IN Server-Side JavaVM: KNOWN RESTRICTIONS AND PROBLEMS


1. WHAT IS SQLJ?

Oracle, IBM, Compaq, Sybase, Informix, Sun Microsystems, and others have
contributed to the SQLJ Standard for embedding SQL statements and
constructs in Java programs (ISO/IEC 9075-10:2000, formerly ANSI
X3.135.10-1998).

SQLJ is a way to reduce the development and maintenance costs of Java
programs that require database connectivity. SQLJ provides a simpler model
for Java code containing static SQL statements (SQL that does not change
each time you run your application or applet. Applications typically
contain more static SQL than dynamic SQL). The reduction in the cost of
development and maintenance occurs because SQLJ uses an easier methodology
of embedding SQL directly into Java programs, resulting in more concise and
more legible code. SQLJ can further make coding easier by providing
compile-time checks on the SQL statements your application uses. In using
dynamic SQL APIs such as JDBC, you won't find out if your SQL is
syntactically or semantically wrong until you test the line in your code
that runs it.

This implementation, the 9.2.0 release of Oracle's SQLJ Implementation, is
in the form of a translator that takes SQLJ programs and turns them into
Java programs that call -via the SQLJ runtime- JDBC. The translator itself
is written purely in Java.

This release supports the ISO (formerly ANSI) standard for embedding SQL in
Java.

This release supports the features of the Oracle 9.2.0 JDBC drivers. In
addition, this release also supports the embedding of dynamic SQL fragments
in SQLJ statements. You can use this release with any Oracle JDBC driver
running against an Oracle database, as well as any standard JDBC Driver
running against any database system.

For more information, visit the Oracle Technology network site at:

     http://technet.oracle.com/tech/java/sqlj_jdbc/

You can also find information on SQLJ at:

     http://www.oracle.com/java/sqlj/

Oracle Support services this product. For more information see:

     http://www.oracle.com/support/

Note on Version Numbers:

Versions of SQLJ are being distributed with Oracle database releases, as
well as Oracle iAS releases. Version 9.2.0 is synonymous with Oracle9i
Database Release 2. Version 9.0.2 is synonymous with Oracle9i iAS Release
2.0. Version 9.0.1 is synonymous with Oracle9i. Version 8.1.7 is synonymous
with Oracle8i Release 3. Version 8.1.6 is synonymous with Oracle8i Release
2. Version 8.1.5 corresponds to Oracle8i.


2. WHAT IS IN THIS DISTRIBUTION?

We assume that you have installed the SQLJ distribution files through the
Oracle 9.2.0 Installer into the directory $ORACLE_HOME/sqlj. $ORACLE_HOME
is the root installation directory for Oracle products.

This distribution also comprises an installation of the JPublisher product.
Please refer to the JPublisher release notes at
[$ORACLE_HOME/sqlj/READMEJPub.txt]. JPublisher is installed simultaneously
with SQLJ.

The SQLJ part of this distribution contains:

   * This file [$ORACLE_HOME/sqlj/README.txt].
   * An HTML file detailing the SQLJ installation. This file also contains
     references to the other documentation [$ORACLE_HOME/sqlj/index.html].
   * The Oracle SQLJ FAQ in HTML format.
     [$ORACLE_HOME/sqlj/doc/sqlj-faq.html].
   * The SQLJ Primer "SQLJ: Tricks, Traps, and Gems" in PDF and HTML
     formats [$ORACLE_HOME/sqlj/doc/sqlj-primer.pdf,.html].
   * Technical White Paper "Overview of SQLJ - Embedded SQL in Java" in PDF
     and HTML formats [$ORACLE_HOME/sqlj/doc/sqlj-overview.pdf,.html].
   * Technical White Paper "Using Oracle Objects in SQLJ programs" in PDF
     and HTML formats [$ORACLE_HOME/sqlj/doc/sqlj-objects.pdf,.html].
   * White Paper "SQLJ: Embedded SQL in Java" in PDF and HTML formats
     [$ORACLE_HOME/sqlj/doc/sqljwhpap.pdf,html].
   * SQLJ Runtime API: runtime packages documented in the javadoc format
     [$ORACLE_HOME/sqlj/doc/runtime].
   * Different SQLJ runtime versions:

        o [$ORACLE_HOME/sqlj/lib/runtime11.jar] - runtime for JDK 1.1,
          Oracle 9.2.0 JDBC (or Oracle 9.0.1 JDBC)
        o [$ORACLE_HOME/sqlj/lib/runtime12.jar] - runtime for JDK 1.2 or
          later, Oracle 9.2.0 JDBC (or Oracle 9.0.1 JDBC)
        o [$ORACLE_HOME/sqlj/lib/runtime12ee.jar] - runtime for JDK 1.2 or
          later Enterprise Edition (with packages javax.*), Oracle 9.2.0
          JDBC (or Oracle 9.0.1 JDBC)
        o [$ORACLE_HOME/sqlj/lib/runtime.jar] - runtime for all JDK, all
          Oracle JDBC drivers
        o [$ORACLE_HOME/sqlj/lib/runtime-nonoracle.zip] - runtime for all
          JDK, and non-Oracle JDBC drivers
     All runtimes are also distributed in uncompressed .zip format.

   * The jar  file containing the *.class files for the SQLJ
     translator[$ORACLE_HOME/sqlj/lib/translator.jar]. In order to use the
     translator you also need one of the runtime jars above. Note that the
     translator is also distributed in .zip format.
   * Files for testing your installation
     [$ORACLE_HOME/sqlj/demo/TestInstall*]
   * Sample files [$ORACLE_HOME/sqlj/demo], see
     [$ORACLE_HOME/sqlj/demo/README.txt] for more information:
        o additional examples using object types
          [$ORACLE_HOME/sqlj/demo/Objects],
        o examples showing JDBC 2.0-derived features from the SQLJ ISO
          standard [$ORACLE_HOME/sqlj/demo/jdbc20],
        o examples using Java stored procedures
          [$ORACLE_HOME/sqlj/demo/server],
        o contributed programs and tools that may be useful when
          programming in a server-side environment
          [$ORACLE_HOME/sqlj/demo/server/contrib],
        o examples showing SQLJ programs in applets
          [$ORACLE_HOME/sqlj/demo/applets],
        o examples of SQLJ customization and SQL checking components
          [$ORACLE_HOME/sqlj/demo/components], and
        o a sample J2EE application dmonstrating SQLJ under iAS with the
          OC4J Server [$ORACLE_HOME/sqlj/demo/oc4j].
   * Wrapper scripts and/or wrapper executables to simplify the invocation
     of SQLJ and related tools [$ORACLE_HOME/bin].

Direct your Web browser to $ORACLE_HOME/sqlj/index.html for an index of the
SQLJ documentation provided with this SQLJ installation.

The book "SQLJ Developer's Guide and Reference" is distributed
independently as part of the Oracle 9.2.0 documentation. It documents the
details of this version of SQLJ - how to test the installation and use the
product. The books in the Oracle 9.2.0 documentation are also available
online at:

     http://technet.oracle.com/docs/index.htm

For information about the JPublisher tool for generating Java classes for
SQL object and collection types, please refer to the book "JPublisher
Developer's Guide and Reference". Please refer to the book "JDBC
Developer's Guide and Reference" for information about Oracle's JDBC
drivers.


3. INSTALLATION

You must have a JDK 1.1.x or JDK 1.2 or later compatible Java environment.
Furthermore, you should have installed the Oracle 9.2.0 JDBC driver (or,
alternatively, the Oracle 9.0.1 JDBC driver).

Ensure that the directory $ORACLE_HOME/bin with the sqlj shell script or
executable is in your PATH.

   * Add the file $ORACLE_HOME/sqlj/lib/translator.jar to the environment
     variable CLASSPATH.
   * If you are using JDK 1.1.x and Oracle JDBC 9.0.1 or later then add the
     file $ORACLE_HOME/sqlj/lib/runtime11.jar to your CLASSPATH.
   * If you are using JDK 1.2 or later and Oracle JDBC 9.0.1 or later then
     add the file $ORACLE_HOME/sqlj/lib/runtime12.jar to your CLASSPATH.
   * If you are using JDK 1.2 or later Enterprise Edition and Oracle JDBC
     9.0.1 or later then add the file $ORACLE_HOME/sqlj/lib/runtime12ee.jar
     to your CLASSPATH.
   * If you are using an Oracle JDBC driver version prior to 9.0.1 add the
     file $ORACLE_HOME/sqlj/lib/runtime.jar to your CLASSPATH.
   * Otherwise (that is, if you are not using an Oracle JDBC driver) add
     the file $ORACLE_HOME/sqlj/lib/runtime-nonoracle.zip to your
     CLASSPATH.
   * If you are using a database that is not Java enabled you may want to
     install the SQLJUTL package into the SYS schema to avoid warning
     messages during SQLJ online translation. This package is defined in
     the script $ORACLE_HOME/sqlj/lib/sqljutl.sql

--- ATTENTION --- ATTENTION --- ATTENTION --- ATTENTION --- ATTENTION ---

   * If you want to use runtime.jar, you must now explicitly specify
     -codegen=iso. By default SQLJ translation uses -codegen=oracle which
     creates code specific to Oracle JDBC 9.0.1 or later and is not
     supported by runtime.jar.
   * SQLJ 8.1.7 and later requires that you place translator.jar and one of
     the runtime jar files (runtime.jar, runtime11.jar, or runtime12.jar)
     in your CLASSPATH in order to translate SQLJ programs. (Earlier SQLJ
     releases only required that you place translator.jar in the CLASSPATH
     in order to translate and/or run SQLJ programs.)

--- ATTENTION --- ATTENTION --- ATTENTION --- ATTENTION --- ATTENTION ---

The "Getting Started" section of the "SQLJ Developer's Guide and Reference"
discusses required settings for PATH and CLASSPATH in more detail.

You should first verify your JDBC installation before using SQLJ. Refer to
the section "Getting Started" in the book "SQLJ Developer's Guide and
Reference" for information on testing your SQLJ and JDBC installations.

This distribution of SQLJ has been run under the following configurations:

   * JDK 1.1, 1.2, and 1.3
   * for Applications: Solaris 2.6 with Oracle's 9.2.0 JDBC-OCI, 9.2.0
     JDBC-Thin drivers, and 9.0.1 JDBC drivers; Windows NT 4.0 with
     Oracle's 9.2.0 JDBC-OCI8, and 9.2.0 JDBC-Thin drivers; Oracle 9.2.0
     server-side JavaVM with server-side (kprb) JDBC drivers
   * for Applets: Netscape 4.7 (Windows NT, Solaris), MS Internet Explorer
     5.0 (Windows NT, Windows 95), AppletViewer (Solaris), Java plug-in
     1.1.2 (NT Explorer, Solaris Netscape, and NT Netscape)


4. WHAT'S NEW?


CHANGES SINCE VERSION 9.0.2.0.0 of DEC 18, 2001

TRANSLATOR

   * The syntax checker for Oracle SQL and Oracle PL/SQL is now always
     enabled by default. This provides full syntax checking of DDL
     statements for the first time.
   * SQLJ now  translates into .java files by default (setting
     -codegen=oracle) which eliminates .ser files. The generated code is
     for the most part straight Oracle JDBC code.
   * Users can request fixed-char semantics for binding strings in WHERE
     clauses to overcome semantic differences between CHAR and VARCHAR
     values (-fixedchar option).
   * The option usage for -optcols, -optparams, and -optparamdefaults has
     been simplified: you can specify the same option setting irrespective
     of whether -codegen=oracle (the default) or -codegen=iso is in effect.

LANGUAGE.

   * JDBC 3.0-style savepoints are supported (requires Oracle JDBC 9.2.0 or
     later driver, supported across all JDKs).
   * SQLJ now permits a style of host variable binding similar to
     bind-by-reference through the new front-end flag -bind-by-identifier.
     For example, multiple occurrences of same host variable :x in a SQLJ
     statement would be bound as a single occurrence with this setting.

RUNTIME

   * SQLJ now provides direct support for a number of  types:
        o CharacterStream (with corresponding NCHAR version). In JDBC 2.0
          this type supersedes UnicodeStream and AsciiStream.
        o SQL TIMESTAMP types are supported in form of oracle.sql.TIMESTAMP
          / TIMESTAMPTZ / TIMESTAMPLTZ.
        o SQL XMLTYPE is supported form of oracle.xdb.XMLType. In general,
          SQL opaque types are now supported.
        o Scalar PL/SQL index-by tables can be read or written in form of
          Java arrays.  This support is much simplified compared to  JDBC.
          Note that this feature is only available in conjunction with the
          OCI JDBC driver.
   * SQLJ uses the new explicit statement caching facility from JDBC 9.2.0
     which can now be controlled independently of JDBC's implicit statement
     caching.


CHANGES SINCE VERSION 9.0.1.0.0 of APRIL 23, 2001

TRANSLATOR

   * A syntax checker for Oracle SQL and Oracle PL/SQL is provided by the
     translator. Specifying the new front-end option -parser=both turns on
     full SQL and PL/SQL syntax checking by the translator for offline as
     well as online translations.
   * The translator supports cross-compilation options for JDK 1.3 and
     later. In particular, the options -C-bootclasspath, -C-extdirs and
     -C-target in a SQLJ command line specify the corresponding options for
     the underlining Java compiler -bootclasspath, -target and -extdirs.

RUNTIME

   * Support for SQLJ-specific datasources. A SQLJ-specific datasource
     extends an Oracle JDBC datasource by adding APIs that create SQLJ
     connection contexts based on the connections in the underlying
     datasource. The SQLJ-specific datasources live in the packages
     oracle.sqlj.runtime and oracle.sqlj.runtime.client. They are
     distributed in runtime12ee.jar.
   * Support for SQLJ connection beans. The SQLJ connection bean
     oracle.sqlj.runtime.SqljConnBean extends the JSP connection bean
     oracle.jsp.dbutil.ConnBean, adding APIs to create the connection
     contexts based on connection in the JSP bean. A similar relationship
     exists between  oracle.sqlj.runtime.SqljConnCacheBean and
     oracle.jsp.dbutil.ConnCacheBean. The SQLJ connection bean and SQLJ
     connection cache bean are distributed in runtime12ee.jar.


CHANGES SINCE VERSION 8.1.7.0.0 OF JUN 30 2000

LANGUAGE

   * Support for dynamic SQL in SQLJ statements. This eliminates the vast
     majority of cases where SQLJ programmers have had to revert to JDBC.
     The following example illustrates the use of a dynamically constructed
     table name.
          String table="EMP";
          #sql iter = { SELECT ename, empno from :{table} };
   * Support for FETCHing FROM a ResultSetIterator or a
     ScrollableResultSetIterator. In conjunction with the "FETCH CURRENT
     FROM ..." syntax, this permits the direct translation of JDBC code
     into SQLJ code.
   * Support for Oracle 9i Unicode Types through new SQLJ-specific types
     (oracle.sql.NCLOB, oracle.sql.NCHAR, oracle.sql.NString, and so on).
   * Improved object serialization in RAW and BLOB columns, using a public
     static _SQL_TYPECODE field

TRANSLATOR

   * The new command line flag -codegen=oracle can be used to generate
     Oracle-JDBC code directly. In this case, only .java source files, but
     no .ser profile files will be generated. This option setting requires
     the Oracle 9.0.1 or later JDBC driver and either the runtime11.jar or
     the runtime12.jar Oracle 9.0.1 or later SQLJ runtime library.
   * The profile customization flags -P-optcols, -P-optparams,
     -P-optparamdefaults have been superseded by new front-end flags
     -optcols, -optparams, and -optparamdefaults to be used in conjunction
     with -codegen=oracle. The flag -P-stmtcache is superseded (under the
     -codegen=oracle setting) by the following API on connection contexts:
     static setDefaultStmtCacheSize(int), static int
     getDefaultStmtCacheSize(), setStmtCacheSize(int), int
     getStmtCacheSize().
   * In order to produce standard SQLJ code, use the setting -codegen=iso.
   * The translator now also works under JDK 1.3.

RUNTIME

   * Support for oracle.sql.CustomDatum implementations as used by BC4J.
     These implementations incorrectly provide getCustomDatumFactory()
     instead of a getFactory() method. The SQLJ runtime now also supports
     the latter.
   * Support for the oracle.sql.ORAData interface, which supersedes the
     oracle.sql.CustomDatum interface
   * Support for Oracle 9i SQL inheritance, using the JPublisher-generated
     ORAData or SQLData implementations.
   * The SQLJ translator library, as well as all SQLJ runtime libraries are
     now also provided in compressed .jar format in addition to the
     previously provided uncompressed .zip format
   * In addition to runtime11.jar, runtime12.jar, which are applet-enabled
     and optimized for use with the Oracle JDBC 9.0.1 drivers, and to
     runtime.jar, which provides compatibility with all earlier Oracle JDBC
     drivers, we now also provide runtime-nonoracle.zip for non-Oracle JDBC
     drivers.


CHANGES SINCE VERSION 8.1.6.0.0 OF SEP 15 1999

LANGUAGE

This version supports the ISO standard ISO/IEC 9075-10:2000.

   * Support for JDBC 2.0 features as defined in the SQLJ ISO standard (see
     $ORACLE_HOME/sqlj/demo/jdbc20 for sample programs).
   * Connection context type maps for reading and writing user defined
     types, such as instances of java.sql.SQLData. This feature requires
     JDK 1.2 and the SQLJ runtime runtime12.jar.
   * Support for scrollable iterators (both, positional and named)
   * Association of connection contexts with DataSources
   * Control of batching and row prefetch through the ExecutionContext
   * Support for JDBC 2.0 connection pooling
   * Support for Java object serialization in RAW and BLOB columns, using
     connection context type maps
   * New "FETCH CURRENT FROM ..." syntax for positional iterators permits
     preserving program logic when migrating JDBC result sets to SQLJ
     iterators

TRANSLATOR

The translator's statusMain method can be called multiple times. This
permits the embedding of SQLJ in Oracle's JSP translator [Bug 1321820].

RUNTIME

   * Availability of different SQLJ runtime libraries:
     runtime11.jar, runtime12.jar: applet-enabled and optimized for use
     with the Oracle JDBC 8.1.7 drivers
     runtime.jar: provides compatibility with all Oracle JDBC drivers

SEE ALSO: Bug fixes - section 6. below.


CHANGES SINCE VERSION 8.1.5.0.0 (AND SOME SINCE 8.1.4)

LANGUAGE

   * This version supports the ANSI standard ANSI X3.135.10-1998.
   * SQLJ support for the Java language has been improved (see [Bug
     808400], [Bug 808729], [Bug 808856], and [Bug 845528] below).
   * Public static final field values (such as attributes in WITH clauses)
     are now properly and fully evaluated at translate time by SQLJ [Bug
     808856].
   * SQLJ now permits subclassing of iterators. SQLJ now permits you to use
     subclasses of oracle.sql.CustomDatum classes as host expressions [Bug
     879170]
   * SQLJ now supports the JDBC 2.0 types
     java.sql.Struct/Ref/Array/Blob/Clob.
   * SQLJ now supports JPublisher-generated JDBC 2.0 Java wrapper classes
     that implement the java.sql.SQLData interface.

TRANSLATOR

   * SQLJ now has a limited form of make functionality, similar to javac
     [Bug 801780].
     Note: you must always specify all relevant .sqlj source files on the
     SQLJ command line. The translator can automatically check for
     additional .java sources, but not for possibly required .sqlj sources.
     Use the following command line option to turn this functionality off.
          -checksource=false
   * Use the new -jdblinemap option instead of -linemap if you expect to
     debug the instrumented class files under the jdb debugger [Bug
     868524].
   * The -help option now prints a usage synopsis. Full help on all option
     settings has been moved to the -help-long option. Command line
     shorthand notation is explained with the option -help-alias.
   * A -version-long option prints out additional version information,
     including the Oracle JDBC driver version (if any) accessible in the
     CLASSPATH and the version of Java that you are using.
   * An -explain flag was added. This flag displays cause/action
     information on error and warning messages issued by the SQLJ
     translator.

CHECKING

The default offline and online checker class is now
oracle.sqlj.checker.OracleChecker. Depending on the JDBC driver detected in
your configuration, an appropriate checker will be selected. You need only
explicitly specify a particular checker when you are using an 8.0.x or
8.1.x or later JDBC driver and want to"downgrade" the types supported by
your program to Oracle7 functionality.

PERFORMANCE

SQLJ now supports all the performance enhancements previously available
through Oracle's JDBC drivers. See "SQLJ User's Guide and Reference -
Appendix A." for a complete description.

   * Statement caching. By default, the SQLJ runtime caches the last five
     SQL statements that it executed on a given JDBC connection. You can
     use the -P-Cstmtcache=NNflag to set the statement cache size to NN.
     Setting the size to 0 disables statement caching.
   * Batching. SQLJ now supports batching of INSERT, DELETE, and UPDATE
     statements. The batching function is controlled through
     theExecutionContext. Note: The batching API provided complies with the
     SQLJ batching support in ISO standard for SQLJ.
   * Parameter size registration. Optimizations for variable-size
     parameters and result set columns can be enabled through new flags
     -P-Coptcols and -P-Coptparams.

RUNTIME

Changes in runtime support:
[$ORACLE_HOME/sqlj/doc/runtime/RELEASENOTES.txt]


5. PLATFORM-SPECIFIC INFORMATION

We offer several general troubleshooting tips that should help in working
around platform specific problems that might occur.

GENERAL TROUBLESHOOTING

   * Turn off the JIT (Just In Time compilation). In many cases, this
     eliminates the problem. Either set the environment variable:
          JAVA_COMPILER=NONE
     or specify the following additional flag to the SQLJ command line:
          -J-Djava.compiler=none
     Note: You can set theSQLJ_OPTIONS environment variable to any
     additional flags that you always want to send to the SQLJ command
     line.
   * Reduce your PATH to contain a single, known JDK version.
   * Reduce your CLASSPATH to only contain:
        o your source files (often this is . - the current directory)
        o the Oracle JDBC driver classes111.jar or classes12.jar
        o the SQLJ translator translator.jar
        o one of the SQLJ runtimes (runtime11.jar, or runtime12.jar, or
          -for JDBC 8.1.7 or older- runtime.jar)
   * If you experience a hang during Java compilation (use the -status flag
     on the SQLJ command line to see how far the SQLJ translator got), then
     add the following flag to the SQLJ command line.
          -passes
   * Use a different JDK version.

UNIX PLATFORMS

   * If you need to customize the way in which SQLJ invokes the JavaVM and
     the Java compiler on your platform, you can edit the shell script:
          $ORACLE_HOME/bin/sqlj

WINDOWS PLATFORMS

Note that the wrapper executable $ORACLE_HOME/bin/sqlj.exe has only been
tested on Windows NT.

   * Some versions of Windows place rather severe restrictions on the
     number of characters in the environment and on the command line.
   * On Windows 95 you cannot use the '=' character in the string that is
     assigned to environment variables. In SQLJ_OPTIONS you can use '#'
     instead. Thus, on Windows 95, instead of specifying, for example, the
     following to turn the JIT off:
          set SQLJ_OPTIONS=-J-Djava.compiler=none
     you would say:
          set SQLJ_OPTIONS=-J-Djava.compiler#none
   * You must not have space characters in your CLASSPATH


6. BUGS FIXED


IN SQLJ VERSION 9.2.0

TRANSLATOR

   * By default SQLJ statement caching is now enabled with a cache size of
     5. In previous releases statement caching was disabled by default,
     since -if enabled- it might have resulted in error in JDBC implicit
     statement caching. As of the 9.0.1.3 release of Oracle JDBC there are
     no known issues with implicit statement caching.


IN SQLJ VERSION 9.0.2

TRANSLATOR

   * SQLJ now works with Oracle JDeveloper's Ferrari VM [Bug 2143574].

SQLJ IN Server-side JavaVM

   * SQLJ supports nested interface declarations in server-side JavaVM
     [Bug 1049817], such as
          interface Nested { #sql static iterator NestedCur(int); }


IN SQLJ VERSION 9.0.1

TRANSLATOR

   * The SQL-99 WITH syntax for SQL queries is now supported [Bug1589996].
   * Lowercase or mixed-case keywords are now accepted in the FETCH
     statement [Bug 1477216].
   * The SQLJ translator now displays Java-compiler error messages under
     JDK 1.3 [Bug 1477092].
   * The server-side SQLJ translator now supports SQLJ WITH-clause
     declarations, for example to specify a type map property [Bug
     1049818].
   * Full message translations are now available with this release [Bugs
     803875, 803869].

RUNTIME

   * .sqlj wrapper classes generated by JPublisher do no longer result in a
     leak of connection context instances when the application is run under
     JDK 1.2 or later with runtime12.jar, or when the setting
     -codegen=oracle is used (in other situations objects need to be
     explicitly released() - see the JPublisher README and documentation)
     [Bug 1691446].


IN SQLJ VERSION 8.1.7

TRANSLATOR

   * SQL source text in SQLJ statements of arbitrary size is now supported
     [Bug 1018117]. Earlier versions were limited to 65k.
   * The -ser2class conversion now supports .ser files of arbitrary size
     [Bug 1061663]. Earlier versions were limited to 32k.

RUNTIME

   * The new Oracle SQLJ library runtime11.jar now permits Oracle SQLJ
     programs to properly work in applet environments [Bug 814704].
     Note: If you use the SQLJ 9.0.1 option -codegen=oracle you eliminate
     all use of reflection, making your SQLJ applet work in all
     environments. If instead you leave code generation at the default
     setting -codegen=iso, then depending on the browser used, certain
     features (CAST statement, CustomDatum and SQLData support) may not be
     available, since these do require the use of reflection. Another
     workaround is to use the Java plug-in provided by Sun Microsystems. It
     will permit all SQLJ statements to run - even in pre-4.X browsers.
     See also the applet demos in [$ORACLE_HOME/sqlj/demo/applets].
   * The following error does not occur any longer in SQLJ applets.[Bug
     850197]:
          java.lang.IllegalAccessError:
                 sqlj.runtime.ExecutionContext.DEBUG
          at sqlj.runtime.ExecutionContext$StatementStack.setStatement


IN SQLJ VERSION 8.1.6

LANGUAGE

   * SQLJ now permits "mixed" Array declarations [Bug 808400], such as
     int[] a,b[];
   * SQLJ host expressions now permit calls to functions that return an
     array [Bug 845528]
   * SQLJ iterator and context declarations inside of method blocks are now
     permitted [Bug 808729].
     Note: In JDK 1.1.X you cannot reference classes declared at the block
     level. You will see a subsequent error by the Java compiler when you
     try to use such a class. However, JDK 1.2 fully supports block-level
     declarations.
   * Japanese halfwidth sound characters such as \uff70 and \uff65 are now
     accepted in SQL identifiers. In general, SQL identifiers can be
     composed of characters from the superset of the definitions of letters
     and digits in Unicode 2.0, Java, and all Oracle SQL character sets.

TRANSLATOR

   * The SQLJ translator now works with JDK 1.2 [Bug 814692]. Ensure that
     you are using the proper Oracle JDBC driver file (classes12.jar).
   * The current release of the SQLJ translator runs under Microsoft's J++
     Java VM (jview), and the generated *.java files compile under
     Microsoft's J++ Java compiler (jvc).
   * The -linemap option now instruments all the Java classes declared in a
     .sqlj source file, including inner classes. The only exception is
     anonymous classes, which are currently not being instrumented.
     Problems that occurred during instrumentation are fixed [Bugs 814687,
     814680].
   * SQL source text in SQLJ statements may now have arbitrary size. [Bug
     1018117].
   * SQLJ -ser2class conversion now works for any number of files [Bug
     859265]. Additionally, -ser2class can now convert .ser files of
     abitrary size to .class representation [Bug 1061663].
   * Certain warnings are no longer reported on the wrong source file [Bug
     859307].
   * SQLJ now detects the invalid use of "/" instead of "\" as directory
     separator on the Windows NT platforms and will issue an error [Bug
     550152].
   * SQLJ should no longer experience hangs on NT [Bug 749636]. The use of
     the -passes option is no longer required on the NT platform.

ONLINE CHECKING

   * Multiple database errors are now correctly reported [Bug 460846].
   * Stored procedure overloading now matches the PL/SQL rules [Bug
     808874].

RUNTIME

A SQLException will now be issued when a SQLJ connection context is
initialized with a null JDBC connection.


7. KNOWN RESTRICTIONS AND PROBLEMS

A list of know issues and bugs in SQLJ version 9.2.0.

LANGUAGE

   * Expression evaluation with the -codegen=oracle option (now the
     default) deviates from the standard evaluation semantics with
     -codegen=iso. You may want to avoid using host expressions that have
     side effects [Bug  2129084].
   * Classes containing executable SQLJ statements should not be named
     "java", "sqlj", or "oracle" [Bug 808835]. Additionally, if your SQLJ
     statements use host variables whose type is a user-defined type
     "x.y.MyClass", then the class using this host variable should not be
     named "x".
     To avoid this problem, we recommend you follow the Java naming
     convention of package names starting with lower case and class names
     starting with upper case.
   * SQLJ translation does not support remote procedure calls [Bug
     1275178], such as
          #sql { call remote_procedure@db_link(...) };
     As workaround, in SQLJ 8.1.7 remote procedure calls are supported
     during offline translation, but result in errors during online
     translation.

TRANSLATOR

   * The SQLJ translator may experience hangs on Solaris under JDK version
     1.3. Use JDK version 1.3.1  instead [Bug 2203431].
   * SQLJ cannot import classes of the form a$b [Bug 1083057]. Instead of
     referencing a$b.c, it will incorrectly use a$b$c. Try to avoid
     referencing classes that have the character "$" in their name.
   * When coding a FETCH from a ResultSetIterator object, you must
     translate with runtime11.jar or runtime12.jar and the Oracle 9i JDBC
     driver. Otherwise you will experience a Java compilation error [Bug
     1590896].
   * When translating dynamic SQLJ statements containing INTO clauses with
     -codegen=iso, you may experience a "StringOutOfBoundException" at
     runtime [Bug 1742301].
   * The SQLJ translator does not support file or directory names
     containing spaces [Bug 864304]. This affects in particular the -d,
     -dir, and -classpath options.
     Note that on the Windows NT platform, Sun's JDK does support file and
     directory names with embedded spaces. As a workaround you could use
     the DOS-style equivalent "short" file or directory name in the -d,
     -dir, or -classpath options.
   * The SQLJ translator might experience hangs on Solaris [Bug N/A]. We
     have experienced occasional hangs when running the SQLJ translator on
     Solaris 2.6. This happened when using the JavaVM in /usr/bin/java
     (java version "1.1.3") distributed with the Solaris Operating System.
     You can work around this problem by using the JavaVM from one of the
     JDK's instead of the VM that came with your operating system.
   * SQLJ translation might occassionally experience a JavaVM core dump
     [Bug 1102856]. The following is the top of the Java stack dump issued:

          java.util.zip.ZipFile.findEND(ZipFile.java)
          java.util.zip.ZipFile.readCEN(ZipFile.java)
          java.util.zip.ZipFile.<init>(ZipFile.java)
          java.util.zip.ZipFile.<init>(ZipFile.java)
          sun.tools.java.ClassPath.<init>(ClassPath.java)
     If you see this problem, you can use one of the following workarounds.
        o Remove $ORACLE_HOME/lib/lclasses11.zip and [JDK
          Home]/lib/classes.zip from your CLASSPATH.
        o Do not use JDK 1.1.6. This problem has not been observed under
          other JDK versions.
        o Call sqlj from the command line. This problems not been observed
          when the SQLJ wrapper script was directly invoked from the
          command line.
   * Some encodings supported by Java are invalid in SQLJ [Bug 1051122].
        o When using JDK1.1.3, the following encodings work with javac but
          not sqlj:
          Cp1125, GBK, ISO2022CN, ISO2022CN_CNS, ISO2022CN_GB, ISO2022KR,
          JIS0208
        o When using JDK1.1.6, the following encodings work with javac but
          not sqlj:
          Cp1125, ISO2022CN, ISO2022CN_CNS, ISO2022CN_GB, JIS0208.
        o When using JDK1.2 the following encodings work with javac but not
          sqlj:
        o ISO2022CN, ISO2022CN_CNS, ISO2022CN_GB.
   * "Unable to convert XXXX.ser to a class file." [Bug 1070362] When you
     use the -ser2class option and have packages, you must either
        o mirror the package hierarchy with your source hierarchy and
          always translate from the root (in this case . must be on the
          CLASSPATH), or
        o translate into a class repository directory (which must be on the
          CLASSPATH) using the -d option.
   * On the Windows NT platform,  under certain circumstances you may see
     one or
     more of the following error messages on Windows platforms:
       Error: java.io.IOException: unable to rename file XXXX.tmp to XXXX
       Error: java.io.IOException: unable to create backup file unable to
     move XXXX to XXXX.orig
     This means that files left over from previous, unfinished SQLJ
     translations or customizations were not removed and block the current
     translation or customization. In this case you need to remove the
     offending file (.tmp, or.orig files) and re-run the SQLJ translator
     [Bug 1890688].

RUNTIME

   * You must not use a SELECT INTO statement to materialize an iterator or
     a ResultSetIterator object. If you do, you may see an ORA-01001 or
     other SQLException. Instead, you should use an ordinary SELECT
     statement and close the associated iterator (or ResultSetIterator)
     once you have retrieved the information from the materialized REF
     CURSOR columns [Bug 1701379].
   * When using JDK 1.1.8 on NT, it has been reported that the SQLJ
     translator generates truncated .java files [Bug 1080299]. In this case
     you will have to use a different JDK version.
   * When writing dynamic SQLJ statements that contain INTO clauses, you
     may experience a "SQLException: invalid column index" at runtime [Bug
     1742301].
   * When using named iterators under JDK 1.3 with the setting -codegen=iso
     you may encounter a "SQLException: Invalid column name" [Bug 1743466].
   * Under JDK 1.2.1 a NullPointerException may be experienced when
     close()ing an iterator instance. This might occur in code of the
     following form:
          NamedOrPositionalIterator iter;
          #sql iter = { SELECT .. };
          ... while loop to process iter ..
          iter.close(); // <== NullPointerException on iter!
     This problem is due to faulty garbage collection under JDK 1.2.1. You
     will have to run your SQLJ program under a different JDK version [Bug
     1130431].
   * Locale dependent literals cannot be used when using the JDBC thin
     driver [Bug 810245].
   * SQLJ supports serialization/deserialization of Java serializable
     objects into and out of RAW columns. When the class to be
     (de-)serialized in this manner is a subclass of one that is defined as
     (de-)serializable, at runtime SQLJ may throw the exception "Invalid
     column type". The problem occurs when using -codegen=iso during
     translation. A workaround is to accept the default -codegen=oracle
     during translation [1891098].


8. USING SQLJ IN Server-side JavaVM: KNOWN RESTRICTIONS AND PROBLEMS

For your convenience we list some additional limitations and differences of
the SQLJ translator that is part of the Oracle 9.0.1 server-side JavaVM.

You may find this useful when you want to compile the same code on both,
client and server. You can also circumvent the limitations below by
compiling your sources on the client and then uploading the compiled files
with loadjava.

IMPORTANT: Since the server-side JavaVM implements JDK 1.3, it is
recommended that you use JDK 1.3, the Oracle 9.2.0 JDBC driver library
classes12.jar, and the SQLJ runtime library runtime12.jar to compile and
test your programs on the client. This ensures the highest degree of
correspondence between your client and server-side environments.

LANGUAGE

   * Class declarations at the block level are not permitted [Bug 1034704].
     Such declarations are permissible in JDK 1.2.

TRANSLATOR

   * Code that uses type maps and is translated on the client with the
     setting -codegen=iso and uploaded to the server with loadjava may
     result in the following error message [Bug 1678741]:
       ORA-29546: Invalid Java type in Typemap at entry
     "class.JavaClassName=SqlTypeName"
     In this case you can try either one of the following: (1) Specify the
     option -ser2class. (2) Use the default option setting -codegen=oracle.
     (3) Upload the source files and type maps rather than compile on the
     client.
   * When a duplicate class declaration occurs, SQLJ may throw a stack
     trace instead of issuing a ClassCircularityError [Bug 1018565].
   * In error messages, the server side SQLJ translator shows the name of
     the first public class, or -if there is no public class- the name of
     the first class in the source as the actual name of the source file
     [Bug 1018533].

RUNTIME

   * During runtime, passing a REF CURSOR variable to a Java stored
     procedure with a parameter of a named iterator type may result in  the
     error "ORA-00932: inconsistent datatypes" [1920084].
