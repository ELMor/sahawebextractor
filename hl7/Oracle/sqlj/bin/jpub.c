/****
 Stub for invoking jpub Command-line tool
 ****/

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <process.h>
#include <string.h>
#include <errno.h>


#define JAVA        "java"              /* invocation of Java interpreter */
#define jpub_MAIN   "oracle.jpub.java.Main"  /* main class for jpub tool */

static int classpathSeen = 0;

char * getJavaFlag(char * str)
{
   if (classpathSeen)
   {
     classpathSeen = 0;
     return str;
   };

   if (strcmp(str,"-J-classpath")==0)
   {
     classpathSeen = 1;
   }

   if (strncmp(str, "-J-", 3)==0)
   {
     return (&str[2]);
   }

   return NULL;
}

char * getJavaVMFlag(char * str)
{
   if (strncmp(str, "-vm=", 4)==0)
   {
     return (&str[4]);
   }

   return NULL;
}

int main (int argc, char ** argv)
{
  char *jpub_prog = JAVA;

  char ** new_argv;

  int i;
  int idx;

  new_argv = calloc(argc + 2, sizeof (char *));
  if (new_argv==NULL)
  {
    printf("Error in jpub stub: not enough memory.");
    return -1;
  }

  /*********************/
  /* JavaVM invocation */
  /*********************/
  idx = 0;
  new_argv[idx] = jpub_prog; idx++;

  /********************/
  /* Flags to JavaVM  */
  /********************/
  for (i = 1; i<argc; i++)
  {
    char * javaArg;

    new_argv[i] = NULL;

    if ((javaArg = getJavaFlag(argv[i])))
    {
      new_argv[idx] = javaArg;
      idx++;
    }
  }
  new_argv[argc] = NULL;
  new_argv[argc+1] = NULL;

  /***************************/
  /* Class invoked by JavaVM */
  /***************************/
  new_argv[idx] = jpub_MAIN ; idx++;

  /*********************/
  /* Arguments to SQLJ */
  /*********************/
  for (i = 1; i<argc; i++)
  { 
    char * javavm;
    char * argi = argv[i];

    if (getJavaFlag(argi))
    {
      // skip it
    }
    else if ((javavm = getJavaVMFlag(argi)))
    {
      new_argv[0] = javavm;
      jpub_prog = javavm;
    }
    else
    {
      new_argv[idx] = argi; idx++;
    }
  }

  /*****************/
  /* Running  SQLJ */
  /*****************/
  i = spawnvp(P_WAIT, jpub_prog, new_argv);

  /********************/
  /* Reporting Errors */
  /********************/
  if (i==-1)
  {
    if (errno==E2BIG)
    {
      printf("Error in jpub stub: argument list too long.");
    }
    else if (errno==EINVAL)
    {
      printf("Error in jpub stub: invalid argument.");
    }
    else if (errno==ENOENT)
    {
     printf("Error in jpub stub: path or file name \"%s\" not found.", jpub_prog);
    }
    else if (errno==ENOEXEC)
    {
     printf("Error in jpub stub: exec format error.");
    }
    else if (errno==ENOMEM)
    {
     printf("Error in jpub stub: not enough memory.");
    }
  }

  return i;
}
