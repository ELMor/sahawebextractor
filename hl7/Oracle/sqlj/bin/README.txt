This directory contains the following wrappers:


For Unix environments:

jpub    - invokes JPublisher. You need to set up your CLASSPATH first. 
jpub92  - invokes JPublisher. Assumes JAVA_HOME and ORACLE_HOME is set.
sqlj    - invokes SQLJ. You need to set CLASSPATH first with translator.jar,
          one of the SQLJ runtimes, and JDBC, and you need to have javac
          in your PATH.


For Unix environments:

jpub.exe - invokes JPublisher. You need to set up your CLASSPATH first.
sqlj.exe - invokes SQLJ. You need to set CLASSPATH first with translator.jar,
           one of the SQLJ runtimes, and JDBC, and you need to have javac
           in your PATH.
jpub.c, sqlj.c - sources for above.
