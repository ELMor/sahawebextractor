/****
 Stub for invoking sqlj Command-line tool
 ****/

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <process.h>
#include <string.h>
#include <errno.h>
#include <io.h>

#ifdef __BORLANDCC
#pragma hdrstop
#endif

#define TRUE  1
#define FALSE 0


#ifdef __BORLANDCC
#define SYSTEM_CALL_SKIPS_ARG0 TRUE
#else
#define SYSTEM_CALL_SKIPS_ARG0 FALSE
#endif


#define DEBUG FALSE


#define JAVA        "java"              /* invocation of Java interpreter */
#define sqlj_MAIN   "sqlj.tools.Sqlj"   /* main class for sqlj tool */
#define ENV_VAR     "SQLJ_OPTIONS="

#define COMPILE_JAVA_SOURCE 90
#define COMPILE_SER_FILES  91

#define COMPILER_WAS_NOT_RUN (-876)


/* #define tempname    "tmpfile"          /* name for temporary file */


static int isSwitch;    /* is this argument a JavaVM switch? */
static int switchHasArg; /* this switch has an additional argument */
static int skipArg;     /* is this argument to be skipped? */
static int toSplit;     /* is this argument to be split at the = character? */
static int isClasspathSwitch; /* The switch -classpath should be sent to both, the Java
							     VM and the Java Compiler */

static int toConcat;    /* is this argument to be concatenated with the next argument
                           using a '='character ? */
static char * newTag;   /* is the tag component of this argument to be replaced with
                           a new tag? */
static char * afterTag; /* the remainder of the argument after the tag, whenever
                           the tag has to be replaced. */


static char *javaVM = NULL;    /* name of the Java VM */
static int  hasPasses = FALSE;
static int  echoArgs  = FALSE; /* echo the arguments instead of executing them */

static char *helpTag     = "-help";
static char *statusTag   = "-status";
static char *encodingTag = "-encoding";
static char *userTag     = "-user";
static char *passwordTag = "-password";

static char *sqljLinemapOn    = "-linemap=true";
static char *sqljLinemapOff   = "-linemap=false";
static char *sqljChecksource  = "-checksource";
static char *sqljWarnOff      = "-warn=none";
static char *sqljDebugOn      = "-P-debug";

static char *javac_gFlag      = "-C-g";
static char *javac_OFlag      = "-C-O";
static char *javac_nowarnFlag = "-C-nowarn";
static char *javac_verboseFlag= "-C-verbose";
static char *javac_dependFlag = "-C-depend";
static char *javac_debugFlag  = "-C-debug";



int startsWith(char *str, char *prefix)
{
  return strncmp(str,prefix,strlen(prefix)) == 0;
}

int indexOf(char *str, char ch)
{
  int i = 0;
  while (str[i]!=ch && str[i]!='\0') i++;
  return (str[i]==ch) ? i : -1;
}

void analyzeArg(char * arg)
{

   /* fprintf(stderr,"Analyzing '%s' ...\n",arg); */

   isSwitch = FALSE;
   switchHasArg = FALSE;
   skipArg = FALSE;
   isClasspathSwitch = FALSE;

   toSplit = FALSE;
   toConcat = FALSE;
   newTag = NULL;
   afterTag = NULL;

   if (strcmp(arg,"-vm=echo") == 0
       || strcmp(arg,"-n") == 0)
   {
     echoArgs = TRUE;
     skipArg  = TRUE;
   }
   else if (startsWith(arg,"-vm="))
   {
     /* fprintf(stderr,"Found JavaVM: '%s'\n",arg); */
     javaVM = &(arg[ strlen("-vm=") ]);
     skipArg = TRUE;

     /* fprintf(stderr,"Found JavaVM: '%s'\n",javaVM); */
   }
   else if (strcmp(arg,"-passes") == 0)
   {
     hasPasses = TRUE;
     skipArg   = TRUE;
   }
 
   else if (strcmp(arg,"-C-classpath")   == 0
            || strcmp(arg,"-C-bootclasspath") == 0
            || strcmp(arg,"-C-extdirs") == 0
            || strcmp(arg,"-C-target") == 0
            || strcmp(arg,"-C-encoding") == 0
            || strcmp(arg,"-C-d")        == 0)
   {
      toConcat = TRUE;
   }
   else if (startsWith(arg,"-classpath="))
   {
      isSwitch = TRUE;
      toSplit  = TRUE;
      newTag   = arg;
      arg[10]  = '\0';
      afterTag = &(arg[11]);
      isClasspathSwitch = TRUE;
   }
   else if (strcmp(arg,"-classpath") == 0)
   {
     isSwitch     = TRUE;
     switchHasArg = TRUE;
     newTag = arg;
	 isClasspathSwitch = TRUE;
   }
   else if (strcmp(arg,"-J-classpath") == 0)
   {
      isSwitch     = TRUE;
      switchHasArg = TRUE;
      newTag = &(arg[2]);
   }
   else if (startsWith(arg,"-J-classpath="))
   {
      isSwitch = TRUE;
      toSplit  = TRUE;
      newTag   = &(arg[2]);
      arg[12]  = '\0';
      afterTag = &(arg[13]);
   }
   else if (startsWith(arg,"-J-"))
   {
      isSwitch = TRUE;
      newTag   = &(arg[2]);
   }

   else if (strcmp(arg,"-h") == 0)
   {
     newTag = helpTag;
   }
   else if (strcmp(arg,"-v") == 0)
   {
     newTag = statusTag;
   }
   else if (strcmp(arg,"-u") == 0)
   {
     newTag = userTag;
     toConcat = TRUE;
   }
   else if (strcmp(arg,"-p") == 0)
   {
     newTag = passwordTag;
     toConcat = TRUE;
   }
   else if (strcmp(arg,"-e") == 0)
   {
     newTag = encodingTag;
     toConcat = TRUE;
   }
   else if (startsWith(arg,"-u="))
   {
     newTag = userTag;
     afterTag = &(arg[3]);
   }
   else if (startsWith(arg,"-p="))
   {
     newTag = passwordTag;
     afterTag = &(arg[3]);
   }
   else if (strcmp(arg,"-d") == 0
            || strcmp(arg,"-encoding") == 0
            || strcmp(arg,"-user") == 0
            || strcmp(arg,"-url") == 0
            || strcmp(arg,"-password") == 0
            || strcmp(arg,"-C-classpath") == 0
            || strcmp(arg,"-C-bootclasspath") == 0
            || strcmp(arg,"-C-extdirs") == 0
            || strcmp(arg,"-C-target") == 0
            || strcmp(arg,"-dir") == 0
            || strcmp(arg,"-driver") == 0
            || strcmp(arg,"-encoding") == 0)
   {
     toConcat = TRUE;
   }
   else if (startsWith(arg,"-u@")
          || startsWith(arg,"-p@"))
   {
     int pos = indexOf(arg,'=');
     newTag = (arg[1]=='u') ? userTag : passwordTag;
     afterTag = &(arg[2]);
     if (pos > 0)
     {
       // saw = character
     }
     else
     {
        toConcat = TRUE;
     }
   }
   else if (startsWith(arg,"-user@")
            || startsWith(arg,"-url@")
            || startsWith(arg,"-password@"))
   {
     if (indexOf(arg,'=') <= 0)
     {
       toConcat = TRUE;
     }
   }
   else if (strcmp(arg,"-g") == 0)
   {
    toSplit = TRUE;
    newTag = javac_gFlag;
    afterTag = sqljLinemapOn;
   }
   else if (strcmp(arg,"-O") == 0)
   {
    toSplit = TRUE;
    newTag = javac_OFlag;
    afterTag = sqljLinemapOff;
   }
   else if (strcmp(arg,"-nowarn") == 0)
   {
    toSplit = TRUE;
    newTag = javac_nowarnFlag;
    afterTag = sqljWarnOff;
   }
   else if (strcmp(arg,"-nowarn") == 0)
   {
    toSplit = TRUE;
    newTag = javac_nowarnFlag;
    afterTag = sqljWarnOff;
   }
   else if (strcmp(arg,"-verbose") == 0)
   {
    toSplit = TRUE;
    newTag = javac_verboseFlag;
    afterTag = statusTag;
   }
   else if (strcmp(arg,"-debug") == 0)
   {
    toSplit = TRUE;
    newTag = javac_debugFlag;
    afterTag = sqljDebugOn;
   }
   else if (strcmp(arg,"-depend") == 0)
   {
    toSplit = TRUE;
    newTag = javac_dependFlag;
    afterTag = sqljChecksource;
   }
}


/******************************************/
/*  Report stub errors                    */
/*  Return true if an error was reported  */
/******************************************/

int reportStubError(int i, char * sqlj_prog)
{
  if (i==-1)
  {
    if (errno==E2BIG)
    {
      printf("Error in sqlj stub: argument list too long.");
    }
    else if (errno==EINVAL)
    {
      printf("Error in sqlj stub: invalid argument.");
    }
    else if (errno==ENOENT)
    {
     printf("Error in sqlj stub: path or file name \"%s\" not found.", sqlj_prog);
    }
    else if (errno==ENOEXEC)
    {
     printf("Error in sqlj stub: exec format error.");
    }
    else if (errno==ENOMEM)
    {
     printf("Error in sqlj stub: not enough memory.");
    }
  else  /* some executables want to fool us and return -1    */
      /* when _they_ have a problem.  We detect them here! */
  {
    return 0;
  }
  return i;
  }

  return 0;
}

/***********************************/
/***  EXECUTE THE JAVA COMPILER  ***/
/***********************************/


char *sqlj_prog = JAVA;
char tempname[255];

int executeJavaCompiler()
{
   FILE * stream;
   long pos;
   char * rawArgs;
   char runfile[255];
   int i;

   /***  try opening the file with the runline information  ***/
   strcpy(runfile, tempname);
   strcat(runfile, ".run");

   if (echoArgs)
   {
     printf("[execute java compiler runline from %s]\n", runfile);
     return 0;
   }

   stream = fopen(runfile, "rb");

   /***  read runline ***/

   if (stream != NULL)
   {
    /* figure out the size of this file */

    fseek(stream, 0, SEEK_END);
    pos = ftell(stream);
    fseek(stream, 0, SEEK_SET);


    rawArgs = (char *)malloc(pos + 1 + strlen(runfile) + strlen("> .log "));

    if (rawArgs==NULL)
    {
       printf("Error in sqlj stub: not enough memory.");
       exit(-1);
    };
    fread(rawArgs, pos, 1, stream);
    fclose(stream);

    /* terminate argument string */
    rawArgs[pos] = '\0';

    pos--;  /* remove trailing CR and LF characters */
    while (0<= pos && rawArgs[pos]=='\r' || rawArgs[pos]=='\n')
    {
      rawArgs[pos]='\0';
      pos--;
    }

    /* put together argument string */
    strcat(rawArgs, " >");
    strcat(rawArgs, tempname);
    strcat(rawArgs, ".log");

    fflush(stdout);

    /* execute the Java compiler */
	/* fprintf(stderr,"[Executing compiler: %s]\n", rawArgs); */

    i = system(rawArgs);
        if (i && reportStubError(i, sqlj_prog)) exit(i);

    return i;
   }
   else
   {
     return COMPILER_WAS_NOT_RUN;
   }
}

/**************************/
/*  Program Main Routine  */
/**************************/


int main (int argc, char *argv[], char *envp[])
{

  char ** new_argv;
  int new_argc;

  int i, j, l;
  int idx, idxtop;
  
  int javaRes = 0;
  int sqljRes = 0;


  int envargscount = 0;
  char ** envargs;
  char *envvalue = NULL;

  char **otherArgs;
  char passArgument[255];

  hasPasses = 0;

  /*************************************/
  /*****  GET ENVIRONMENT SETTING  *****/
  /*************************************/

  /* printf("Started SQLJ wrapper."); */

  /***  Check for environment variable  ***/

  while( *envp != NULL )
  {
   if (strncmp(*envp, ENV_VAR, strlen(ENV_VAR))==0)
   {
    envvalue = &(*envp)[ strlen(ENV_VAR)];
    break;
   }
   envp++;
  }


  /*** Retrieve options from environment variable. ***/

  if (envvalue!=NULL)
  {
   l = strlen(envvalue);

   /* Guess the number of arguments */
   envargscount=1;      /* at least one argument + number of whitespace chars */
   for (i=0; i<l; i++)
   {
     if (envvalue[i]=='#') envvalue[i]='=';
     else if (envvalue[i]==' ' || envvalue[i]=='\t')
     {
      envargscount++;
     }
   }

   // fprintf(stderr,"Found %d envargs\n",envargscount);

     /* Allocate array for arguments */
     envargs = (char **)calloc(envargscount, sizeof (char *));
     if (envargs==NULL)
     {
       printf("Error in sqlj stub: not enough memory.");
       exit(1);
     }

     /* Populate argument array */
     envargscount=0;
     i = 0;
     while (envvalue != NULL)
     {
       int j;
       int done;

       if (envvalue[i]=='#') envvalue[i]='=';

       while (envvalue[i]==' ' || envvalue[i]=='\t') i++;

       j=i;
       while (envvalue[i]!=' ' && envvalue[i]!='\t' && envvalue[i]!='\0') i++;
       done = (envvalue[i]=='\0');

       if (i!=j)
       {
         envvalue[i]='\0';
         envargs[envargscount] = &(envvalue[j]);
         envargscount++;
       }
       if (done)
       {
         break;
       }
       else
       {
         i++;
       }
     }
   }


  /**************************************/
  /*****  CONSTRUCT ARGUMENT ARRAY  *****/
  /**************************************/

  new_argc = 3 + envargscount + argc + 10;
  new_argv = (char **)calloc(new_argc, sizeof (char *));
  if (new_argv==NULL)
  {
    printf("Error in sqlj stub: not enough memory.");
    return -1;
  }

  idxtop = new_argc - 1;

  for (i=0; i < new_argc; i++) new_argv[i] = NULL;


  /***  Add JavaVM invocation  ***/

  idx = 0;
  new_argv[idx] = sqlj_prog;
  idx++;


  /*** Add Arguments ***/
#define getArgument(pos) ((i<=envargscount) ? envargs[i-1] : argv[i - envargscount])

  for (i = 1; i < envargscount + argc; i++)
  {

    char * argi = getArgument(i);

    analyzeArg(argi);

    if (skipArg)
    {
      /* nothing to do */
    }
    else if (isSwitch)
    {
      if (newTag)
      {
         if (afterTag)
         {
           if (toSplit)
           {
             new_argv[idx] = newTag;
             idx++;
             new_argv[idx] = afterTag;
             idx++;
           }
           else
           {
             char *anArg = (char *)malloc(strlen(newTag) + strlen(afterTag) + 1 + 1);
             if (anArg==NULL)
             {
               printf("Error in sqlj stub: not enough memory.");
               exit(1);
             }
             strcpy(anArg,newTag);
             strcat(anArg,"=");
             strcat(anArg,afterTag);
             new_argv[idx] = anArg;
             idx++;
           }
         }
         else
         {
           new_argv[idx] = newTag;
           idx++;
         }
      }

      if (switchHasArg)
      {
         i++;
         if (i < envargscount + argc)
         {
           argi = getArgument(i);
           new_argv[idx] = argi;
           idx++;
         }
      }

	  if (isClasspathSwitch)
	  {
         char * javacClasspath = (char *)malloc(strlen("-C-classpath=")+1+
			                                    strlen(new_argv[idx-1]));
		 strcpy(javacClasspath, "-C-classpath=");
		 strcat(javacClasspath, new_argv[idx-1]);
		 new_argv[idxtop] = javacClasspath;
		 idxtop--;
	  }
    }
    else /* this is not a switch to JavaVM */
    {
      if (toSplit && newTag && afterTag)
      {
        new_argv[idxtop] = newTag;
        idxtop--;
        new_argv[idxtop] = afterTag;
        idxtop--;
      }
      else if (newTag || toConcat)
      {
         char * nextArg = NULL;
         char * anArg;

         if (!newTag) newTag = argi;

         if (toConcat)
         {
           i++;
           nextArg=getArgument(i);
         }

         if (afterTag || nextArg)
         {
            anArg = (char *)malloc(strlen(newTag) +
                            ((afterTag) ? strlen(afterTag) : 0) +
                             1 +                                         /* '=' */
                             ((nextArg)  ? strlen(nextArg)  : 0) +
                             1);                                         /* '\0' */
            if (anArg==NULL)
            {
              printf("Error in sqlj stub: not enough memory.");
              exit(1);
            }
            strcpy(anArg,newTag);
            if (afterTag)
            {
              strcat(anArg,afterTag);
            }
            if (nextArg)
            {
              strcat(anArg,"=");
              strcat(anArg,nextArg);
            }
         }
         else
         {
            anArg = newTag;
         }
         new_argv[idxtop] = anArg;
         idxtop--;

      }
      else
      {
        new_argv[idxtop] = argi;
        idxtop--;
      }
    }
  }

  /*** Add JavaVM executable ***/

  if (javaVM)
  {
    sqlj_prog = javaVM;
    new_argv[0] = javaVM;
  }

  /***  Add Class invoked by JavaVM  ***/

  new_argv[idx] = sqlj_MAIN;
  idx++;


  /*** Copy the arguments to SQLJ back to the beginning ***/
  if (hasPasses)
  {
     /* fprintf(stderr,"hasPasses=true!\n"); */

     int l;

     if (echoArgs)
     {
       strcpy(tempname,"<tempfile>");
	 }
	 else
	 {
	   tmpnam(tempname);
	 }

	 l = strlen(tempname) - 1;
	 while (l>0 && (tempname[l]=='.' || tempname[l]=='$')) { tempname[l]='\0'; l--; }


     strcpy(passArgument,"-pass1=");
     strcat(passArgument, tempname);
     new_argv[idx] = passArgument;

     hasPasses = idx;
     idx++;
  }


  /***  Add remaining SQLJ arguments to runline  ***/

  /* printf("new_argc: %d, idxtop: %d\n",new_argc, idxtop); */

  otherArgs = (char **)calloc(new_argc - idxtop, sizeof(char *));
  if (otherArgs==NULL)
  {
     printf("Error in sqlj stub: not enough memory.");
     exit(1);
  }

  j = 0;
  for (i = new_argc - 1; i > idxtop; i--)
  {
    otherArgs[j] = new_argv[i]; j++;
    new_argv[i] = NULL;
  }

  for (i=0; i<j; i++)
  {
    new_argv[idx] = otherArgs[i];
    idx++;
  }

  /*********************************/
  /*****  EXECUTE THE RUNLINE  *****/
  /*********************************/

  if (echoArgs)
  {
    int j=1;
    printf("%s",sqlj_prog);
    while (new_argv[j])
    {
      printf(" %s",new_argv[j]);
      j++;
    }
    printf("\n");
    sqljRes = (hasPasses) ? COMPILE_JAVA_SOURCE : 0;
  }
  else
  {
	if (DEBUG)
	{
	  fflush(stdout);
	  printf("[Executing sqlj pass 1]\n");
	  fflush(stdout);
	}


    sqljRes = spawnvp(P_WAIT, sqlj_prog, new_argv);
	if (DEBUG)
	{
	   fflush(stdout);
	   printf("[SQLJ returns %d]\n",sqljRes);
	   fflush(stdout);
	}
  }

  if (sqljRes && reportStubError(sqljRes, sqlj_prog)) exit(sqljRes);


  /******************************************************/
  /*****  EXECUTE THE JAVA COMPILER - IF REQUESTED  *****/
  /******************************************************/

  if (hasPasses && sqljRes==COMPILE_JAVA_SOURCE)
  {

   if (DEBUG)
   {
	  fflush(stdout);
	  printf("[Executing java compiler pass 1]\n");
	  fflush(stdout);
   }
   javaRes = executeJavaCompiler();

   if (javaRes==COMPILER_WAS_NOT_RUN)
   {
	 if (DEBUG)
	 {
	   fflush(stdout);
	   printf("[Returning %d]\n",sqljRes);
	   fflush(stdout);
	 }
	 return sqljRes;
   }
   else if (DEBUG)
   {
      fflush(stdout);
	  printf("[Java compilation returns %d]\n",javaRes);
	  fflush(stdout);
   } 
  }
  else
  {
     if (DEBUG)
	 {
	   fflush(stdout);
	   printf("[Returning %d]\n",sqljRes);
	   fflush(stdout);
	 }
	 return sqljRes;
  }


  /**********************************************************/
  /*****  EXECUTE THE SECOND SQLJ PHASE - IF REQUESTED  *****/
  /**********************************************************/

  if (hasPasses)
  {
	  char * pflag = (javaRes) ? ((echoArgs) ? "-pass2" : "-fail2")
		                       : "-pass2";
   
   int k;

   for (k=0; k<6; k++)
   {
     new_argv[hasPasses][k] = pflag[k];
   }

   if (echoArgs)
   {
     int kk=1;
     printf("%s",sqlj_prog);
     while (new_argv[kk])
     {
       printf(" %s",new_argv[kk]);
       kk++;
     }
     printf("\n");
     i = (hasPasses) ? COMPILE_SER_FILES : 0;
   }
   else
   {
     if (DEBUG)
	 {
	   fflush(stdout);
	   printf("[Executing (javaRes %d) sqlj %s 2]\n", javaRes, pflag);
	   fflush(stdout);
	 }
     sqljRes = spawnvp(P_WAIT, sqlj_prog, new_argv);
	 if (DEBUG)
	 {
	    fflush(stdout);
	    printf("[SQLJ returns %d]\n",sqljRes);
	    fflush(stdout);
	 }
   }

   if (sqljRes && reportStubError(sqljRes, sqlj_prog)) exit(sqljRes);
  }

  /******************************************************/
  /*****  EXECUTE THE JAVA COMPILER - IF REQUESTED  *****/
  /******************************************************/

  if (hasPasses && sqljRes==COMPILE_SER_FILES)
  {
   if (DEBUG)
   {
	 fflush(stdout);
	 printf("[Executing java compiler pass 2]\n");
	 fflush(stdout);
   }
   javaRes = executeJavaCompiler();

   if (javaRes==COMPILER_WAS_NOT_RUN)
   {
	 if (DEBUG)
	 {
	   fflush(stdout);
	   printf("[Returning %d]\n",sqljRes);
	   fflush(stdout);
	 }
	 return sqljRes;
   }
  }
  else
  {
	if (DEBUG)
	{
	 fflush(stdout);
	 printf("[Returning %d]\n",sqljRes);
	 fflush(stdout);
	}
    return sqljRes;
  }


  /*********************************************************/
  /*****  EXECUTE THE THIRD SQLJ PHASE - IF REQUESTED  *****/
  /*********************************************************/

  if (hasPasses)
  {
   char * pflag = "-pass3";
   int j;

   for (j=0; j<6; j++)
   {
     new_argv[hasPasses][j] = pflag[j];
   }

   if (echoArgs)
   {
     int j=1;
     printf("%s",sqlj_prog);
     while (new_argv[j])
     {
       printf(" %s",new_argv[j]);
       j++;
     }
     printf("\n");
     i = 0;
   }
   else
   {
     if (DEBUG)
	 {
	  	fflush(stdout);
		printf("[Executing sqlj pass 3]\n");
	    fflush(stdout);
	 }
     sqljRes = spawnvp(P_WAIT, sqlj_prog, new_argv);
	 if (DEBUG)
	 {
	   fflush(stdout);
	   printf("[SQLJ returns %d]\n",sqljRes);
	   fflush(stdout);
	 }
   }
   if (sqljRes && reportStubError(sqljRes, sqlj_prog)) exit(sqljRes);
  }

  return sqljRes;
}
