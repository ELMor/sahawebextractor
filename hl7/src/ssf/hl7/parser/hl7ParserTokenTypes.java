// $ANTLR 2.7.1: "ssf/hl7/parser/hl7.g" -> "hl7Parser.java"$

	package ssf.hl7.parser;
	import  ssf.hl7.msg.*;

public interface hl7ParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int MSH = 4;
	int ADTA28 = 5;
	int ENDSEG = 6;
	int ADTA31 = 7;
	int ADTA34 = 8;
	int ORRO02 = 9;
	int ORUR01 = 10;
	int DFTP03 = 11;
	int EVN = 12;
	int PID = 13;
	int PV1 = 14;
	int IN1 = 15;
	int MRG = 16;
	int ORC = 17;
	int OBR = 18;
	int OBX = 19;
	int FT1 = 20;
	int CONTENT = 21;
	int BEGINFIELD = 22;
	int BEGINREP = 23;
	int BEGINCOMP = 24;
	int BEGINSUBCOMP = 25;
}
