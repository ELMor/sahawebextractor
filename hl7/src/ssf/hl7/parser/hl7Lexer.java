// $ANTLR 2.7.1: "ssf/hl7/parser/hl7.g" -> "hl7Lexer.java"$

	package ssf.hl7.parser;
	import  ssf.hl7.msg.*;

import java.io.InputStream;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.TokenStreamRecognitionException;
import antlr.CharStreamException;
import antlr.CharStreamIOException;
import antlr.ANTLRException;
import java.io.Reader;
import java.util.Hashtable;
import antlr.CharScanner;
import antlr.InputBuffer;
import antlr.ByteBuffer;
import antlr.CharBuffer;
import antlr.Token;
import antlr.CommonToken;
import antlr.RecognitionException;
import antlr.NoViableAltForCharException;
import antlr.MismatchedCharException;
import antlr.TokenStream;
import antlr.ANTLRHashString;
import antlr.LexerSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.SemanticException;

public class hl7Lexer extends antlr.CharScanner implements hl7ParserTokenTypes, TokenStream
 {
public hl7Lexer(InputStream in) {
	this(new ByteBuffer(in));
}
public hl7Lexer(Reader in) {
	this(new CharBuffer(in));
}
public hl7Lexer(InputBuffer ib) {
	this(new LexerSharedInputState(ib));
}
public hl7Lexer(LexerSharedInputState state) {
	super(state);
	literals = new Hashtable();
caseSensitiveLiterals = true;
setCaseSensitive(true);
}

public Token nextToken() throws TokenStreamException {
	Token theRetToken=null;
tryAgain:
	for (;;) {
		Token _token = null;
		int _ttype = Token.INVALID_TYPE;
		resetText();
		try {   // for char stream error handling
			try {   // for lexical error handling
				switch ( LA(1)) {
				case '^':
				{
					mBEGINCOMP(true);
					theRetToken=_returnToken;
					break;
				}
				case '~':
				{
					mBEGINREP(true);
					theRetToken=_returnToken;
					break;
				}
				case '&':
				{
					mBEGINSUBCOMP(true);
					theRetToken=_returnToken;
					break;
				}
				case '\n':  case '\r':
				{
					mENDSEG(true);
					theRetToken=_returnToken;
					break;
				}
				default:
					if ((LA(1)=='|') && (LA(2)=='A') && (LA(3)=='D') && (LA(4)=='T') && (LA(5)=='^') && (LA(6)=='A') && (LA(7)=='3') && (LA(8)=='1')) {
						mADTA31(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (LA(2)=='A') && (LA(3)=='D') && (LA(4)=='T') && (LA(5)=='^') && (LA(6)=='A') && (LA(7)=='3') && (LA(8)=='4')) {
						mADTA34(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (LA(2)=='A') && (LA(3)=='D') && (LA(4)=='T') && (LA(5)=='^') && (LA(6)=='A') && (LA(7)=='2')) {
						mADTA28(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='M') && (LA(2)=='S') && (LA(3)=='H') && (LA(4)=='|')) {
						mMSH(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (LA(2)=='O') && (LA(3)=='R') && (LA(4)=='R')) {
						mORRO02(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (LA(2)=='O') && (LA(3)=='R') && (LA(4)=='U')) {
						mORUR01(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='E') && (LA(2)=='V') && (LA(3)=='N') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mEVN(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='P') && (LA(2)=='I') && (LA(3)=='D') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mPID(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='P') && (LA(2)=='V') && (LA(3)=='1') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mPV1(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='I') && (LA(2)=='N') && (LA(3)=='1') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mIN1(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='M') && (LA(2)=='R') && (LA(3)=='G') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mMRG(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='O') && (LA(2)=='R') && (LA(3)=='C') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mORC(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='O') && (LA(2)=='B') && (LA(3)=='R') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mOBR(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='O') && (LA(2)=='B') && (LA(3)=='X') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mOBX(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='F') && (LA(2)=='T') && (LA(3)=='1') && (true) && (true) && (true) && (true) && (true) && (true)) {
						mFT1(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (LA(2)=='D')) {
						mDFTP03(true);
						theRetToken=_returnToken;
					}
					else if ((LA(1)=='|') && (true)) {
						mBEGINFIELD(true);
						theRetToken=_returnToken;
					}
					else if ((_tokenSet_0.member(LA(1))) && (true) && (true) && (true) && (true) && (true) && (true) && (true) && (true)) {
						mCONTENT(true);
						theRetToken=_returnToken;
					}
				else {
					if (LA(1)==EOF_CHAR) {uponEOF(); _returnToken = makeToken(Token.EOF_TYPE);}
				else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine());}
				}
				}
				if ( _returnToken==null ) continue tryAgain; // found SKIP token
				_ttype = _returnToken.getType();
				_ttype = testLiteralsTable(_ttype);
				_returnToken.setType(_ttype);
				return _returnToken;
			}
			catch (RecognitionException e) {
				throw new TokenStreamRecognitionException(e);
			}
		}
		catch (CharStreamException cse) {
			if ( cse instanceof CharStreamIOException ) {
				throw new TokenStreamIOException(((CharStreamIOException)cse).io);
			}
			else {
				throw new TokenStreamException(cse.getMessage());
			}
		}
	}
}

	public final void mMSH(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MSH;
		int _saveIndex;
		
		match("MSH|^~\\&");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mEVN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = EVN;
		int _saveIndex;
		
		match("EVN");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mPID(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PID;
		int _saveIndex;
		
		match("PID");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mPV1(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = PV1;
		int _saveIndex;
		
		match("PV1");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mIN1(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = IN1;
		int _saveIndex;
		
		match("IN1");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mMRG(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = MRG;
		int _saveIndex;
		
		match("MRG");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mORC(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ORC;
		int _saveIndex;
		
		match("ORC");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mOBR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OBR;
		int _saveIndex;
		
		match("OBR");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mOBX(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = OBX;
		int _saveIndex;
		
		match("OBX");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mFT1(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = FT1;
		int _saveIndex;
		
		match("FT1");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mADTA28(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ADTA28;
		int _saveIndex;
		
		match("|ADT^A28");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mADTA31(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ADTA31;
		int _saveIndex;
		
		match("|ADT^A31");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mADTA34(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ADTA34;
		int _saveIndex;
		
		match("|ADT^A34");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mORRO02(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ORRO02;
		int _saveIndex;
		
		match("|ORR^O02");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mORUR01(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ORUR01;
		int _saveIndex;
		
		match("|ORU^R01");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mDFTP03(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = DFTP03;
		int _saveIndex;
		
		match("|DFT^P03");
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBEGINCOMP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BEGINCOMP;
		int _saveIndex;
		
		match('^');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBEGINREP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BEGINREP;
		int _saveIndex;
		
		match('~');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBEGINSUBCOMP(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BEGINSUBCOMP;
		int _saveIndex;
		
		match('&');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mBEGINFIELD(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = BEGINFIELD;
		int _saveIndex;
		
		match('|');
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mENDSEG(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = ENDSEG;
		int _saveIndex;
		
		{
		if ((LA(1)=='\r') && (LA(2)=='\n')) {
			match("\r\n");
		}
		else if ((LA(1)=='\n')) {
			match('\n');
		}
		else if ((LA(1)=='\r') && (true)) {
			match('\r');
		}
		else {
			throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine());
		}
		
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	public final void mCONTENT(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
		int _ttype; Token _token=null; int _begin=text.length();
		_ttype = CONTENT;
		int _saveIndex;
		
		{
		int _cnt129=0;
		_loop129:
		do {
			if ((_tokenSet_0.member(LA(1)))) {
				{
				match(_tokenSet_0);
				}
			}
			else {
				if ( _cnt129>=1 ) { break _loop129; } else {throw new NoViableAltForCharException((char)LA(1), getFilename(), getLine());}
			}
			
			_cnt129++;
		} while (true);
		}
		if ( _createToken && _token==null && _ttype!=Token.SKIP ) {
			_token = makeToken(_ttype);
			_token.setText(new String(text.getBuffer(), _begin, text.length()-_begin));
		}
		_returnToken = _token;
	}
	
	
	private static final long _tokenSet_0_data_[] = { -274877916168L, -5764607524107976705L, -1L, -1L, 0L, 0L, 0L, 0L };
	public static final BitSet _tokenSet_0 = new BitSet(_tokenSet_0_data_);
	
	}
