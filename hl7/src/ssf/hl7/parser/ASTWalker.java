package ssf.hl7.parser;

import antlr.*;

/** ASTWalker is a utility class that traverses the AST tree.  It is
  * useful for displaying the structure of an AST (BaseAST) as a text tree.
  *
  * <p>Copyright (c) 1999 Richard S. Schilling.  All rights reserved
  *
  * <p>Distributed under the GNU public license, version 2.
  *
  * @author Richard S. Schilling
  * @since JDK 1.2
  * @version 0.1
  *
  */

public class ASTWalker {

        /** The tree that is being walked.  Set by the class constructor.
         */
        private BaseAST TargetTree;

        /** @param ast the BaseAST object that is to be "walked".
         */
        public ASTWalker (BaseAST ast){
                TargetTree = ast;
        }

        /** Outputs to standard output a verbose text tree with each level
          * indented.  Each level is indented by the string specified in
          * PadString.
          * <p>For example to get an output similar to the following
          *
          * <p><pre>
          * NODE 1
          *     NODE 1.1
          *          NODE 1.1.1
          *          NODE 1.1.2
          *     NODE 1.2
          * NODE 2
          * </pre>
          *
          * @param Level the indentation level of the tree.
          *
          */

        public void asTextOutline(int Level){

                // Output the current level
                System.out.print(Level + ":");

                // Pad spaces for the current level.
                for (int i = 0; i < Level; i++){
                        System.out.print(" ");
                 }


                //output text for the current node.
                System.out.println("(" + TargetTree.getText() +
                                   ")" +
                                   TargetTree.toString());

                //process the current node's children.
                BaseAST ChildAST = (BaseAST)TargetTree.getFirstChild();
                if (ChildAST != null) {

                       ASTWalker ChildWalker = new ASTWalker(ChildAST);
                       ChildWalker.asTextOutline(Level+1);
                }


                // now output trees for all siblings
                if (TargetTree.getNextSibling() != null)
                    {
                        ASTWalker SiblingWalker = new ASTWalker((BaseAST)TargetTree.getNextSibling());
                        SiblingWalker.asTextOutline(Level);
               }



        }

        /** Outputs to standard output an abridged text tree with each level
          * indented.  Each level is indented by the string specified in
          * PadString.
          * <p>For example to get an output similar to the following
          *
          * <p><pre>
          *  NODE 1
          *     NODE 1.1
          *          NODE 1.1.1
          *          NODE 1.1.2
          *     NODE 1.2
          * NODE 2
          * </pre>
          *
          * @param Level the indentation level of the tree.
          *
          */
        public void asAbridgedTextOutline(int Level){

                // Output the current level
                System.out.print(Level + ":");

                // Pad spaces for the current level.
                for (int i = 0; i < Level; i++){
                        System.out.print(" ");
                }


                //output text for the current node.
                System.out.println(TargetTree.getText());

                //process the current node's children.
                BaseAST ChildAST = (BaseAST)TargetTree.getFirstChild();
                if (ChildAST != null) {

                       ASTWalker ChildWalker = new ASTWalker(ChildAST);
                       ChildWalker.asAbridgedTextOutline(Level+1);
                }


                // now output trees for all siblings
                if (TargetTree.getNextSibling() != null)
                    {
                        ASTWalker SiblingWalker = new ASTWalker((BaseAST)TargetTree.getNextSibling());
                        SiblingWalker.asAbridgedTextOutline(Level);
                }



        }




}

