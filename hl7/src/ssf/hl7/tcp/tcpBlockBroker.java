package ssf.hl7.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import ssf.hl7.Debug;
import ssf.hl7.msg.Field;
import ssf.hl7.msg.Msg;
import ssf.hl7.msg.Segment;

//Encargada de envio, recepcion y ACK de bloques Tcp/Ip
//Se usa tanto en servidor como en cliente
public class tcpBlockBroker
    implements tcpable {
  private OutputStream output;
  private InputStream input;
  int so, sd;
  private String sbb, seb;

  public tcpBlockBroker(int or, int de,
                        OutputStream ofs, InputStream ifs,
                        String bb, String eb) throws Exception {
    so = or; //Sistema Origen
    sd = de; //Sistema Destino
    output = ofs;
    input = ifs;
    sbb = bb;
    seb = eb;
    if (output == null || input == null) {
      throw new Exception("tcpBlockBroker: input y/o output no inicializados");
    }
  }

  //Enviar mensaje (el que est� en block)
  public String sendBlock(Debug dd, String block, boolean withAnswer) throws
      BlockBrokerException,
      socketClosedException,
      IOException,
      InterruptedException {
    dd.ifdeb("Enviando BeginBlock:" + dd.AsciiString(sbb));
    output.write(sbb.getBytes());
    dd.ifdeb("Enviando      Block:<" + dd.AsciiString(block) + ">");
    output.write(block.getBytes());
    dd.ifdeb("Enviando   EndBlock:" + dd.AsciiString(seb));
    output.write(seb.getBytes());
    dd.ifdeb("Forzando vaciado de buffer de conexion");
    output.flush();
    String sRespuesta = null;
    if (withAnswer) {
      dd.ifdeb("Esperando ACK/ERR...");
      sRespuesta = getACK(dd);
      dd.ifdeb("ACK/ERR Recibido.");
    }
    return sRespuesta;
  }

  //Recibir mensaje
  public Msg getBlock(Debug dd) throws IOException,
      BlockBrokerException,
      InterruptedException,
      socketClosedException {
    StringBuffer data = new StringBuffer();

    //Leer hasta encontrar seb
    boolean bbb = true; //Comprobacion de BeginBlock
    int sbbl = sbb.length();
    int sebl = seb.length();

    while (true) {
      int c = input.read();
      if (c == -1) {
        throw new socketClosedException();
      }
      data.append( (char) c);
      if (bbb && data.length() >= sbbl) {
        dd.ifdeb("getBlock():Comprobando BeginBlock");
        if (!data.toString().startsWith(sbb)) {
          dd.ifdeb("getBlock():BeginBlock no encontrado:");
          dd.ifdeb("  BeginBlock=<" + sbb + ">");
          dd.ifdeb("  data      =<" + data.substring(0, sbbl) + ">");
          throw new BlockBrokerException("BeginBlock no encontrado");
        }
        else {
          dd.ifdeb("getBlock():BeginBlock OK");
        }
        bbb = false;
      }
      if (data.length() > sbbl + sebl) {
        if (seb.equals(data.substring(data.length() - sebl, data.length()))) {
          dd.ifdeb("getBlock():EndBlock OK");
          break;
        }
      }
    }
    //Quitamos las cadenas de Inicio y Fin de Mensaje
    dd.ifdeb("getBlock():Raw Stream:" + dd.AsciiString(data.toString()));
    data.delete(data.length() - seb.length(), data.length());
    data.delete(0, sbb.length());
    char rEOS[] = new char[1];
    rEOS[0] = (char) 13;
    return Msg.readMessage(data.toString(), rEOS);
  }

  public String getACK(Debug dd) throws BlockBrokerException,
      socketClosedException,
      IOException,
      InterruptedException {
    dd.ifdeb("getACK() Esperando Block");
    Msg msgResp = getBlock(dd);
    Segment MSAsegment = msgResp.findSegment(new Segment(null, "MSA"));
    if (MSAsegment == null) {
      throw new BlockBrokerException("No es un mensaje ACK");
    }
    Field ACKfield = MSAsegment.getField(1); //AA o ER
    return ACKfield.toString();
  }

  public void sendACK(Debug dd, Msg forthis, int msatype) throws
      BlockBrokerException,
      socketClosedException,
      IOException,
      SQLException,

      InterruptedException {
    String tACK = MSAtxt[msatype - 1];
    dd.ifdeb("sendACK():" + tACK);
    //Segment Separator real.
    char rEOS[] = new char[1];
    rEOS[0] = (char) 13;
    //Construir ack y enviar
    sendBlock(dd, forthis.getACKfor(so, sd, tACK, rEOS), false);
  }

}