
package ssf.hl7.tcp;

public class BlockBrokerException extends Exception {
	BlockBrokerException( String s ){
		super(s);
	}
}