package ssf.hl7.msg;

import java.util.Vector;

public class Segment {

  private String name;
  private Vector fields;
  private Msg hl7Msg;

  public Segment(Msg m, String s) {
    if (m == null) {
      m = new Msg("|", "^");
    }
    hl7Msg = m;
    if (s == null) {
      s = new String("");
    }
    name = s;
    fields = new Vector();
  }

  public void addField(Field f) {
    putField(fields.size() + 1, f);
  }

  public void putField(int i, Field f) {
    if (i < 1) {
      return;
    }
    if (i <= fields.size()) {
      fields.setElementAt(f, i - 1);
    }
    else {
      for (int j = fields.size(); j < i - 1; j++) {
        fields.addElement(new Field(hl7Msg, ""));

      }
      fields.addElement(f);
    }
  }

  public Field getField(int i) {
    if (i < 1) {
      return null;
    }
    if (i > fields.size()) {
      return null;
    }
    else {
      return (Field) fields.elementAt(i - 1);
    }
  }

  public void putComponent(int f, int c, Component comp) {
    if (f < 1) {
      return;
    }
    Field fld = getField(f);
    if (fld == null) {
      fld = new Field(hl7Msg, "");
      putField(f, fld);
    }
    fld.putComponent(c, comp);
  }

  public Component getComponent(int f, int c) {
    Field fld = getField(f);
    if (fld == null) {
      return new Component("");
    }
    else {
      Component comp = fld.getComponent(c);
      return comp;
    }
  }

  public int getNumberOfFields() {
    return fields.size();
  }

  public boolean equals(Segment s) {
    if (s == null) {
      return false;
    }
    return s.getName().equals(name);
  }

  public String getName() {
    return name;
  }

  public String toString() {
    String s = new String("");
    s = s + name;
    for (int i = 0; i < fields.size(); i++) {
      s = s + hl7Msg.getFieldSeparator() +
          ( (Field) fields.elementAt(i)).toString();
    }
    return s;
  }
}
