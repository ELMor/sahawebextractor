
import java.net.*;

class pruebaSocket {
	class pruebaServer extends Thread {
		ServerSocket ss;
		Socket s;
		void pruebaServer() throws Exception{
			System.out.println("Server constructing");
			ss=new ServerSocket(1232);
			s=ss.accept();
		}
		public void run(){
			System.out.println("Server running");
		}
	}
	class pruebaClient extends Thread {
		Socket s;
		void pruebaClient() throws Exception{
			System.out.println("Client  constructing");
			s=new Socket("localhost",1232);
		}
		public void run(){
			System.out.println("Client running");
		}
	}
	pruebaServer s;
	pruebaClient c;
	void pruebaSocket(){
		s=new pruebaServer();
		c=new pruebaClient();
		s.start();
		c.start();
	}

	boolean running(){
		return s.isAlive() || c.isAlive();
	}

	public static void main(String arg[]){
		pruebaSocket k=new pruebaSocket();
		try {
		while( k.running() )
			Thread.sleep(1000);
		}catch(Exception e){
			e.printStackTrace();
		}
		return;
	}

}
