//
//	Palm App Name:   		"BasicApp"
//
//	Palm App Version:		"1.0"


//	Resource: tFRM 1000
#define MainForm            1000	//(Left Origin = 0, Top Origin = 0, Width = 160, Height = 160, Usable = 1, Modal = 0, Save Behind = 0, Help ID = 0, Menu Bar ID = 1000, Default Button ID = 0)
#define UpperTable			1001
#define StackTable			1002
#define EditText			1003
#define b11					1011
#define b12					1012
#define b13					1013
#define b14					1014
#define b15					1015
#define b16					1016
#define b21					1021
#define b22					1022
#define b23					1023
#define b24					1024
#define b25					1025
#define b26					1026
#define b31					1031
#define b32					1032
#define b33					1033
#define b34					1034
#define b35					1035
#define b36					1036
#define b41					1041
#define b42					1042
#define b43					1043
#define b44					1044
#define b45					1045
#define b46					1046
#define b51					1051
#define b52					1052
#define b53					1053
#define b54					1054
#define b55					1055

//	Resource: Talt 1001
#define RomIncompatibleAlert                      1001
#define RomIncompatibleOK                         0


//	Resource: MBAR 1000
#define MainFormMenuBar                           1000


//	Resource: MENU 1000
#define MainOptionsMenu                           1000
#define MainOptionsAboutStarterApp                1000


//	Resource: PICT 1001
#define Bitmap                                    1001

//	Resource: PICT 1002
#define Bitmap2                                   1002

//	Resource: PICT 1008
#define Bitmap3                                   1008

//	Resource: PICT 1011
#define Bitmap4                                   1011

//	Resource: PICT 1012
#define Bitmap5                                   1012

//	Resource: PICT 1018
#define Bitmap6                                   1018


//	Resource: taif 1000
#define Largeicons12and8bitsAppIconFamily         1000

//	Resource: taif 1001
#define Smallicons12and8bitsAppIconFamily         1001

