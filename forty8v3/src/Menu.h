




typedef struct MenuItemType {
	Int8  tipo; /* -2 Menu de 26 items,
				   -1 Menu de 29 items,
					0 marca de fin de serie de items, 
					1 Menu superior, 
					2 Comando, 
					3 Internal Function void (*fptr)() 
					4 Internal Function Stackable (*fptr)(Stack)*/
	char* caption;
	void* content; 
} MenuItem ;

static MenuItem part[]={
{2,"abs",NULL},{2,"sign",NULL},{2,"conj",NULL},{2,"arg",NULL},{2,"re",NULL},{2,"im",NULL},
{2,"MIN",NULL},{2,"MAX",NULL},{2,"MOD",NULL},{2,"%",NULL},{2,"%CH",NULL},{2,"%T",NULL},
{2,"MANT",NULL},{2,"XPON",NULL},{2,"IP",NULL},{2,"FP",NULL},{2,"FLOOR",NULL},{2,"CEIL",NULL},
{2,"RND",NULL},{2,"TRNC",NULL},{2,"MAXR",NULL},{2,"MINR",NULL},{0,NULL,NULL}
};

static MenuItem prob[]={
{2,"comb",NULL},{2,"perm",NULL},{2,"!",NULL},{2,"rand",NULL},{2,"rdz",NULL}, {0,NULL,NULL}
};

static MenuItem hyp[]={
{2,"SINH",NULL},{2,"ASINH",NULL},{2,"COSH",NULL},{2,"ACOSH",NULL},{2,"TANH",NULL},{2,"ATAN",NULL},
{2,"EXPM",NULL},{2,"LNP1",NULL},  {0,NULL,NULL}
};

static MenuItem matr[]={
{2,"CON",NULL},{2,"IDN",NULL},{2,"TRN",NULL},{2,"RDM",NULL},{2,"DET",NULL},{2,"RSD",NULL},
{2,"ABS",NULL},{2,"RNRM",NULL},{2,"CNRM",NULL},  {0,NULL,NULL}
};

static MenuItem vect[]={
{2,"XYZ",NULL},{2,"RAZ",NULL},{2,"RAA",NULL},{2,"CROSS",NULL},{2,"DOT",NULL},{2,"ABS",NULL},
{2,"V->",NULL},{2,"->V2",NULL},{2,"->V3",NULL},{2,"D->R",NULL},{2,"R->D",NULL},{0,NULL,NULL}
};

static MenuItem base[]={
{2,"HEX",NULL},{2,"DEC",NULL},{2,"OCT",NULL},{2,"BIN",NULL},{2,"STWS",NULL},{2,"RCWS",NULL},
{2,"RL",NULL},{2,"RR",NULL},{2,"RLB",NULL},{2,"RRB",NULL},{2,"R->B",NULL},{2,"B->R",NULL},
{2,"SL",NULL},{2,"SR",NULL},{2,"SLB",NULL},{2,"SRB",NULL},{2,"ASR",NULL},{2,NULL,NULL},
{2,"AND",NULL},{2,"OR",NULL},{2,"XOR",NULL},{2,"NOT",NULL},{0,NULL,NULL}
};

static MenuItem stk[]={
{2,"OVER",NULL},{2,"ROT",NULL},{2,"ROLL",NULL},{2,"ROLLD",NULL},{2,"PICK",NULL},{2,"DEPTH",NULL},
{2,"DUP",NULL},{2,"DUP2",NULL},{2,"DUPN",NULL},{2,"DROP2",NULL},{2,"DRPN",NULL},{0,NULL,NULL}
};

static MenuItem obj[]={
{2,"OBJ->",NULL},{2,"EQ->",NULL},{2,"->ARR",NULL},{2,"->LIST",NULL},{2,"->STR",NULL},{2,"->TAG",NULL},
{2,"R->C",NULL},{2,"C->R",NULL},{2,"DTAG",NULL},{2,"->UNIT",NULL},{2,"TYPE",NULL},{2,"VTYPE",NULL},
{2,"SIZE",NULL},{2,"POS",NULL},{2,"REPL",NULL},{2,"SUB",NULL},{2,"NUM",NULL},{2,"CHR",NULL},
{2,"PUT",NULL},{2,"GET",NULL},{2,"PUTI",NULL},{2,"GETI",NULL},{0,NULL,NULL}
};
	

static MenuItem Normal[]={
{1,"part",part}, {1,"prob",prob},{1,"hyp",hyp},{1,"matr",matr},{1,"vect",vect},{1,"base",base},
{1,"Left",NULL}, {2,"7",NULL},{2,"8",NULL},{2,"9",NULL},  {2,"/",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{2,"4",NULL},{2,"5",NULL},{2,"6",NULL},  {2,"*",NULL},{1,"alpha",NULL},
{1,"Up",NULL},   {2,"1",NULL},{2,"2",NULL},{2,"3",NULL},  {2,"-",NULL},{1,"RET",NULL},
{1,"Down",NULL}, {2,".",NULL},{2,"0",NULL},{2,"SPC",NULL},{2,"+",NULL},{0,NULL,NULL}
};

static MenuItem alpha[]={
{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},
{1,"Left",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"alpha",NULL},
{1,"Up",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"RET",NULL},
{1,"Down",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{0,NULL,NULL}
};

static MenuItem Left[]={
{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},
{1,"Left",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"alpha",NULL},
{1,"Up",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"RET",NULL},
{1,"Down",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{0,NULL,NULL}
};

static MenuItem Right[]={
{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},
{1,"Left",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"alpha",NULL},
{1,"Up",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"RET",NULL},
{1,"Down",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{0,NULL,NULL}
};

static MenuItem Up[]={
{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},
{1,"Left",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"alpha",NULL},
{1,"Up",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"RET",NULL},
{1,"Down",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{0,NULL,NULL}
};

static MenuItem Down[]={
{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},
{1,"Left",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{3,"next",nxtUpperMenuPage},
{1,"Right",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"alpha",NULL},
{1,"Up",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"RET",NULL},
{1,"Down",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{1,"cap",NULL},{0,NULL,NULL}
};


static MenuItem MainMenus[]={
	{ -1,"Normal",Normal },{ -1,"Alpha",alpha },
	{ -1,"Left",Left },	   { -1,"Right",Right },
	{ -1,"Up",Up },        { -1,"Down",Down }
};
