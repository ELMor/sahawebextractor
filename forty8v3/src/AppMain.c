/******************************************************************************
 *
 * Copyright (c) 1999-2004 PalmSource, Inc. All rights reserved.
 *
 * File: AppMain.c
 *
 *****************************************************************************/

#include <PalmOS.h>

#include "AppResources.h"
#include "AppMain.h"
#include "Menu.h"


/***********************************************************************
 *
 *	Entry Points
 *
 ***********************************************************************/


/***********************************************************************
 *
 *	Internal Constants
 *
 ***********************************************************************/
#define appFileCreator			'ELM1'	// register your own at http://www.palmos.com/dev/creatorid/
#define appVersionNum			0x01
#define appPrefID				0x00
#define appPrefVersionNum		0x01



/*
 * GlobalVar status
 */
MenuItem* UpperMenu=NULL;
Int8 ShiftStatus=0;
Int8 UpperMenuPage=0;
Int8 UpperMenuMaxPage=0;

/***********************************************************************
 *
 *	Internal Functions
 *
 ***********************************************************************/


/***********************************************************************
 *
 * FUNCTION:    MainFormDoCommand
 *
 * DESCRIPTION: This routine performs the menu command specified.
 *
 * PARAMETERS:  command  - menu item id
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormDoCommand(UInt16 command)
{
	Boolean handled = false;
	//FormType * pForm;

	switch (command) {
		case MainOptionsAboutStarterApp:
		/*
			pForm = FrmInitForm(AboutForm);
			FrmDoDialog(pForm);					// Display the About Box.
			FrmDeleteForm(pForm);
			handled = true;
		*/
			break;
	}
	
	return handled;
}



static Boolean ShiftButton(Int16 cid){
	Int8 oldSS=ShiftStatus;
	if(cid==1016)
		ShiftStatus = (ShiftStatus==1 ?  0 : 1 );
	if(cid==1022)
		ShiftStatus = (ShiftStatus==2 ?  0 : 2 );
	if(cid==1027)
		ShiftStatus = (ShiftStatus==3 ?  0 : 3 );
	if(cid==1028)
		ShiftStatus = (ShiftStatus==4 ?  0 : 4 );
	if(cid==1034)
		ShiftStatus = (ShiftStatus==5 ?  0 : 5 );
	if(oldSS!=ShiftStatus)
		return true;
	return false;
}



static void setLabel(Int8 i, char* lbl){
	FormType* pf;
	pf=FrmGetActiveForm();
		CtlSetLabel( 
			FrmGetObjectPtr(pf,FrmGetObjectIndex(pf,1010+i)), 
			lbl
			);
}

static void repaintButtons(){
	int i=0;
	do {
		setLabel(i,((MenuItem*)(MainMenus[ShiftStatus].content)+i)->caption);
	}while(((MenuItem*)(MainMenus[ShiftStatus].content)+(++i))->tipo);
	
}

static void nxtUpperMenuPage(){
	int i=0;
	Boolean atEnd=false;
	if( ++UpperMenuPage> UpperMenuMaxPage )
		UpperMenuPage=0;
	for(;i<6;i++){
		if( (UpperMenu+(i+6*UpperMenuPage))->tipo == 0)
			atEnd=true;
		setLabel(i, atEnd ? "" : (UpperMenu+(i+6*UpperMenuPage))->caption );
	}
}

static void setUpperMenu(MenuItem *cnt){
	Int8 i=0,i2=6;
	UpperMenu=cnt;
	UpperMenuMaxPage=0;
	while( (cnt+i)->tipo != 0 ){
		i++;
		if(--i2==0){
			i2=6;
			UpperMenuMaxPage++;
		}
	}
	UpperMenuPage=-1;
	nxtUpperMenuPage();		
}

static void appendTextEdit(){
}

/*
 * Funcion principal de botones presionados
 */
static void calcButton(Int16 cid){
	MenuItem *mi,*pressed;
	void   (*fptr)(void);
	
	if(ShiftButton(cid)){
		repaintButtons();
		return;
	}
	mi=MainMenus[ShiftStatus].content;
	pressed=mi+(cid-1010);
	switch(pressed->tipo){
		case -1:	break;
		case  0:	break;
		case  1:	setUpperMenu(pressed->content); 
					break;
		case  2:	appendTextEdit(pressed->caption); 
					break;
		case  3:  	fptr=pressed->content;
					(*fptr)();
					break;
					
	}
	
}


/***********************************************************************
 *
 * FUNCTION:    MainFormHandleEvent
 *
 * DESCRIPTION: This routine is the event handler for the 
 *              "MainForm" of this application.
 *
 * PARAMETERS:  pEvent  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormHandleEvent(EventType* pEvent)
{
	Boolean 	handled = false;
	FormType* 	pForm;
	Int16		cid;
	
	switch (pEvent->eType) {
		case menuEvent:
			return MainFormDoCommand(pEvent->data.menu.itemID);

		case frmOpenEvent:
			pForm = FrmGetActiveForm();
			FrmDrawForm(pForm);
			handled = true;
			break;
			
		case ctlSelectEvent:
			cid=pEvent->data.ctlSelect.controlID;
			if(cid>=1010 && cid<=1038)
				calcButton(cid);
		default:
			break;
	}
	
	return handled;
}
/***********************************************************************
 *
 * FUNCTION:    AppHandleEvent
 *
 * DESCRIPTION: This routine loads form resources and set the event
 *              handler for the form loaded.
 *
 * PARAMETERS:  event  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean AppHandleEvent(EventType* pEvent)
{
	UInt16 		formId;
	FormType* 	pForm;
	Boolean		handled = false;

	if (pEvent->eType == frmLoadEvent) {
		// Load the form resource.
		formId = pEvent->data.frmLoad.formID;
		
		pForm = FrmInitForm(formId);
		FrmSetActiveForm(pForm);

		// Set the event handler for the form.  The handler of the currently
		// active form is called by FrmHandleEvent each time is receives an
		// event.
		switch (formId) {
			case MainForm:
				FrmSetEventHandler(pForm, MainFormHandleEvent);
				break;

			default:
				break;
		}
		handled = true;
	}
	
	return handled;
}


/***********************************************************************
 *
 * FUNCTION:     AppStart
 *
 * DESCRIPTION:  Get the current application's preferences.
 *
 * PARAMETERS:   nothing
 *
 * RETURNED:     Err value errNone if nothing went wrong
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Err AppStart(void)
{
	FrmGotoForm(MainForm);
	return errNone;
}


/***********************************************************************
 *
 * FUNCTION:    AppStop
 *
 * DESCRIPTION: Save the current state of the application.
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppStop(void)
{
	// Close all the open forms.
	FrmCloseAllForms();
}


/***********************************************************************
 *
 * FUNCTION:    AppEventLoop
 *
 * DESCRIPTION: This routine is the event loop for the application.  
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppEventLoop(void)
{
	Err			error;
	EventType	event;

	do {
		EvtGetEvent(&event, evtWaitForever);

		if (SysHandleEvent(&event))
			continue;
			
		if (MenuHandleEvent(0, &event, &error))
			continue;
			
		if (AppHandleEvent(&event))
			continue;

		FrmDispatchEvent(&event);

	} while (event.eType != appStopEvent);
}


/***********************************************************************
 *
 * FUNCTION:    PilotMain
 *
 * DESCRIPTION: This is the main entry point for the application.
 *
 * PARAMETERS:  cmd - word value specifying the launch code. 
 *              cmdPB - pointer to a structure that is associated with the launch code. 
 *              launchFlags -  word value providing extra information about the launch.
 * RETURNED:    Result of launch
 *
 * REVISION HISTORY: 
 *
 *
 ***********************************************************************/
UInt32 PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{
	Err error = errNone;

	switch (cmd) {
		case sysAppLaunchCmdNormalLaunch:
			if ((error = AppStart()) == 0) {			
				AppEventLoop();
				AppStop();
			}
			break;

		default:
			break;
	}
	
	return error;
}


