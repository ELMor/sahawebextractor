// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Parametros.java

package com.ssi.util;


public class Parametros
{

    public static final String DBURL = "jdbc:oracle:thin:@172.19.1.221:1521:osic";
    public static final String USER = "osic";
    public static final String PASSWORD = "osic";
    public static final String TIPOBASE = "Oracle";
    public static int ConnDBQty = 5;
    public static final String FechaAsc = "cagendas.fecha asc,cagendas.hora asc";
    public static final String FechaDesc = "cagendas.fecha desc,cagendas.hora desc";
    public static final String CentroAsc = "centros.nombre_centro asc";
    public static final String CentroDesc = "centros.nombre_centro desc";
    public static final String EspecialidadAsc = "servicios.servicio asc";
    public static final String EspecialidadDesc = "servicios.servicio desc";
    public static final String ProfesionalAsc = "fpersona.nombre_corto asc";
    public static final String ProfesionalDesc = "fpersona.nombre_corto desc";
    public static final String EstadoAsc = "cagendas.cod_estado asc";
    public static final String EstadoDesc = "cagendas.cod_estado desc";
    public static final String PacFechaAsc = "cagendas.fecha asc";
    public static final String PacFechaDesc = "cagendas.fecha desc";
    public static final String PacHoraAsc = "cagendas.hora asc";
    public static final String PacHoraDesc = "cagendas.hora desc";
    public static final String PacCentroAsc = "centros.nombre_centro asc";
    public static final String PacCentroDesc = "centros.nombre_centro desc";
    public static final String PacEspecialidadAsc = "servicios.servicio asc";
    public static final String PacEspecialidadDesc = "servicios.servicio desc";
    public static final String PacEstadoAsc = "cagendas.cod_estado asc";
    public static final String PacEstadoDesc = "cagendas.cod_estado desc";
    public static final String PacPacienteAsc = "clientes.apellido1 asc,clientes.apellido2 asc,clientes.nombre asc";
    public static final String PacPacienteDesc = "clientes.apellido1 desc,clientes.apellido2 desc,clientes.nombre desc";
    public static final String EstadoImplementado = "1";
    public static final String EstadoLiberado = "2";
    public static final String EstadoValidado = "3";
    public static final String EstadoCancelado = "4";

    public Parametros()
    {
    }

}
