// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Format.java

package com.ssi.util;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Format
{

    public static final int INFORMIX = 1;
    public static final int ORACLE = 2;
    public static final int DBENGINE = 1;
    public static final String DATETIME_SECONDS_YMD = "yyyy-MM-dd hh:mm:ss";
    public static final String DATETIME_SECONDS_MDY = "MM-dd-yyyy hh:mm:ss";
    public static final String DATETIME_YMD = "yyyy-MM-dd";
    public static final String DATETIME_MDY = "MM-dd-yyyy";

    public static String FormatDatetoDB(GregorianCalendar _gFecha)
    {
        String _sDate = "";
        _sDate = FormatDateToInformix(_gFecha);
        return _sDate;
    }

    public static String FormatDatetoDBLong(GregorianCalendar _gFecha)
    {
        String _sDate = "";
        _sDate = FormatDateToInformixLong(_gFecha);
        return _sDate;
    }

    public static String FormatDateToInformix(GregorianCalendar _gFecha)
    {
        SimpleDateFormat _formatoDate = new SimpleDateFormat("yyyy-MM-dd");
        return "'" + _formatoDate.format(_gFecha.getTime()) + "'";
    }

    public static String FormatDateToInformixLong(GregorianCalendar _gFecha)
    {
        System.out.println("Fecha: " + _gFecha.getTime());
        SimpleDateFormat _formatoDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return "'" + _formatoDate.format(_gFecha.getTime()) + "'";
    }

    public static String FormatDateToOracle(GregorianCalendar _gFecha)
    {
        SimpleDateFormat _formatoDate = new SimpleDateFormat("MM-dd-yyyy");
        String _sFecha = _formatoDate.format(_gFecha.getTime());
        String _sQueryOracle = "TO_DATE('" + _sFecha + "','MM-DD-YYYY')";
        return _sQueryOracle;
    }

    public static String FormatDateToOracleLong(GregorianCalendar _gFecha)
    {
        SimpleDateFormat _formatoDate = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
        String _sFecha = _formatoDate.format(_gFecha.getTime());
        String _sQueryOracle = "TO_DATE('" + _sFecha + "','MM-DD-YYYY HH24:MI:SS')";
        return _sQueryOracle;
    }

    public static GregorianCalendar dateFormatWithDTL3(String fecha)
    {
        String year = fecha.substring(0, 4);
        String month = fecha.substring(5, 7);
        String day = fecha.substring(8, 10);
        String hour = fecha.substring(11, 13);
        String minutes = fecha.substring(14, 16);
        int _anio = Integer.parseInt(year);
        int _mes = Integer.parseInt(month);
        int _dia = Integer.parseInt(day);
        int _hora = Integer.parseInt(hour);
        int _minutos = Integer.parseInt(minutes);
        GregorianCalendar _fecha = new GregorianCalendar(_anio, _mes - 1, _dia, _hora, _minutos);
        return _fecha;
    }

    public Format()
    {
    }
}
