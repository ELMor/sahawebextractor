// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   MailThread.java

package com.ssi.util;

import java.io.PrintStream;

// Referenced classes of package com.ssi.util:
//            SmtpClient

public class MailThread extends Thread
{

    public String mailServerHost;
    public int port;
    public String domain;
    public String sender;
    public String recipient;
    public String subject;
    public String body;

    public MailThread(String paramMailServerHost, int paramPort, String paramDomain, String paramSender, String paramRecipient, String paramSubject, String paramBody)
    {
        mailServerHost = paramMailServerHost;
        port = paramPort;
        domain = paramDomain;
        sender = paramSender;
        recipient = paramRecipient;
        subject = paramSubject;
        body = paramBody;
    }

    public void run()
    {
        try
        {
            SmtpClient.sendMail(mailServerHost, port, domain, sender, recipient, subject, body);
        }
        catch(Exception e)
        {
            System.out.println("MailThread::run()::Error: " + e);
        }
    }
}
