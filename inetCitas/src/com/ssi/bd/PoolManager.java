// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PoolManager.java

package com.ssi.bd;

import com.ssi.util.VStat;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Stack;
import oracle.jdbc.driver.OracleDriver;

public class PoolManager
{

    private static PoolManager theInstance = null;
    private Stack connectionsHandled;

    private PoolManager()
    {
        connectionsHandled = new Stack();
        String jdbcUrl = VStat.urlDB;
        String jdbcUser = VStat.userName;
        String jdbcPassword = VStat.password;
        String jdbcDriver = VStat.urlDriver;
        try
        {
            Class.forName(jdbcDriver);
            DriverManager.registerDriver(new OracleDriver());
            for(int i = 0; i < VStat.maxCon; i++)
            {
                Connection newConnection = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
                connectionsHandled.push(newConnection);
            }

        }
        catch(ClassNotFoundException c)
        {
            c.printStackTrace();
            System.out.println("AAA:" + c);
        }
        catch(SQLException s)
        {
            s.printStackTrace();
        }
    }

    public static PoolManager getInstance()
    {
        if(theInstance == null)
        {
            System.out.println("initializing connections...");
            theInstance = new PoolManager();
            VStat.inicializarVariablesEstaticas();
        }
        return theInstance;
    }

    public synchronized Connection pop()
    {
        Connection result = null;
        result = (Connection)connectionsHandled.pop();
        return result;
    }

    public synchronized void push(Connection theConnection)
    {
        connectionsHandled.push(theConnection);
    }

    public static void main(String args[])
    {
        getInstance();
    }

}
