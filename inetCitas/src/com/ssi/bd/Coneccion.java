// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Coneccion.java

package com.ssi.bd;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Stack;

class Coneccion
{

    public int waitTime;
    public int attempts;
    public static int MaxConections;
    public static Stack stack = new Stack();
    private Connection connectAux;
    private static Connection connect1;
    private static Coneccion miConecc = null;

    private Coneccion(String url, String login, String password, int attempts, int MaxConec, int waitTime)
    {
        MaxConections = MaxConec;
    }

    public static Coneccion creo(String url, String login, String password, int attempts, int MaxConec, int waitTime)
        throws Exception
    {
        if(miConecc == null)
        {
            miConecc = new Coneccion(url, login, password, attempts, MaxConec, waitTime);
            inicializoConecciones(url, login, password);
        }
        return miConecc;
    }

    public static void inicializoConecciones(String url, String login, String password)
    {
        for(int i = 0; i < MaxConections; i++)
        {
            try
            {
                connect1 = DriverManager.getConnection(url, login, password);
            }
            catch(Exception e)
            {
                System.out.println("Error::Coneccion::inicializoConecciones()::" + e);
            }
            stack.push(connect1);
        }

    }

    public Connection getConnection()
        throws SQLException
    {
        synchronized(stack)
        {
            int cnt = attempts;
            while(stack.empty()) 
            {
                try
                {
                    stack.wait(waitTime);
                }
                catch(Exception _ex)
                {
                    throw new SQLException("Se ha interrumpido la conexion.");
                }
                if(--cnt < 1)
                    throw new SQLException("Todas las conexiones estan ocupadas.");
            }
            Connection connection = (Connection)stack.pop();
            Connection connection1 = connection;
            return connection1;
        }
    }

    public void returnConnection(Connection con)
    {
        synchronized(stack)
        {
            stack.push(con);
            stack.notify();
        }
    }

    public void finalize()
    {
        for(int i = 0; i < MaxConections; i++)
        {
            connectAux = (Connection)stack.pop();
            try
            {
                connectAux.close();
            }
            catch(Exception e)
            {
                System.out.println("Error::Coneccion::finalize()::" + e);
            }
        }

    }

}
