// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ClienteValidatorDAO.java

package com.ssi.citas.cliente;

import com.ssi.bd.PoolManager;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

// Referenced classes of package com.ssi.citas.cliente:
//            Cliente

public class ClienteValidatorDAO
{

    public String isValid(Cliente unCliente)
    {
        String resultadoString = "TRUE";
        boolean tablaLogin = false;
        String query = getSelectQuery(unCliente);
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet resultado = stmt.executeQuery(query);
            if(resultado.next())
            {
                unCliente.setCodigo(resultado.getString(1));
                unCliente.setApellido1(resultado.getString(2));
                unCliente.setApellido2(resultado.getString(3));
                unCliente.setNombre(resultado.getString(4));
            } else
            {
                resultadoString = "FALSE";
            }
        }
        catch(Exception s)
        {
            resultadoString = "FALSE";
            System.out.println(String.valueOf(s));
        }
        finally
        {
            pool.push(conn);
        }
        return resultadoString;
    }

    private String getSelectQuery(Cliente unCliente)
    {
        String result = "";
        String apellido1 = unCliente.getApellido1().toUpperCase();
        String tipodocumento = unCliente.getTipoDocumento();
        String nrodocumento = unCliente.getNroDocumento();
        result = "Select codigo_cliente,apellido1,apellido2,nombre,tipo_dni_pk,codigo1 From clientes Where apellido1 = '" + apellido1 + "' and tipo_dni_pk = '" + tipodocumento + "' and codigo1 = '" + nrodocumento + "'";
        return result;
    }

    public ClienteValidatorDAO()
    {
    }
}
