// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   CitasCliente.java

package com.ssi.citas.cliente;


public class CitasCliente
{

    String fecha;
    String hora;
    String nombreCentro;
    String direccionCentro;
    String servicio;
    String nombreCorto;
    String estado;
    String mensaje;

    public CitasCliente()
    {
        fecha = "";
        hora = "";
        nombreCentro = "";
        direccionCentro = "";
        servicio = "";
        nombreCorto = "";
        estado = "";
        mensaje = "";
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String vfecha)
    {
        fecha = vfecha;
    }

    public String getHora()
    {
        return hora;
    }

    public void setHora(String vhora)
    {
        hora = vhora;
    }

    public String getNombreCentro()
    {
        return nombreCentro;
    }

    public void setNombreCentro(String vnombreCentro)
    {
        nombreCentro = vnombreCentro;
    }

    public String getDireccionCentro()
    {
        return direccionCentro;
    }

    public void setDireccionCentro(String vdireccionCentro)
    {
        if(vdireccionCentro == null || vdireccionCentro.equals(""))
            direccionCentro = "";
        else
            direccionCentro = vdireccionCentro;
    }

    public String getServicio()
    {
        return servicio;
    }

    public void setServicio(String vservicio)
    {
        servicio = vservicio;
    }

    public String getNombreCorto()
    {
        return nombreCorto;
    }

    public void setNombreCorto(String vnombreCorto)
    {
        nombreCorto = vnombreCorto;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String vestado)
    {
        String vestadodesc = "";
        if(vestado.trim().equals("CI"))
            vestadodesc = "Citado";
        if(vestado.trim().equals("VI"))
            vestadodesc = "Visitado";
        estado = vestadodesc;
    }
}
