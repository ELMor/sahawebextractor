// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   AgendaProfesional.java

package com.ssi.citas.profesional;


public class AgendaProfesional
{

    String fecha;
    String hora;
    String nombreCentro;
    String direccionCentro;
    String servicio;
    String nombreCorto;
    String nombrePaciente;
    String apellido1Paciente;
    String apellido2Paciente;
    String estado;
    String telefono1Paciente;
    String codigoPaciente;
    String codigoPlan;
    String garante;
    String mensaje;

    public AgendaProfesional()
    {
        fecha = "";
        hora = "";
        nombreCentro = "";
        direccionCentro = "";
        servicio = "";
        nombreCorto = "";
        nombrePaciente = "";
        apellido1Paciente = "";
        apellido2Paciente = "";
        estado = "";
        telefono1Paciente = "";
        codigoPaciente = "";
        mensaje = "";
        codigoPlan = "";
        garante = "";
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public String getCodigoPlan()
    {
        return codigoPlan;
    }

    public void setCodigoPlan(String pcodigoPlan)
    {
        codigoPlan = pcodigoPlan;
    }

    public String getGarante()
    {
        return garante;
    }

    public void setGarante(String pgarante)
    {
        garante = pgarante;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String vfecha)
    {
        fecha = vfecha;
    }

    public String getHora()
    {
        return hora;
    }

    public void setHora(String vhora)
    {
        hora = vhora;
    }

    public String getNombreCentro()
    {
        return nombreCentro;
    }

    public void setNombreCentro(String vnombreCentro)
    {
        nombreCentro = vnombreCentro;
    }

    public String getTelefono1Paciente()
    {
        return telefono1Paciente;
    }

    public void setTelefono1Paciente(String ptelefono1Paciente)
    {
        telefono1Paciente = ptelefono1Paciente;
    }

    public String getCodigoPaciente()
    {
        return codigoPaciente;
    }

    public void setCodigoPaciente(String pcodigoPaciente)
    {
        codigoPaciente = pcodigoPaciente;
    }

    public String getDireccionCentro()
    {
        return direccionCentro;
    }

    public void setDireccionCentro(String vdireccionCentro)
    {
        if(vdireccionCentro == null || vdireccionCentro.equals(""))
            direccionCentro = "";
        else
            direccionCentro = vdireccionCentro;
    }

    public String getServicio()
    {
        return servicio;
    }

    public void setServicio(String vservicio)
    {
        servicio = vservicio;
    }

    public String getNombrePaciente()
    {
        return nombrePaciente;
    }

    public void setNombrePaciente(String pnombrePaciente)
    {
        nombrePaciente = pnombrePaciente;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String vestado)
    {
        String vestadodesc = "";
        if(vestado.trim().equals("CI"))
            vestadodesc = "Citado";
        if(vestado.trim().equals("VI"))
            vestadodesc = "Visitado";
        if(vestado.trim().equals("LI"))
            vestadodesc = "Libre";
        estado = vestadodesc;
    }

    public String getApellido1Paciente()
    {
        return apellido1Paciente;
    }

    public void setApellido1Paciente(String papellido1Paciente)
    {
        apellido1Paciente = papellido1Paciente;
    }

    public String getApellido2Paciente()
    {
        return apellido2Paciente;
    }

    public void setApellido2Paciente(String papellido2Paciente)
    {
        apellido2Paciente = papellido2Paciente;
    }
}
