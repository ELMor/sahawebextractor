// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   ProgramacionProfesionalDAO.java

package com.ssi.citas.profesional;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.profesional:
//            ProgramacionProfesional

public class ProgramacionProfesionalDAO
    implements Cloneable
{

    static String mensaje = "";

    public ProgramacionProfesionalDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerProgramacionProfesional(String pcodigoprofesional, String pfechadesde, String pfechahasta, String centro, String especialidad)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryProgramacion(pcodigoprofesional, pfechadesde, pfechahasta, centro, especialidad);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            ProgramacionProfesional programacionProfesional;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(programacionProfesional))
            {
                programacionProfesional = new ProgramacionProfesional();
                programacionProfesional.setCodigoModelo(set.getString(1));
                programacionProfesional.setFechaDesde(set.getString(2));
                programacionProfesional.setFechaHasta(set.getString(3));
                programacionProfesional.setFechaDesdeExclusion(set.getString(4));
                programacionProfesional.setFechaHastaExclusion(set.getString(5));
                programacionProfesional.setNombreCentro(set.getString(6));
                programacionProfesional.setDireccionCentro(set.getString(7));
                programacionProfesional.setServicio(set.getString(8));
                programacionProfesional.setConsultorio(set.getString(9));
                programacionProfesional.setLunes(set.getString(10));
                programacionProfesional.setMartes(set.getString(11));
                programacionProfesional.setMiercoles(set.getString(12));
                programacionProfesional.setJueves(set.getString(13));
                programacionProfesional.setViernes(set.getString(14));
                programacionProfesional.setSabado(set.getString(15));
                programacionProfesional.setDomingo(set.getString(16));
                programacionProfesional.setHoraDesde(set.getString(17));
                programacionProfesional.setHoraHasta(set.getString(18));
                programacionProfesional.setHoraDesdeExclusion(set.getString(19));
                programacionProfesional.setHoraHastaExclusion(set.getString(20));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryProgramacion(String pcodigoprofesional, String pfechadesde, String pfechahasta, String pcentro, String pespecialidad)
    {
        String codigoprofesional = pcodigoprofesional.trim();
        String fechadesde = pfechadesde.trim();
        String fechahasta = pfechahasta.trim();
        String centro = pcentro.trim();
        String especialidad = pespecialidad.trim();
        String where = "fageprog.cod_modelo = fmodelos.cod_modelo and servicios.codigo_servicio = fageprog.codigo_servicio and centros.cod_centro = servicios.cod_centro and tconsultorio.consult_pk = fageprog.consult_pk and fmodelos.codigo_personal = '" + pcodigoprofesional + "' " + "and ((fmodelos.desde_f <= to_date ('" + fechadesde + "', 'DD/MM/YYYY') " + "and fmodelos.hasta_f >= to_date ('" + fechahasta + "', 'DD/MM/YYYY')) " + "or (fmodelos.desde_f <= to_date ('" + fechadesde + "', 'DD/MM/YYYY') " + "and fmodelos.hasta_f >= to_date ('" + fechahasta + "', 'DD/MM/YYYY')) " + "or (fmodelos.desde_f >= to_date ('" + fechadesde + "', 'DD/MM/YYYY') " + "and fmodelos.hasta_f <= to_date ('" + fechahasta + "', 'DD/MM/YYYY')))";
        if(!centro.equals("S/D"))
            where = where + " and centros.cod_centro = '" + centro + "'";
        if(!especialidad.equals("S/D"))
            where = where + " and servicios.gfh_pk = '" + especialidad + "'";
        String query = "select fmodelos.cod_modelo, fmodelos.desde_f, fmodelos.hasta_f, fmodelos.ex_desde, fmodelos.ex_hasta, centros.nombre_centro, centros.ctro_direcc, servicios.servicio, tconsultorio.consult_desc, fageprog.l, fageprog.m, fageprog.x, fageprog.j, fageprog.v, fageprog.s, fageprog.d, fageprog.hora_ini, fageprog.hora_fin, fageprog.exc_desde, fageprog.exc_hasta from fmodelos, fageprog, servicios, centros, tconsultorio WHERE " + where;
        return query;
    }

}
