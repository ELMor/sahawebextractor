// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ProfesionalFicha.java

package com.ssi.citas.profesional;


public class ProfesionalFicha
{

    String codigo;
    String apellido1;
    String apellido2;
    String nombre;
    String usuario;
    String password;
    String cod_perso;
    String nombre_corto;
    String codigo_persona;
    String codigo_colegiado;
    String activo_sn;
    String prof_con_agen_sn;
    String codigo_interno;
    String fpers_doc;
    String fpers_dom_via;
    String fpers_dom_mun;
    String thonmed_desc;
    String nom_categ;
    String nombre_ld1;
    String nombre_ld2;
    String nombre_ld3;
    String nombre_ld4;
    String nombre_ld5;
    String tipo_dni_desc;
    String del_centro_sn;
    String grpmed_det_tit;
    String grpmed_cab_desc;
    String cpostal;

    public ProfesionalFicha()
    {
        codigo = "";
        apellido1 = "";
        apellido2 = "";
        nombre = "";
        usuario = "";
        password = "";
        cod_perso = "";
        nombre_corto = "";
        codigo_persona = "";
        codigo_colegiado = "";
        activo_sn = "";
        prof_con_agen_sn = "";
        codigo_interno = "";
        fpers_doc = "";
        fpers_dom_via = "";
        fpers_dom_mun = "";
        thonmed_desc = "";
        nom_categ = "";
        nombre_ld1 = "";
        nombre_ld2 = "";
        nombre_ld3 = "";
        nombre_ld4 = "";
        nombre_ld5 = "";
        tipo_dni_desc = "";
        del_centro_sn = "";
        grpmed_det_tit = "";
        grpmed_cab_desc = "";
        cpostal = "";
    }

    public String getCpostal()
    {
        return cpostal;
    }

    public void setCpostal(String pcpostal)
    {
        cpostal = pcpostal;
    }

    public String getGrpmed_cab_desc()
    {
        return grpmed_cab_desc;
    }

    public void setGrpmed_cab_desc(String pgrpmed_cab_desc)
    {
        grpmed_cab_desc = pgrpmed_cab_desc;
    }

    public String getGrpmedDetTit()
    {
        return grpmed_det_tit;
    }

    public void setGrpmedDetTit(String pgrpmed_det_tit)
    {
        if(pgrpmed_det_tit.trim().equals("1"))
            pgrpmed_det_tit = "SI";
        if(pgrpmed_det_tit.trim().equals("0"))
            pgrpmed_det_tit = "NO";
        grpmed_det_tit = pgrpmed_det_tit;
    }

    public String getDelCentroSn()
    {
        return del_centro_sn;
    }

    public void setDelCentroSn(String pdel_centro_sn)
    {
        if(pdel_centro_sn.trim().equals("1"))
            pdel_centro_sn = "SI";
        if(pdel_centro_sn.trim().equals("0"))
            pdel_centro_sn = "NO";
        del_centro_sn = pdel_centro_sn;
    }

    public String getTipoDniDesc()
    {
        return tipo_dni_desc;
    }

    public void setTipoDniDesc(String ptipo_dni_desc)
    {
        tipo_dni_desc = ptipo_dni_desc;
    }

    public String getNombre_ld1()
    {
        return nombre_ld1;
    }

    public void setNombre_ld1(String pnombre_ld1)
    {
        nombre_ld1 = pnombre_ld1;
    }

    public String getNombre_ld2()
    {
        return nombre_ld2;
    }

    public void setNombre_ld2(String pnombre_ld2)
    {
        nombre_ld2 = pnombre_ld2;
    }

    public String getNombre_ld3()
    {
        return nombre_ld3;
    }

    public void setNombre_ld3(String pnombre_ld3)
    {
        nombre_ld3 = pnombre_ld3;
    }

    public String getNombre_ld4()
    {
        return nombre_ld4;
    }

    public void setNombre_ld4(String pnombre_ld4)
    {
        nombre_ld4 = pnombre_ld4;
    }

    public String getNombre_ld5()
    {
        return nombre_ld5;
    }

    public void setNombre_ld5(String pnombre_ld5)
    {
        nombre_ld5 = pnombre_ld5;
    }

    public String getThonmedDesc()
    {
        return thonmed_desc;
    }

    public void setThonmedDesc(String pthonmed_desc)
    {
        thonmed_desc = pthonmed_desc;
    }

    public String getNomCateg()
    {
        return nom_categ;
    }

    public void setNomCateg(String pnom_categ)
    {
        nom_categ = pnom_categ;
    }

    public String getFpersDomVia()
    {
        return fpers_dom_via;
    }

    public void setFpersDomVia(String pfpers_dom_via)
    {
        fpers_dom_via = pfpers_dom_via;
    }

    public String getFpersDomMun()
    {
        return fpers_dom_mun;
    }

    public void setFpersDomMun(String pfpers_dom_mun)
    {
        fpers_dom_mun = pfpers_dom_mun;
    }

    public String getCodigoInterno()
    {
        return codigo_interno;
    }

    public void setCodigoInterno(String pcodigo_interno)
    {
        codigo_interno = pcodigo_interno;
    }

    public String getFpersDoc()
    {
        return fpers_doc;
    }

    public void setFpersDoc(String pfpers_doc)
    {
        fpers_doc = pfpers_doc;
    }

    public String getActivoSn()
    {
        return activo_sn;
    }

    public void setActivoSn(String pactivo_sn)
    {
        if(pactivo_sn.trim().equals("1"))
            pactivo_sn = "SI";
        if(pactivo_sn.trim().equals("0"))
            pactivo_sn = "NO";
        activo_sn = pactivo_sn;
    }

    public String getProfConAgenSn()
    {
        return prof_con_agen_sn;
    }

    public void setProfConAgenSn(String pprof_con_agen_sn)
    {
        if(pprof_con_agen_sn.trim().equals("1"))
            pprof_con_agen_sn = "SI";
        if(pprof_con_agen_sn.trim().equals("0"))
            pprof_con_agen_sn = "NO";
        prof_con_agen_sn = pprof_con_agen_sn;
    }

    public String getCodigoPersona()
    {
        return codigo_persona;
    }

    public void setCodigoPersona(String pcodigo_persona)
    {
        codigo_persona = pcodigo_persona;
    }

    public String getCodigoColegiado()
    {
        return codigo_colegiado;
    }

    public void setCodigoColegiado(String pcodigo_colegiado)
    {
        codigo_colegiado = pcodigo_colegiado;
    }

    public String getCodPerso()
    {
        return cod_perso;
    }

    public void setCodPerso(String pcod_perso)
    {
        cod_perso = pcod_perso;
    }

    public String getNombreCorto()
    {
        return nombre_corto;
    }

    public void setNombreCorto(String pnombre_corto)
    {
        nombre_corto = pnombre_corto;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String vcodigo)
    {
        codigo = vcodigo;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String vusuario)
    {
        usuario = vusuario;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String vpassword)
    {
        password = vpassword;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String vapellido1)
    {
        apellido1 = vapellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String vapellido2)
    {
        apellido2 = vapellido2;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String vnombre)
    {
        nombre = vnombre;
    }
}
