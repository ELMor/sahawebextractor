// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ProgramacionProfesional.java

package com.ssi.citas.profesional;


public class ProgramacionProfesional
{

    String fechaDesde;
    String fechaHasta;
    String fechaDesdeExclusion;
    String fechaHastaExclusion;
    String horaDesde;
    String horaHasta;
    String horaDesdeExclusion;
    String horaHastaExclusion;
    String nombreCentro;
    String direccionCentro;
    String servicio;
    String consultorio;
    String lunes;
    String martes;
    String miercoles;
    String jueves;
    String viernes;
    String sabado;
    String domingo;
    String codigoModelo;
    String mensaje;

    public ProgramacionProfesional()
    {
        fechaDesde = "";
        fechaHasta = "";
        fechaDesdeExclusion = "";
        fechaHastaExclusion = "";
        horaDesde = "";
        horaHasta = "";
        horaDesdeExclusion = "";
        horaHastaExclusion = "";
        nombreCentro = "";
        direccionCentro = "";
        servicio = "";
        consultorio = "";
        lunes = "";
        martes = "";
        miercoles = "";
        jueves = "";
        viernes = "";
        sabado = "";
        domingo = "";
        codigoModelo = "";
        mensaje = "";
    }

    public String getCodigoModelo()
    {
        return codigoModelo;
    }

    public void setCodigoModelo(String pcodigoModelo)
    {
        codigoModelo = pcodigoModelo;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public String getFechaDesde()
    {
        return fechaDesde;
    }

    public void setFechaDesde(String pfechaDesde)
    {
        fechaDesde = pfechaDesde;
    }

    public String getFechaHasta()
    {
        return fechaHasta;
    }

    public void setFechaHasta(String pfechaHasta)
    {
        fechaHasta = pfechaHasta;
    }

    public String getFechaDesdeExclusion()
    {
        return fechaDesdeExclusion;
    }

    public void setFechaDesdeExclusion(String pfechaDesdeExclusion)
    {
        fechaDesdeExclusion = pfechaDesdeExclusion;
    }

    public String getFechaHastaExclusion()
    {
        return fechaHastaExclusion;
    }

    public void setFechaHastaExclusion(String pfechaHastaExclusion)
    {
        fechaHastaExclusion = pfechaHastaExclusion;
    }

    public String getHoraDesde()
    {
        return horaDesde;
    }

    public void setHoraDesde(String phoraDesde)
    {
        horaDesde = phoraDesde;
    }

    public String getHoraHasta()
    {
        return horaHasta;
    }

    public void setHoraHasta(String phoraHasta)
    {
        horaHasta = phoraHasta;
    }

    public String getHoraDesdeExclusion()
    {
        return horaDesdeExclusion;
    }

    public void setHoraDesdeExclusion(String phoraDesdeExclusion)
    {
        horaDesdeExclusion = phoraDesdeExclusion;
    }

    public String getHoraHastaExclusion()
    {
        return horaHastaExclusion;
    }

    public void setHoraHastaExclusion(String phoraHastaExclusion)
    {
        horaHastaExclusion = phoraHastaExclusion;
    }

    public String getNombreCentro()
    {
        return nombreCentro;
    }

    public void setNombreCentro(String vnombreCentro)
    {
        nombreCentro = vnombreCentro;
    }

    public String getDireccionCentro()
    {
        return direccionCentro;
    }

    public void setDireccionCentro(String vdireccionCentro)
    {
        if(vdireccionCentro == null || vdireccionCentro.equals(""))
            direccionCentro = "";
        else
            direccionCentro = vdireccionCentro;
    }

    public String getServicio()
    {
        return servicio;
    }

    public void setServicio(String vservicio)
    {
        servicio = vservicio;
    }

    public String getLunes()
    {
        return lunes;
    }

    public void setLunes(String plunes)
    {
        if(plunes == null)
            plunes = "";
        lunes = plunes;
    }

    public String getMartes()
    {
        return martes;
    }

    public void setMartes(String pmartes)
    {
        if(pmartes == null)
            pmartes = "";
        martes = pmartes;
    }

    public String getMiercoles()
    {
        return miercoles;
    }

    public void setMiercoles(String pmiercoles)
    {
        if(pmiercoles == null)
            pmiercoles = "";
        miercoles = pmiercoles;
    }

    public String getJueves()
    {
        return jueves;
    }

    public void setJueves(String pjueves)
    {
        if(pjueves == null)
            pjueves = "";
        jueves = pjueves;
    }

    public String getViernes()
    {
        return viernes;
    }

    public void setViernes(String pviernes)
    {
        if(pviernes == null)
            pviernes = "";
        viernes = pviernes;
    }

    public String getSabado()
    {
        return sabado;
    }

    public void setSabado(String psabado)
    {
        if(psabado == null)
            psabado = "";
        sabado = psabado;
    }

    public String getDomingo()
    {
        return domingo;
    }

    public void setDomingo(String pdomingo)
    {
        if(pdomingo == null)
            pdomingo = "";
        domingo = pdomingo;
    }

    public String getConsultorio()
    {
        return consultorio;
    }

    public void setConsultorio(String pconsultorio)
    {
        consultorio = pconsultorio;
    }
}
