// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   PacienteGaranteDAO.java

package com.ssi.citas.comun;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.comun:
//            PacienteGarante

public class PacienteGaranteDAO
    implements Cloneable
{

    static String mensaje = "";

    public PacienteGaranteDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerPacienteGarante(String pcodigoPaciente)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "SELECT garantes.nombre_garante,cliente_pagadores.clipag_activo_sn,co_planes.plan_desc,co_cli_planes.num_afil FROM cliente_pagadores,clientes,co_cli_planes,co_planes,garantes,pagadores WHERE cliente_pagadores.codigo_cliente = clientes.codigo_cliente AND cliente_pagadores.cod_pagador_pk = pagadores.cod_pagador_pk AND pagadores.codigo_garante_pk = garantes.codigo_garante_pk AND co_cli_planes.clipag_pk = cliente_pagadores.clipag_pk AND co_cli_planes.plan_pk = co_planes.plan_pk AND clientes.codigo_cliente = '" + pcodigoPaciente.trim() + "' ";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            PacienteGarante pacienteGarante;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(pacienteGarante))
            {
                pacienteGarante = new PacienteGarante();
                pacienteGarante.setNombreGarante(set.getString(1));
                pacienteGarante.setClipagActivoSn(set.getString(2));
                pacienteGarante.setDescripcionPlan(set.getString(3));
                pacienteGarante.setNumAfiliado(set.getString(4));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

}
