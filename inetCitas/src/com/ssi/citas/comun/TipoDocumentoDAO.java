// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   TipoDocumentoDAO.java

package com.ssi.citas.comun;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.comun:
//            TipoDocumento

public class TipoDocumentoDAO
    implements Cloneable
{

    static String mensaje = "";

    public TipoDocumentoDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerTipoDocumento()
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "Select tipo_dni_pk, tipo_dni_desc from tpo_dni";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            TipoDocumento tipoDocumento;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(tipoDocumento))
            {
                tipoDocumento = new TipoDocumento();
                tipoDocumento.setTipo(set.getString(1));
                tipoDocumento.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try {
              conn.rollback();
              conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

}
