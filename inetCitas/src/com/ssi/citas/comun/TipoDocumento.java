// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   TipoDocumento.java

package com.ssi.citas.comun;


public class TipoDocumento
{

    String tipo;
    String descripcion;

    public TipoDocumento()
    {
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String ptipo)
    {
        tipo = ptipo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String pdescripcion)
    {
        descripcion = pdescripcion;
    }
}
