// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   EspecialidadDAO.java

package com.ssi.citas.comun;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.comun:
//            Especialidad

public class EspecialidadDAO
    implements Cloneable
{

    static String mensaje = "";

    public EspecialidadDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerEspecialidad(String pcodigoProfesional)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "SELECT gfh.gfh_pk,gfh.gfh_desc FROM fpersona,gfh,fpersona_servicio,servicios WHERE fpersona_servicio.codigo_personal = fpersona.codigo_personal and servicios.gfh_pk = gfh.gfh_pk and servicios.codigo_servicio = fpersona_servicio.codigo_servicio and fpersona.codigo_personal ='" + pcodigoProfesional.trim() + "'";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            Especialidad especialidad;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(especialidad))
            {
                especialidad = new Especialidad();
                especialidad.setCodigo(set.getString(1));
                especialidad.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public Vector obtenerEspecialidadesPorCentro(String pcodigoCentro)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryEspecialidadesCentro(pcodigoCentro);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            Especialidad especialidad;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(especialidad))
            {
                especialidad = new Especialidad();
                especialidad.setCodigo(set.getString(1));
                especialidad.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryEspecialidadesCentro(String pcodigocentro)
    {
        String query = "SELECT distinct gfh.gfh_pk,gfh.gfh_desc FROM gfh,servicios WHERE gfh.gfh_pk = servicios.gfh_pk and servicios.cod_centro ='" + pcodigocentro.trim() + "' " + "ORDER BY gfh.gfh_desc asc";
        return query;
    }

}
