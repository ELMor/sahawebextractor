// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Servicio.java

package com.ssi.citas.comun;


public class Servicio
{

    String codigo;
    String descripcion;

    public Servicio()
    {
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String pcodigo)
    {
        codigo = pcodigo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String pdescripcion)
    {
        descripcion = pdescripcion;
    }
}
