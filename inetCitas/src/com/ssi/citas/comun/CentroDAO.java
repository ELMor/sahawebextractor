// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   CentroDAO.java

package com.ssi.citas.comun;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.comun:
//            Centro

public class CentroDAO
    implements Cloneable
{

    static String mensaje = "";

    public CentroDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerCentro()
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "Select cod_centro, nombre_centro\tFrom centros";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            Centro centro;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(centro))
            {
                centro = new Centro();
                centro.setCodigo(set.getString(1));
                centro.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public Vector obtenerCentrosParaPacientes()
    {
        Vector vectorCentros = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "Select centros.cod_centro, centros.nombre_centro from centros";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            Centro centro;
            for(ResultSet set = stmt.executeQuery(query); set.next(); vectorCentros.addElement(centro))
            {
                centro = new Centro();
                centro.setCodigo(set.getString(1));
                centro.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return vectorCentros;
        }
    }

}
