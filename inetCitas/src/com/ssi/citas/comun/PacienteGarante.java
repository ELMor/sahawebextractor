// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PacienteGarante.java

package com.ssi.citas.comun;


public class PacienteGarante
{

    String nombreGarante;
    String descripcionPlan;
    String numAfiliado;
    String clipagActivoSn;

    public PacienteGarante()
    {
        nombreGarante = "";
        descripcionPlan = "";
        numAfiliado = "";
        clipagActivoSn = "";
    }

    public String getNumAfiliado()
    {
        return numAfiliado;
    }

    public void setNumAfiliado(String pnumAfiliado)
    {
        numAfiliado = pnumAfiliado;
    }

    public String getClipagActivoSn()
    {
        return clipagActivoSn;
    }

    public void setClipagActivoSn(String pclipagActivoSn)
    {
        if(pclipagActivoSn.trim().equals("1"))
            pclipagActivoSn = "SI";
        if(pclipagActivoSn.trim().equals("0"))
            pclipagActivoSn = "NO";
        clipagActivoSn = pclipagActivoSn;
    }

    public String getNombreGarante()
    {
        return nombreGarante;
    }

    public void setNombreGarante(String pnombreGarante)
    {
        nombreGarante = pnombreGarante;
    }

    public String getDescripcionPlan()
    {
        return descripcionPlan;
    }

    public void setDescripcionPlan(String pdescripcionPlan)
    {
        descripcionPlan = pdescripcionPlan;
    }
}
