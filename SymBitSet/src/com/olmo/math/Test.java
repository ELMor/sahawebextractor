package com.olmo.math;

public class Test {

	public static void main(String[] args) {
		try {
			test1();		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void test1() throws Exception {
		SymBS t1=new SymBS("test1", 0x80000000);
		SymBS t2=new SymBS("test2", 0x08000000);
		SymBS t3=t1.plus(t2);
		p(t3);
		
		SymBS t4=t3.rightRotate(1);
		p(t4);
		
		SymBS t5=t4.rightShift(3);
		p(t5);
		
		SymBS t6=t5.rightRotate(24);
		p(t6);
		
		SymBS t7=t1.xor(t6);
		p(t7);
		
		SymBS t8=t1.not();
		p(t8);
		
		SymBS t9=t6.and(t7);
		p(t9);
		
		SHA256 s2=new SHA256("hello".getBytes());
		s2.print();
		byte sec[]=s2.getBytes();
		SHA256 s3=new SHA256(sec);
		s3.print();
	}

	public static void p(SymBS s) throws Exception{
		StringBuffer sb=new StringBuffer();
		s.getOp(sb);
		System.out.println(sb);
	}
	
}
