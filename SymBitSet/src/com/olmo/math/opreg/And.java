package com.olmo.math.opreg;


public class And extends BiOperand {
	
	public And(OpReg o1, OpReg o2){
		super(opRegTypes.and);
		op1=o1;
		op2=o2;
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		sb.append("And(");
		op1.getOp(sb);
		sb.append(",");
		op2.getOp(sb);
		sb.append(")");
	}

}
