package com.olmo.math.opreg;

public class RightShift extends MonoOperandCons {

	public RightShift(OpReg o1, long o2) {
		super(opRegTypes.rightShift);
		op=o1;
		val=o2;
	}

	@Override
	public void getOp(StringBuffer sb) throws Exception {
		op.getOp(sb);
		sb.append(">>>").append(val);
	}

}
