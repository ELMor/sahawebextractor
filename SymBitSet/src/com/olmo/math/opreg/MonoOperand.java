package com.olmo.math.opreg;

public abstract class MonoOperand extends OpReg {
	public OpReg op;
	public MonoOperand(opRegTypes t) {
		super(t);
	}
}
