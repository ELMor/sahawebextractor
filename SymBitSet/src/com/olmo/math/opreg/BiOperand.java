package com.olmo.math.opreg;

public abstract class BiOperand extends OpReg {
	OpReg op1,op2;
	public BiOperand(opRegTypes t) {
		super(t);
	}

}
