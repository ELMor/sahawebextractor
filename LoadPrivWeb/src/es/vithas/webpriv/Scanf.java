package es.vithas.webpriv;

public class Scanf {
	public static String get(String in, String header, int wordsAfter) {
		int pos=in.indexOf(header);
		if(pos<0)
			return null;
		pos+=header.length();
		String resto=in.substring(pos+1);
		String parts[]=resto.split(" |\n");
		String ret="";
		for(int i=0;i<wordsAfter && i<parts.length;i++) {
			if(ret.length()==0)
				ret+=parts[i];
			else
				ret+=" "+parts[i];
		}
		return ret;
	}
	
	public static String get(String in, String header, String footer) {
		int pos=in.indexOf(header);
		if(pos<0)
			return null;
		pos+=header.length();
		String resto=in.substring(pos+1);
		int pos2=resto.indexOf(footer);
		if(pos2<0)
			return null;
		return resto.substring(0,pos2);
	}
}
