package es.vithas.webpriv;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.converter.WordToHtmlUtils;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.converter.xhtml.XWPF2XHTMLConverter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

public class MedicamentosToHTML {
	public static String headers[][]= {
			{"DESCRIPCI&Oacute;N Y PARA QU&Eacute; SE UTILIZA","DESCRIPCION Y PARA QUE SE UTILIZA"},
			{"NOMBRES COMERCIALES"},
			{"QU&Eacute; NECESITA SABER ANTES DE TOMAR EL MEDICAMENTO","QUE NECESITA SABER ANTES DE TOMAR EL MEDICAMENTO","QUE NECESITA SABER ANTES DE EMPEZAR A TOMAR EL MEDICAMENTO","QU&Eacute; NECESITA SABER ANTES DE EMPEZAR A TOMAR EL MEDICAMENTO"},
			{"C&Oacute;MO TOMAR","COMO TOMAR","C&Oacute;MO TOMARLO","COMO TOMARLO"},
			{"PRECAUCIONES DE USO"},
			{"PROBLEMAS QUE PUEDE PRODUCIR"},
			{"CONSERVACI&Oacute;N","CONSERVACION"}
	};
	public static String substitution[][]= {
			{"á","a"},{"é","e"},{"í","i"},{"ó","o"},{"ú","u"},
			{"Á","A"},{"É","E"},{"Í","I"},{"Ó","O"},{"Ú","U"},
			//{"&Aacute;","A"},{"&Eacute;","E"},{"&Iacute;","I"},{"&Oacute;","O"},{"&Uacute;","U"},
			//{"&aacute;","a"},{"&eacute;","e"},{"&iacute;","i"},{"&oacute;","o"},{"&uacute;","u"}
	};
	public static void main(String args[]) {
		String filename=args[0];
		File f=new File(filename);
		if(!f.isDirectory()) {
			checkFile(f);
		}else
			checkDir(f);
	}
	
	public static void checkDir(File f) {
		File lf[]=f.listFiles();
		Arrays.sort(lf);
		for(File f2:lf)
			if(f2.isDirectory())
				checkDir(f2);
			else
				checkFile(f2);
	}
	
	public static void checkFile(File f) {
		if(f.getName().toLowerCase().endsWith(".doc"))
			checkHWPFDoc(f);   
		else if(f.getName().toLowerCase().endsWith(".docx"))
			checkXWPFDoc(f);
	}
	private static void checkXWPFDoc(File f) 
	{       
		try {
			InputStream is=new FileInputStream(f);
			XWPFDocument doc=new XWPFDocument(is);
			XHTMLOptions opts=new XHTMLOptions();
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			XWPF2XHTMLConverter.getInstance().convert(doc,baos,opts);
			String result=new String(baos.toByteArray());
			String oFileName=f.getAbsolutePath();
			oFileName=oFileName.substring(0,oFileName.length()-5)+".html";
			File oFile=new File(oFileName);
			FileOutputStream fos=new FileOutputStream(oFile);
			fos.write(result.getBytes());
			fos.close();
			procText(result, f.getAbsolutePath(), "");
		} catch (IOException e) {
			System.err.println(f.getName());
			e.printStackTrace();
		}
	}

	private static void checkHWPFDoc(File f) {
		try {
			HWPFDocumentCore wordDocument = WordToHtmlUtils.loadDoc(new FileInputStream(f));
			WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
			wordToHtmlConverter.processDocument(wordDocument);
			
			Document htmlDocument = wordToHtmlConverter.getDocument();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DOMSource domSource = new DOMSource(htmlDocument);
			StreamResult streamResult = new StreamResult(out);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "html");
			serializer.transform(domSource, streamResult);
			out.close();
			String styles=domSource.getNode().getChildNodes().item(0).getChildNodes().item(0).getChildNodes().item(0).getChildNodes().item(0).getTextContent();
			styles="<head><style type=\"text/css\">"+styles+"</style></head>";
			String result = new String(out.toByteArray());
			String oFileName=f.getAbsolutePath();
			oFileName=oFileName.substring(0,oFileName.length()-4)+".html";
			File oFile=new File(oFileName);
			FileOutputStream fos=new FileOutputStream(oFile);
			fos.write(result.getBytes());
			fos.close();
			procText(result, f.getAbsolutePath(), styles);
		} catch (IllegalArgumentException | IOException | ParserConfigurationException
				| TransformerFactoryConfigurationError | TransformerException e) {
			e.printStackTrace();
		}
	}
	

	
	public static void procText(String trx0, String fname, String styles) {
		TrxText trx=new TrxText(trx0);
		String cnt[]=new String[headers.length];
		trx.apply(substitution);

		boolean matches[]=new boolean[headers.length];
		int positions[]=new int[headers.length];
		int longitudes[]=new int[headers.length];
		
		for(int i=0;i<headers.length;i++) {
			matches[i]=false;
			for(int j=0;!matches[i] && j<headers[i].length;j++) {
				positions[i]=trx.despues().indexOf(headers[i][j]);
				longitudes[i]=headers[i][j].length();
				matches[i]|=(positions[i]>=0);
			}
		}
		boolean isOk=true;
		for(int i=0;i<headers.length;i++)
			isOk&=matches[i];
		if(!isOk) {
			System.out.print("KO!"+fname+"|");
			for(int i=0;i<headers.length;i++)
				if(!matches[i])
					System.out.print(headers[i][0]+"|");
				else
					System.out.print("|");
			System.out.println("");
			return;
		}
		int lpos=0,npos=positions[0]+longitudes[0];
		for(int i=1;i<headers.length;i++) {
			lpos=npos;
			npos=positions[i]+longitudes[i];
			cnt[i-1]=trx.backString(lpos, npos-longitudes[i]);
		}
		cnt[cnt.length-1]=trx.backString(positions[headers.length-1]+longitudes[headers.length-1]);
		styles=styles.replace("\n", "");
		for(int i=0;i<cnt.length;i++) {
			cnt[i]=cnt[i].replace("\n", "");
			cnt[i]=removeLastCharsUntilMayor(cnt[i]);
		}
		System.out.println("|"+getOnlyName(fname)+"|"+cnt[0]+"|"+cnt[1]+"|"+cnt[2]+"|"+cnt[3]+"|"+cnt[4]+"|"+cnt[5]);
	}
	
	private static String removeLastCharsUntilMayor(String str) {
		int liom=str.lastIndexOf(">");
		if(liom>=0)
			return str.substring(0, liom+1);
		return str;
	}

	private static String getOnlyName(String s) {
		String t=s.substring(s.lastIndexOf("/")+1,s.lastIndexOf('.')).toLowerCase();
		return t.substring(0,1).toUpperCase()+t.substring(1);
	}
	
	public static String surround(String styles, String body) {
		return "<html>"+styles+"<body>"+body+"</body>";
	}
}
